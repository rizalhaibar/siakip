<div class="form-horizontal" >
    <div class="form-group">
        <label class="col-md-2 control-label">Sasaran*</label>
        <div class="col-md-10">
            <input type="text" class="form-control" id="sasaranedit" misi="sasaranedit" value="{{@$sasarandetail[0]->tst_sasaran_tujuan}}" placeholder="Tujuan..." required>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-2 control-label">Deskripsi*</label>
        <div class="col-md-10">
            <textarea class="form-control" rows="5" id="descsasaran" misi="descsasaran" placeholder="Deskripsi..." >{{@$sasarandetail[0]->tst_keterangan}}</textarea>
        </div>
    </div>


    <div class="form-group">
        <div class="col-md-offset-10 col-md-8">
            <button type="button" onClick="editSasaran()" class="btn btn-primary waves-effect waves-light btn-rounded">
                Simpan
            </button>
        </div>
    </div>
</div>


<script>
function editSasaran(){
       var sasaran   = $('#sasaranedit').val();
       var desc   = $('#descsasaran').val();
      
          if(sasaran){
            
                  var datapost={
                      "idsasaran" : "{{$id}}",
                      "sasaran"   : sasaran,
                      "desc"   : desc,
                      "tahun"  : tahun
                    };
                    $.ajax({
                      type: "POST",
                      url: "{{ route('backend.rpjmd.editsasaran') }}",
                      data : JSON.stringify(datapost),
                      dataType: 'json',
                      contentType: 'application/json; charset=utf-8',
                      success: function(response) {
                        if (response.success == true) {
                            $("#panel-modal .close").click();
                            swal({
                                  title: 'Success!',
                                  text: response.message,
                                  type: 'success',
                                  showCancelButton: false,
                                  confirmButtonText: 'Ok'
                              }).then(function () {
                                window.location.href = "{{ route('backend.rpjmd.masuk', ['id'=>$iddinas, 'tahun'=>$_COOKIE['tahun']]) }}";;
                             
                                
                              })



                        } else{
                          swal("Failed!", response.message, "error");
                        }
                      }
                    });
             
          }else{
              swal("Failed!", "Misi Belum Terisi!", "error");
          }
        
  }
</script>