@extends('backend.layouts.app')

@section('title', 'RPJMD/Restra')

@section('content')
                

     <table id="listkota" class="table ">
	  <thead>
		<tr>
			<th>No</th>
			<th>Instansi</th>
			<th>Action</th>
		</tr>
	  </thead>
	  
		<tbody>
			@php($sl=1)
				@foreach($listdinas as $dinas)
				 <tr>
					<td>{{$sl}}</td>
					<td>{{$dinas->td_dinas}}</td>
					<td>
						<div class="btn-group">
							<a href="{{ route('backend.rpjmd.masuk', ['id'=>$dinas->td_id, 'tahun'=>$_COOKIE['tahun']]) }}" type="button" class="btn btn-icon btn-success"><i class="fa fa-search"></i></a>
						</div>
					</td>
				 </tr>
				 
				@php($sl++)
			@endforeach
		
		</tbody>

	</table>

@endsection

@section('customjs')
<script>
    $( document ).ready(function() {
		
    });
      
    function hapus(){
            swal({
                title: "Apa anda yakin?",
                text: "Anda akan menghapus ini yang berdampak pada semua segment!",
                type: "error",
                showCancelButton: true,
                confirmButtonClass: 'btn-danger waves-effect waves-light',
                confirmButtonText: 'Hapus!'
            });
    }
</script>
@endsection


