@extends('backend.layouts.app')

@section('title', 'RPJMD')

@section('content')>
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
													<span class="kt-portlet__head-icon kt-hide">
														<i class="la la-gear"></i>
													</span>
                            <h3 class="kt-portlet__head-title">
                                Input Visi
                            </h3>
                        </div>
                    </div>
                <div class="kt-portlet__body">
                    <div class="form-horizontal" >
                        <div class="form-group">
                            <label class="col-md-2 control-label">Visi*</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="visi" name="visi" placeholder="Visi..." required>
                                <input type="hidden" class="form-control" id="desc" name="desc" placeholder="Deskripsi..." >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2" >
                                <button type="button" onClick="saveVisi()" class="btn btn-primary waves-effect waves-light btn-rounded" style="width:100%">
                                    Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>      


<div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-md-12">
                <h4 class="header-title m-t-0 m-b-30">List Visi & Misi</h4>
                    
                <ol style="margin-top: 40px;">
                
                    <?php 
                    // print_r($visi); die; 
                    foreach($visi as $newvisi){
                        $isimisi = ''; 
                        if(isset($newvisi->misi)){
                            foreach($newvisi->misi as $misi){ 
                                $isimisi .= '<li>'.$misi->tm_misi.' <a href="javascript:void(0)" class="btn btn-primary btn-circle"  onClick="editmisi('.$misi->tm_id.')"><i class="fa fa-pencil"></i></a> <a href="'.base_url("rpjmd/tujuan/".$misi->tm_id."/".$tahun."/".$id).'" class="btn btn-info btn-circle"><i class="fa fa-eye" ></i></a> <a href="javascript:void(0)" class="btn btn-danger btn-circle" onClick="hapusmisi('.$misi->tm_id.')"><i class="fa fa-trash" id="danger-alert"></i></a></li>';
                            }
                        }
                        

                        echo '<h4 >Visi</h4><li>'.$newvisi->tv_visi.' <a href="javascript:void(0)" class="btn btn-primary btn-circle" onClick="editvisi('.$newvisi->tv_id.')"><i class="fa fa-edit"></i></a> <a href="javascript:void(0)" class="btn btn-danger btn-circle" onClick="hapusvisi('.$newvisi->tv_id.')"> <i class="fa fa-trash" id="danger-alert"></i></a>'
                        .'<ol><h4>Misi</h4>'
                        .$isimisi.'</ol>'
                        .'</li>';    
                    }?>
                    
                </ol>
            </div>
        </div>
</div>   


	
    <div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content p-0 b-0">
                <div class="panel panel-color panel-primary panel-filled">
                    <div class="panel-heading">
                        <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 class="panel-title"></h3>
                    </div>
                    <div class="panel-body">
                        <p></p>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection

@section('onpage-js')
@include('backend.layouts.message')
    
    <script>

    $.ajaxSetup({

    headers: {

        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

    });
        $(document).ready(function () {
            
     //Danger
        $('#danger-alert').click(function () {
            swal({
                title: "Apa anda yakin?",
                text: "Anda akan menghapus ini yang berdampak pada semua segment!",
                type: "error",
                showCancelButton: true,
                confirmButtonClass: 'btn-danger waves-effect waves-light',
                confirmButtonText: 'Hapus!'
            });
        });
    });
      
    function editvisi(id){
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .panel-body').load('{{ route("backend.rpjmd.visidetail",["id"=>"'+id+'", "ids"=> $id]) }}');
        $('#panel-modal  .panel-title').html('<i class="fa fa-user"></i> Edit Visi');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    function editmisi(id){
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .panel-body').load('{{ route("backend.rpjmd.misidetail",["id"=>"'+id+'", "ids"=> $id]) }}');
        $('#panel-modal  .panel-title').html('<i class="fa fa-user"></i> Edit Misi');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    
    function hapusvisi(id){
              swal({
				title: 'Apa anda yakin?',
				  text: "Anda akan menghapus visi!",
				  type: 'error',
				  showCancelButton: true,
				  confirmButtonText: 'Ok'
			}).then(function () {
				  var datapost={
					"id"  :   id
                  };
				  $.ajax({
					type: "POST",
					url: "{{ route('backend.rpjmd.deletevisi') }}",
					data : JSON.stringify(datapost),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(response) {
					  if (response.success == true) {

						  swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () {
								window.location.href =  "{{ route('backend.rpjmd.masuk', ['id'=>$id, 'tahun'=>$_COOKIE['tahun']]) }}";
							})



					  } else{
						swal("Failed!", response.message, "error");
					  }
					}
				  });
			})
    }
    
    function hapusmisi(id){
              swal({
				title: 'Apa anda yakin?',
				  text: "Anda akan menghapus misi!",
				  type: 'error',
				  showCancelButton: true,
				  confirmButtonText: 'Ok'
			}).then(function () {
				var datapost={
					"id"  :   id
				  };
				  $.ajax({
					type: "POST",
					url: "{{ route('backend.rpjmd.deletemisi') }}",
					data : JSON.stringify(datapost),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(response) {
					  if (response.success == true) {

						  swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () {
								window.location.href =  "{{ route('backend.rpjmd.masuk', ['id'=>$id, 'tahun'=>$_COOKIE['tahun']]) }}";
							})



					  } else{
						swal("Failed!", response.message, "error");
					  }
					}
				  });
			})
    }
    
    
    function saveVisi(){
         var visi   = $('#visi').val();
          var desc   = $('#desc').val();
            if(visi){
              
                    var datapost={
                        "iddinas"  :   "{{$id}}",
                        "visi"  :   visi,
                        "desc"  :   desc,
						"tahun" : tahun
                      };
                      $.ajax({
                        type: "POST",
                        url: "{{ route('backend.rpjmd.savevisi') }}",
                        data : JSON.stringify(datapost),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        success: function(response) {
                          if (response.success == true) {

                              swal({
                                    title: 'Success!',
                                    text: response.message,
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonText: 'Ok'
                                }).then(function () {
                                   window.location.href = "{{ route('backend.rpjmd.masuk', ['id'=>$id, 'tahun'=>$_COOKIE['tahun']]) }}";
                                })



                          } else{
                            swal("Failed!", response.message, "error");
                          }
                        }
                      });
               
            }else{
                swal("Failed!", "Visi Belum Terisi!", "error");
            }
          
    }
    
</script>
@endsection