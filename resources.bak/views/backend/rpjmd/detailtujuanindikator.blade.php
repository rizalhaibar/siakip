<div class="form-horizontal" >
            <div class="form-group">
                <label class="col-md-2 control-label">Indikator Tujuan*</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" id="sasaranedit" misi="sasaranedit" value="{{@$indikatortujuandetail[0]->tit_indikator_tujuan}}" placeholder="Indikator Tujuan..." required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Deskripsi</label>
                <div class="col-md-10">
                    <textarea class="form-control" rows="5" id="descsasaran" misi="descsasaran" placeholder="Deskripsi..." >{{@$indikatortujuandetail[0]->tit_keterangan}}</textarea>
                </div>
            </div>
			
			<div class="form-group">
                <label class="col-md-2 control-label">Satuan*</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" id="satuan" misi="satuan" value="{{@$indikatortujuandetail[0]->tit_satuan}}" placeholder="Satuan..." required>
                </div>
            </div>

			<div class="form-group">
                <label class="col-md-2 control-label">Target*</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" id="target" misi="target" value="{{@$indikatortujuandetail[0]->tit_target}}" placeholder="Target..." required>
                </div>
            </div>

        <div class="form-group">
            <div class="col-md-offset-10 col-md-8">
                <button type="button" onClick="editIndiTujuan()" class="btn btn-primary waves-effect waves-light btn-rounded">
                    Simpan
                </button>
            </div>
        </div>
        </div>

<script>
function editIndiTujuan(){
        var tujuan   = $('#sasaranedit').val();
          var desc   = $('#descsasaran').val();
          var satuan   = $('#satuan').val();
          var target   = $('#target').val();
      
          if(tujuan){
            
                  var datapost={
                        "idtujuan" : "{{$id}}",
                        "inditujuan"  :   tujuan,
                        "satuan"  :   satuan,
                        "target"  :   target,
                        "desc"  :   desc
                    };
                    $.ajax({
                      type: "POST",
                      url: "{{ route('backend.rpjmd.editindikatortujuan') }}",
                      data : JSON.stringify(datapost),
                      dataType: 'json',
                      contentType: 'application/json; charset=utf-8',
                      success: function(response) {
                        if (response.success == true) {
                            $("#panel-modal .close").click();
                            swal({
                                  title: 'Success!',
                                  text: response.message,
                                  type: 'success',
                                  showCancelButton: false,
                                  confirmButtonText: 'Ok'
                              }).then(function () {
                                window.location.href = "{{ route('backend.rpjmd.masuk', ['id'=>$iddinas, 'tahun'=>$_COOKIE['tahun']]) }}";;
                             
                                
                              })



                        } else{
                          swal("Failed!", response.message, "error");
                        }
                      }
                    });
             
          }else{
              swal("Failed!", "Misi Belum Terisi!", "error");
          }
        
  }
</script>