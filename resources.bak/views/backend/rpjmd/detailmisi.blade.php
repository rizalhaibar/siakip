<div class="form-horizontal" >
    <div class="form-group">
        <label class="col-md-2 control-label">Misi*</label>
        <div class="col-md-12">
            <input type="text" class="form-control" id="visiedit" misi="visiedit" value="{{$misidetail[0]->tm_misi}}" placeholder="Misi..." required>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-2 control-label">Deskripsi*</label>
        <div class="col-md-12">
            <textarea class="form-control" rows="5" id="descedit" misi="descedit" placeholder="Deskripsi..." >{{$misidetail[0]->tm_keterangan}}</textarea>
        </div>
    </div>


<div class="form-group">
    <div class="col-md-offset-10 col-md-8">
        <button type="button" onClick="editMisi()" class="btn btn-primary waves-effect waves-light btn-rounded">
            Simpan
        </button>
    </div>
</div>
</div>
<script>
    
   
    
    function editMisi(){
       
         var visi   = $('#visiedit').val();
         var desc   = $('#descedit').val();
        
        
            if(visi){
                    var datapost={
                        "idvisi"  :   "{{$id}}",
                        "visi"  :   visi,
                        "desc"  :   desc
                      };
                      $.ajax({
                        type: "POST",
                        url: "{{route('backend.rpjmd.editmisi')}}",
                        data : JSON.stringify(datapost),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        success: function(response) {
                          if (response.success == true) {
							  $("#panel-modal .close").click();
							  swal({
                                    title: 'Success!',
                                    text: response.message,
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonText: 'Ok'
                                }).then(function () {
                                    window.location.href = "{{ route('backend.rpjmd.masuk', ['id'=>$iddinas, 'tahun'=>$_COOKIE['tahun']]) }}";;
                               
                                })



                          } else{
                            swal("Failed!", response.message, "error");
                          }
                        }
                      });
               
            }else{
                swal("Failed!", "Misi Belum Terisi!", "error");
            }
          
    }
    
</script>