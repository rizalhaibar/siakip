@extends('backend.layouts.app')

@section('title', 'RPJMD - Tujuan')

@section('content')>

<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Misi : {{@$misidetail[0]->tm_misi}}
                            </h3>
                        </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="form-horizontal" role="form" data-parsley-validate novalidate>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Tujuan*</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" placeholder="Tujuan..." id="tujuan" name="tujuan" value="" required>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-2 control-label">Deskripsi*</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" rows="5" id="desc" name="desc" placeholder="Deskripsi..." ></textarea>
                                </div>
                            </div>


                        <div class="form-group">
                            <div class="col-sm-offset-10 col-md-8">
                                <button type="button" onClick="tujuanSave()" class="btn btn-primary waves-effect waves-light btn-rounded">
                                    Tambah Tujuan
                                </button>
                            </div>
                        </div>
                    </div>
                </div>              
            </div>              
        </div>              
    </div>              
</div>     
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                <h4 class="header-title m-t-0 m-b-30"> Sasaran & Tujuan</h4>
                            </h3>
                        </div>
                </div>
                <div class="kt-portlet__body">
                    <h4>Tujuan</h4>
                    <ol>
                        @php($s = 1)
                            @foreach($misidetail as $newmisi)
                                @if(isset($newmisi->tujuan ))
                                    @foreach($newmisi->tujuan as $tujuan)
                                        <li style="display:block; margin-left: -20px">
                                            {{$s}}. {{$tujuan->tt_tujuan}}
                                            <span> 
                                                <a href="javascript:void(0)" class="btn btn-success btn-icon btn-sm" onClick="edittujuan({{$tujuan->tt_id}},'{{$ids}}')"><i class="fa fa-edit" ></i></a>  
                                            </span>
                                            <ul style="margin-left: 20px">
                                                <h4 style="margin-left:-30px; color: #336600">Indikator Tujuan</h4>
                                            </ul>
                                            <ol style="margin-left: 20px">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th><center>NO</center></th>
                                                            <th><center>Indikator Tujuan</center></th>
                                                            <th><center>Satuan</center></th>
                                                            <th><center>Target</center></th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php($sasssas=1)
                                                        @foreach($indikatortujuan as $indi)
                                                        <tr>
                                                            <td><?php echo $sasssas;?></td>
                                                            <td><?php echo $indi['tit_indikator_tujuan'];?></td>
                                                            <td><?php echo $indi['tit_satuan'];?></td>
                                                            <td><?php echo $indi['tit_target'];?></td>
                                                            <td> 
                                                                <span> 
                                                                    <a href="javascript:void(0)" onClick="detailtujuanindikator({{$indi->tit_id}},{{$id}})"><i class="fa fa-pencil" style="color:blue"></i></a> 
                                                                    <a href="javascript:void(0)" onClick="hapustujuanindikator({{$indi->tit_id}},{{$id}})"><i class="fa fa-trash" style="color:red" id="danger-alert"></i></a> 
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        @php($sasssas++)
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </ol>
                                            <ul style="margin-left: 20px">
                                                <h4 style="margin-left:-30px; color: #336600">Sasaran Strategis</h4>
                                            </ul>
                                            <ol  style="margin-left: 20px">
                                                @if(isset($tujuan->sasaran))
                                                    @foreach($tujuan->sasaran as $k => $sasarannya)
                                                        @php($sa = $k+1)
                                                        @foreach($sasaran as $newsasaran)
                                                            @if($sasarannya->tst_id == $newsasaran->tst_id)
                                                                <li style="display:block">
                                                                    {{$sa}}. {{$newsasaran->tst_sasaran_tujuan}}
                                                                    <span> 
                                                                        <a href="javascript:void(0)"  onClick="editsasaran({{$newsasaran->tst_id}},{{$id}})"><i class="fa fa-pencil" style="color:blue"></i></a> 
                                                                    </span>

                                                                    <ol>
                                                                        <li style="display:block">
                                                                            <table class="table table-bordered">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th><center>NO</center></th>
                                                                                        <th><center>Indikator Sasaran Strategis</center></th>
                                                                                        <th><center>Deskripsi</center></th>
                                                                                        <th><center>Satuan</center></th>
                                                                                        <th><center>IKU</center></th>
                                                                                        <th>Action</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>{{$sas}}</td>
                                                                                        <td>{{$indisasaran->tis_indikator_sasaran}}</td>
                                                                                        <td>{{$indisasaran->tis_keterangan}}</td>
                                                                                        <td>{{$indisasaran->tis_satuan}}</td>
                                                                                        <td style="text-align:center">
                                                                                            @php($sas=1)
                                                                                            @if(isset($newsasaran->indikatorsasaran))
                                                                                                @foreach($newsasaran->indikatorsasaran as $indisasaran)
                                                                                                    @if($indisasaran->tis_iku == "T")
                                                                                                        <a href="javascript:void(0)" class="btn btn-primary" onClick="rubahIKU({{$indisasaran->tis_id}},&#x27;F&#x27;)" placeholder="Aktif?"><i class="fa fa-check-square-o"></i></a>
                                                                                                    @else
                                                                                                        <a href="javascript:void(0)" class="btn btn-danger" onClick="rubahIKU({{$indisasaran->tis_id}},&#x27;T&#x27;)" placeholder="Tidak Aktif?"><i class="fa fa-times"></i></a>
                                                                                                    @endif
                                                                                                @endforeach
                                                                                            @endif
                                                                                        </td>
                                                                                        <td>
                                                                                            <center> 
                                                                                                <span> 
                                                                                                    <a href="javascript:void(0)" onClick="detailsasaranindikator({{$indisasaran->tis_id}},$id)"><i class="fa fa-pencil" style="color:blue"></i></a> 
                                                                                                </span>
                                                                                            </center>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </li>
                                                                    </ol>
                                                                </li>
                                                            @endif
                                                        @endforeach
                                                    @endforeach
                                                    
                                                    @php($sa++)
                                                @endif
                                            </ol>
                                        </li>
                                    @endforeach
                                    @php($s++)
                                @endif
                            @endforeach
                    </ol>
                </div>              
            </div>              
        </div>              
    </div>              
</div>


<div id="panel-modal" class="modal fade" id="kt_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
@endsection

@section('onpage-js')
@include('backend.layouts.message')
<script>
    $.ajaxSetup({

    headers: {

        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

    });

    function edittujuan(id, idmisi){
        var url = '{{ route("backend.rpjmd.tujuandetail",["id"=>":id", "ids"=> $id]) }}';
        url = url.replace(':id', id);
        url = url.replace(':ids', idmisi);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('<i class="fa fa-edit"></i> Edit Tujuan');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    
    function editsasaran(id, idmisi){
        var url = '{{ route("backend.rpjmd.sasaranindikator",["id"=>":id", "ids"=> $id]) }}';;
        url = url.replace(':id', id);
        url = url.replace(':ids', idmisi);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('<i class="fa fa-useeditr"></i> Edit Sasaran');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    
    function detailsasaranindikator(id, idmisi){
        var url = '{{ route("backend.rpjmd.detailsasaranindikator",["id"=>":id", "ids"=> $id]) }}';
        url = url.replace(':id', id);
        url = url.replace(':ids', idmisi);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('<i class="fa fa-edit"></i> Edit Indikator Sasaran');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    function detailtujuanindikator(id, idmisi){
        var url = '{{ route("backend.rpjmd.detailtujuanindikator",["id"=>":id", "ids"=> $id]) }}';
        url = url.replace(':id', id);
        url = url.replace(':ids', idmisi);
	    $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('<i class="fa fa-edit"></i> Edit Indikator Tujuan');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }

    
    function tujuanSave(){
       
       var tujuan   = $('#tujuan').val();
        var desc   = $('#desc').val();
          if(tujuan){
            
                  var datapost={
                      "idmisi"  :   "{{$id}}",
                      "tujuan"  :   tujuan,
                      "desc"  :   desc
                    };
                    $.ajax({
                      type: "POST",
                      url: '{{ route("backend.rpjmd.savetujuan") }}',
                      data : JSON.stringify(datapost),
                      dataType: 'json',
                      contentType: 'application/json; charset=utf-8',
                      success: function(response) {
                        if (response.success == true) {

                            swal({
                                  title: 'Success!',
                                  text: response.message,
                                  type: 'success',
                                  showCancelButton: false,
                                  confirmButtonText: 'Ok'
                              }).then(function () {
                                 window.location.href = '{{ route("backend.rpjmd.tujuan",["id"=>$id, "tahun"=> $tahun, "ids"=> $ids]) }}';
                              })



                        } else{
                          swal("Failed!", response.message, "error");
                        }
                      }
                    });
            
          }else{
              swal("Failed!", "Tujuan Belum Terisi!", "error");
          }
        
  }
  function rubahIKU(id, status){
     
            
        var datapost={
          "id"  :  id,
          "status"  :   status
        };
        // console.log(datapost);
        // return false;
        $.ajax({
          type: "POST",
          url: '{{ route("backend.rpjmd.rubahiku") }}',
          data : JSON.stringify(datapost),
          dataType: 'json',
          contentType: 'application/json; charset=utf-8',
          success: function(response) {
            if (response.success == true) {

                swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                  }).then(function () {
                                 window.location.href = '{{ route("backend.rpjmd.tujuan",["id"=>$id, "tahun"=> $tahun, "ids"=> $ids]) }}';
                  })



            } else{
              swal("Failed!", response.message, "error");
            }
          }
        });

          
        
  }

  
  function hapustujuanindikator(id, ids){
              swal({
				title: 'Apa anda yakin?',
				  text: "Anda akan menghapus visi!",
				  type: 'error',
				  showCancelButton: true,
				  confirmButtonText: 'Ok'
			}).then(function () {
				  var datapost={
					"id"  :   id,
					"ids"  :   ids
                  };
				  $.ajax({
					type: "POST",
					url: "{{ route('backend.rpjmd.deleteinditujuan') }}",
					data : JSON.stringify(datapost),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(response) {
					  if (response.success == true) {

						  swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () {
								window.location.href =  "{{ route('backend.rpjmd.masuk', ['id'=>$id, 'tahun'=>$_COOKIE['tahun']]) }}";
							})



					  } else{
						swal("Failed!", response.message, "error");
					  }
					}
				  });
			})
    }
</script>
@endsection