
<div class="row">
    <div class="col-md-12">
        <div class="form-horizontal" >
            <div class="form-group">
                <label class="col-md-2 control-label">Tujuan*</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" id="tujuan" misi="tujuan" placeholder="Tujuan..." required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Deskripsi*</label>
                <div class="col-md-10">
                    <textarea class="form-control" rows="5" id="descmisi" misi="descmisi"  placeholder="Deskripsi..." ></textarea>
                </div>
            </div>


        <div class="form-group">
            <div class="col-md-offset-10 col-md-8">
                <button type="button" onClick="saveMisi()" class="btn btn-primary waves-effect waves-light btn-rounded">
                    Simpan
                </button>
            </div>
        </div>
    </div>
</div>

<script>
function saveMisi(){
       var tujuan   = $('#tujuan').val();
       var desc   = $('#descmisi').val();
      
          if(tujuan){
            
                  var datapost={
                      "idmisi" : "{{$id}}",
                      "tujuan"   : tujuan,
                      "desc"   : desc,
                      "tahun"  : tahun
                    };
                    $.ajax({
                      type: "POST",
                      url: "{{ route('backend.rpjmd.savetujuan') }}",
                      data : JSON.stringify(datapost),
                      dataType: 'json',
                      contentType: 'application/json; charset=utf-8',
                      success: function(response) {
                        if (response.success == true) {
                            $("#panel-modal .close").click();
                            swal({
                                  title: 'Success!',
                                  text: response.message,
                                  type: 'success',
                                  showCancelButton: false,
                                  confirmButtonText: 'Ok'
                              }).then(function () {
                                window.location.href = "{{ route('backend.rpjmd.masuk', ['id'=>$iddinas, 'tahun'=>$_COOKIE['tahun']]) }}";;
                             
                                
                              })



                        } else{
                          swal("Failed!", response.message, "error");
                        }
                      }
                    });
             
          }else{
              swal("Failed!", "Misi Belum Terisi!", "error");
          }
        
  }
</script>