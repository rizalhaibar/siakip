<div class="form-horizontal" >
	<div class="form-group">
		<label class="col-md-2 control-label">Indikator Sasaran*</label>
		<div class="col-md-12">
			<input type="text" class="form-control" id="indisasaran" name="indisasaran" placeholder="Indikator Sasaran..." required>
		</div>
	</div> 


	<div class="form-group">
		<label class="col-md-2 control-label">Deskripsi*</label>
		<div class="col-md-12">
			<textarea class="form-control" rows="5" id="descindisasaran" name="descindisasaran"  placeholder="Deskripsi..." ></textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-2 control-label">Formula*</label>
		<div class="col-md-12">
			<textarea class="form-control" rows="5" id="descindiformula" name="descindiformula"  placeholder="Formula..." ></textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-2 control-label">Satuan*</label>
		<div class="col-md-12">
			<input type="text" class="form-control" id="indisatuan" name="indisatuan" placeholder="Indikator Sasaran Satuan..." required>
		</div>
	</div>
	<!--div class="form-group">
		<label class="col-md-2 control-label">Target*</label>
		<div class="col-md-10">
			<input type="text" class="form-control"  id="inditarget" name="inditarget" placeholder="Indikator Sasaran Target..." required>
		</div>
	</div-->
	<div class="form-group">
		<label class="col-md-2 control-label">IKU*</label>
		<div class="col-md-12">
			<select class="form-control" id="indiiku" name="indiiku">
				<option value="T">Ya</option>
				<option value="F">Tidak</option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<div class="col-md-offset-10 col-md-8">
			<button type="button" onClick="saveIndikator()" class="btn btn-primary waves-effect waves-light btn-rounded">
				Simpan
			</button>
		</div>
	</div>
</div>

<script>
function saveIndikator(){
       
	   var indisasaran         = $('#indisasaran').val();
	   var descindisasaran             = $('#descindisasaran').val();
	   var satuan             = $('#indisatuan').val();
	   // var target             = $('#inditarget').val();
	   var iku             = $('#indiiku').val();
	   var formula             = $('#descindiformula').val();
	  
		  if(indisasaran){
			 
				  var datapost={
					  "idsasaran"  :   "{{$id}}",
					  "indisasaran"  :   indisasaran,
					  "descindisasaran"  :   descindisasaran,
					  "satuan"  :   satuan,
					  "target"  :   '',
					  "iku"  :   iku,
					  "formula"  :   formula,
					  "tahun"  :   tahun
					  
					};
					$.ajax({
					  type: "POST",
					  url: "{{ route('backend.rpjmd.saveindikatorsasaran') }}",
					  data : JSON.stringify(datapost),
					  dataType: 'json',
					  contentType: 'application/json; charset=utf-8',
					  success: function(response) {
						if (response.success == true) {
							$("#panel-modal .close").click();
							swal({
								  title: 'Success!',
								  text: response.message,
								  type: 'success',
								  showCancelButton: false,
								  confirmButtonText: 'Ok'
							  }).then(function () {
								window.location.href = "{{ route('backend.rpjmd.masuk', ['id'=>$iddinas, 'tahun'=>$_COOKIE['tahun']]) }}";
							  })



						} else{
						  swal("Failed!", response.message, "error");
						}
					  }
					});
			  
		  }else{
			  swal("Failed!", "Indikator Sasaran Belum Terisi!", "error");
		  } 
		
  }


</script>