<div class="form-horizontal" >
	 <div class="form-group">
		<label class="col-md-2 control-label">Indikator Tujuan*</label>
		<div class="col-md-12">
			<input type="text" class="form-control" id="inditujuan" misi="inditujuan" placeholder="Indikator Tujuan..." required>
		</div>
	</div> 
	

	<div class="form-group">
		<label class="col-md-2 control-label">Deskripsi*</label>
		<div class="col-md-12">
			<textarea class="form-control" rows="5" id="descinditujuan" misi="descinditujuan"  placeholder="Deskripsi..." ></textarea>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-md-2 control-label">Satuan*</label>
		<div class="col-md-12">
			<input type="text" class="form-control" id="satuan" misi="satuan" placeholder="Satuan..." required>
		</div>
	</div>            
	<div class="form-group">
		<label class="col-md-2 control-label">Target*</label>
		<div class="col-md-12">
			<input type="text" class="form-control" id="target" misi="target" placeholder="Target..." required>
		</div>
	</div>


<div class="form-group">
	<div class="col-md-offset-10 col-md-8">
		<button type="button" onClick="saveIndikator()" class="btn btn-primary waves-effect waves-light btn-rounded">
			Simpan
		</button>
	</div>
</div>
</div>

<script>
	function saveIndikator(){
       
         var inditujuan         = $('#inditujuan').val();
         var satuan             = $('#satuan').val();
         var descinditujuan     = $('#descinditujuan').val();
         var target               = $('#target').val();
        
            if(inditujuan){
				if(satuan){
					if(target){
						var datapost={
							"idtujuan"  :   "{{$id}}",
							"inditujuan"  :   inditujuan,
							"descinditujuan"  :   descinditujuan,
							"target"  :   target,
							"tahun"  :   tahun,
							"satuan"  :   satuan
						  };
						  $.ajax({
							type: "POST",
							url: "{{ route('backend.rpjmd.saveindikatortujuan') }}",
							data : JSON.stringify(datapost),
							dataType: 'json',
							contentType: 'application/json; charset=utf-8',
							success: function(response) {
							  if (response.success == true) {
								$("#panel-modal .close").click();
								  swal({
										title: 'Success!',
										text: response.message,
										type: 'success',
										showCancelButton: false,
										confirmButtonText: 'Ok'
									}).then(function () {
									   window.location.href = "{{ route('backend.rpjmd.masuk', ['id'=>$iddinas, 'tahun'=>$_COOKIE['tahun']]) }}";
									})



							  } else{
								swal("Failed!", response.message, "error");
							  }
							}
						  });
					}else{
						swal("Failed!", "Target Belum Terisi!", "error");
					}
				}else{
					swal("Failed!", "Satuan Belum Terisi!", "error");
				} 
           
            
            }else{
                swal("Failed!", "Indikator Tujuan Belum Terisi!", "error");
            }
          
    }
</script>