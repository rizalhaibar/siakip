@extends('backend.layouts.app')

@section('title', 'Edit Dinas')

@section('content')
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Perangkat Daerah : {{@$dinas[0]->td_dinas}}
                            </h3>
                        </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="row">
                        <div class="col-lg-12">
                        
                            <div class="ibox-tools right">
                                <a class="btn btn-default btn-xs" type="button" onClick="tambaheselon({{$id}})" href="javascript:void(0)">
                                    <i class="fa fa-plus"></i>
                                    <strong>New</strong>
                                </a>
                            </div>
                            <br/>
                            <ol>
                                @php($parent = "")
                                @php($child1 = "")
                                @php($child2 = "")
                                    @if(isset($eselonisi[0]))
                                        @foreach($eselonisi as $new)
                                            @if($new->te_parent == 1)
                                                <li> {{$new->te_isi}} ({{$new->te_jabatan}}) 
                                                    <span> 
                                                        <a href="javascript:void(0)" onClick="addEselon2({{$new->te_id}})">
                                                            <i class="fa fa-edit" style="color:blue"></i>
                                                        </a> 
                                                        <a href="javascript:void(0)" onClick="deleteEselon({{$new->te_id}})">
                                                            <i class="fa fa-trash" style="color:red"></i>
                                                        </a> 
                                                    </span>
                                            @endif
                                            <ul>
                                                @foreach($eselonisi as $anak)
                                                    @if($anak->id_parent == $new->te_id)
                                                        @if($anak->te_child == 1)
                                                            <li>{{$anak->te_isi}}  ({{$anak->te_jabatan}}) 
                                                                <span> 
                                                                    <a href="javascript:void(0)" onClick="addEselon3({{$anak->te_id}})">
                                                                        <i class="fa fa-edit" style="color:blue"></i>
                                                                    </a> 
                                                                    <a href="javascript:void(0)" onClick="deleteEselon({{$anak->te_id}})">
                                                                        <i class="fa fa-trash" style="color:red"></i>
                                                                    </a>
                                                                </span>
                                                                <ul>
                                                                    @foreach($eselonisi as $anak2)
                                                                        @if($anak2->id_parent == $anak->te_id)
                                                                            @if($anak2->te_child == 2)
                                                                                <li>{{$anak2->te_isi}}  ({{$anak2->te_jabatan}}) 
                                                                                    <span> 
                                                                                        <a href="javascript:void(0)" onClick="addEselon4({{$anak2->te_id}})">
                                                                                            <i class="fa fa-edit" style="color:blue"></i>
                                                                                        </a> 
                                                                                        <a href="javascript:void(0)" onClick="deleteEselon({{$anak2->te_id}})">
                                                                                            <i class="fa fa-trash" style="color:red"></i>
                                                                                        </a>
                                                                                    </span>
                                                                                </li>
                                                                                <ul>
                                                                                    @foreach($eselonisi as $anak3)
                                                                                        @if($anak3->id_parent == $anak2->te_id)
                                                                                      
                                                                                            @if($anak3->te_child == 3)
                                                                                                <li>{{$anak3->te_isi}}  ({{$anak3->te_jabatan}}) 
                                                                                                    <span> 
                                                                                                        <a href="javascript:void(0)" onClick="addEselon5({{$anak3->te_id}})">
                                                                                                            <i class="fa fa-edit" style="color:blue"></i>
                                                                                                        </a> 
                                                                                                        <a href="javascript:void(0)" onClick="deleteEselon({{$anak3->te_id}})">
                                                                                                            <i class="fa fa-trash" style="color:red"></i>
                                                                                                        </a>
                                                                                                    </span>
                                                                                                </li>
                                                                                            @endif
                                                                                        @endif
                                                                                    @endforeach
                                                                                </ul>
                                                                            @endif
                                                                        @endif
                                                                    @endforeach
                                                                </ul>
                                                            </li>                 
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </ul>
                                        @endforeach
                                    @endif

                                    </li>
                            </ol>
                        </div>                
                    </div>                
                </div>                
            </div>                
        </div>                
    </div>                
</div>                          


<div id="panel-modal" class="modal fade" id="kt_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
@endsection

@section('onpage-js')
@include('backend.layouts.message')
<script>
   $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $( document ).ready(function() {
     
    });
      
    function tambahDinass(){
        var url = '{{ route("backend.dinas.tambahdinas") }}';
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('<i class="fa fa-pencil"></i> Tambah Perangkat Daerah');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    
    
    
    function editDinas(id){
        var url = '{{ route("backend.dinas.editdinas",["id"=>":id"]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('<i class="fa fa-pencil"></i> Edit Perangkat Daerah');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    function tambaheselon(id){
        var url = '{{ route("backend.dinas.tambaheselon",["id"=>":id"]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html(' Tambah Eselon');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }


    function addEselon2(id){
        
        var url = '{{ route("backend.dinas.addeselon",["id"=>":id", "ids"=>":ids"]) }}'
        url = url.replace(':id', id);
        url = url.replace(':ids', {{$id}});
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Edit & Tambah Eselon');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    
    function addEselon3(id){
        var url = '{{ route("backend.dinas.addeselon3",["id"=>":id", "ids"=>":ids"]) }}'
        url = url.replace(':id', id);
        url = url.replace(':ids', {{$id}});
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Edit & Tambah Eselon');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    
    function addEselon4(id){
        var url = '{{ route("backend.dinas.addeselon4",["id"=>":id", "ids"=>":ids"]) }}'
        url = url.replace(':id', id);
        url = url.replace(':ids', {{$id}});
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Edit & Tambah Eselon');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    
    
    function addEselon5(id){
        var url = '{{ route("backend.dinas.addeselon5",["id"=>":id", "ids"=>":ids"]) }}'
        url = url.replace(':id', id);
        url = url.replace(':ids', {{$id}});
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Edit & Tambah Eselon');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    
    
    function deleteDinas(id){
        swal({
            title: 'Apa anda yakin?',
              text: "Anda akan menghapus dinas!",
              type: 'error',
              showCancelButton: true,
              confirmButtonText: 'Ok'
        }).then(function () {
            var datapost={
                "id"  :   id
              };
              $.ajax({
                type: "POST",
                url: '{{ route("backend.dinas.dinasdelete") }}',
                data : JSON.stringify(datapost),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function(response) {
                  if (response.success == true) {

                      swal({
                            title: 'Success!',
                            text: response.message,
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        }).then(function () {
                            window.location.href = "{{url()->current()}}";
                        })



                  } else{
                    swal("Failed!", response.message, "error");
                  }
                }
              });
        })
    }
    function deleteEselon(id){
        swal({
            title: 'Apa anda yakin?',
              text: "Anda akan menghapus eselon!",
              type: 'error',
              showCancelButton: true,
              confirmButtonText: 'Ok'
        }).then(function () {
            var datapost={
                "id"  :   id
              };
              $.ajax({
                type: "POST",
                url: '{{ route("backend.dinas.eselondelete") }}',
                data : JSON.stringify(datapost),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function(response) {
                  if (response.success == true) {

                      swal({
                            title: 'Success!',
                            text: response.message,
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        }).then(function () {
                            window.location.href = "{{url()->current()}}";
                        })



                  } else{
                    swal("Failed!", response.message, "error");
                  }
                }
              });
        })
    }

//     function eselonSave(){
       
//        var eselon   = $('#eselons').val();
//        var bagian   = $('#bagian').val();
      
//               if(eselon){
//                   if(bagian){
//                       var datapost={
//                           "iddinas"  :   "{{$id}}",
//                           "eselon"  :   eselon,
//                           "bagian"  :   bagian
//                         };
//                         $.ajax({
//                           type: "POST",
//                           url: '{{ route("backend.dinas.saveeselon") }}',
//                           data : JSON.stringify(datapost),
//                           dataType: 'json',
//                           contentType: 'application/json; charset=utf-8',
//                           success: function(response) {
//                             if (response.success == true) {
//                                 $("#panel-modal .close").click();
//                                 swal({
//                                       title: 'Success!',
//                                       text: response.message,
//                                       type: 'success',
//                                       showCancelButton: false,
//                                       confirmButtonText: 'Ok'
//                                   }).then(function () {
//                                     window.location.href = "{{url()->current()}}";
//                                   })



//                             } else{
//                                 $("#panel-modal .close").click();
//                               swal("Failed!", response.message, "error");
//                             }
//                           }
//                         });
//                   }else{
//                       $("#panel-modal .close").click();
//                       swal("Failed!", "Keterangan Belum Terisi!", "error");
//                   }  
              
              
//               }else{
//                   $("#panel-modal .close").click();
//                   swal("Failed!", "Dinas Belum Terisi!", "error");
//               }
        
//   }
</script>
@endsection