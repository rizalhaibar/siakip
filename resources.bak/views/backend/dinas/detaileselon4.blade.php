<ul class="nav nav-tabs  nav-tabs-line nav-tabs-line-2x nav-tabs-line-success" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#sasaran" role="tab">Edit</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#program" role="tab">Tambah Jabatan</a>
    </li>
</ul>

<div class="tab-content">
    <div class="tab-pane active" id="sasaran" role="tabpanel">
    <div class="form-horizontal" >       
        <div class="form-group">
            <label class="col-md-2 control-label">Nama Jabatan*</label>
            <div class="col-md-10">
                <input class="form-control" id="editbagian" name="editbagian" value="@if(isset($detaileselon[0]))  {{$detaileselon[0]->te_isi}} @endif" placeholder="bagian..." required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-10 col-md-8">
                <button type="button" onClick="editEselon()" class="btn btn-primary waves-effect waves-light btn-rounded">
                    Simpan
                </button>
            </div>
        </div>

    </div>
    </div>
    <div class="tab-pane" id="program" role="tabpanel">
        <div class="form-horizontal" >
            <h3>Isi Eselon 3</h3>
    
                <div class="form-group">
                    <label class="col-md-2 control-label">Jabatan*</label>
                    <div class="col-md-10">
                        <select class="form-control" id="eselons4" name="eselons4">
                            <option disabled selected>Pilih Jabatan...</option>
                            <option value='2A_Jabatan Pimpinan Tinggi'>Jabatan Pimpinan Tinggi</option>
                            <option value='3A_Administrator'>Administrator</option>
                            <option value='4A_Pengawas'>Pengawas</option>
                                            
                        
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Nama Jabatan*</label>
                    <div class="col-md-10">
                    <input class="form-control" id="bagian4" name="bagian4" placeholder="nama jabatan..." required>
                    </div>
                </div>
    
        
            <div class="form-group">
                <div class="col-md-offset-10 col-md-2" >
                    <button type="button" onClick="eselonSave4()" style="width:100%" class="btn btn-primary waves-effect waves-light btn-rounded">
                        Simpan
                    </button>
                </div>
            </div>

        </div>
    </div>
</div>


<script>
    function editEselon(){
       
         var bagian   = $('#editbagian').val();
        
                if(bagian){
                    var datapost={
                        "ideselon2"  :   "@if(isset($detaileselon[0]))  {{$detaileselon[0]->te_id}} @endif",
                        "bagian"  :   bagian
                      };
                      $.ajax({
                        type: "POST",
                        url: "{{route('backend.dinas.eselonedit')}}",
                        data : JSON.stringify(datapost),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        success: function(response) {
                          if (response.success == true) {
                                $("#panel-modal .close").click();
                              swal({
                                    title: 'Success!',
                                    text: response.message,
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonText: 'Ok'
                                }).then(function () {
                                     window.location.href = "{{route('backend.dinas.eselon', $iddinas)}}";
                                })



                          } else{
							  $("#panel-modal .close").click();
                            swal("Failed!", response.message, "error");
                          }
                        }
                      });
                }else{
					$("#panel-modal .close").click();
                    swal("Failed!", "Bagian Belum Terisi!", "error");
                }  


          
    }
    
    
    
    
    function eselonSave4(){
       
         var eselon1   = $('#eselons4').val();
         var bagian1   = $('#bagian4').val();
        
                if(eselon1){
                    if(bagian1){
                        var datapost={
                            "iddinas"  :   "<?php echo $iddinas; ?>",
                            "ideselon3"  :   "@if(isset($detaileselon[0]))  {{$detaileselon[0]->te_id}} @endif",
                            "eselon"  :   eselon1,
                            "bagian"  :   bagian1
                          };
                          $.ajax({
                            type: "POST",
                            url: "{{route('backend.dinas.saveeselon5')}}",
                            data : JSON.stringify(datapost),
                            dataType: 'json',
                            contentType: 'application/json; charset=utf-8',
                            success: function(response) {
                              if (response.success == true) {
                                $("#panel-modal .close").click();
                                  swal({
                                        title: 'Success!',
                                        text: response.message,
                                        type: 'success',
                                        showCancelButton: false,
                                        confirmButtonText: 'Ok'
                                    }).then(function () {
                                        window.location.href = "{{route('backend.dinas.eselon', $iddinas)}}";
                                    })



                              } else{
								  $("#panel-modal .close").click();
                                swal("Failed!", response.message, "error");
                              }
                            }
                          });
                    }else{
						$("#panel-modal .close").click();
                        swal("Failed!", "Keterangan Belum Terisi!", "error");
                    }  
                
                
                }else{
					$("#panel-modal .close").click();
                    swal("Failed!", "Dinas Belum Terisi!", "error");
                }
          
    }
</script>