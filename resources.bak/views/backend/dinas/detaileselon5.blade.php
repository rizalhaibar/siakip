<div class="form-horizontal" >       
    <div class="form-group">
        <label class="col-md-2 control-label">Nama Jabatan*</label>
        <div class="col-md-10">
            <input class="form-control" id="editbagian" name="editbagian" value="@if(isset($detaileselon[0]))  {{$detaileselon[0]->te_isi}} @endif" placeholder="bagian..." required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-10 col-md-8">
            <button type="button" onClick="editEselon()" class="btn btn-primary waves-effect waves-light btn-rounded">
                Simpan
            </button>
        </div>
    </div>

</div>

<script>
    function editEselon(){
       
         var bagian   = $('#editbagian').val();
        
                if(bagian){
                    var datapost={
                        "ideselon2"  :   "@if(isset($detaileselon[0]))  {{$detaileselon[0]->te_id}} @endif",
                        "bagian"  :   bagian
                      };
                      $.ajax({
                        type: "POST",
                        url: "{{route('backend.dinas.eselonedit')}}",
                        data : JSON.stringify(datapost),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        success: function(response) {
                          if (response.success == true) {
                                $("#panel-modal .close").click();
                              swal({
                                    title: 'Success!',
                                    text: response.message,
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonText: 'Ok'
                                }).then(function () {
                                     window.location.href = "{{route('backend.dinas.eselon', $iddinas)}}";
                                })



                          } else{
							  $("#panel-modal .close").click();
                            swal("Failed!", response.message, "error");
                          }
                        }
                      });
                }else{
					$("#panel-modal .close").click();
                    swal("Failed!", "Bagian Belum Terisi!", "error");
                }  


          
    }
    
    
    
    
</script>