@extends('backend.layouts.app')

@section('title', 'Home')

@section('content')
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                List Dinas
                            </h3>
                        </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="row">
                        <div class="col-lg-12">
                            
                            <div class="ibox-tools right">
                                <a class="btn btn-default btn-xs" type="button" onClick="tambahDinass()" href="javascript:void(0)">
                                    <i class="fa fa-plus"></i>
                                    <strong>New</strong>
                                </a>
                            </div>
                            <table id="listkota" class="table ">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Perangkat Daerah</th>
                                        <th>Action</th>
                                    </tr>
                                    <tbody>
                                        @php($sl=1)
                                            @foreach($listdinas as $dinas)
                                            <tr>
                                                <td>{{$sl}}</td>
                                                <td>{{$dinas->td_dinas}}</td>
                                                <td>
                                                    <a href="javascript:void(0)" onClick="editDinas({{$dinas->td_id}})" type="button" class="btn btn-info btn-icon">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href='{{ route("backend.dinas.eselon", $dinas->td_id) }}' type="button" class="btn btn-success btn-icon">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                    <a href="javascript:void(0)" onClick="deleteDinas({{$dinas->td_id}})" type="button" class="btn btn-danger btn-icon">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                   
                                                </td>
                                            </tr>
                                            
                                            @php($sl++)
                                        @endforeach
                                    
                                    </tbody>

                                </thead>
                            </table>
                        </div>                
                    </div>                
                </div>                
            </div>                
        </div>                
    </div>                
</div>                          


<div id="panel-modal" class="modal fade" id="kt_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
@endsection

@section('onpage-js')
@include('backend.layouts.message')
<script>
   $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $( document ).ready(function() {
     
    });
      
    function tambahDinass(){
        var url = '{{ route("backend.dinas.tambahdinas") }}';
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('<i class="fa fa-pencil"></i> Tambah Perangkat Daerah');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    
    
    
    function editDinas(id){
        var url = '{{ route("backend.dinas.editdinas",["id"=>":id"]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('<i class="fa fa-pencil"></i> Edit Perangkat Daerah');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    
    
    function deleteDinas(id){
        swal({
            title: 'Apa anda yakin?',
              text: "Anda akan menghapus dinas!",
              type: 'error',
              showCancelButton: true,
              confirmButtonText: 'Ok'
        }).then(function () {
            var datapost={
                "id"  :   id
              };
              $.ajax({
                type: "POST",
                url: '{{ route("backend.dinas.dinasdelete") }}',
                data : JSON.stringify(datapost),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function(response) {
                  if (response.success == true) {

                      swal({
                            title: 'Success!',
                            text: response.message,
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        }).then(function () {
                            window.location.href = "{{url()->current()}}";
                        })



                  } else{
                    swal("Failed!", response.message, "error");
                  }
                }
              });
        })
    }
</script>
@endsection