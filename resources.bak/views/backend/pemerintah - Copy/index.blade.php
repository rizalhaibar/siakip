@extends('backend.layouts.landing')

@section('title', 'Home')

@section('content')
     <!-- HOME -->
     <section id="home" data-stellar-background-ratio="0.5" style="background: url({{ asset('images/ch1.jpg') }}); background-size: cover; ">
          <div class="overlay"></div>
          <div class="container">
               <div class="row">

                    <div class="col-md-6 col-sm-12">
                         <div class="home-info">
                              <h1>Selamat Datang di SIAKIP CIMAHI</h1>
                              <a href="#fitur" class="btn section-btn smoothScroll">Fitur Siakip</a>
                              <span>
                                   Lihat di bawah ini
                                   <small>Untuk fitur melihat/mengunduh LAKIP SKPD Kota Cimahi</small>
                              </span>
                         </div>
                    </div>

                    <div class="col-md-6 col-sm-12">
                         <div class="home-video">
                              <div class="embed-responsive embed-responsive-16by9">
                                   <iframe src="https://www.youtube.com/embed/UCaqzifm7BM" frameborder="0" allowfullscreen></iframe>
                              </div>
                         </div>
                    </div>
                    
               </div>
          </div>
     </section>


     <!-- ABOUT -->
     <section id="about" data-stellar-background-ratio="0.5">
          <div class="container">
               <div class="row">

                    <div class="col-md-8 col-sm-8">
                         <div class="about-info">
                              <div class="section-title">
                                   <h2>Selamat Datang di Siakip</h2>
                                   <span class="line-bar">...</span>
                              </div>
                              <p>Sistem Akuntabilitas Kinerja Instansi Pemerintah yang selanjutnya disingkat SAKIP,
									adalah rangkaian sistematik dari berbagai aktivitas, alat, dan prosedur yang dirancang
									untuk tujuan penetapan dan pengukuran, pengumpulan data,
									pengklasifikasian, pengikhtisaran, dan pelaporan kinerja pada instansi pemerintah, dalam rangka
									pertanggungjawaban dan peningkatan kinerja instansi pemerintah.</p>
                  
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                         <div class="about-image">
                              <img src="images/about-image.jpg" class="img-responsive" alt="">
                         </div>
                    </div>
                    
               </div>
          </div>
     </section>

     <!-- ABOUT -->
     <section id="fitur" data-stellar-background-ratio="0.5">
          <div class="container">
               <div class="row">

                    <div class="col-md-1 col-sm-1">
					          </div>
                    <div class="col-md-5 col-sm-11">
                         <div class="about-image">
                              <img src="images/pns.png" class="img-responsive" style="width: 200px; height: auto" alt="">
                         </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="about-info">
                            <div class="section-title" style="margin-right: 65px">
                                <h2>Fitur Siakip</h2>
                                <span class="line-bar">...</span>
                            </div>
                            <div class="col-md-12">
                              <div class="col-md-4">
                              </div>
                              <div class="portfolio-item apps col-xs-6 col-sm-4 col-md-2">
                                <a href="{{ route('pemerintah.perencanaan',['id'=>Crypt::encryptString(0), 'idsasaran'=>Crypt::encryptString(0)]) }}"><div class="box">
                                  <img src="{{ asset('images/perencanaankerja.png') }}"  style="width: 100px; height: auto">
                                </div></a>
                              
                                
                              </div>
                              <div class="col-md-4">
                              </div>
                            </div>
                            
                            <div class="col-md-12">
                              <div class="portfolio-item bootstrap wordpress col-xs-6 col-sm-4 col-md-1">
                              </div>
                              <div class="portfolio-item bootstrap wordpress col-xs-6 col-sm-4 col-md-1">
                                <a href="{{ route('pemerintah.pelaporan',['id'=>Crypt::encryptString(0), 'idsasaran'=>Crypt::encryptString(0)]) }}"><div class="box">
                                  <img src="{{ asset('images/pelaporankinerja.png') }}"  style="width: 100px; height: auto">
                                </div></a>
                              
                              </div>
                              <div class="col-md-5">
                              </div>
                              <div class="portfolio-item joomla bootstrap col-xs-6 col-sm-4 col-md-2">
                              
                                <a href="{{ route('pemerintah.pengukuran',['id'=>Crypt::encryptString(0), 'idsasaran'=>Crypt::encryptString(0)]) }}"><div class="box">
                                  <img src="{{ asset('images/pengukurankerja.png') }}"  style="width: 100px; height: auto">
                                </div></a>
                              
                              </div>
                              <!--/.portfolio-item-->
                            </div>
                            

                            <div class="col-md-12">
                              <div class="col-md-4">
                              </div>
                                <div class="portfolio-item joomla wordpress apps col-xs-6 col-sm-4 col-md-2">
                                
                                <a href="{{ route('pemerintah.evaluasi') }}"><div class="box">
                                  <img src="{{ asset('images/evaluasikerja.png') }}"  style="width: 100px; height: auto">
                                </div></a>
                              
                                </div>
                              <div class="col-md-4">
                              </div>
                            </div>	


                        </div>
					
                    </div>

                    

               </div>
          </div>
     </section>


     <!-- CONTACT -->
     <section data-stellar-background-ratio="0.5" style="margin-top: -85px !important">	  
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3961.166936441682!2d107.55264901477257!3d-6.870591195035049!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e43e4d3ab263%3A0x485bd63d0648c89b!2sPemkot+Cimahi!5e0!3m2!1sid!2sid!4v1550913647325" width="100%" height="380"  frameborder="2" style="border:0; margin-bottom: -85px !important" allowfullscreen></iframe>
		
     </section>


     @endsection

@section('customjs')
@endsection