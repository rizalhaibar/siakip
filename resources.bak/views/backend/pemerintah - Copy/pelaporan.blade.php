@extends('backend.layouts.landing')
@section('title', 'Pelaporan')

@section('content')
	<section id="about" style="margin-top: 20px">      
        <div class="container" style="width: auto !important">
			<div class="col-md-12" style="margin-top: 10px" >
				<div class="row">
					<div class="col-md-12">
					<h3>Pelaporan Kinerja <span id="initahun"></span></h3>
					<br/>
					<br/>
					</div>
				</div>
				<div class="row">
					 
					  <div class="col-md-3">
						  <div class="form-group row">
							<label for="colFormLabelSm" class="col-sm-2 " style="margin-top:5px">Tahun</label>
							<div class="col-sm-10">
							<select class="form-control right" id="tahunini" name="tahunini" onChange="tahunnya(this.value)">

							<option disabled selected>Pilih Tahun..</option>
							 <?php
								$already_selected_value = date("Y");
								$earliest_year = 2013;

								foreach (range(date('Y'), $earliest_year) as $x) {
									if($_COOKIE['tahun'] == $x){
										echo '<option value="'.$x.'" selected>'.$x.'</option>';
									}else{
										echo '<option value="'.$x.'">'.$x.'</option>';
									}
									
								}


								?>
							</select>
							</div>
						  </div>
						  
					 </div>
					 <div class="col-md-8">
					 	<a href="{{ route('pemerintah.perencanaan',['id'=>Crypt::encryptString(0), 'idsasaran'=>Crypt::encryptString(0)]) }}" class="btn btn-default"><i class="fa fa-cogs"></i> Perencanaan Kinerja</a>
						<a href="{{ route('pemerintah.pengukuran',['id'=>Crypt::encryptString(0), 'idsasaran'=>Crypt::encryptString(0)]) }}" class="btn btn-default "><i class="fa fa-dashboard"></i> Pengukuran Kinerja</a>
						<a href="{{ route('pemerintah.pelaporan',['id'=>Crypt::encryptString(0), 'idsasaran'=>Crypt::encryptString(0)]) }}" class="btn btn-default active"><i class="fa fa-clipboard"></i> Pelaporan Kinerja</a>
						<a href="{{ route('pemerintah.evaluasi') }}" class="btn btn-default "><i class="fa fa-check"></i> Evaluasi Kinerja</a>
					</div>
				 </div> 
			
				<div class="row">
					<div class="col-md-3">
						<table id="example" class="table " style="font-size: 12px">
							<thead>
								<tr>
									<th></th>
									<th>Instansi</th>
									<th>Action</th>
								</tr>
							</thead>

							<tbody>
							
								@php($sl=1)
								 @foreach($data as $dinas)

								 @if(Crypt::decryptString($id) ==$dinas->td_id )
								 <tr style="background: #ccc">
								 
								 @else
								 
								 <tr>
								 @endif
									<td>{{$sl}}</td>
									<td>{{$dinas->td_dinas}}</td>
									<td><a href="{{route('pemerintah.pelaporan',['id'=>Crypt::encryptString($dinas->td_id), 'idsasaran'=>Crypt::encryptString(0)])}}" type="button" class="btn btn-block btn-success" ><i class="fa fa-search"></i></a></td>
								 </tr>
								@php($sl++)
								@endforeach
							</tbody>
						</table>
					</div>
					<div class="col-md-9">
						<ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#home">Hasil Pelaporan</a></li>
						</ul>

						<div class="tab-content">
							@if(Crypt::decryptString($id) != 0)
								<div class="form-group">
									<label for="exampleSelect1">Pilih Sasaran Strategis: </label>
									<select class="form-control"  onChange="cariData(this.value)">
										<option selected disabled>Pilih Sasaran Strategis....</option>
										@foreach($listsasaran as $sasaran)
											<option value="{{Crypt::encryptString($sasaran->tst_id)}}" @if($idsasaran != 0 ) {{$sasaran->tst_id == $idsasaran ? 'selected' : ''}} @endif >{{$sasaran->tst_sasaran_tujuan}}</option>
										@endforeach
									</select>
								</div>
								@if(isset($pelaporan))
									<h3>{{$pelaporan->tp_judul}}</h3>
									<?php
										echo $pelaporan->tp_content;
									?>
								@endif
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
    
   <div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
       <div class="modal-dialog  modal-lg">
           <div class="modal-content p-0 b-0">
               <div class="panel panel-color panel-success panel-filled">
                   <div class="panel-heading">
                       <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                       <h3 class="panel-title"></h3>
                   </div>
                   <div class="panel-body">
                       <p></p>
                   </div>
               </div>
           </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
   </div><!-- /.modal -->
@endsection


@section('onpage-js')
@include('backend.layouts.message')
<script> 	
	function cariData(id){
		var url = '{{ route("pemerintah.pelaporan",["id"=> $id, "idsasaran"=> ":idsasaran" ]) }}';
		url = url.replace(':idsasaran', id);
		window.location.href =  url;
	}
</script> 	
@endsection