@extends('backend.layouts.landing')
@section('title', 'Evaluasi')

@section('content')
	<section id="about" style="margin-top: 20px">      
        <div class="container" style="width: auto !important">
			<div class="col-md-11" style="margin: 5px" id="up">
			
			<div class="row">
				<div class="col-md-12">
				  <h3>Evaluasi Kinerja <span id="initahun"></span></h3>
				  <br/>
				  <br/>
				</div>
			</div>
			 
			 <div class="row">
				 
				  <div class="col-md-3">
					  <div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 " style="margin-top:5px">Tahun</label>
						<div class="col-sm-10">
						<select class="form-control right" id="tahunini" name="tahunini" onChange="tahunnya(this.value)">

						<option disabled selected>Pilih Tahun..</option>
						 <?php
							$already_selected_value = date("Y");
							$earliest_year = 2013;

							foreach (range(date('Y'), $earliest_year) as $x) {
								if($_COOKIE['tahun'] == $x){
									echo '<option value="'.$x.'" selected>'.$x.'</option>';
								}else{
									echo '<option value="'.$x.'">'.$x.'</option>';
								}
								
							}


							?>
						</select>
						</div>
					  </div>
					  
				 </div>
				 <div class="col-md-8">
				 	<a href="{{ route('pemerintah.perencanaan',['id'=>Crypt::encryptString(0), 'idsasaran'=>Crypt::encryptString(0)]) }}" class="btn btn-default "><i class="fa fa-cogs"></i> Perencanaan Kinerja</a>
					<a href="{{ route('pemerintah.pengukuran',['id'=>Crypt::encryptString(0), 'idsasaran'=>Crypt::encryptString(0)]) }}" class="btn btn-default "><i class="fa fa-dashboard"></i> Pengukuran Kinerja</a>
					<a href="{{ route('pemerintah.pelaporan',['id'=>Crypt::encryptString(0), 'idsasaran'=>Crypt::encryptString(0)] ) }}" class="btn btn-default "><i class="fa fa-clipboard"></i> Pelaporan Kinerja</a>
					<a href="{{ route('pemerintah.evaluasi') }}" class="btn btn-default active"><i class="fa fa-check"></i> Evaluasi Kinerja</a>
				</div>
			 </div> 
			 
			 
			</div>
			<div class="col-md-12" style="margin-top: 10px">
				<table id="example" class="table " style="font-size: 12px">
				  <thead>
					<tr>
						<th></th>
						<th><h4>Instansi</h4></th>
						<th>Kategori </th>
					</tr>
				  </thead>

					<tbody>
					
					@php($sl=1)
					 @foreach($data as $dinas)
					 <tr>
						<td>{{$sl}}</td>
						<td>{{$dinas->td_dinas}}</td>
						<td>{{$dinas->nilai}}</td>
					 </tr>
					 
                    @php($sl++)
                    @endforeach
					</tbody>
				</table>
			</div>
		</div>
	</section>
    
@endsection

@section('customjs')
@endsection