
<div class="form-group">
    <label class="col-md-2 control-label">Triwulan I*</label>
    <div class="col-md-12">
        <input type="text" class="form-control" id="tw1" name="tw1" value="{{@$detail->twtarget1}}" placeholder="Triwulan I..." required>
    </div>
</div>
<div class="form-group">
    <label class="col-md-2 control-label">Triwulan II*</label>
    <div class="col-md-12">
        <input type="text" class="form-control" id="tw2" name="tw2" value="{{@$detail->twtarget2}}" placeholder="Triwulan II..." required>
    </div>
</div>
<div class="form-group">
    <label class="col-md-2 control-label">Triwulan III*</label>
    <div class="col-md-12">
        <input type="text" class="form-control" id="tw3" name="tw3" value="{{@$detail->twtarget3}}" placeholder="Triwulan III..." required>
    </div>
</div>
<div class="form-group">
    <label class="col-md-2 control-label">Triwulan IV*</label>
    <div class="col-md-12">
        <input type="text" class="form-control" id="tw4" name="tw4" value="{{@$detail->twtarget4}}" placeholder="Triwulan IV..." required>
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-10 col-md-8">
        <button type="button" onClick="simpanTarget()" class="btn btn-primary waves-effect waves-light btn-rounded">
            Simpan
        </button>
    </div>
</div>


<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    
    
    function simpanTarget(){
       
        var tw1   = $('#tw1').val();
        var tw2   = $('#tw2').val();
        var tw3   = $('#tw3').val();
        var tw4   = $('#tw4').val();
        
                if(tw1){
                        var datapost={
                            "id"  :   "{{$id}}",
                            "tw1"  :   tw1,
                            "tw2"  :   tw2,
                            "tw3"  :   tw3,
                            "tw4"  :   tw4,
                            };
                            $.ajax({
                            type: "POST",
                            url: "{{route('backend.rencanaaksi.edittargetindikatorsasaran')}}",
                            data : JSON.stringify(datapost),
                            dataType: 'json',
                            contentType: 'application/json; charset=utf-8',
                            success: function(response) {
                                if (response.success == true) {
                                    swal({
                                        title: 'Success!',
                                        text: response.message,
                                        type: 'success',
                                        showCancelButton: false,
                                        confirmButtonText: 'Ok'
                                    }).then(function () {
                                        var url = '{{ route("backend.rencanaaksi.home",["id"=>"$iddinas", "idsasaran"=> "$idsasaran" ]) }}';
                                        window.location.href =  url;
                                    })
                                } else{
                                swal("Failed!", response.message, "error");
                                }
                            }
                            });
                
                
                }else{
                    swal("Failed!", "Target Triwulan 1 Belum Terisi!", "error");
                }
            
    }
</script>