<form class="kt-form" method="post" action="{{route('backend.iku.print')}}">
    @csrf
    <div class="form-group">
        <label class="col-md-2 control-label">Pihak Pertama*</label>
        <div class="col-md-12">
            <input type="hidden" class="form-control" id="id" name="id" value="{{$id}}">
            <input type="text" class="form-control" id="pihak_pertama" name="pihak_pertama" placeholder="Pihak Pertama..." required>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-2 control-label">Jabatan Pihak Pertama*</label>
        <div class="col-md-12">
            <select class="form-control" id="jabatan_pihak_pertama" name="jabatan_pihak_pertama" required>
                <option selected disabled>Pilih Jabatan ....</option>
                <option value="WALIKOTA CIMAHI" >WALIKOTA</option>
                <option value="KEPALA DINAS" >KEPALA DINAS</option>
                <option value="KEPALA BADAN" >KEPALA BADAN</option>
                <option value="KEPALA CAMAT" >KEPALA CAMAT</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-2 control-label">NIP*</label>
        <div class="col-md-12">
            <input type="text" class="form-control" id="nip" name="nip" placeholder="NIP..." required>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-offset-10 col-md-8">
            <button type="submit" class="btn btn-primary waves-effect waves-light btn-rounded">
                Simpan
            </button>
        </div>
    </div>
</form>
