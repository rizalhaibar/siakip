@extends('backend.layouts.app')

@section('title', 'IKU')

@section('content')

<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <div class="row">
                        <div class="col-md-11">
                            <h3 class="kt-portlet__head-title">
                                List Indikator Kinerja Utama
                            </h3>
                        </div>
                        <div class="col-md-1" >
                            <a href="javascript:void(0)" onclick="printAwals({{$id}})" class="btn btn-primary waves-effect waves-light btn-rounded">
                                <i class="fas fa-print"></i> Prints
                            </a>
                        </div>
                    </div>
                    <ul style="margin-left: 20px">
                        <h4 style="margin-left:-30px; color: #336600; margin-top:10px">Sasaran Strategis </h4>
                    </ul>
                    <ol  style="margin-left: 20px">
                        @php($sa = 1)
                        @foreach($listiku as $newsasaran)


                        <li style="display:block">
                            {{$sa}}. {{$newsasaran->tst_sasaran_tujuan}}

                                <ul style="margin-left: 20px">
                                    <h4 style="margin-left:-30px; color: #336600; margin-top:10px">Indikator Sasaran </h4>
                                </ul>
                                    <ol>
                                    <li style="display:block">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th><center>NO</center></th>
                                                    <th><center>Indikator Sasaran Strategis</center></th>
                                                    <th><center>Satuan</center></th>
                                                    <th><center>Formula</center></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(isset($newsasaran->indikatorsasaran))
                                            @php($sas = 1)
                                            @foreach($newsasaran->indikatorsasaran as $indisasaran)

                                                <tr>
                                                    <td>{{$sas}}</td>
                                                    <td>{{$indisasaran->tis_indikator_sasaran}}</td>
                                                    <td>{{$indisasaran->tis_satuan}}</td>
                                                    <td>{{$indisasaran->formula}}</td>
                                                </tr>
                                                @php($sas++)
                                                @endforeach


                                            @endif
                                            </tbody>
                                        </table>
                                    </li>
                                </ol>


                        </li>

                        @php($sa++)
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

    <div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content p-0 b-0">
                <div class="panel panel-color panel-primary panel-filled">
                    <div class="panel-heading">
                        <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 class="panel-title"></h3>
                    </div>
                    <div class="panel-body">

                        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                            <div class="row">
                                <div class="col-md-12">
                                     <p></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection

@section('onpage-js')
@include('backend.layouts.message')
<script>


    function tambahFormula(id){
        var url = '{{ route("backend.iku.tambahformula",["id"=>":id"]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .panel-body').load(url);
        $('#panel-modal  .panel-title').html('<i class="fa fa-user"></i> Tambah/Edit Formula');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }

    function printAwals(id){
        var url = '{{ route("backend.iku.printiku",["id"=>":id"]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .panel-body').load(url);
        $('#panel-modal  .panel-title').html('Print IKU');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }


</script>
@endsection


