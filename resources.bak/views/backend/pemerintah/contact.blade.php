@extends('backend.layouts.landing')

@section('title', 'Contact')

@section('content')
	<section id="about" style="margin-top: 180px">      
        <div class="container">
            <div class="row">
                <section class="page col-sm-4">
                    <h2 class="page-title">Kontak Kami</h2>
                    <div class="entry">

                        
                            Pemerintah Kota Cimahi<br>
                            Jl. Rd. Demang Hardjakusumah <br>
                            Blok Jati, Cihanjuang, Cimahi<br>
                            Telp. (022) 6654274 Fax: (022) 6654274<br>
                            <br>
                            Pesan Singkat Penduduk : 081221700800<br>
                            Email Layanan Informasi (PPID) : humas@cimahikota.go.id<br>
                            Email Administrator Website : admin@cimahikota.go.id</div>                    
                   
                </section>
                <section class="page col-sm-8">
                    <div id="map"></div>                   
                   
                </section>
                
            </div>
        </div>
	</section>
@endsection

@section('customjs')
@endsection