@extends('backend.layouts.landing')

@section('title', 'Home')

@section('content')
     <!-- HOME -->
     <section id="home" data-stellar-background-ratio="0.5" style="background: url({{ asset('images/ch1.jpg') }}); background-size: cover; ">
          <div class="overlay"></div>
          <div class="container">
               <div class="row">

                    <div class="col-md-6 col-sm-12">
                         <div class="home-info">
                              <h1>Selamat Datang di SIAKIP CIMAHI</h1>
                              <a href="#fitur" class="btn section-btn smoothScroll">Fitur Siakip</a>
                              <span>
                                   Lihat di bawah ini
                                   <small>Untuk fitur melihat/mengunduh LAKIP SKPD Kota Cimahi</small>
                              </span>
                         </div>
                    </div>

                    <div class="col-md-6 col-sm-12">
                         <div class="home-video">
                              <div class="embed-responsive embed-responsive-16by9">
                                   <iframe src="https://www.youtube.com/embed/UCaqzifm7BM" frameborder="0" allowfullscreen></iframe>
                              </div>
                         </div>
                    </div>
                    
               </div>
          </div>
     </section>


     <!-- ABOUT -->
     <section id="about" data-stellar-background-ratio="0.5">
          <div class="container">
               <div class="row">

                    <div class="col-md-8 col-sm-8">
                         <div class="about-info">
                              <div class="section-title">
                                   <h2>Selamat Datang di Siakip</h2>
                                   <span class="line-bar">...</span>
                              </div>
                              <p>Sistem Akuntabilitas Kinerja Instansi Pemerintah yang selanjutnya disingkat SAKIP,
									adalah rangkaian sistematik dari berbagai aktivitas, alat, dan prosedur yang dirancang
									untuk tujuan penetapan dan pengukuran, pengumpulan data,
									pengklasifikasian, pengikhtisaran, dan pelaporan kinerja pada instansi pemerintah, dalam rangka
									pertanggungjawaban dan peningkatan kinerja instansi pemerintah.</p>
                  
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                         <div class="about-image">
                              <img src="images/about-image.jpg" class="img-responsive" alt="">
                         </div>
                    </div>
                    
               </div>
          </div>
     </section>


     <!-- ABOUT -->
     <section id="fitur" data-stellar-background-ratio="0.5">
          <div class="container">
               <div class="row">

                    <div class="col-md-1 col-sm-1">
					          </div>
                    <div class="col-md-5 col-sm-11">
                         <div class="about-image">
                              <img src="images/pns.png" class="img-responsive" style="width: 200px; height: auto" alt="">
                         </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="about-info">
                            <div class="section-title" style="margin-right: 65px">
                                <h2>Fitur Siakip</h2>
                                <span class="line-bar">...</span>
                            </div>
                            <div class="col-md-12">
                              <div class="col-md-4">
                              </div>
                              <div class="portfolio-item apps col-xs-6 col-sm-4 col-md-2">
                                <a href="{{ route('pemerintah.perencanaan',['id'=>Crypt::encryptString(0), 'idsasaran'=>Crypt::encryptString(0), 'idjabatan'=>Crypt::encryptString(0)]) }}"><div class="box">
                                  <img src="{{ asset('images/perencanaankerja.png') }}"  style="width: 100px; height: auto">
                                </div></a>
                              
                                
                              </div>
                              <div class="col-md-4">
                              </div>
                            </div>
                            
                            <div class="col-md-12">
                              <div class="portfolio-item bootstrap wordpress col-xs-6 col-sm-4 col-md-1">
                              </div>
                              <div class="portfolio-item bootstrap wordpress col-xs-6 col-sm-4 col-md-1">
                                <a href="{{ route('pemerintah.pelaporan',['id'=>Crypt::encryptString(0), 'idsasaran'=>Crypt::encryptString(0)]) }}"><div class="box">
                                  <img src="{{ asset('images/pelaporankinerja.png') }}"  style="width: 100px; height: auto">
                                </div></a>
                              
                              </div>
                              <div class="col-md-5">
                              </div>
                              <div class="portfolio-item joomla bootstrap col-xs-6 col-sm-4 col-md-2">
                              
                                <a href="{{ route('pemerintah.pengukuran',['id'=>Crypt::encryptString(0), 'idsasaran'=>Crypt::encryptString(0)]) }}"><div class="box">
                                  <img src="{{ asset('images/pengukurankerja.png') }}"  style="width: 100px; height: auto">
                                </div></a>
                              
                              </div>
                              <!--/.portfolio-item-->
                            </div>
                            

                            <div class="col-md-12">
                              <div class="col-md-4">
                              </div>
                                <div class="portfolio-item joomla wordpress apps col-xs-6 col-sm-4 col-md-2">
                                
                                <a href="{{ route('pemerintah.evaluasi') }}"><div class="box">
                                  <img src="{{ asset('images/evaluasikerja.png') }}"  style="width: 100px; height: auto">
                                </div></a>
                              
                                </div>
                              <div class="col-md-4">
                              </div>
                            </div>	


                        </div>
					
                    </div>

                    

               </div>
          </div>
     </section>


     <!-- INFO -->
     <section id="info" data-stellar-background-ratio="0.5">
          
     <div class="container">
          <h2>Info Siakip</h2>  
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
               <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
               <li data-target="#myCarousel" data-slide-to="1"></li>
               <li data-target="#myCarousel" data-slide-to="2"></li>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner">
               <div class="item active">
                    <img src="{{ asset('images/ch1.jpg') }}" alt="Los Angeles" style="width:100%;">
                    <div class="carousel-caption" style="background: rgba(0,0,0,.5);">
                         <h3 style=" color: white !important">Siakip Cimahi</h3>
                         <p style=" color: #ddd !important">More....</p>
                    </div>
               </div>

               <div class="item">
                    <img src="{{ asset('images/ch2.jpg') }}" alt="Chicago" style="width:100%;">
                    <div class="carousel-caption" style="background: rgba(0,0,0,.5);">
                         <h3 style=" color: white !important">Siakip Cimahi</h3>
                         <p style=" color: #ddd !important">More....</p>
                    </div>
               </div>
          
               <div class="item">
                    <img src="{{ asset('images/ch11.jpg') }}" alt="New york" style="width:100%;">
                    <div class="carousel-caption" style="background: rgba(0,0,0,.5);">
                         <h3 style=" color: white !important">Siakip Cimahi</h3>
                         <p style=" color: #ddd !important">More....</p>
                    </div>
               </div>
          </div>

          <!-- Left and right controls -->
          <a class="left carousel-control" href="#myCarousel" data-slide="prev" style="margin-top: 160px">
               <span class="fa fa-chevron-left "></span>
               <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#myCarousel" data-slide="next" style="margin-top: 160px">
               <span class="fa fa-chevron-right"></span>
               <span class="sr-only">Next</span>
          </a>
          </div>
     </div>

     </section>
     <!-- CONTACT -->
     <section data-stellar-background-ratio="0.5" style="margin-top: -85px !important">	  
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3961.166936441682!2d107.55264901477257!3d-6.870591195035049!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e43e4d3ab263%3A0x485bd63d0648c89b!2sPemkot+Cimahi!5e0!3m2!1sid!2sid!4v1550913647325" width="100%" height="380"  frameborder="2" style="border:0; margin-bottom: -85px !important" allowfullscreen></iframe>
		
     </section>


     @endsection

@section('customjs')
@endsection