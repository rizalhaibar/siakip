@extends('backend.layouts.landing')
@section('title', 'Pelaporan')

@section('content')
	<section id="about" style="margin-top: 20px">      
        <div class="container" style="width: auto !important">
			<div class="col-md-12" style="margin-top: 10px" >

			<div class="row">
					<div class="col-md-12">
					<h3>Pengukuran Kinerja <span id="initahun"></span></h3>
					<br/>
					<br/>
					</div>
				</div>
				<div class="row">
				 
				  <div class="col-md-3">
					  <div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 " style="margin-top:5px">Tahun</label>
						<div class="col-sm-10">
						<select class="form-control right" id="tahunini" name="tahunini" onChange="tahunnya(this.value)">

						<option disabled selected>Pilih Tahun..</option>
						 <?php
							$already_selected_value = date("Y");
							$earliest_year = 2013;

							foreach (range(date('Y'), $earliest_year) as $x) {
								if($_COOKIE['tahun'] == $x){
									echo '<option value="'.$x.'" selected>'.$x.'</option>';
								}else{
									echo '<option value="'.$x.'">'.$x.'</option>';
								}
								
							}


							?>
						</select>
						</div>
					  </div>
					  
				 </div>
				 <div class="col-md-8">
				 	<a href="{{ route('pemerintah.perencanaan',['id'=>0, 'idsasaran'=>0, 'idjabatan'=> 0]) }}" class="btn btn-default "><i class="fa fa-cogs"></i> Perencanaan Kinerja</a>
					<a href="{{ route('pemerintah.pengukuran',['id'=>0, 'idsasaran'=>0]) }}" class="btn btn-default active"><i class="fa fa-dashboard"></i> Pengukuran Kinerja</a>
					<a href="{{ route('pemerintah.pelaporan',['id'=>0, 'idsasaran'=>0] ) }}" class="btn btn-default "><i class="fa fa-clipboard"></i> Pelaporan Kinerja</a>
				
				 	<a href="{{ route('pemerintah.evaluasi') }}" class="btn btn-default "><i class="fa fa-check"></i> Evaluasi Kinerja</a>
			
				 </div>
			 </div> 
			
				<div class="row">
					<div class="col-md-3">
					<table id="example" class="table " style="font-size: 12px">
							<thead>
								<tr>
									<th></th>
									<th>Instansi</th>
									<th>Action</th>
								</tr>
							</thead>

							<tbody>
							
								@php($sl=1)
								 @foreach($data as $dinas)

								 @if($id ==$dinas->td_id )
								 <tr style="background: #ccc">
								 
								 @else
								 
								 <tr>
								 @endif
									<td>{{$sl}}</td>
									<td>{{$dinas->td_dinas}}</td>
									<td><a href="{{route('pemerintah.pengukuran',['id'=>$dinas->td_id, 'idsasaran'=>0])}}" type="button" class="btn btn-block btn-success" ><i class="fa fa-search"></i></a></td>
								 </tr>
								@php($sl++)
								@endforeach
							</tbody>
						</table>
					</div>
					<div class="col-md-9">
						<ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#home">Hasil Pengukuran</a></li>
						</ul>

						<div class="tab-content">
							@if($id != 0)
								<div class="form-group">
									<label for="exampleSelect1">Pilih Sasaran Strategis : </label>
									<select class="form-control"  onChange="cariData(this.value)">
										<option selected disabled>Pilih Sasaran Strategis ....</option>
										@foreach($listsasaran as $sasaran)
											<option value="{{$sasaran->tst_id}}" @if($idsasaran != 0 ) {{$sasaran->tst_id == $idsasaran ? 'selected' : ''}} @endif >{{$sasaran->tst_sasaran_tujuan}}</option>
										@endforeach
									</select>
								</div>
								@if($idsasaran != 0)
									<ul class="nav nav-tabs  nav-tabs-line nav-tabs-line-2x nav-tabs-line-success" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" data-toggle="tab" href="#sasaran" role="tab">Sasaran Strategis</a>
										</li>
										<li class="nav-item">
											<a class="nav-link " data-toggle="tab" href="#program" role="tab">Sasaran Program</a>
										</li>
										<li class="nav-item">
											<a class="nav-link " data-toggle="tab" href="#kegiatan" role="tab">Sasaran Kegiatan</a>
										</li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="sasaran" role="tabpanel">
											<ul style="margin-left: 20px">
												<h4 style="margin-left:-30px; color: #336600; margin-top:10px">Indikator Sasaran Strategis</h4>
											</ul>
											<div class="row">
												<div class="col-12">
												<table class="table table-bordered" style="font-size: 10.5px">
												<thead>
													<tr>
														<th rowspan="2">No</th>
														<th rowspan="2">Indikator Sasaran Strategis</th>
														<th rowspan="2">Satuan</th>
														<th rowspan="2">Target Per Tahun</th>
														<th colspan="2"><center>Triwulan 1</center></th>
														<th colspan="2"><center>Triwulan 2</center></th>
														<th colspan="2"><center>Triwulan 3</center></th>
														<th colspan="2"><center>Triwulan 4</center></th>
													</tr>
													
													<tr>
														
														<th>Target</th>
														<th>Realisasi</th>
														<th>Target</th>
														<th>Realisasi</th>
														<th>Target</th>
														<th>Realisasi</th>
														<th>Target</th>
														<th>Realisasi</th>
														
													</tr>
												</thead>
												<tbody>
												@php($sas = 1)
												@foreach($indikatorsasaran as $indisasaran)
													
													<tr>
														<td>{{$sas}}</td>
														<td>{{$indisasaran->tis_indikator_sasaran}}</td>
														<td>{{$indisasaran->tis_satuan}}</td>
														<td>{{$indisasaran->tis_target}}</td>
														<td>{{$indisasaran->twtarget1}}</td>
														<td><b>{{$indisasaran->tw1}}</b></td>
														<td>{{$indisasaran->twtarget2}}</td>
														<td><b>{{$indisasaran->tw2}}</b></td>
														<td>{{$indisasaran->twtarget3}}</td>
														<td><b>{{$indisasaran->tw3}}</b></td>
														<td>{{$indisasaran->twtarget4}}</td>
														<td><b>{{$indisasaran->tw4}}</b></td>
													</tr>
													@php($sas++) 
													@endforeach
												</tbody>
											</table>
												</div>
											</div>
											
										</div>
										<div class="tab-pane " id="program" role="tabpanel">

											<ul style="margin-left: 20px">
												<h4 style="margin-left:-30px; color: #336600; margin-top:10px">Program </h4>
											</ul>
											<table class="table table-bordered" style="font-size: 10.5px">
														<thead>
															<tr>
																<th rowspan="2">No</th>
																<th rowspan="2">Indikator Sasaran Strategis</th>
																<th rowspan="2">Sasaran Program</th>
																<th rowspan="2">Indikator Sasaran Program</th>
																<th rowspan="2">Satuan</th>
																<th rowspan="2">Target Per Tahun</th>
																<th colspan="2"><center>Triwulan 1</center></th>
																<th colspan="2"><center>Triwulan 2</center></th>
																<th colspan="2"><center>Triwulan 3</center></th>
																<th colspan="2"><center>Triwulan 4</center></th>
															</tr>
															<tr>
																<th>Target</th>
																<th>Realisasi</th>
																<th>Target</th>
																<th>Realisasi</th>
																<th>Target</th>
																<th>Realisasi</th>
																<th>Target</th>
																<th>Realisasi</th>
																
															</tr>
														</thead>
														<?php 
																$i=1; 
																
																$html = '';                                  
																$isi1 = '';                                  
																$isi2 = '';  
																$isi3 = '';  
																$isi4 = '';  
															?>
														<tbody>
														@foreach($program as $new)
														@php($totalprogram=0)
														
																@if(isset($new->program))

																@php($alltotalindikatorprogram=0)
																@php($allatas=0)
																@php($allbawah=0)
																@foreach($new->program as $k=>$prg)
																	@php($totalindikatorprogram=0)
																	@php($totalindikatorprogrambawah=0)
																	@php($x=0)
																	@if(isset($prg->indikatorprogram))
																	



																	
																		@foreach($prg->indikatorprogram as $s=>$indprg)  
																			@php($actionhtml = '')
																			@if($indprg->twtarget1)
																				@if($indprg->tw1)
																					@php($actionhtml = '<a href="javascript:void(0)" class="btn btn-info btn-sm btn-icon" onClick="edittargetprogram('.$indprg->tip_id.')"><i class="fa fa-edit"></i></a>')
																				@else
																					@php($actionhtml = '<a href="javascript:void(0)" class="btn btn-success btn-sm btn-icon" onClick="tambahtargetprogram('.$indprg->tip_id.')"><i class="fa fa-search"></i></a>')
																				@endif
																			@endif
																			@if($s == 0)
																				@php($totalindikatorprogram += array_count_values(array_column($prg->indikatorprogram, 'idprogram'))[$prg->tps_id])

																			
																				@php($totalindikatorprogrambawah = 1)
																				@php($isi2 = '<td>'.$indprg->tip_indikator_program.' </td>'
																								.'<td>'.$indprg->tip_satuan.'</td>'
																								.'<td>'.$indprg->tip_target.'</td>'
																								.'<td>'.$indprg->twtarget1.'</td>'
																								.'<td><b>'.$indprg->tw1.'</b></td>'
																								.'<td>'.$indprg->twtarget2.'</td>'
																								.'<td><b>'.$indprg->tw2.'</b></td>'
																								.'<td>'.$indprg->twtarget3.'</td>'
																								.'<td><b>'.$indprg->tw3.'</b></td>'
																								.'<td>'.$indprg->twtarget4.'</b></td>'
																								.'<td><b>'.$indprg->tw4.'</td>'
																							.'</tr>'
																							)

																				
																			@else
																				@php($totalindikatorprogrambawah += array_count_values(array_column($prg->indikatorprogram, 'idprogram'))[$prg->tps_id])

																				@php(
																					$isi2 .= '<tr ><td >'.$indprg->tip_indikator_program.' </td>'
																								.'<td>'.$indprg->tip_satuan.'</td>'
																								.'<td>'.$indprg->tip_target.'</td>'
																								.'<td>'.$indprg->twtarget1.'</td>'
																								.'<td><b>'.$indprg->tw1.'</b></td>'
																								.'<td>'.$indprg->twtarget2.'</td>'
																								.'<td><b>'.$indprg->tw2.'</b></td>'
																								.'<td>'.$indprg->twtarget3.'</td>'
																								.'<td><b>'.$indprg->tw3.'</b></td>'
																								.'<td>'.$indprg->twtarget4.'</b></td>'
																								.'<td><b>'.$indprg->tw4.'</td>'
																							.'</tr>'
																				)
																			@endif

																		@endforeach



																		
																		@if($k == 0)
																			@php($totalprogram += array_count_values(array_column($new->program, 'idindisasaran'))[$new->tis_id])
																			
																			@php($allatas = $totalindikatorprogram)
																			
																			@php($isi1 = '<td rowspan="'.$totalindikatorprogram.'"> '.$prg->tps_program_sasaran.' </td>'
																							.$isi2)
																		@else
																			@php($totalindikatorprogrambawah = $totalindikatorprogrambawah+1)
																			@php($allbawah += $totalindikatorprogrambawah)
																			@php(
																				$isi1 .= '<tr ><td rowspan="'.$totalindikatorprogrambawah.'"> '.$prg->tps_program_sasaran.'</td>'
																							.$isi2
																			)
																		@endif

																		
																		
																	@else

																	
																		@php($alltotalindikatorprogram = $alltotalindikatorprogram+1)
																		@if($k == 0)
																			@php($totalprogram += array_count_values(array_column($new->program, 'idindisasaran'))[$new->tis_id])
																			@php($isi1 = '<td>'.$prg->tps_program_sasaran.' </td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																						.'</tr>')
																		@else
																			@php(
																				$isi1 .= '<tr ><td >'.$prg->tps_program_sasaran.' </span></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																						.'</tr>'
																			)
																		@endif
																	@endif
																@endforeach

																		
																	
																@php($alltotalindikatorprogram = $totalindikatorprogram+$totalindikatorprogrambawah)
																@php($totalkeseluruhan = $allatas+$allbawah)
																@php($html .= '<tr>'
																				.'<td rowspan="'.$totalkeseluruhan.'">'.$i.'</td>'
																				.'<td rowspan="'.$totalkeseluruhan.'">'.$new->tis_indikator_sasaran.' </td>'
																				.$isi1)
																@else
																
																	@php($html .= '<tr>'
																				.'<td>'.$i.'</td>'
																				.'<td>'.$new->tis_indikator_sasaran.'</td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																			.'</tr>')
																@endif
																@php($i++)
														@endforeach
														
														@php(print_r($html))
														</tbody>
											</table>
										
										</div>
										<div class="tab-pane " id="kegiatan" role="tabpanel">
											<ul style="margin-left: 20px">
												<h4 style="margin-left:-30px; color: #336600; margin-top:10px">Kegiatan </h4>
											</ul>
											<table class="table table-bordered" style="font-size: 9px">
												<thead>
													<tr>

														<tr>
															<th rowspan="2">No</th>
															<th rowspan="2">Sasaran Program</th>
															<th rowspan="2">Indikator Sasaran Program</th>
															<th rowspan="2">Sasaran Kegiatan</th>
															<th rowspan="2">Indikator Sasaran Kegiatan</th>
															<th rowspan="2">Satuan</th>
															<th rowspan="2">Target Per Tahun</th>
															<th colspan="2"><center>Triwulan 1</center></th>
															<th colspan="2"><center>Triwulan 2</center></th>
															<th colspan="2"><center>Triwulan 3</center></th>
															<th colspan="2"><center>Triwulan 4</center></th>
														</tr>
														<tr>
															<th>Target</th>
															<th>Realisasi</th>
															<th>Target</th>
															<th>Realisasi</th>
															<th>Target</th>
															<th>Realisasi</th>
															<th>Target</th>
															<th>Realisasi</th>
															
														</tr>
													</tr>
												</thead>
												<tbody>
													<?php 
														$ik=1; 
														
														$htmlk = '';                                  
														$isik1 = '';                                  
														$isik2 = '';  
														$isik3 = '';  
														$isik4 = '';  
														$isik5 = '';  
														$isik5s = '';  
														$isik6 = '';  
														$isik7 = '';  
														$akhirtotala = 0;  
													?>

														@foreach($kegiatan as $keg)
															
															@php($totalprogramk=0)
															@php($totalatasprogram=0)
															@php($testatas=0)
															@php($alltotalkegiatan=0)
															@php($alltotalkegiatanbawah=0)
															
															@php($totalbawahprogram=0)
															@php($totalbawahprogram2=0)
															@if(isset($keg->indikatorprogram))
																@foreach($keg->indikatorprogram as $m=>$indikatorprogram)
																	<!-- param -->
																	
																	@php($totalkegiatan=0)
																	@php($totalkegiatanbawah=0)
																	@php($totalindikatorkegiatank=0)
																	@php($allataskegiatan=0)
																	@php($allbawahkegiatan=0)

																	@if(isset($indikatorprogram->kegiatan))
																		<!-- INDIKATOR KEGIATAN -->
																		
																		@php($totalindikatorkegiatan=0)
																		@php($totalindikatorkegiatanbawah=0)

																		@foreach($indikatorprogram->kegiatan as $as=>$kga)
																		
																			@if(isset($kga->indikatorkegiatan))

																			
																				@foreach($kga->indikatorkegiatan as $inkas=>$indikatorkegiatan)
																				@php($actionhtml = '')
																					@if($indikatorkegiatan->twtarget1)
																						@if($indikatorkegiatan->tw1)
																							@php($actionhtml = '<a href="javascript:void(0)" class="btn btn-info btn-sm btn-icon" onClick="edittargetkegiatan('.$indikatorkegiatan->tik_id.')"><i class="fa fa-edit"></i></a>')
																						@else
																							@php($actionhtml = '<a href="javascript:void(0)" class="btn btn-success btn-sm btn-icon" onClick="tambahtargetkegiatan('.$indikatorkegiatan->tik_id.')"><i class="fa fa-search"></i></a>')
																						@endif
																					@endif
																					@if($inkas == 0)
																						@php($totalindikatorkegiatan += array_count_values(array_column($kga->indikatorkegiatan, 'idkegiatan'))[$kga->tkp_id])
																						
																						@php($totalindikatorkegiatanbawah = 1)
																						@php($isik5 = '<td > '.$indikatorkegiatan->tik_indikator_kegiatan.'  </td>'
																											.'<td>'.$indikatorkegiatan->tik_satuan.'</td>'
																											.'<td>'.$indikatorkegiatan->tik_target.'</td>'
																											.'<td>'.$indikatorkegiatan->twtarget1.'</td>'
																											.'<td><b>'.$indikatorkegiatan->tw1.'</b></td>'
																											.'<td>'.$indikatorkegiatan->twtarget2.'</td>'
																											.'<td><b>'.$indikatorkegiatan->tw2.'</b></td>'
																											.'<td>'.$indikatorkegiatan->twtarget3.'</td>'
																											.'<td><b>'.$indikatorkegiatan->tw3.'</b></td>'
																											.'<td>'.$indikatorkegiatan->twtarget4.'</td>'
																											.'<td><b>'.$indikatorkegiatan->tw4.'</b></td>'
																										.'</tr>'
																									)
																					@else
																					
																						@php($totalindikatorkegiatanbawah += 1)
																						
																						@php(
																							$isik5 .= '<tr >'
																												.'<td >'.$indikatorkegiatan->tik_indikator_kegiatan.' </td>'
																												.'<td>'.$indikatorkegiatan->tik_satuan.'</td>'
																												.'<td>'.$indikatorkegiatan->tik_target.'</td>'
																												.'<td>'.$indikatorkegiatan->twtarget1.'</td>'
																												.'<td><b>'.$indikatorkegiatan->tw1.'</b></td>'
																												.'<td>'.$indikatorkegiatan->twtarget2.'</td>'
																												.'<td><b>'.$indikatorkegiatan->tw2.'</b></td>'
																												.'<td>'.$indikatorkegiatan->twtarget3.'</td>'
																												.'<td><b>'.$indikatorkegiatan->tw3.'</b></td>'
																												.'<td>'.$indikatorkegiatan->twtarget4.'</td>'
																												.'<td><b>'.$indikatorkegiatan->tw4.'</b></td>'
																											.'</tr>'
																						)
																					@endif
																				@endforeach

																				@if($as == 0)

																					@php($allataskegiatan+=$totalindikatorkegiatan)
																					@php($totalkegiatan += array_count_values(array_column($indikatorprogram->kegiatan, 'idindiprogram'))[$indikatorprogram->tip_id])
																					@php($isik2 = '<td rowspan="'.$totalindikatorkegiatan.'">'.$kga->tkp_kegiatan_program.'  </td>'
																										.$isik5
																										
																									
																									)
																				@else
																					@php($totalkegiatan += 1)
																					
																					
																					@php($allbawahkegiatan += $totalindikatorkegiatanbawah)
																					
																					@php(
																						$isik2 .= '<tr ><td rowspan="'.$totalindikatorkegiatanbawah.'"> '.$kga->tkp_kegiatan_program.'</td>'
																											.$isik5
																											
																											
																					)
																				@endif

																			@else
																				@if($as == 0)
																					@php($allataskegiatan+=1)
																					@php($totalkegiatan += array_count_values(array_column($indikatorprogram->kegiatan, 'idindiprogram'))[$indikatorprogram->tip_id])
																					@php($isik2 = '<td>'.$kga->tkp_kegiatan_program.' </td>'
																										.'<td></td>'
																											.'<td></td>'
																											.'<td></td>'
																											.'<td></td>'
																											.'<td></td>'
																											.'<td></td>'
																											.'<td></td>'
																											.'<td></td>'
																											.'<td></td>'
																											.'<td></td>'
																											.'<td></td>'
																										.'</tr>'
																									)
																				@else
																				
																					@php($allbawahkegiatan = $allbawahkegiatan + 1)
																					@php(
																						$isik2 .= '<tr ><td > '.$kga->tkp_kegiatan_program.'</td>'
																									.'<td></td>'
																											.'<td></td>'
																											.'<td></td>'
																											.'<td></td>'
																											.'<td></td>'
																											.'<td></td>'
																											.'<td></td>'
																											.'<td></td>'
																											.'<td></td>'
																											.'<td></td>'
																											.'<td></td>'
																										.'</tr>'
																									
																					)
																				@endif
																			@endif
																		@endforeach

																		<!-- KEGIATAN ISI -->
																	
																		@if($m == 0)
																			@php($alltotalkegiatan = $allataskegiatan + $allbawahkegiatan)
																			@php($isik1 = '<td rowspan="'.$alltotalkegiatan.'"> '.$indikatorprogram->tip_indikator_program.'  </td>'
																							.$isik2
																				)
																		@else
																			@php($alltotalkegiatanbawah = $allataskegiatan + $allbawahkegiatan)
																			@php($testatas += $alltotalkegiatanbawah)
																			@php(
																				$isik1 .= '<tr>'
																							.'<td rowspan="'.$alltotalkegiatanbawah.'">'.$indikatorprogram->tip_indikator_program.'   </td>'
																							.$isik2
																			)
																		@endif

																	@else


																		@if($m == 0)
																			
																		@php($alltotalkegiatan = $alltotalkegiatan + 1)
																			@php($isik1 = '<td>'.$indikatorprogram->tip_indikator_program.'</td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																						.'</tr>')
																		@else
																		
																		@php($testatas += 1)
																			@php(
																				$isik1 .= '<tr ><td >'.$indikatorprogram->tip_indikator_program.' </td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																						.'</tr>'
																			)
																		@endif
																	
																	@endif
																@endforeach
																@php($akhirtotal =  $alltotalkegiatan + $testatas)
															<!-- Program ISI -->
																@php($htmlk .= '<tr>'
																					.'<td rowspan="'.$akhirtotal.'">'.$ik.'</td>'
																					.'<td rowspan="'.$akhirtotal.'">'.$keg->tps_program_sasaran.' </td>'
																					.$isik1
																					)
															@else
															
															<!-- Program Kosong -->
																@php($htmlk .= '<tr>'
																				.'<td>'.$ik.'</td>'
																				.'<td>'.$keg->tps_program_sasaran.'</td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																			.'</tr>')
															@endif
															@php($ik++)    
														@endforeach

														@php(print_r($htmlk))
												</tbody>
											</table>
										</div>
									</div>
									@endif
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('onpage-js')
@include('backend.layouts.message')
<script> 	
	function cariData(id){
		var url = '{{ route("pemerintah.pengukuran",["id"=> $id, "idsasaran"=> ":idsasaran" ]) }}';
		url = url.replace(':idsasaran', id);
		window.location.href =  url;
	}
</script> 	
@endsection