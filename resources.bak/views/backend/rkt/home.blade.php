@extends('backend.layouts.app')

@section('title', 'IKU')

@section('content')

<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__body">

                    <div class="row">
                        <div class="col-md-11">
                            <h3 class="kt-portlet__head-title">
                                List Sasaran
                            </h3>
                        </div>
                        <div class="col-md-1" >
                            <a href="javascript:void(0)" onclick="printAwal({{$id}})" class="btn btn-primary waves-effect waves-light btn-rounded">
                                <i class="fas fa-print"></i> Print
                            </a>
                        </div>
                    </div>
                <ul style="margin-left: 20px">
                        <h4 style="margin-left:-30px; color: #336600; margin-top:10px">Sasaran Strategis </h4>
                    </ul>
                    <ol  style="margin-left: 20px">
                        @php($sa = 1)
                        @foreach($listsasaran as $newsasaran)


                        <li style="display:block">
                            {{$sa}}. {{$newsasaran->tst_sasaran_tujuan}}

                                <ul style="margin-left: 20px">
                                    <h4 style="margin-left:-30px; color: #336600; margin-top:10px">Indikator Sasaran </h4>
                                </ul>
                                    <ol>
                                    <li style="display:block">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th><center>NO</center></th>
                                                    <th><center>Indikator Sasaran Strategis</center></th>
                                                    <th><center>Satuan</center></th>
                                                    <th><center>Target</center></th>
                                                    <th><center>Action</center></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(isset($newsasaran->indikatorsasaran))
                                            @php($sas = 1)
                                            @foreach($newsasaran->indikatorsasaran as $indisasaran)

                                                <tr>
                                                    <td>{{$sas}}</td>
                                                    <td>{{$indisasaran->tis_indikator_sasaran}}</td>
                                                    <td>{{$indisasaran->tis_satuan}}</td>
                                                    <td>{{$indisasaran->tis_target}}</td>
                                                    <td>
                                                        <center>
                                                            <div class="btn-group">
                                                                <a href="javascript:void(0)"  onClick="tambahFormula({{$indisasaran->tis_id}})" type="button" class="btn btn-success btn-icon"><i class="fa fa-search"></i></a>
                                                            </div>
                                                        </center>
                                                    </td>
                                                </tr>
                                                @php($sas++)
                                                @endforeach


                                            @endif
                                            </tbody>
                                        </table>
                                    </li>
                                </ol>


                        </li>

                        @php($sa++)
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="panel-modal" class="modal fade" id="kt_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

@endsection

@section('onpage-js')
@include('backend.layouts.message')
<script>
    function tambahFormula(id){
        var url = '{{ route("backend.rkt.tambahtarget",["id"=>":id","iddinas"=>":iddinas"]) }}';
        url = url.replace(':id', id);
        url = url.replace(':iddinas', {{$id}});
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('<i class="fa fa-pencil"></i> Isi Target');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }

    function printAwal(id){
        var url = '{{ route("backend.rkt.printpk",["id"=>":id"]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Print Perjanjian Kinerja');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
</script>
@endsection

