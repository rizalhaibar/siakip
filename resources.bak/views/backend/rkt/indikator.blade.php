@extends('backend.layouts.app')

@section('title', 'IKU')

@section('content')
                
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                List Indikator Sasaran 
                            </h3>
                        </div>
                </div>
                <div class="kt-portlet__body">
                    <table id="listkota" class="table ">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Indikator Sasaran</th>
                                <th>Satuan</th>
                                <th>Target</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php($sl=1)
                            @foreach($listindikator as $indikator)
                            <tr>
                                <td>{{$sl}}</td>
                                <td>{{$indikator->tis_indikator_sasaran}}</td>
                                <td>{{$indikator->tis_satuan}}</td>
                                <td>{{$indikator->formula}}</td>
                                <td>{{$indikator->tis_target}}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="javascript:void(0)"  onClick="tambahFormula({{$indikator->tis_id}})" type="button" class="btn btn-success btn-icon"><i class="fa fa-search"></i></a>
                                    </div>
                                </td>
                            </tr>
                            
                            @php($sl++)
                            @endforeach               
                        </tbody>
                    </table>
                </div>              
            </div>              
        </div>              
    </div>              
</div>         
	
    <div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content p-0 b-0">
                <div class="panel panel-color panel-primary panel-filled">
                    <div class="panel-heading">
                        <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 class="panel-title"></h3>
                    </div>
                    <div class="panel-body">
                    
                        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                            <div class="row">
                                <div class="col-md-12">
                                     <p></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection

@section('customjs')
<script>
    $( document ).ready(function() {
		
    });
      
    
    function tambahFormula(id){
        var url = '{{ route("backend.rkt.tambahtarget",["id"=>":id"]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .panel-body').load(url);
        $('#panel-modal  .panel-title').html('<i class="fa fa-user"></i> Tambah/Edit Target');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
</script>
@endsection


