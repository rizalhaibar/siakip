<form class="kt-form" method="post" action="{{route('backend.keselarasan.print')}}">
    @csrf
    <div class="form-group">
        <label class="col-md-2 control-label">Pihak Pertama(Kepala Dinaa)*</label>
        <div class="col-md-12">
            <input type="hidden" class="form-control" id="id" name="id" value="{{$id}}">
            <input type="text" class="form-control" id="pihak_pertama" name="pihak_pertama" placeholder="Kepala Dinas..." required>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-2 control-label">Pihak Kedua(WALIKOTA)*</label>
        <div class="col-md-12">
            <input type="text" class="form-control" id="pihak_kedua" name="pihak_kedua" placeholder="WALIKOTA..." required>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-offset-10 col-md-8">
            <button type="submit" class="btn btn-primary waves-effect waves-light btn-rounded">
                Simpan
            </button>
        </div>
    </div>
</form>
