<!DOCTYPE html>

<html lang="en">

<!-- begin::Head -->
<head>
    <base href="../">
    <meta charset="utf-8" />
    <!--title>{{ env('APP_NAME') }} | Dashboard</title-->
    <title>{{ config('app.name', ' | SIAKIP CIMAHI') }}</title>
    <meta name="description" content="Latest updates and statistic">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta content="text/html; charset=UTF-8; X-Content-Type-Options=nosniff" http-equiv="Content-Type" />
    <!--begin::Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">

    <!--end::Fonts -->

    <!--begin::Page Vendors Styles(used by this page) -->

    <!--end::Page Vendors Styles -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{ asset('themes/metronic/assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('themes/metronic/assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('sweetalert/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('icheck/custom.css') }}" rel="stylesheet" type="text/css" />


    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="{{ asset('themes/metronic/assets/plugins/global/plugins.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('themes/metronic/assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('themes/metronic/assets/js/pages/crud/forms/widgets/bootstrap-timepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('themes/metronic/assets/js/pages/crud/forms/widgets/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('sweetalert/sweetalert2.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('icheck/icheck.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('themes/metronic/assets/plugins/custom/flot/flot.bundle.js') }}" type="text/javascript"></script>
    <!--end::Global Theme Bundle -->

    <!--begin::Page Vendors(used by this page) -->

    <!--end::Page Vendors -->

    <!--begin::Page Scripts(used by this page) -->
    <script src="{{ asset('themes/metronic/assets/js/pages/dashboard.js') }}" type="text/javascript"></script>

</head>

<body class="kt-page--loading-enabled kt-page--loading kt-page--fluid kt-header--fixed kt-header--minimize-topbar kt-header-mobile--fixed kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-subheader--enabled kt-subheader--transparent kt-page--loading">

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
			@guest
            @else

            @endguest






			<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed " data-ktheader-minimize="on">
                <div class="kt-header__top">
                    <div class="kt-container ">

                        <!-- begin:: Brand -->
                        <div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
                            <div class="kt-header__brand-logo">
                                <a href="{{route('pemerintah.index')}}">

									<img src="{{ asset('images/logosiakip.png') }}" style="width: 200px; height: 100%; margin-top: -10px" />
                                </a>
                            </div>
                            <div class="kt-header__brand-nav">
                            </div>
                        </div>

                        <!-- end:: Brand -->

                        <!-- begin:: Header Topbar -->
                        <div class="kt-header__topbar">



                            <!--end: Quick actions -->

                            <!--begin: User bar -->
                            <div class="kt-header__topbar-item kt-header__topbar-item--user">
                                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,10px">
                                    <span class="kt-hidden kt-header__topbar-welcome">Hi,</span>
                                    <span class="kt-hidden kt-header__topbar-username">{{Session::get('data')->user->name}}</span>
                                    <img class="kt-hidden-" alt="Pic" src="{{ asset('themes/custom/image/default.png') }}" />
                                    <span class="kt-header__topbar-icon kt-header__topbar-icon--brand kt-hidden"><b>S</b></span>
                                </div>
                                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">

                                    <!--begin: Head -->
                                    <div class="kt-user-card kt-user-card--skin-light kt-notification-item-padding-x">
                                        <div class="kt-user-card__avatar">
                                            <img class="kt-hidden-" alt="Pic" src="{{ asset('themes/custom/image/default.png') }}" />

                                            <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                                            <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden">S</span>
                                        </div>
                                        <div class="kt-user-card__name">

                                            Hi, {{Session::get('data')->user->name}}
                                        </div>
                                        <div class="kt-user-card__badge">
                                            {{--                                            <span class="btn btn-label-primary btn-sm btn-bold btn-font-md">23 messages</span>--}}
                                        </div>
                                    </div>

                                    <!--end: Head -->

                                    <!--begin: Navigation -->
                                    <div class="kt-notification">
                                        <a href="{{ route('backend.user.editprofile', Crypt::encrypt(Session::get('data')->user->id) ) }}" class="kt-notification__item">
                                            <div class="kt-notification__item-icon">
                                                <i class="flaticon2-calendar-3 kt-font-success"></i>
                                            </div>
                                            <div class="kt-notification__item-details">
                                                <div class="kt-notification__item-title kt-font-bold">
                                                    My Profile
                                                </div>
                                            </div>
                                        </a>
                                        <div class="kt-notification__custom kt-space-between">
                                            <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="btn btn-label btn-label-brand btn-sm btn-bold">Sign Out</a>

											<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
												@csrf
											</form>

                                        </div>
                                    </div>

                                    <!--end: Navigation -->
                                </div>
                            </div>

                            <!--end: User bar -->
                        </div>

                        <!-- end:: Header Topbar -->
                    </div>
                </div>
                <div class="kt-header__bottom">
                    <div class="kt-container ">

                        <!-- begin: Header Menu -->
                        <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
                        <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
                            <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">

							 <ul class="kt-menu__nav ">





										@php
											$menu_role = Session::get('data')->menus;
										@endphp
										@foreach( $menu_role as $menus)
										@if($menus->url == '#')

											<li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel"  data-ktmenu-submenu-toggle="click" aria-haspopup="true">
												<a  href="javascript:;" class="kt-menu__link kt-menu__toggle">
													 <span class="kt-menu__link-text"><i class="{{ $menus->icon }}"></i>&nbsp; {{ $menus->label }}</span>
												</a>
												<div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--right">
													<ul class="kt-menu__subnav">
															@foreach($menu_role as $menus2)

																@if($menus2->parent_id == $menus->id)

																<li class="kt-menu__item "  data-ktmenu-submenu-toggle="hover" aria-haspopup="true">
																	<a  href="{{ route($menus2->url) }}" class="kt-menu__link ">
																		<span class="kt-menu__link-text"> <i class="{{ $menus2->icon }}"></i>&nbsp; {{ $menus2->label }}</span>
																	</a>
																</li>
																@endif

															@endforeach
													</ul>
											</li>

										@else
											@if($menus->parent_id == 0)

											<li class="kt-menu__item kt-menu__item--rel {{ Route::is('backend.user.home') ? 'kt-menu__item--active' : '' }}" aria-haspopup="true">
												<a href="{{ route($menus->url) }}" class="kt-menu__link">
													<span class="kt-menu__link-text"> <i class="{{ $menus->icon }}"></i>&nbsp;  {{ $menus->label }} </span>
												</a>
											</li>

											@endif
										@endif


									@endforeach
									<li class="kt-menu__item kt-menu__item--rel " aria-haspopup="true">
									 <select class="form-control right" id="tahunini" name="tahunini" onChange="tahunnya(this.value)">

										<option disabled selected>Pilih Tahun..</option>
										 <?php
											$already_selected_value = date("Y");
											$earliest_year = 2013;

											foreach (range(date('Y'), $earliest_year) as $x) {
												if($_COOKIE['tahun'] == $x){
													echo '<option value="'.$x.'" selected>'.$x.'</option>';
												}else{
													echo '<option value="'.$x.'">'.$x.'</option>';
												}

											}


											?>
										</select>
									</li>
								</ul>

                            </div>
                        </div>

                        <!-- end: Header Menu -->
                    </div>
                </div>
            </div>



            <!-- end:: Header -->
            <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
                <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                    <!-- begin:: Subheader -->
                    <div class="kt-subheader kt-grid__item kt-margin-b-10" id="kt_subheader">
                        <div class="kt-container ">
                            <div class="kt-subheader__main">
                                <h3 class="kt-subheader__title">
                                    {{ isset($breadcrumb['parent']) ? $breadcrumb['parent'] : 'Home' }} </h3>
                                <span class="kt-subheader__separator kt-hidden"></span>
                                <div class="kt-subheader__breadcrumbs">
                                    <a href="{{ url('/') }}" class="kt-subheader__breadcrumbs-home kt-margin-l-10"><i class="flaticon2-shelter"></i></a>
                                    <span class="kt-subheader__breadcrumbs-separator kt-margin-l-10"></span>
                                    <a href="javascript:void(0)" class="kt-subheader__breadcrumbs-link kt-margin-l-10">
                                        {{ isset($breadcrumb['child']) ? $breadcrumb['child'] : '' }} </a>

                                    <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- end:: Subheader -->

                    <!-- begin:: Content -->
                    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                        @yield('content')
                    </div>

                    <!-- end:: Content -->
                </div>
            </div>





            <!-- begin:: Footer -->
            <div class="kt-footer  kt-footer--extended  kt-grid__item" id="kt_footer">
                <div class="kt-footer__bottom">
                    <div class="kt-container ">
                        <div class="kt-footer__wrapper">
                            <div class="kt-footer__copyright">
                                &copy; {{ date('Y').' '.env('APP_NAME') }}
                            </div>
                            <div class="kt-footer__menu">
                               <img src="{{ asset('images/logosiakip.png') }}" style="width: 130px; height: 100%; ">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- end:: Footer -->
        </div>
    </div>
</div>

<!-- end:: Page -->

<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="fa fa-arrow-up"></i>
</div>

<!-- end::Scrolltop -->

<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#3d94fb",
                "light": "#ffffff",
                "dark": "#282a3c",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#3d94fb",
                "warning": "#ffb822",
                "danger": "#fd27eb"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };
</script>
<script>

var tahun  = localStorage.getItem("tahun");
    $( document ).ready(function() {
		 if(!tahun){
            var d = new Date();
            var n = d.getFullYear();
             localStorage.setItem("tahun", n);
			  setCookie("tahun", n, 30000000);
        }
		var user=getCookie("tahun");
		if (user == "") {

			var d = new Date();
            var n = d.getFullYear();
		     setCookie("tahun", n, 30);

		}
        $('#initahun').html('<span id="initahun">Data Tahun : '+tahun+'</span>');
    });

    function tahunnya(id){
        localStorage.setItem("tahun", id);
         setCookie("tahun", id, 30000000);
         location.reload();

    }
	function getCookie(cname) {
		var name = cname + "=";
		var decodedCookie = decodeURIComponent(document.cookie);
		var ca = decodedCookie.split(';');
		for(var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return "";
	}
	function setCookie(cname,cvalue,exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = "expires=" + d.toGMTString();
		document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	}
</script>
<!-- end::Global Config -->

<script>
$(document).ready(function () {
	$("select").select2({
		dropdownAutoWidth : true,
		width: '100%'
	});
});
</script>
@yield('onpage-js')

<!--end::Page Scripts -->
</body>

<!-- end::Body -->
</html>
