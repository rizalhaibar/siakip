@extends('backend.layouts.app')

@section('title', 'IKU')

@section('content')

<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <div class="row">
                        <div class="col-md-11">
                            <h3 class="kt-portlet__head-title">
                                Dinas {{$dinas->td_dinas}}
                            </h3>
                        </div>
                        <div class="col-md-1" >
                            <a href="javascript:void(0)" onclick="printAwal({{$id}})" class="btn btn-primary waves-effect waves-light btn-rounded">
                                <i class="fas fa-print"></i> Print
                            </a>
                        </div>
                    </div>


                <div class="form-group">
                    <label for="exampleSelect1">Pilih Sasaran Strategis : </label>
                    <select class="form-control"  onChange="cariData(this.value)">
                        <option selected disabled>Pilih Sasaran Strategis ....</option>
                        @foreach($listsasaran as $sasaran)
                        <option value="{{$sasaran->tst_id}}" @if($idsasaran != 0 ) {{$sasaran->tst_id == $idsasaran ? 'selected' : ''}} @endif >{{$sasaran->tst_sasaran_tujuan}}</option>
                        @endforeach
                    </select>
                </div>
            @if($idsasaran != 0)
                <ul class="nav nav-tabs  nav-tabs-line nav-tabs-line-2x nav-tabs-line-success" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#sasaran" role="tab">Sasaran Strategis</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#program" role="tab">Sasaran Program</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#kegiatan" role="tab">Sasaran Kegiatan</a>
                    </li>
                </ul>


                <div class="tab-content">
                    <div class="tab-pane active" id="sasaran" role="tabpanel">
                        <ul style="margin-left: 20px">
                            <h4 style="margin-left:-30px; color: #336600; margin-top:10px">Indikator Sasaran Strategis</h4>
                        </ul>

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th><center>NO</center></th>
                                        <th><center>Indikator Sasaran Strategis</center></th>
                                        <th><center>Satuan</center></th>
                                        <th><center>Target</center></th>
                                        <th><center>Penanggung Jawab</center></th>
                                        <th><center>Action</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php($sas = 1)
                                @foreach($indikatorsasaran as $indisasaran)

                                    <tr>
                                        <td>{{$sas}}</td>
                                        <td>{{$indisasaran->tis_indikator_sasaran}}</td>
                                        <td>{{$indisasaran->tis_satuan}}</td>
                                        <td>{{$indisasaran->tis_target}}</td>
                                        <td>{{$indisasaran->te_isi ? $indisasaran->te_isi.' ('.$indisasaran->te_jabatan.')' : ''}}</td>
                                        <td>
                                            <center>
                                                <div class="btn-group">
                                                    <a href="javascript:void(0)"  onClick="tambahFormula({{$indisasaran->tis_id}})" type="button" class="btn btn-success btn-icon"><i class="fa fa-search"></i></a>
                                                </div>
                                            </center>
                                        </td>
                                    </tr>
                                    @php($sas++)
                                    @endforeach
                                </tbody>
                            </table>

                    </div>
                    <div class="tab-pane " id="program" role="tabpanel">

                        <ul style="margin-left: 20px">
                            <h4 style="margin-left:-30px; color: #336600; margin-top:10px"> Sasaran Program </h4>
                        </ul>
                        <table class="table table-bordered">
									<thead>
										<tr>
											<th>No</th>
											<th>Indikator Sasaran Strategis</th>
											<th>Satuan</th>
											<th>Target</th>
											<th>Sasaran Program</th>
											<th>Indikator Sasaran Program</th>
											<th>Satuan</th>
											<th>Target</th>
											<th>Penanggung Jawab</th>
											<th>Action</th>
										</tr>
									</thead>
									<?php
                                            $i=1;

                                            $html = '';
                                            $isi1 = '';
                                            $isi2 = '';
                                            $isi3 = '';
                                            $isi4 = '';
                                        ?>
									<tbody>
                                      @foreach($program as $new)
                                      @php($totalprogram=0)

                                            @if(isset($new->program))

                                            @php($alltotalindikatorprogram=0)
                                            @php($allatas=0)
                                            @php($allbawah=0)
                                            @foreach($new->program as $k=>$prg)
                                                @php($totalindikatorprogram=0)
                                                @php($totalindikatorprogrambawah=0)
                                                @php($x=0)
                                                @if(isset($prg->indikatorprogram))

                                                    @php($x=sizeof($prg->indikatorprogram))
                                                    @php($isi3 = '')
                                                    @if ($x == 1)
                                                        @php($isi3 = '<tr><td><a href="javascript:void(0)" onClick="tambahindikatorprogram('.$prg->tps_id.')"><i class="fa fa-plus"></i> Tambah Indikator Program</a></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                        .'</tr>')
                                                    @else
                                                        @php($isi4 = '<tr><td><a href="javascript:void(0)" onClick="tambahindikatorprogram('.$prg->tps_id.')"><i class="fa fa-plus"></i> Tambah Indikator Program</a></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                        .'</tr>')

                                                    @endif




                                                    @foreach($prg->indikatorprogram as $s=>$indprg)

                                                        @if($s == 0)
                                                            @php($totalindikatorprogram += array_count_values(array_column($prg->indikatorprogram, 'idprogram'))[$prg->tps_id])

                                                            @php($totalindikatorprogrambawah = 1)
                                                            @php($isi2 = '<td> '.$indprg->tip_indikator_program.' <br /><span> <a href="javascript:void(0)" class="btn btn-primary btn-sm btn-icon" onClick="editIndikatorProgram('.$indprg->tip_id.','.$prg->tps_id.')"><i class="fa fa-edit" ></i></a> <a href="javascript:void(0)" class="btn btn-danger btn-sm btn-icon"  onClick="deteleIndikatorProgram('.$indprg->tip_id.')"><i class="fa fa-trash"></i></a></span></td>'
                                                                            .'<td>'.$indprg->tip_satuan.'</td>'
                                                                            .'<td>'.$indprg->tip_target.'</td>'
                                                                            .'<td>'.$indprg->te_isi.' ('.$indprg->te_jabatan.')</td>'
                                                                            .'<td><a href="javascript:void(0)" class="btn btn-primary btn-sm btn-icon" onClick="tambahFormulaProgram('.$indprg->tip_id.')"><i class="fa fa-search"></i></a></td>'
                                                                        .'</tr>'
                                                                        .$isi3
                                                                        )


                                                        @else

                                                            @php($totalindikatorprogrambawah += 1)
                                                            @php($dalemisi2 = '')
                                                            @if($x == $totalindikatorprogrambawah)
                                                                @php($dalemisi2 = '<tr><td><a href="javascript:void(0)" onClick="tambahindikatorprogram('.$prg->tps_id.')"><i class="fa fa-plus"></i> Tambah Indikator Program</a></td>'
                                                                                .'<td></td>'
                                                                                .'<td></td>'
                                                                                .'<td></td>'
                                                                                .'<td></td>'
                                                                            .'</tr>')
                                                            @endif
                                                            @php(
                                                                $isi2 .= '<tr ><td > '.$indprg->tip_indikator_program.' <br /><span> <a href="javascript:void(0)" class="btn btn-primary btn-sm btn-icon" onClick="editIndikatorProgram('.$indprg->tip_id.','.$prg->tps_id.')"><i class="fa fa-edit" ></i></a> <a href="javascript:void(0)" class="btn btn-danger btn-sm btn-icon"  onClick="deteleIndikatorProgram('.$indprg->tip_id.')"><i class="fa fa-trash"></i></a></span></td>'
                                                                            .'<td>'.$indprg->tip_satuan.'</td>'
                                                                            .'<td>'.$indprg->tip_target.'</td>'
                                                                            .'<td>'.$indprg->te_isi.' ('.$indprg->te_jabatan.')</td>'
                                                                            .'<td><a href="javascript:void(0)" class="btn btn-primary btn-sm btn-icon" onClick="tambahFormulaProgram('.$indprg->tip_id.')"><i class="fa fa-search"></i></a></td>'
                                                                        .'</tr>'
                                                                        .$dalemisi2
                                                            )
                                                        @endif

                                                    @endforeach




                                                    @if($k == 0)


                                                        @php($totalprogram += array_count_values(array_column($new->program, 'idindisasaran'))[$new->tis_id])
                                                        @php($totalindikatorprogram = $totalindikatorprogram+1)

                                                        @php($allatas += $totalindikatorprogram)

                                                        @php($isi1 = '<td rowspan="'.$totalindikatorprogram.'">'.$prg->tps_program_sasaran.' <br /><span> <a href="javascript:void(0)"  class="btn btn-primary btn-sm btn-icon" onClick="editProgram('.$prg->tps_id.')"><i class="fa fa-edit"></i></a> <a href="javascript:void(0)"  class="btn btn-primary btn-sm btn-icon" onClick="deteleProgram('.$prg->tps_id.')"><i class="fa fa-trash"  id="danger-alert"></i></a> </span></td>'
                                                                        .$isi2)
                                                    @else
                                                        @php($totalindikatorprogrambawah = $totalindikatorprogrambawah+1)
                                                        @php($allbawah += $totalindikatorprogrambawah)
                                                        @php(
                                                            $isi1 .= '<tr ><td rowspan="'.$totalindikatorprogrambawah.'">'.$prg->tps_program_sasaran.'<br /><span> <a href="javascript:void(0)"  class="btn btn-primary btn-sm btn-icon" onClick="editProgram('.$prg->tps_id.')"><i class="fa fa-edit"></i></a> <a href="javascript:void(0)"  class="btn btn-primary btn-sm btn-icon" onClick="deteleProgram('.$prg->tps_id.')"><i class="fa fa-trash"  id="danger-alert"></i></a> </span></td>'
                                                                        .$isi2
                                                        )
                                                    @endif



                                                @else


                                                    @php($alltotalindikatorprogram = $alltotalindikatorprogram+1)
                                                    @if($k == 0)
                                                        @php($allatas = $totalindikatorprogram+1)
                                                        @php($isi1 = '<td>'.$prg->tps_program_sasaran.' <br /><span> <a href="javascript:void(0)"  class="btn btn-primary btn-sm btn-icon" onClick="editProgram('.$prg->tps_id.')"><i class="fa fa-edit"></i></a> <a href="javascript:void(0)"  class="btn btn-primary btn-sm btn-icon" onClick="deteleProgram('.$prg->tps_id.')"><i class="fa fa-trash"  id="danger-alert"></i></a> </span></td>'
                                                                        .'<td><a href="javascript:void(0)" onClick="tambahindikatorprogram('.$prg->tps_id.')"><i class="fa fa-plus"></i> Tambah Indikator Program</a></td>'
                                                                        .'<td></td>'
                                                                        .'<td></td>'
                                                                        .'<td></td>'
                                                                        .'<td></td>'
                                                                    .'</tr>')
                                                    @else

                                                        @php($allbawah = $allbawah + 1)
                                                        @php(
                                                            $isi1 .= '<tr ><td >'.$prg->tps_program_sasaran.' <br /><span> <a href="javascript:void(0)"  class="btn btn-primary btn-sm btn-icon" onClick="editProgram('.$prg->tps_id.')"><i class="fa fa-edit"></i></a> <a href="javascript:void(0)"  class="btn btn-primary btn-sm btn-icon" onClick="deteleProgram('.$prg->tps_id.')"><i class="fa fa-trash"  id="danger-alert"></i></a> </span></td>'
                                                                        .'<td><a href="javascript:void(0)" onClick="tambahindikatorprogram('.$prg->tps_id.')"><i class="fa fa-plus"></i> Tambah Indikator Program</a></td>'
                                                                        .'<td></td>'
                                                                        .'<td></td>'
                                                                        .'<td></td>'
                                                                        .'<td></td>'
                                                                    .'</tr>'
                                                        )
                                                    @endif
                                                @endif
                                            @endforeach



                                            @php($totalkeseluruhan = $allatas+$allbawah)
                                            @php($totalkeseluruhan = $totalkeseluruhan+1)
                                            @php($html .= '<tr>'
                                                            .'<td rowspan="'.$totalkeseluruhan.'">'.$i.'</td>'
                                                            .'<td rowspan="'.$totalkeseluruhan.'">'.$new->tis_indikator_sasaran.' </td>'
                                                            .'<td rowspan="'.$totalkeseluruhan.'">'.$new->tis_satuan.'</td>'
                                                            .'<td rowspan="'.$totalkeseluruhan.'">'.$new->tis_target.'</td>'
                                                            .$isi1

                                                            .'<tr>'
                                                                .'<td ><a href="javascript:void(0)" onClick="tambahprogram('.$new->tis_id.')"><i class="fa fa-plus"></i> Tambah Program</a></td>'
                                                                .'<td></td>'
                                                                .'<td></td>'
                                                                .'<td></td>'
                                                                .'<td></td>'
                                                                .'<td></td>'
                                                            .'</tr>')
                                            @else

                                                @php($html .= '<tr>'
                                                            .'<td>'.$i.'</td>'
                                                            .'<td>'.$new->tis_indikator_sasaran.'</td>'
                                                            .'<td>'.$new->tis_satuan.'</td>'
                                                            .'<td>'.$new->tis_target.'</td>'
                                                            .'<td><a href="javascript:void(0)" onClick="tambahprogram('.$new->tis_id.')"><i class="fa fa-plus"></i> Tambah Program</a></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                        .'</tr>')
                                            @endif
                                            @php($i++)
                                      @endforeach

                                      @php(print_r($html))
									</tbody>
                        </table>

                    </div>
                    <div class="tab-pane  " id="kegiatan" role="tabpanel">

                        <ul style="margin-left: 20px">
                            <h4 style="margin-left:-30px; color: #336600; margin-top:10px">Sasaran Kegiatan </h4>
                        </ul>

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Sasaran Program</th>
                                    <th>Indikator Sasaran Program</th>
                                    <th>Sasaran Kegiatan</th>
                                    <th>Indikator Sasaran Kegiatan</th>
                                    <th>Satuan</th>
                                    <th>Target</th>
                                    <th>Anggaran</th>
                                    <th>Penanggung Jawab</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $ik=1;

                                    $htmlk = '';
                                    $isik1 = '';
                                    $isik2 = '';
                                    $isik3 = '';
                                    $isik4 = '';
                                    $isik5 = '';
                                    $isik5s = '';
                                    $isik6 = '';
                                    $isik7 = '';
                                    $akhirtotala = 0;
                                ?>

                                    @foreach($kegiatan as $keg)

                                        @php($totalprogramk=0)
                                        @php($totalatasprogram=0)
                                        @php($testatas=0)
                                        @php($alltotalkegiatan=0)
                                        @php($alltotalkegiatanbawah=0)

                                        @php($totalbawahprogram=0)
                                        @php($totalbawahprogram2=0)
                                        @if(isset($keg->indikatorprogram))
                                            @foreach($keg->indikatorprogram as $m=>$indikatorprogram)
                                                <!-- param -->

                                                @php($totalkegiatan=0)
                                                @php($totalkegiatanbawah=0)
                                                @php($totalindikatorkegiatank=0)
                                                @php($allataskegiatan=0)
                                                @php($allbawahkegiatan=0)

                                                @if(isset($indikatorprogram->kegiatan))
                                                    <!-- INDIKATOR KEGIATAN -->

                                                    @php($totalindikatorkegiatan=0)
                                                    @php($totalindikatorkegiatanbawah=0)
                                                    @php($xs=sizeof($indikatorprogram->kegiatan))
                                                    @php($isik3 = '')
                                                    @if ($xs == 1)

                                                       @php($isik3 = ' <td><a href="javascript:void(0)" onClick="tambahkegiatan('.$indikatorprogram->tip_id.')"><i class="fa fa-plus"></i>  Tambah Kegiatan</a></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                        .'</tr>')
                                                    @else

                                                        @php($isik4 = '<tr><td><a href="javascript:void(0)" onClick="tambahkegiatan('.$indikatorprogram->tip_id.')"><i class="fa fa-plus"></i>  Tambah Kegiatan</a></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                        .'</tr>')

                                                    @endif

                                                    @foreach($indikatorprogram->kegiatan as $as=>$kga)

                                                        @if(isset($kga->indikatorkegiatan))

                                                            @php($xm=sizeof($kga->indikatorkegiatan))
                                                            @php($isik6 = '')
                                                            @if ($xm == 1)
                                                               @php($isik6 = ' <td><a href="javascript:void(0)" onClick="tambahindikatorkegiatan('.$kga->tkp_id.')"><i class="fa fa-plus"></i> Tambah Indikator Kegiatan</a></td>'
                                                                                    .'<td></td>'
                                                                                    .'<td></td>'
                                                                                    .'<td></td>'
                                                                                    .'<td></td>'
                                                                                    .'<td></td>'
                                                                                .'</tr>')
                                                            @else
                                                                @php($isik7 = '<tr><td><a href="javascript:void(0)" onClick="tambahindikatorkegiatan('.$kga->tkp_id.')"><i class="fa fa-plus"></i> Tambah Indikator Kegiatan</a></td>'
                                                                                    .'<td></td>'
                                                                                    .'<td></td>'
                                                                                    .'<td></td>'
                                                                                    .'<td></td>'
                                                                                    .'<td></td>'
                                                                                .'</tr>')

                                                            @endif

                                                            @foreach($kga->indikatorkegiatan as $inkas=>$indikatorkegiatan)
                                                                @if($inkas == 0)
                                                                    @php($totalindikatorkegiatan += array_count_values(array_column($kga->indikatorkegiatan, 'idkegiatan'))[$kga->tkp_id])

                                                                @php($totalindikatorkegiatanbawah = 1)
                                                                    @php($isik5 = '<td >  '.$indikatorkegiatan->tik_indikator_kegiatan.'  <br /><span> <a href="javascript:void(0)" class="btn btn-primary btn-sm btn-icon" onClick="editIndikatorKegiatan('.$indikatorkegiatan->tik_id.','.$kga->tkp_id.')"><i class="fa fa-edit" ></i></a> <a href="javascript:void(0)" class="btn btn-danger btn-sm btn-icon"  onClick="deteleIndikatorKegiatan('.$indikatorkegiatan->tik_id.')"><i class="fa fa-trash"></i></a></span></td>'
                                                                                        .'<td>'.$indikatorkegiatan->tik_satuan.'</td>'
                                                                                        .'<td>'.$indikatorkegiatan->tik_target.'</td>'
                                                                                        .'<td>'.$indikatorkegiatan->ta_anggaran.'</td>'
                                                                                        .'<td>'.$indikatorkegiatan->te_jabatan.' ('.$indikatorkegiatan->te_isi.')</td>'
                                                                                        .'<td><a href="javascript:void(0)" class="btn btn-primary btn-sm btn-icon" onClick="tambahFormulaKegiatan('.$indikatorkegiatan->tik_id.')"><i class="fa fa-search"></i></a></td>'
                                                                                    .'</tr>'
                                                                                    .$isik6
                                                                                )
                                                                @else

                                                                    @php($totalindikatorkegiatanbawah += 1)

                                                                    @php($dalamisik6 = '')
                                                                    @if($xm == $totalindikatorkegiatanbawah)
                                                                        @php($dalamisik6 = ' <td><a href="javascript:void(0)" onClick="tambahindikatorkegiatan('.$kga->tkp_id.')"><i class="fa fa-plus"></i> Tambah Indikator Kegiatan</a></td>'
                                                                                        .'<td></td>'
                                                                                        .'<td></td>'
                                                                                        .'<td></td>'
                                                                                        .'<td></td>'
                                                                                        .'<td></td>'
                                                                                    .'</tr>')
                                                                    @endif
                                                                    @php(
                                                                        $isik5 .= '<tr >'
                                                                                            .'<td > '.$indikatorkegiatan->tik_indikator_kegiatan.' <br /><span> <a href="javascript:void(0)" class="btn btn-primary btn-sm btn-icon" onClick="editIndikatorKegiatan('.$indikatorkegiatan->tik_id.','.$kga->tkp_id.')"><i class="fa fa-edit" ></i></a> <a href="javascript:void(0)" class="btn btn-danger btn-sm btn-icon"  onClick="deteleIndikatorKegiatan('.$indikatorkegiatan->tik_id.')"><i class="fa fa-trash"></i></a></span></td>'
                                                                                            .'<td>'.$indikatorkegiatan->tik_satuan.'</td>'
                                                                                            .'<td>'.$indikatorkegiatan->tik_target.'</td>'
                                                                                            .'<td>'.$indikatorkegiatan->ta_anggaran.'</td>'
                                                                                            .'<td>'.$indikatorkegiatan->te_jabatan.' ('.$indikatorkegiatan->te_isi.')</td>'
                                                                                            .'<td><a href="javascript:void(0)" class="btn btn-primary btn-sm btn-icon" onClick="tambahFormulaKegiatan('.$indikatorkegiatan->tik_id.')"><i class="fa fa-search"></i></a></td>'
                                                                                        .'</tr>'
                                                                                        .$dalamisik6
                                                                    )
                                                                @endif
                                                            @endforeach

                                                            @if($as == 0)
                                                                 @php($totalindikatorkegiatan = $totalindikatorkegiatan + 1)
                                                                @php($allataskegiatan+=$totalindikatorkegiatan)
                                                                @php($totalkegiatan += array_count_values(array_column($indikatorprogram->kegiatan, 'idindiprogram'))[$indikatorprogram->tip_id])
                                                                @php($isik2 = '<td rowspan="'.$totalindikatorkegiatan.'"> '.$kga->tkp_kegiatan_program.'  <br /><span> <a href="javascript:void(0)" class="btn btn-primary btn-sm btn-icon" onClick="editKegiatan('.$kga->tkp_id.','.$indikatorprogram->tip_id.')"><i class="fa fa-edit" ></i></a> <a href="javascript:void(0)" class="btn btn-danger btn-sm btn-icon"  onClick="deteleKegiatan('.$kga->tkp_id.')"><i class="fa fa-trash"></i></a></span></td>'
                                                                                    .$isik5


                                                                                )
                                                            @else
                                                                @php($totalkegiatan += 1)
                                                                 @php($totalindikatorkegiatanbawah += 1)
                                                                @php($allbawahkegiatan += $totalindikatorkegiatanbawah)

                                                                @php(
                                                                    $isik2 .= '<tr ><td rowspan="'.$totalindikatorkegiatanbawah.'"> '.$kga->tkp_kegiatan_program.'<br /><span> <a href="javascript:void(0)" class="btn btn-primary btn-sm btn-icon" onClick="editKegiatan('.$kga->tkp_id.','.$indikatorprogram->tip_id.')"><i class="fa fa-edit" ></i></a> <a href="javascript:void(0)" class="btn btn-danger btn-sm btn-icon"  onClick="deteleKegiatan('.$kga->tkp_id.')"><i class="fa fa-trash"></i></a></span></td>'
                                                                                        .$isik5


                                                                )
                                                            @endif

                                                        @else
                                                            @if($as == 0)
                                                                @php($allataskegiatan+=1)
                                                                @php($totalkegiatan += array_count_values(array_column($indikatorprogram->kegiatan, 'idindiprogram'))[$indikatorprogram->tip_id])
                                                                @php($isik2 = '<td>'.$kga->tkp_kegiatan_program.'  <br /><span> <a href="javascript:void(0)" class="btn btn-primary btn-sm btn-icon" onClick="editKegiatan('.$kga->tkp_id.','.$indikatorprogram->tip_id.')"><i class="fa fa-edit" ></i></a> <a href="javascript:void(0)" class="btn btn-danger btn-sm btn-icon"  onClick="deteleKegiatan('.$kga->tkp_id.')"><i class="fa fa-trash"></i></a></span></td>'
                                                                                    .'<td><a href="javascript:void(0)" onClick="tambahindikatorkegiatan('.$kga->tkp_id.')"><i class="fa fa-plus"></i> Tambah Indikator Kegiatan</a></td>'
                                                                                        .'<td></td>'
                                                                                        .'<td></td>'
                                                                                        .'<td></td>'
                                                                                        .'<td></td>'
                                                                                        .'<td></td>'
                                                                                    .'</tr>'
                                                                                )
                                                            @else

                                                                @php($allbawahkegiatan = $allbawahkegiatan + 1)
                                                                @php(
                                                                    $isik2 .= '<tr ><td >'.$kga->tkp_kegiatan_program.'<br /><span> <a href="javascript:void(0)" class="btn btn-primary btn-sm btn-icon" onClick="editKegiatan('.$kga->tkp_id.','.$indikatorprogram->tip_id.')"><i class="fa fa-edit" ></i></a> <a href="javascript:void(0)" class="btn btn-danger btn-sm btn-icon"  onClick="deteleKegiatan('.$kga->tkp_id.')"><i class="fa fa-trash"></i></a></span></td>'
                                                                                .'<td><a href="javascript:void(0)" onClick="tambahindikatorkegiatan('.$kga->tkp_id.')"><i class="fa fa-plus"></i> Tambah Indikator Kegiatan</a></td>'
                                                                                        .'<td></td>'
                                                                                        .'<td></td>'
                                                                                        .'<td></td>'
                                                                                        .'<td></td>'
                                                                                        .'<td></td>'
                                                                                    .'</tr>'

                                                                )
                                                            @endif
                                                        @endif
                                                    @endforeach

                                                    <!-- KEGIATAN ISI -->

                                                    @if($m == 0)
                                                         @php($alltotalkegiatan = $allataskegiatan + $allbawahkegiatan)
                                                         @php($alltotalkegiatan = $alltotalkegiatan + 1)
                                                        @php($isik1 = '<td rowspan="'.$alltotalkegiatan.'">'.$indikatorprogram->tip_indikator_program.'  </td>'
                                                                        .$isik2
                                                                        .'<tr><td><a href="javascript:void(0)" onClick="tambahkegiatan('.$indikatorprogram->tip_id.')"><i class="fa fa-plus"></i>  Tambah Kegiatan</a></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                        .'</tr>'
                                                            )
                                                    @else
                                                        @php($alltotalkegiatanbawah = $allataskegiatan + $allbawahkegiatan)
                                                        @php($alltotalkegiatanbawah = $alltotalkegiatanbawah + 1)
                                                        @php($testatas += $alltotalkegiatanbawah)
                                                        @php(
                                                            $isik1 .= '<tr>'
                                                                        .'<td rowspan="'.$alltotalkegiatanbawah.'">'.$indikatorprogram->tip_indikator_program.'   </td>'
                                                                        .$isik2

                                                                        .'<tr><td><a href="javascript:void(0)" onClick="tambahkegiatan('.$indikatorprogram->tip_id.')"><i class="fa fa-plus"></i>  Tambah Kegiatan</a></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                        .'</tr>'
                                                        )
                                                    @endif

                                                @else


                                                    @if($m == 0)

                                                    @php($alltotalkegiatan = $alltotalkegiatan + 1)
                                                        @php($isik1 = '<td>'.$indikatorprogram->tip_indikator_program.'</td>'
                                                                        .'<td><a href="javascript:void(0)" onClick="tambahkegiatan('.$indikatorprogram->tip_id.')"><i class="fa fa-plus"></i>  Tambah Kegiatan</a></td>'
                                                                        .'<td></td>'
                                                                        .'<td></td>'
                                                                        .'<td></td>'
                                                                        .'<td></td>'
                                                                        .'<td></td>'
                                                                        .'<td></td>'
                                                                    .'</tr>')
                                                    @else

                                                    @php($testatas += 1)
                                                         @php(
                                                            $isik1 .= '<tr ><td >'.$indikatorprogram->tip_indikator_program.' </td>'
                                                                        .'<td><a href="javascript:void(0)" onClick="tambahkegiatan('.$indikatorprogram->tip_id.')"><i class="fa fa-plus"></i> Tambah Kegiatan</a></td>'
                                                                        .'<td></td>'
                                                                        .'<td></td>'
                                                                        .'<td></td>'
                                                                        .'<td></td>'
                                                                        .'<td></td>'
                                                                        .'<td></td>'
                                                                    .'</tr>'
                                                        )
                                                    @endif

                                                @endif
                                            @endforeach
                                            @php($akhirtotal =  $alltotalkegiatan + $testatas)
                                           <!-- Program ISI -->
                                            @php($htmlk .= '<tr>'
                                                                .'<td rowspan="'.$akhirtotal.'">'.$ik.'</td>'
                                                                .'<td rowspan="'.$akhirtotal.'">'.$keg->tps_program_sasaran.' </td>'
                                                                .$isik1
                                                                )
                                        @else

                                           <!-- Program Kosong -->
                                            @php($htmlk .= '<tr>'
                                                            .'<td>'.$ik.'</td>'
                                                            .'<td>'.$keg->tps_program_sasaran.'</td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                        .'</tr>')
                                        @endif
                                        @php($ik++)
                                    @endforeach

                                    @php(print_r($htmlk))
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
                </div>
            </div>
        </div>
    </div>
</div>


<div id="panel-modal" class="modal fade" id="kt_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

@endsection

@section('onpage-js')
@include('backend.layouts.message')
<script>


    $.ajaxSetup({
        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }
    });
    function tambahFormula(id){
        var url = '{{ route("backend.pengukurankinerja.tambahindikatorsasaranpic",["id"=>":id","idsasaran"=>":idsasaran","iddinas"=>":iddinas"]) }}';
        url = url.replace(':id', id);
        url = url.replace(':idsasaran', {{$idsasaran}});
        url = url.replace(':iddinas', {{$id}});
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Tambah Penanggung Jawab Indikator Sasaran');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    function tambahFormulaProgram(id){
        var url = '{{ route("backend.pengukurankinerja.tambahindikatorprogrampic",["id"=>":id","idsasaran"=>":idsasaran","iddinas"=>":iddinas"]) }}';
        url = url.replace(':id', id);
        url = url.replace(':idsasaran', {{$idsasaran}});
        url = url.replace(':iddinas', {{$id}});
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Tambah Penanggung Jawab Indikator Program');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    function tambahFormulaKegiatan(id){
        var url = '{{ route("backend.pengukurankinerja.tambahindikatorkegiatanpic",["id"=>":id","idsasaran"=>":idsasaran","iddinas"=>":iddinas"]) }}';
        url = url.replace(':id', id);
        url = url.replace(':idsasaran', {{$idsasaran}});
        url = url.replace(':iddinas', {{$id}});
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Tambah Penanggung Jawab Indikator Kegiatan');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }


    //indikator kegiatan
    function tambahindikatorkegiatan(id){
        var url = '{{ route("backend.pengukurankinerja.tambahindikatorkegiatan",["id"=>":id", "iddinas"=> $id, "idsasaran"=> $idsasaran]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Tambah Kegiatan');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }

    //kegiatan
    function tambahkegiatan(id){
        var url = '{{ route("backend.pengukurankinerja.tambahkegiatan",["id"=>":id", "iddinas"=> $id, "idsasaran"=> $idsasaran]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Tambah Kegiatan');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }

    function editKegiatan(id){
        var url = '{{ route("backend.pengukurankinerja.editkegiatan",["id"=>":id", "iddinas"=> $id, "idsasaran"=> $idsasaran]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Edit Kegiatan');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }

    function deteleKegiatan(id){
              swal({
				title: 'Apa anda yakin?',
				  text: "Anda akan menghapus kegiatan!",
				  type: 'error',
				  showCancelButton: true,
				  confirmButtonText: 'Ok'
			}).then(function () {
				var datapost={
					"id"  :   id
				  };
				  $.ajax({
					type: "POST",
					url: "{{ route('backend.pengukurankinerja.deletekegiatan') }}",
					data : JSON.stringify(datapost),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(response) {
					  if (response.success == true) {

						  swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () {
								window.location.href =  "{{ route('backend.pengukurankinerja.home', ['id'=>$id, 'ids'=>$idsasaran]) }}";
							})



					  } else{
						swal("Failed!", response.message, "error");
					  }
					}
				  });
			})
    }



    // Program
    function tambahprogram(id){
        var url = '{{ route("backend.pengukurankinerja.tambahprogram",["id"=>":id", "iddinas"=> $id, "idsasaran"=> $idsasaran]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Tambah Program');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }

    function editProgram(id){
        var url = '{{ route("backend.pengukurankinerja.editprogram",["id"=>":id", "iddinas"=> $id, "idsasaran"=> $idsasaran]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Edit Program');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }

    function editIndikatorProgram(id, idprogram){
        var url = '{{ route("backend.pengukurankinerja.editindikatorprogram",["id"=>":id", "idprogram"=> ":idprogram", "iddinas"=> $id, "idsasaran"=> $idsasaran]) }}'
        url = url.replace(':id', id);
        url = url.replace(':idprogram', idprogram);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Edit Indikator Program');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    function editIndikatorKegiatan(id, idprogram){
        var url = '{{ route("backend.pengukurankinerja.editindikatorkegiatan",["id"=>":id", "idprogram"=> ":idprogram", "iddinas"=> $id, "idsasaran"=> $idsasaran]) }}'
        url = url.replace(':id', id);
        url = url.replace(':idprogram', idprogram);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Edit Indikator Program');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    function printAwal(id){
        var url = '{{ route("backend.pengukurankinerja.printpk",["id"=>":id"]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Print Perjanjian Kinerja');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }

    function deteleProgram(id){
              swal({
				title: 'Apa anda yakin?',
				  text: "Anda akan menghapus program!",
				  type: 'error',
				  showCancelButton: true,
				  confirmButtonText: 'Ok'
			}).then(function () {
				var datapost={
					"id"  :   id
				  };
				  $.ajax({
					type: "POST",
					url: "{{ route('backend.pengukurankinerja.deleteprogram') }}",
					data : JSON.stringify(datapost),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(response) {
					  if (response.success == true) {

						  swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () {
								window.location.href =  "{{ route('backend.pengukurankinerja.home', ['id'=>$id, 'ids'=>$idsasaran]) }}";
							})



					  } else{
						swal("Failed!", response.message, "error");
					  }
					}
				  });
			})
    }
    function deteleIndikatorKegiatan(id){
              swal({
				title: 'Apa anda yakin?',
				  text: "Anda akan menghapus indikator kegiatan!",
				  type: 'error',
				  showCancelButton: true,
				  confirmButtonText: 'Ok'
			}).then(function () {
				var datapost={
					"id"  :   id
				  };
				  $.ajax({
					type: "POST",
					url: "{{ route('backend.pengukurankinerja.deleteindikatorkegiatan') }}",
					data : JSON.stringify(datapost),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(response) {
					  if (response.success == true) {

						  swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () {
								window.location.href =  "{{ route('backend.pengukurankinerja.home', ['id'=>$id, 'ids'=>$idsasaran]) }}";
							})



					  } else{
						swal("Failed!", response.message, "error");
					  }
					}
				  });
			})
    }

    function deteleIndikatorProgram(id){
              swal({
				title: 'Apa anda yakin?',
				  text: "Anda akan menghapus indikator program!",
				  type: 'error',
				  showCancelButton: true,
				  confirmButtonText: 'Ok'
			}).then(function () {
				var datapost={
					"id"  :   id
				  };
				  $.ajax({
					type: "POST",
					url: "{{ route('backend.pengukurankinerja.deleteindikatorprogram') }}",
					data : JSON.stringify(datapost),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(response) {
					  if (response.success == true) {

						  swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () {
								window.location.href =  "{{ route('backend.pengukurankinerja.home', ['id'=>$id, 'ids'=>$idsasaran]) }}";
							})



					  } else{
						swal("Failed!", response.message, "error");
					  }
					}
				  });
			})
    }

    // Indikator Program

    function tambahindikatorprogram(id){
        var url = '{{ route("backend.pengukurankinerja.tambahindikatorprogram",["id"=>":id", "iddinas"=> $id, "idsasaran"=> $idsasaran]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Tambah Program');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    function cariData(id){

        var url = '{{ route("backend.pengukurankinerja.home",["id"=>"$id", "idsasaran"=> ":idsasaran" ]) }}';
        url = url.replace(':idsasaran', id);
        window.location.href =  url;
    }
</script>
@endsection


