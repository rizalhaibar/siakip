
<div class="row">
    <div class="col-md-12">
        <div class="form-horizontal" >
            <div class="form-group">
                <label class="col-md-2 control-label">Kegiatan*</label>
                <div class="col-md-12">
                    <input type="text" class="form-control" id="kegiatan_program" name="kegiatan_program" placeholder="Kegiatan..." required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Deskripsi</label>
                <div class="col-md-12">
                    <textarea class="form-control" rows="5" id="desc" name="desc"  placeholder="Deskripsi..." ></textarea>
                </div>
            </div>


        <div class="form-group">
            <div class="col-md-offset-10 col-md-8">
                <button type="button" onClick="saveProgram()" class="btn btn-primary waves-effect waves-light btn-rounded">
                    Simpan
                </button>
            </div>
        </div>
    </div>
</div>

<script>
function saveProgram(){
       
       var kegiatan   = $('#kegiatan_program').val();
       var desc   = $('#desc').val();
          if(program){
            
                  var datapost={
                      "idprogram" : "{{$id}}",
                      "iddinas"   : "{{$iddinas}}",
                      "kegiatan"   : kegiatan,
                      "desc"   : desc,
                      "tahun"  : tahun
                    };
                    $.ajax({
                      type: "POST",
                      url: "{{ route('backend.pengukurankinerja.savekegiatan') }}",
                      data : JSON.stringify(datapost),
                      dataType: 'json',
                      contentType: 'application/json; charset=utf-8',
                      success: function(response) {
                        if (response.success == true) {
                            $("#panel-modal .close").click();
                            swal({
                                  title: 'Success!',
                                  text: response.message,
                                  type: 'success',
                                  showCancelButton: false,
                                  confirmButtonText: 'Ok'
                              }).then(function () {
                                window.location.href = "{{ route('backend.pengukurankinerja.home', ['id'=>$iddinas, 'ids'=>$idsasaran]) }}";   
                              })



                        } else{
                          swal("Failed!", response.message, "error");
                        }
                      }
                    });
             
          }else{
              swal("Failed!", "Kegiatan Belum Terisi!", "error");
          }
        
  }
</script>