<?php

namespace App\Imports;

use App\Models\Person;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class PersonsImport implements ToModel, WithHeadingRow, WithValidation
{
    use Importable;

    public function __construct($attributes)
    {
        $this->attributes = $attributes;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if( !array_filter($row) ) return null;

        return new Person([
            'company_id'    => $this->attributes->company_id,
            'department_id' => $this->attributes->department_id,
            'name'      => $row['name'],
            'email'     => $row['email'],
            'phone'     => $row['phone'],
            'address'   => $row['address'],
            'status'    => $this->attributes->status,
        ]);
    }

    public function rules(): array
    {
        return [
            'name' => 'required|string',
            '*.name' => 'required|string',
        ];
    }
}
