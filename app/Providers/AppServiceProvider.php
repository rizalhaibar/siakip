<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Traits\FlashMessageTraits;

class AppServiceProvider extends ServiceProvider
{
    use FlashMessageTraits;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);

        view()->composer('backend.layouts.message', function ($view) {
            
            $messages = self::messages();

            return $view->with('messages', $messages);
            
        });
    }
}
