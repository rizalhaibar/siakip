<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Crypt;
use DB;

/**
 * Class UserController.
 */
class PemerintahController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
	 
	function __construct()
	{
		
	}
	public function index()
    {
		return view('backend.pemerintah.index');
    }
	
	public function about()
    {
		return view('backend.pemerintah.about');
    }
	
	public function contact()
    {
		return view('backend.pemerintah.contact');
    }
	
	public function evaluasi()
    {
		$data		 = $this->listcabangevaluasi();
		return view('backend.pemerintah.evaluasi',
            [
                'data' => $data
            ]);
    }
	
	
	
	
	public function program()
    {
		return view('backend.pemerintah.program');
    }
	


	public function perencanaan(Request $request)
    {
		$id = 0;
		$idsasaran = 0;
		$idjabatan = 0;
		$paramjabatan =  '0';
		$ideselon = '';
		
		if($request->id){

			if($request->idjabatan){
				$JAP = explode("_", $request->idjabatan);
				if(isset($JAP[0])){
					$idjabatan = $JAP[0];
				}
				if(isset($JAP[1])){
					$ideselon = $JAP[1];
				}
				
				
				$paramjabatan = $request->idjabatan;
			}
			
			$id = $request->id;
			// $id = Crypt::decryptString($request->id);
			$idsasaran 	= $request->idsasaran;
			// $idsasaran 	= Crypt::decryptString($request->idsasaran);
			$idadmin 	= $request->idadmin;
			$idpengawas = $request->idpengawas;
			
			$data['listsasaran'] 	= $this->listsasaran($id);
			$data['listjabatan'] 	= $this->listjabatan($id);
			// $data['listjpt'] 		= $this->listjpt($id);
			// $data['listadmin'] 		= $this->listadmin($id);
			// $data['listpengawas'] 	= $this->listpengawas($id);
			$data['listiku'] 		= $this->listiku($id);
			$data['listrkt'] 		= $this->listrkt($id);
			$data['keselarasan'] 	= $this->keselarasan($id);

			if($ideselon == '2A'){
				$data['pk_indikatorsasaranra'] = $this->listindikatorsasaranpk($id, $idsasaran, $idjabatan);
			}
			if($ideselon == '3A'){
				$data['pk_programs'] = $this->getProgrampk($id, $idjabatan);
			}
			if($ideselon == '4A'){
				$data['pk_kegiatans'] = $this->getKegiatanpk($id, $idjabatan);
			}
			

			if($idsasaran != 0){
				//Rencana Aksi
				$data['indikatorsasaranra'] = $this->listindikatorsasaran($id, $idsasaran);
				$data['programsra'] = $this->getProgram($idsasaran);
				$data['kegiatansra'] = $this->getKegiatan($idsasaran);
			}
		}

		$data['data']			= $this->listcabang();
		$data['id'] 			= $request->id;
		$data['idsasaran'] 		= $request->idsasaran;
		$data['idjabatan'] 		= $idjabatan;
		$data['param_jabatan'] 	= $paramjabatan;
		$data['ideselon'] 		= $ideselon;
		// dd($data);
		return view('backend.pemerintah.perencanaan',$data);
	}
	

	public function pengukuran(Request $request)
    {
		$id = 0;
		$idsasaran = 0;
		if($request->id){

			// $id = Crypt::decryptString($request->id);
			// $idsasaran = Crypt::decryptString($request->idsasaran);
			$id =$request->id;
			$idsasaran = $request->idsasaran;
			
			$data['listsasaran'] 	= $this->listsasaran($id);
			if($idsasaran != 0){
				$data['indikatorsasaran'] = $this->listindikatorsasaran($id, $idsasaran);
				$data['program'] = $this->getProgram($idsasaran);
				$data['kegiatan'] = $this->getKegiatan($idsasaran);
			}

		}
		$data['data']			= $this->listcabang();
		$data['id'] 			= $request->id;
		$data['idsasaran'] 		= $idsasaran;
		return view('backend.pemerintah.pengukuran',$data);
    }
	
	public function pelaporan(Request $request)
    {
		$id = 0;
		$idsasaran = 0;
		// dd($request);
		if($request->id){

			// $id = Crypt::decryptString($request->id);
			// $idsasaran = Crypt::decryptString($request->idsasaran);
			$id = $request->id;
			$idsasaran = $request->idsasaran;
			
			$data['listsasaran'] 	= $this->listsasaran($id);
			if($idsasaran != 0){
				$data['pelaporan']		= $this->getpelaporan($idsasaran);
			}

		}
		$data['data']			= $this->listcabang();
		$data['id'] 			= $request->id;
		$data['idsasaran'] 		= $idsasaran;
		return view('backend.pemerintah.pelaporan',$data);
    }
   
	public function listcabangevaluasi(){ 
		$hasil = DB::select('SELECT aa.td_id, aa.td_dinas, aa.td_keterangan , bb.nilai
						FROM
								( 
								 SELECT td_id, td_dinas, td_keterangan , `index`
								 FROM tbl_dinas 
								 WHERE
									status = 1
								 )aa
						LEFT JOIN
								(
								 SELECT tbl_dinas.td_id, tbl_dinas.td_dinas, tbl_dinas.td_keterangan , b.nilai
								 FROM tbl_dinas 
								 LEFT JOIN tbl_evaluasi b on tbl_dinas.td_id = b.iddinas
								 WHERE
									status = 1
								 AND b.tahun = '.$_COOKIE['tahun'].'
								 )bb on bb.td_id = aa.td_id

						GROUP BY aa.td_dinas ORDER BY aa.index ASC');
		return $hasil;
	}
	
	public function listcabang(){ 
		$hasil = DB::select('SELECT td_id, td_dinas, td_keterangan , `index`
								FROM tbl_dinas 
								WHERE
								status = 1
								ORDER BY `index` ASC');
		return $hasil;
	}
	
	public function listsasaran($id){
		$where = '';
		$where = 'aa.`iddinas` = '.$id.' AND';

		$result4 	= DB::select('SELECT a.* FROM tbl_sasaran_tujuan a
		
								LEFT JOIN tbl_tujuan cc ON cc.tt_id = a.idtujuan
								LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
								LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
								WHERE '.$where.' a.tst_status = 0');
		return $result4;
	}
	
	public function listjabatan($id){
		$result4 	= DB::select('SELECT * FROM tbl_eselon WHERE iddinas = '.$id.' AND `status` = 1 ');
		return $result4;
	}
	
	public function listjpt($id){
		$result4 	= DB::select('SELECT * FROM tbl_eselon WHERE iddinas = '.$id.' AND `status` = 1 AND te_eselon = "2A"');
		return $result4;
	}

	public function listadmin($id){
		$result4 	= DB::select('SELECT * FROM tbl_eselon WHERE iddinas = '.$id.' AND `status` = 1 AND te_eselon = "3A"');
		return $result4;
	}

	public function listpengawas($id){
		$result4 	= DB::select('SELECT * FROM tbl_eselon WHERE iddinas = '.$id.' AND `status` = 1 AND te_eselon = "4A"');
		return $result4;
	}

	
	public function getpelaporan($id){ 
		$hasil = DB::table('tbl_pelaporan')
		->where('idsasaran', $id)
		->first();

		return $hasil;
	}
	
	public function listindikatorsasaran($id, $idsasaran){

		$where = '';
		$where = 'aa.`iddinas` = '.$id.' AND';
		$result6 	= DB::select('SELECT  aadc.tis_id, aadc.tis_indikator_sasaran, aadc.tis_satuan,  aadc.tis_target,
							bbdc.twtarget1, 
							bbdc.tw1, 
							bbdc.twtarget2, 
							bbdc.tw2, 
							bbdc.twtarget3, 
							bbdc.tw3, 
							bbdc.twtarget4,
							bbdc.tw4
					FROM
					(
					SELECT a.tis_id, a.tis_indikator_sasaran, a.tis_satuan, a.tis_target, a.idsasaran, a.tis_status
					FROM tbl_indikator_sasaran_pk a
					LEFT JOIN tbl_sasaran_tujuan dd ON a.`idsasaran` = dd.`tst_id`
					LEFT JOIN tbl_tujuan cc ON cc.tt_id = dd.idtujuan
					LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
					LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
					)aadc
					LEFT JOIN
					(

					SELECT a.tis_id, a.tis_indikator_sasaran, a.tis_satuan,  a.idsasaran,
						sa.`tw1`, sa.`twtarget1`, 
						sa.`tw2`, sa.`twtarget2`, 
						sa.`tw3`, sa.`twtarget3`, 
						sa.`tw4`, sa.`twtarget4`
					FROM tbl_indikator_sasaran_pk a
					LEFT JOIN tbl_indikator_sasaran_triwulan sa ON a.tis_id = sa.indisasaran
					LEFT JOIN tbl_sasaran_tujuan dd ON dd.`tst_id` = a.`idsasaran`
					LEFT JOIN tbl_tujuan cc ON cc.tt_id = dd.idtujuan
					LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
					LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
					)bbdc ON aadc.tis_id = bbdc.tis_id 
					WHERE  aadc.idsasaran =  '.$idsasaran.' AND aadc.tis_status = 0');
		return $result6;
	}
	public function listindikatorsasaranpk($id, $idsasaran, $idjabatan){

		$hasil = array();

		$wherepk = '';
		if($idjabatan != 0){
			$wherepk = 'AND a.pic = '.$idjabatan;
		}

		$result1 	= DB::select('SELECT a.* 
							FROM tbl_sasaran_tujuan a 
							LEFT JOIN tbl_tujuan cc ON cc.tt_id = a.idtujuan
							LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
							LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
							WHERE aa.`iddinas` = '. $id.'
							AND a.tahun = '.$_COOKIE['tahun'].'
							AND a.`tst_status` = 0');
        foreach ($result1 as $sasaran) {
			$result2 	= DB::select('SELECT a.tis_id, a.tis_indikator_sasaran, a.tis_satuan, a.tis_target, a.idsasaran, a.tis_status, a.pic, aa.`iddinas`
										FROM tbl_indikator_sasaran_pk a
										LEFT JOIN tbl_sasaran_tujuan dd ON a.`idsasaran` = dd.`tst_id`
										LEFT JOIN tbl_tujuan cc ON cc.tt_id = dd.idtujuan
										LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
										LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
										WHERE aa.`iddinas` = '.$id.' AND a.tis_status = 0 '.$wherepk);
			foreach ($result2 as $indikatorsasaran) {
                if ($sasaran->tst_id == $indikatorsasaran->idsasaran) {
					$sasaran->indikator_sasaran[] = $indikatorsasaran;
				}
			}

			array_push($hasil, $sasaran);
        }

		// dd($hasil);
		
		return $hasil;
	}
	

	public function getProgram($id){
		$hasil = array();

		$result1 	= DB::select('SELECT a.* FROM tbl_indikator_sasaran_pk a 
							LEFT JOIN tbl_iku b ON b.tis_id = a.tis_id 
							LEFT JOIN tbl_eselon c ON c.te_id = a.pic 
							WHERE idsasaran = '.$id.' AND tis_status = 0');

		// echo '<pre>';
        foreach ($result1 as $indikatorsasaran) {
			$result2 = DB::select('SELECT a.*
									FROM tbl_program_sasaran a
									WHERE a.tahun = '.$_COOKIE['tahun'].' AND idindisasaran = '.$indikatorsasaran->tis_id.' AND status=0');
            foreach ($result2 as $program) {
				if($indikatorsasaran->tis_id == $program->idindisasaran){
					$result3 	= DB::select('SELECT aadc.tip_id, aadc.idprogram, aadc.tip_indikator_program,  aadc.tip_satuan, aadc.tip_target,
											bbdc.twtarget1, 
											bbdc.tw1, 
											bbdc.twtarget2, 
											bbdc.tw2, 
											bbdc.twtarget3, 
											bbdc.tw3, 
											bbdc.twtarget4,
											bbdc.tw4
											FROM
											(
												SELECT a.tip_id, a.idprogram, a.tip_indikator_program, a.tip_satuan, a.tip_target, a.`status`
												FROM tbl_indikator_program a
											)aadc
											LEFT JOIN
											(
												SELECT a.tip_id, a.idprogram, a.tip_indikator_program, a.tip_satuan, a.tip_target, a.`status`, 
												sa.`tw1`, sa.`twtarget1`, 
												sa.`tw2`, sa.`twtarget2`, 
												sa.`tw3`, sa.`twtarget3`, 
												sa.`tw4`, sa.`twtarget4`
												FROM tbl_indikator_program a
												LEFT JOIN tbl_indikator_program_triwulan sa ON a.tip_id = sa.indiprogram
											)bbdc ON aadc.tip_id = bbdc.tip_id 
											WHERE aadc.idprogram = '.$program->tps_id.' AND aadc.`status` = 0');


					foreach($result3 as $indikatorprogram){
						
						
						if($program->tps_id == $indikatorprogram->idprogram){
							
							$program->indikatorprogram = $result3;
						}
					}	
					$indikatorsasaran->program[] = $program;
				}
				
            }
			array_push($hasil, $indikatorsasaran);
		}
		// print_r($hasil);
		// die;
		$hasil = (object) $hasil;
		return $hasil;
	}

	public function getProgrampk($id, $idadmin){
		$hasil = array();

		
		$wherepk = '';
		if($idadmin != 0){
			$wherepk = 'AND a.pic = '.$idadmin;
		}

		// $result2 = DB::select('SELECT a.*
		// FROM tbl_program_sasaran a
		// WHERE a.tahun = '.$_COOKIE['tahun'].' AND idindisasaran = '.$indikatorsasaran->tis_id.' AND status=0');

		$result2 = DB::select('SELECT a.* 
							FROM tbl_program_sasaran a 
							
							LEFT JOIN tbl_indikator_sasaran_pk ee ON ee.tis_id = a.idindisasaran
							LEFT JOIN tbl_sasaran_tujuan dd ON dd.tst_id = ee.idsasaran
							LEFT JOIN tbl_tujuan cc ON cc.tt_id = dd.idtujuan 
							LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
							LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
							WHERE aa.`iddinas` = '. $id.'
							AND a.tahun = '.$_COOKIE['tahun'].'
							AND a.`status` = 0');
		// dd($result2);					
		foreach ($result2 as $program) {
				$result3 	= DB::select('SELECT 
										a.tip_id, a.idprogram, a.tip_indikator_program, 
										a.tip_satuan, a.tip_target, a.`status`, a.pic,
										a.te_isi
										FROM tbl_indikator_program a
										WHERE a.idprogram = '.$program->tps_id.' AND a.tahun = '.$_COOKIE['tahun'].' AND a.`status` = 0 '.$wherepk);


				foreach($result3 as $indikatorprogram){
					
					
					if($program->tps_id == $indikatorprogram->idprogram){
						
						$program->indikatorprogram = $result3;
					}
				}	

			array_push($hasil, $program);
		}
		$hasil = (object) $hasil;
		return $hasil;
	}

	public function getKegiatan($id){
		$hasil = array();

		$result1 	= DB::select('SELECT a.* 
		FROM tbl_program_sasaran a
		LEFT JOIN tbl_indikator_sasaran_pk b ON a.`idindisasaran` = b.`tis_id`
		LEFT JOIN tbl_sasaran_tujuan c ON b.`idsasaran` = c.`tst_id`
		WHERE  a.tahun = '.$_COOKIE['tahun'].' AND c.`tst_id` = '.$id.' AND a.status = 0');

		foreach($result1 as $program){

			$result2 	= DB::select('SELECT a.* FROM tbl_indikator_program a
										WHERE  a.idprogram = '.$program->tps_id.' AND a.`status` = 0');

			foreach($result2 as $indikatorprogram){
				if($program->tps_id == $indikatorprogram->idprogram){
					$result3 	= DB::select('SELECT a.*
					FROM tbl_kegiatan_program a
					WHERE a.idindiprogram = '.$indikatorprogram->tip_id.' AND a.`status` = 0');

					foreach($result3 as $kegiatan){
						if($kegiatan->idindiprogram == $indikatorprogram->tip_id){
							$result4 	= DB::select('SELECT 
							aadc.tik_id, aadc.idkegiatan, aadc.tik_indikator_kegiatan, aadc.tik_satuan, aadc.tik_target, 
							bbdc.twtarget1, 
							bbdc.tw1, 
							bbdc.twtarget2, 
							bbdc.tw2, 
							bbdc.twtarget3, 
							bbdc.tw3, 
							bbdc.twtarget4,
							bbdc.tw4
							FROM ( 
								SELECT a.tik_id, a.idkegiatan, a.tik_indikator_kegiatan, a.tik_satuan, a.tik_target, a.`tik_status` 
								FROM tbl_indikator_kegiatan a 
							)aadc 
							LEFT JOIN ( 
								SELECT 
								a.tik_id, a.idkegiatan, a.tik_indikator_kegiatan, a.tik_satuan, a.tik_target, a.`tik_status` ,
								sa.`tw1`, sa.`twtarget1`, sa.`tw2`, sa.`twtarget2`, sa.`tw3`, sa.`twtarget3`, sa.`tw4`, sa.`twtarget4` 
								FROM tbl_indikator_kegiatan a 
								LEFT JOIN tbl_indikator_kegiatan_triwulan sa 
								ON a.tik_id = sa.indikegiatan 
							)bbdc ON aadc.tik_id = bbdc.tik_id 
							WHERE aadc.idkegiatan = '.$kegiatan->tkp_id.' AND aadc.`tik_status` = 0');
							foreach($result4 as $indikatorkegiatan){
								if($kegiatan->tkp_id == $indikatorkegiatan->idkegiatan){
									$kegiatan->indikatorkegiatan = $result4;
								}
							}	
							$indikatorprogram->kegiatan[] = $kegiatan;
						}
					}	

					$program->indikatorprogram[] = $indikatorprogram;
				}
			}
			array_push($hasil, $program);
		}
		$hasil = (object) $hasil;
		return $hasil;
	}

	
	public function getKegiatanpk($id, $idpengawas){


		
		$hasil = array();

		$wherepk = '';
		if($idpengawas != 0){
			$wherepk = 'AND a.pic = '.$idpengawas;
		}
		
		$result2 = DB::select('SELECT a.* 
							FROM tbl_kegiatan_program a 
							
							LEFT JOIN tbl_indikator_program gg ON gg.tip_id = a.idindiprogram
							LEFT JOIN tbl_program_sasaran ff ON ff.tps_id = gg.idprogram
							LEFT JOIN tbl_indikator_sasaran_pk ee ON ee.tis_id = ff.idindisasaran
							LEFT JOIN tbl_sasaran_tujuan dd ON dd.tst_id = ee.idsasaran
							LEFT JOIN tbl_tujuan cc ON cc.tt_id = dd.idtujuan 
							LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi 
							LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi 
							WHERE aa.`iddinas` = '. $id.'
							AND a.tahun = '.$_COOKIE['tahun'].'
							AND a.`status` = 0');
		// dd($result2);					
		foreach ($result2 as $kegiatan) {
				$result3 	= DB::select('SELECT a.tik_id, a.idkegiatan, a.tik_indikator_kegiatan, a.tik_satuan, a.tik_target, a.`tik_status`, a.pic , a.te_isi
										FROM tbl_indikator_kegiatan a 
										WHERE a.idkegiatan = '.$kegiatan->tkp_id.' AND a.tahun = '.$_COOKIE['tahun'].'  AND a.`tik_status` = 0 '.$wherepk);


				foreach($result3 as $indikatorprogram){
					
					
					if($kegiatan->tkp_id == $indikatorprogram->idkegiatan){
						
						$kegiatan->indikatorkegiatan = $result3;
					}
				}	

			array_push($hasil, $kegiatan);
		}
		// dd($hasil);
		$hasil = (object) $hasil;
		return $hasil;
	}

	
   
	public function listiku($id){ 
		$hasil = array();
		$where = 'aa.`iddinas` = '.$id.' AND';
		$result4 	= DB::select('SELECT a.* FROM tbl_sasaran_tujuan a
		
								LEFT JOIN tbl_tujuan cc ON cc.tt_id = a.idtujuan
								LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
								LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
								WHERE '.$where.' tst_status = 0');
		
		
		
		
		foreach($result4 as $new4){

				$result6 	= DB::select('SELECT * FROM tbl_indikator_sasaran a 
				
											LEFT JOIN tbl_iku b ON b.tis_id = a.tis_id 
											WHERE idsasaran = '.$new4->tst_id.' AND tis_status = 0');

				foreach($result6 as $new6){
					
					
					if($new4->tst_id == $new6->idsasaran){
						
						$new4->indikatorsasaran = $result6;
					}
				}
			
			array_push($hasil, $new4);
		}
		// $hasil =$hasil;
		return $hasil;
	}

	public function listrkt($id){ 
		$hasil = array();
		$where = '';
		$where = 'aa.`iddinas` = '.$id.' AND';

		$result4 	= DB::select('SELECT a.* FROM tbl_sasaran_tujuan a
		
								LEFT JOIN tbl_tujuan cc ON cc.tt_id = a.idtujuan
								LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
								LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
								WHERE '.$where.' tst_status = 0');
		
		
		// dd($result4);
		
		foreach($result4 as $new4){

				$result6 	= DB::select('SELECT * FROM tbl_indikator_sasaran a 
				
											LEFT JOIN tbl_iku b ON b.tis_id = a.tis_id 
											WHERE idsasaran = '.$new4->tst_id.' AND tis_status = 0');

				foreach($result6 as $new6){
					
					
					if($new4->tst_id == $new6->idsasaran){
						
						$new4->indikatorsasaran = $result6;
					}
				}
			
			array_push($hasil, $new4);
		}
		// $hasil = (object) $hasil;
		return $hasil;
	}

	public function keselarasan($iddinas){
		
		
		// dd($whereEselon4);
        
		$hasil = array();
		
		$result1 	= DB::select('SELECT a.* 
		FROM tbl_tujuan a
		LEFT JOIN tbl_misi b ON a.`idmisi` = b.`tm_id`
		LEFT JOIN tbl_visi c ON b.`idvisi` = c.`tv_id`
		WHERE c.`iddinas` = '. $iddinas.'
		AND a.tahun = '.$_COOKIE['tahun'].'
		AND a.`status` = 0');

		
		foreach($result1 as $tujuan){
			$result2 	= DB::select('SELECT * FROM tbl_sasaran_tujuan WHERE idtujuan = '.$tujuan->tt_id.' AND tst_status = 0');
			
			foreach($result2 as $sasaran){
				if($tujuan->tt_id == $sasaran->idtujuan){
					$result3 	= DB::select('SELECT * FROM tbl_indikator_sasaran_pk WHERE idsasaran = '.$sasaran->tst_id.' AND tis_status = 0 ');
	
					foreach($result3 as $indikatorsasaran){
						if($sasaran->tst_id == $indikatorsasaran->idsasaran){
							$result4 	= DB::select('SELECT a.*
							FROM tbl_program_sasaran a
							WHERE a.tahun = '.$_COOKIE['tahun'].' AND idindisasaran = '.$indikatorsasaran->tis_id.' AND status=0');
							
							
							foreach ($result4 as $program) {
                                if($indikatorsasaran->tis_id == $program->idindisasaran){

									$result5 	= DB::select('SELECT a.* FROM tbl_indikator_program a
										WHERE a.idprogram = '.$program->tps_id.' AND a.`status` = 0 ');

										foreach($result5 as $indikatorprogram){
											if($program->tps_id == $indikatorprogram->idprogram){
												$result6 	= DB::select('SELECT a.*
												FROM tbl_kegiatan_program a
												WHERE a.idindiprogram = '.$indikatorprogram->tip_id.' AND a.`status` = 0');

												foreach($result6 as $kegiatan){
													if($kegiatan->idindiprogram == $indikatorprogram->tip_id){
														$result7 	= DB::select('SELECT a.*
														FROM tbl_indikator_kegiatan a
														WHERE a.idkegiatan = '.$kegiatan->tkp_id.' AND a.`tik_status` = 0 ');
														foreach($result7 as $indikatorkegiatan){
															if($kegiatan->tkp_id == $indikatorkegiatan->idkegiatan){
																$kegiatan->indikatorkegiatan = $result7;
															}
														}	
														$indikatorprogram->kegiatan[] = $kegiatan;
													}
												}	

												$program->indikatorprogram[] = $indikatorprogram;
											}
										}
									$indikatorsasaran->program[] = $program;
                                }
                            }
							
	
							
							$sasaran->indikatorsasaran[] = $indikatorsasaran;
						}
					}
					$tujuan->sasaran[] = $sasaran;
				}
				
			}
			array_push($hasil, $tujuan);
		}
		// dd($hasil);
		$hasil = $hasil;
		return $hasil;
	}

}
