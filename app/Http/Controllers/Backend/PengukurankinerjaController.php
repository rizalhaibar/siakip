<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserValidation;
use App\Models\Role;
use Carbon\Carbon;
use Redirect;
use DB;
use PDF;
/**
 * Class UserController.
 */
class PengukurankinerjaController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */


	function __construct()
	{
		$this->middleware(['auth']);

        $this->middleware(function ($request, $next)
        {
            $this->user = session('data')->user;
            return $next($request);
        });
	}


	public function index()
    {
		if($this->user->role_id == 99 || $this->user->role_id == 97){
			$data['breadcrumb'] = [
				'parent' => 'Perjanjian Kinerja',
				'child' => 'List Dinas'
			];
			$data['listdinas'] = $this->listdinas();

			return view('backend.pengukurankinerja.index', $data);
		}else{
			$id = $this->user->dinas_id;

			$url = Redirect::route('backend.pengukurankinerja.home',array('id' => $id,'idsasaran' => 0));
			return $url;
		}
	}

	public function home($id, $idsasaran)
    {
		$data['breadcrumb'] = [
			'parent' => 'Perjanjian Kinerja',
			'child' => 'List Pengukuran Kinerja'
		];
		$data['id'] = $id;

		$data['idsasaran'] = $idsasaran;
		$data['dinas'] = $this->getDinas($id);
		$data['listsasaran'] = $this->listsasaran($id);
		if($idsasaran != 0){
			$data['indikatorsasaran'] = $this->listindikatorsasaran($idsasaran);
			$data['program'] = $this->getProgram($idsasaran);
			$data['kegiatan'] = $this->getKegiatan($idsasaran);
		}

		// dd($data);
		// $data['listprogram'] = $this->listprogram($id);
		// $data['listkegiatan'] = $this->listkegiatan($id);
		// dd($data);
		return view('backend.pengukurankinerja.home', $data);
	}

	public function printpk($id)
    {
		$data['id'] = $id;

		return view('backend.pengukurankinerja.print', $data);
	}
	public function tambahindikatorsasaranpic($id, $idsasaran, $ids)
    {
		$data['id'] = $id;
		$data['iddinas'] = $ids;
		$data['idsasaran'] = $idsasaran;
		$data['listpic'] = $this->listpic($ids, '2A');
		$data['detail'] = DB::table('tbl_indikator_sasaran_pk')->where('tis_id',$id)->first();


		return view('backend.pengukurankinerja.tambahindikatorsasaranpic', $data);
	}
	public function tambahindikatorprogrampic($id, $idsasaran, $ids)
    {
		$data['id'] = $id;
		$data['iddinas'] = $ids;
		$data['idsasaran'] = $idsasaran;
		$data['listpic'] = $this->listpic($ids, '3A');
		$data['detail'] = DB::table('tbl_indikator_program')->where('tip_id',$id)->first();


		return view('backend.pengukurankinerja.tambahindikatorprogrampic', $data);
	}
	public function tambahindikatorkegiatanpic($id, $idsasaran, $ids)
    {
		$data['id'] = $id;
		$data['iddinas'] = $ids;
		$data['idsasaran'] = $idsasaran;
		$data['listpic'] = $this->listpic($ids, '4A');
		$data['detail'] = DB::table('tbl_indikator_kegiatan')->where('tik_id',$id)->first();


		return view('backend.pengukurankinerja.tambahindikatorkegiatanpic', $data);
	}




    public function tambahindikatorkegiatan($id, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;

        return view('backend.pengukurankinerja.tambahindikatorkegiatan',$data);
	}



    public function tambahkegiatan($id, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;

        return view('backend.pengukurankinerja.tambahkegiatan',$data);
	}



    public function tambahprogram($id, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;

        return view('backend.pengukurankinerja.tambahprogram',$data);
	}

    public function tambahindikatorprogram($id, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;

        return view('backend.pengukurankinerja.tambahindikatorprogram',$data);
	}

    public function editprogram($id, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;
		$data['detail'] = DB::table('tbl_program_sasaran')->where('tps_id',$id)->first();
		// dd($data);
        return view('backend.pengukurankinerja.editprogram',$data);
	}

    public function editkegiatan($id, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;
		$data['detail'] = DB::table('tbl_kegiatan_program')->where('tkp_id',$id)->first();
		// dd($data);
        return view('backend.pengukurankinerja.editkegiatan',$data);
	}
    public function editindikatorprogram($id, $idprogram, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['idprogram'] = $idprogram;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;
		$data['detail'] = DB::table('tbl_indikator_program')->where('tip_id',$id)->first();
		// dd($data);
        return view('backend.pengukurankinerja.editindikatorprogram',$data);
	}
    public function editindikatorkegiatan($id, $idprogram, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['idprogram'] = $idprogram;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;
		$data['detail'] = DB::table('tbl_indikator_kegiatan')->where('tik_id',$id)->first();
		$data['anggaran'] = DB::table('tbl_anggaran')->where('idindkegiatan',$id)->where('status','=', '0')->first();
		// dd($data);
        return view('backend.pengukurankinerja.editindikatorkegiatan',$data);
	}


	// CURD

	public function savekegiatan(Request $request){
		$input = $request->all();
		DB::beginTransaction();
		try {
		DB::table('tbl_kegiatan_program')->insert(
			[
				'idindiprogram' => $input['idprogram'],
				'tkp_kegiatan_program' => $input['kegiatan'],
				'tkp_keterangan' => $input['desc'],
				'te_isi' => $input['iddinas'],
				'tahun' => $input['tahun']
				]
		);
		DB::table('tbl_kegiatan_program_log')->insert(
			[
				'idindiprogram' => $input['idprogram'],
				'tkp_kegiatan_program' => $input['kegiatan'],
				'tkp_keterangan' => $input['desc'],
				'te_isi' => $input['iddinas'],
				'tahun' => $input['tahun']
				]
		);

		DB::commit();
		$result = array( 'success' => true, 'message' => 'Kegiatan Sukses Tersimpan!');
		}catch(\Illuminate\Database\QueryException $ex) {
			dd($ex);
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);
	}

	public function saveprogram(Request $request){
		$input = $request->all();
		DB::beginTransaction();
		try {
		DB::table('tbl_program_sasaran')->insert(
			[
				'idindisasaran' => $input['idindikatorsasaran'],
				'tps_program_sasaran' => $input['program'],
				'tps_keterangan' => $input['desc'],
				'te_isi' => $input['iddinas'],
				'tahun' => $input['tahun']
				]
		);
		DB::table('tbl_program_sasaran_log')->insert(
			[
				'idindisasaran' => $input['idindikatorsasaran'],
				'tps_program_sasaran' => $input['program'],
				'tps_keterangan' => $input['desc'],
				'te_isi' => $input['iddinas'],
				'tahun' => $input['tahun']
				]
		);

		DB::commit();
		$result = array( 'success' => true, 'message' => 'Program Sukses Tersimpan!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);
	}

	public function saveindikatorkegiatan(Request $request){
		$input = $request->all();
		DB::beginTransaction();
		try {
		DB::table('tbl_indikator_kegiatan')->insert(
			[
				'idkegiatan' => $input['idkegiatan'],
				'tik_indikator_kegiatan' => $input['kegiatan'],
				'tik_satuan' => $input['satuan'],
				'tik_keterangan' => $input['desc'],
				'tik_target' => $input['target'],
				'te_isi' => $input['iddinas'],
				'tahun' => $input['tahun']
				]
		);
		$id 		= DB::getPdo()->lastInsertId();
		DB::table('tbl_indikator_kegiatan_log')->insert(
			[
				'idkegiatan' => $input['idkegiatan'],
				'tik_indikator_kegiatan' => $input['kegiatan'],
				'tik_satuan' => $input['satuan'],
				'tik_keterangan' => $input['desc'],
				'tik_target' => $input['target'],
				'te_isi' => $input['iddinas'],
				'tahun' => $input['tahun']
				]
		);
		DB::table('tbl_anggaran')->insert(
			[
				'idindkegiatan' => $id,
				'ta_anggaran' => $input['anggaran'],
				'tahun' => $input['tahun']
			]
		);
		DB::table('tbl_anggaran_log')->insert(
			[
				'idindkegiatan' => $id,
				'ta_anggaran' => $input['anggaran'],
				'tahun' => $input['tahun']
			]
		);

		DB::commit();
		$result = array( 'success' => true, 'message' => 'Indikator Kegiatan Sukses Tersimpan!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);
	}
	public function saveindikatorprogram(Request $request){
		$input = $request->all();
		DB::beginTransaction();
		try {
		DB::table('tbl_indikator_program')->insert(
			[
				'idprogram' => $input['idprogram'],
				'tip_indikator_program' => $input['program'],
				'tip_keterangan' => $input['desc'],
				'tip_satuan' => $input['satuan'],
				'tip_target' => $input['target'],
				'te_isi' => $input['iddinas'],
				'tahun' => $input['tahun']
				]
		);
		DB::table('tbl_indikator_program_log')->insert(
			[
				'idprogram' => $input['idprogram'],
				'tip_indikator_program' => $input['program'],
				'tip_keterangan' => $input['desc'],
				'tip_satuan' => $input['satuan'],
				'tip_target' => $input['target'],
				'te_isi' => $input['iddinas'],
				'tahun' => $input['tahun']
				]
		);

		DB::commit();
		$result = array( 'success' => true, 'message' => 'Indikator Program Sukses Tersimpan!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);
	}




	public function programedit(Request $request){
		$input = $request->all();
		DB::beginTransaction();
		try {

			DB::table('tbl_program_sasaran')
			->where('tps_id', $input['idprogram'])
			->update([
				'tps_program_sasaran' => $input['program'],
				'tps_keterangan' => $input['desc'],
			]);

			DB::table('tbl_program_sasaran_log')
			->where('tps_id', $input['idprogram'])
			->update([
				'tps_program_sasaran' => $input['program'],
				'tps_keterangan' => $input['desc'],
			]);

			DB::commit();
			$result = array( 'success' => true, 'message' => 'Program Sukses Teredit!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);
	}


	public function kegiatanedit(Request $request){
		$input = $request->all();
		DB::beginTransaction();
		try {

			DB::table('tbl_kegiatan_program')
			->where('tkp_id', $input['idprogram'])
			->update([
				'tkp_kegiatan_program' => $input['kegiatan'],
				'tkp_keterangan' => $input['desc'],
			]);

			DB::table('tbl_kegiatan_program_log')
			->where('tkp_id', $input['idprogram'])
			->update([
				'tkp_kegiatan_program' => $input['kegiatan'],
				'tkp_keterangan' => $input['desc'],
			]);

			DB::commit();
			$result = array( 'success' => true, 'message' => 'Kegiatan Sukses Teredit!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);
	}


	public function indikatorprogramedit(Request $request){
		$input = $request->all();
		DB::beginTransaction();
		try {

			DB::table('tbl_indikator_program')
			->where('tip_id', $input['idprogram'])
			->update([
				'tip_indikator_program' => $input['program'],
				'tip_keterangan' => $input['desc'],
				'tip_satuan' => $input['satuan'],
				'tip_target' => $input['target'],
			]);

			DB::table('tbl_indikator_program_log')
			->where('tip_id', $input['idprogram'])
			->update([
				'tip_indikator_program' => $input['program'],
				'tip_keterangan' => $input['desc'],
				'tip_satuan' => $input['satuan'],
				'tip_target' => $input['target'],
			]);

			DB::commit();
			$result = array( 'success' => true, 'message' => 'Indikator Program Sukses Teredit!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);
	}
	public function indikatorkegiatanedit(Request $request){
		$input = $request->all();
		DB::beginTransaction();
		try {

			DB::table('tbl_indikator_kegiatan')
			->where('tik_id', $input['idkegiatan'])
			->update([
				'tik_indikator_kegiatan' => $input['kegiatan'],
				'tik_keterangan' => $input['desc'],
				'tik_satuan' => $input['satuan'],
				'tik_target' => $input['target'],
			]);

			DB::table('tbl_anggaran')
			->where('idindkegiatan', $input['idkegiatan'])
			->update([
				'ta_anggaran' => $input['anggaran'],
				'status' => 1
			]);


			DB::table('tbl_anggaran')->insert(
				[
					'idindkegiatan' => $input['idkegiatan'],
					'ta_anggaran' => $input['anggaran'],
					'tahun' => $input['tahun']
				]
			);
			DB::table('tbl_anggaran_log')->insert(
				[
					'idindkegiatan' => $input['idkegiatan'],
					'ta_anggaran' => $input['anggaran'],
					'tahun' => $input['tahun']
				]
			);

			DB::table('tbl_indikator_kegiatan_log')->insert(
				[
					'idkegiatan' => $input['idkegiatan'],
					'tik_indikator_kegiatan' => $input['kegiatan'],
					'tik_satuan' => $input['satuan'],
					'tik_keterangan' => $input['desc'],
					'tik_target' => $input['target'],
					'te_isi' => $input['iddinas'],
					'tahun' => $input['tahun']
					]
			);

			DB::commit();
			$result = array( 'success' => true, 'message' => 'Indikator Kegiatan Sukses Teredit!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);
	}


	public function deleteprogram(Request $request){
		$input = $request->all();
		$data = DB::table('tbl_program_sasaran')
				->where('tps_id', $input['id'])
				->update(['status' => 1]);
		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'Program Sukses Terdelete!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}
	}

	public function deleteindikatorkegiatan(Request $request){
		$input = $request->all();
		$data = DB::table('tbl_indikator_kegiatan')
				->where('tik_id', $input['id'])
				->update(['tik_status' => 1]);
		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'Indikator Kegiatan Sukses Terdelete!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}
    }

	public function deletekegiatan(Request $request){
		$input = $request->all();
		$data = DB::table('tbl_kegiatan_program')
				->where('tkp_id', $input['id'])
				->update(['status' => 1]);
		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'Kegiatan Sukses Terdelete!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}
    }


	public function deleteindikatorprogram(Request $request){
		$input = $request->all();
		$data = DB::table('tbl_indikator_program')
				->where('tip_id', $input['id'])
				->update(['status' => 1]);
		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'Indikator Program Sukses Terdelete!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}
    }




	// LIST DATA


	public function listpic($id, $code){
		$hasil = DB::select('SELECT te_id,
									te_jabatan,
									te_isi
								FROM tbl_eselon
								WHERE
								iddinas = '.$id.'
								AND te_eselon = "'.$code.'"');
		return $hasil;
	}

	public function getDinas($id){
		$where = '';
		if($this->user->role_id == 99 || $this->user->role_id == 97 ){
			$where = 'aa.`iddinas` = '.$id.' AND';
		}else{
			$where = 'aa.`iddinas` = '.$this->user->dinas_id.' AND';
		}
		$hasil = DB::table('tbl_dinas')->where('td_id',$id)->first();
		return $hasil;
	}


	public function listdinas(){
		$hasil = DB::select('SELECT td_id,
									td_dinas,
									td_keterangan
								FROM tbl_dinas
								WHERE
									status = 1
                                    and deleted_at is null
								ORDER BY `index`');
		return $hasil;
	}
	public function listsasaran($id){
		$where = '';
		if($this->user->role_id == 99 || $this->user->role_id == 97 ){
			$where = 'aa.`iddinas` = '.$id.' AND';
		}else{
			$where = 'aa.`iddinas` = '.$this->user->dinas_id.' AND';
		}
		$result4 	= DB::select('SELECT a.* FROM tbl_sasaran_tujuan a

								LEFT JOIN tbl_tujuan cc ON cc.tt_id = a.idtujuan
								LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
								LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
								WHERE '.$where.' tst_status = 0');
        $hasil = array();
        foreach($result4 as $sasaran){
            $result1 	= DB::select('SELECT *
            FROM tbl_indikator_sasaran_pk
            WHERE tbl_indikator_sasaran_pk.idsasaran = '.$sasaran->tst_id);
                foreach($result1 as $indikator_sasaran){
                    if($sasaran->tst_id == $indikator_sasaran->idsasaran){
                        $sasaran->indikatorsasaran[] = $indikator_sasaran;
                    }
                }
			array_push($hasil, $sasaran);
		}
		return $hasil;
	}
	public function listindikatorsasaran($id){
        $wherepk = '';
        if ($_COOKIE['pkperubahan'] == 'normal'){

            $wherepk = 'AND tis_status = 0';
        }
		$result6 	= DB::select('SELECT *, c.te_isi, c.te_jabatan FROM tbl_indikator_sasaran_pk a

							LEFT JOIN tbl_iku b ON b.tis_id = a.tis_id

							LEFT JOIN tbl_eselon c ON c.te_id = a.pic
							WHERE idsasaran = '.$id.' '.$wherepk);
		return $result6;
	}
	public function getKegiatan($id){
		$hasil = array();

		$result1 	= DB::select('SELECT a.*
		FROM tbl_program_sasaran a
		LEFT JOIN tbl_indikator_sasaran_pk b ON a.`idindisasaran` = b.`tis_id`
		LEFT JOIN tbl_sasaran_tujuan c ON b.`idsasaran` = c.`tst_id`
		WHERE a.tahun = '.$_COOKIE['tahun'].' AND c.`tst_id` = '.$id.' AND a.status = 0');

		foreach($result1 as $program){

			$result2 	= DB::select('SELECT a.* FROM tbl_indikator_program a
										WHERE a.idprogram = '.$program->tps_id.' AND a.`status` = 0');

			foreach($result2 as $indikatorprogram){
				if($program->tps_id == $indikatorprogram->idprogram){
					$result3 	= DB::select('SELECT a.*
					FROM tbl_kegiatan_program a
					WHERE a.idindiprogram = '.$indikatorprogram->tip_id.' AND a.`status` = 0');

					foreach($result3 as $kegiatan){
						if($kegiatan->idindiprogram == $indikatorprogram->tip_id){
							$result4 	= DB::select('SELECT a.*, b.ta_anggaran, c.te_jabatan, c.te_isi
							FROM tbl_indikator_kegiatan a
							LEFT JOIN tbl_anggaran b ON a.tik_id = b.idindkegiatan AND b.status = 0
							LEFT JOIN tbl_eselon c ON c.te_id = a.pic AND c.te_eselon = "4A"
							WHERE  a.idkegiatan = '.$kegiatan->tkp_id.' AND a.`tik_status` = 0');
							foreach($result4 as $indikatorkegiatan){
								if($kegiatan->tkp_id == $indikatorkegiatan->idkegiatan){
									$kegiatan->indikatorkegiatan = $result4;
								}
							}
							$indikatorprogram->kegiatan[] = $kegiatan;
						}
					}

					$program->indikatorprogram[] = $indikatorprogram;
				}
			}
			array_push($hasil, $program);
		}
		$hasil = (object) $hasil;
		return $hasil;
    }
	public function getProgramBak($id){
		$hasil = array();
        $wherepk = '';
        if ($_COOKIE['pkperubahan'] == 'normal'){

            $wherepk = 'AND tis_status = 0';
        }
		$result1 	= DB::select('SELECT a.* FROM tbl_indikator_sasaran_pk a
							LEFT JOIN tbl_iku b ON b.tis_id = a.tis_id
							LEFT JOIN tbl_eselon c ON c.te_id = a.pic
							WHERE idsasaran = '.$id.' '.$wherepk);

		// echo '<pre>';
        foreach ($result1 as $indikatorsasaran) {
			$result2 = DB::select('SELECT a.*
									FROM tbl_program_sasaran a
									WHERE a.tahun = '.$_COOKIE['tahun'].' AND idindisasaran = '.$indikatorsasaran->tis_id.' AND status=0');
            foreach ($result2 as $program) {
				if($indikatorsasaran->tis_id == $program->idindisasaran){

					$indikatorsasaran->program[] = $program;
				}

            }
			array_push($hasil, $indikatorsasaran);
		}
		// print_r($hasil);
		// die;
		$hasil = (object) $hasil;
		return $hasil;
	}
	public function getProgram($id){
		$hasil = array();
        $wherepk = '';
        if ($_COOKIE['pkperubahan'] == 'normal'){

            $wherepk = 'AND tis_status = 0';
        }
		$result1 	= DB::select('SELECT a.* FROM tbl_indikator_sasaran_pk a
							LEFT JOIN tbl_iku b ON b.tis_id = a.tis_id
							LEFT JOIN tbl_eselon c ON c.te_id = a.pic
							WHERE idsasaran = '.$id.' '.$wherepk);

		// echo '<pre>';
        foreach ($result1 as $indikatorsasaran) {
			$result2 = DB::select('SELECT a.*
									FROM tbl_program_sasaran a
									WHERE a.tahun = '.$_COOKIE['tahun'].' AND idindisasaran = '.$indikatorsasaran->tis_id.' AND status=0');
            foreach ($result2 as $program) {
				if($indikatorsasaran->tis_id == $program->idindisasaran){
					$result3 	= DB::select('SELECT a.*, c.te_isi, c.te_jabatan
												FROM tbl_indikator_program a
												LEFT JOIN tbl_eselon c ON c.te_id = a.pic AND c.te_eselon = "3A"
												WHERE a.idprogram = '.$program->tps_id.' AND a.`status` = 0');


					foreach($result3 as $indikatorprogram){


						if($program->tps_id == $indikatorprogram->idprogram){

							$program->indikatorprogram = $result3;
						}
					}
					$indikatorsasaran->program[] = $program;
				}

            }
			array_push($hasil, $indikatorsasaran);
		}
		// print_r($hasil);
		// die;
		$hasil = (object) $hasil;
		return $hasil;
	}

	public function getProgramJson($id){
		$hasil = array();
        $wherepk = '';
        if ($_COOKIE['pkperubahan'] == 'normal'){

            $wherepk = 'AND tis_status = 0';
        }
		$result1 	= DB::select('SELECT a.* FROM tbl_indikator_sasaran_pk a
							LEFT JOIN tbl_iku b ON b.tis_id = a.tis_id
							LEFT JOIN tbl_eselon c ON c.te_id = a.pic
							WHERE idsasaran = '.$id.' '.$wherepk);

        foreach ($result1 as $indikatorsasaran) {
			$result2 = DB::select('SELECT a.*
									FROM tbl_program_sasaran a
									WHERE a.tahun = '.$_COOKIE['tahun'].' AND idindisasaran = '.$indikatorsasaran->tis_id.' AND status=0');
            foreach ($result2 as $program) {
				if($indikatorsasaran->tis_id == $program->idindisasaran){
					$result3 	= DB::select('SELECT a.*, c.te_isi, c.te_jabatan
												FROM tbl_indikator_program a
												LEFT JOIN tbl_eselon c ON c.te_id = a.pic AND c.te_eselon = "3A"
												WHERE a.idprogram = '.$program->tps_id.' AND a.`status` = 0');


					foreach($result3 as $indikatorprogram){


						if($program->tps_id == $indikatorprogram->idprogram){

							$program->indikatorprogram = $result3;
						}
					}
					$indikatorsasaran->program[] = $program;
				}

            }
			array_push($hasil, $indikatorsasaran);
		}
		$hasil = (object) $hasil;
		return $hasil;
	}

	public function listsasaranbak($id){
		$hasil = array();
		$where = '';
		if($this->user->role_id != 99){
			$where = 'aa.`iddinas` = '.$this->user->dinas_id.' AND';
		}else{
			$where = 'aa.`iddinas` = '.$id.' AND';
		}

		$result4 	= DB::select('SELECT a.* FROM tbl_sasaran_tujuan a

								LEFT JOIN tbl_tujuan cc ON cc.tt_id = a.idtujuan
								LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
								LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
								WHERE '.$where.' tst_status = 0');


		// dd($result4);

		foreach($result4 as $new4){
                $wherepk = '';
                if ($_COOKIE['pkperubahan'] == 'normal'){

                    $wherepk = 'AND tis_status = 0';
                }
				$result6 	= DB::select('SELECT *, c.te_isi, c.te_jabatan FROM tbl_indikator_sasaran a

											LEFT JOIN tbl_iku b ON b.tis_id = a.tis_id

	 										LEFT JOIN tbl_eselon c ON c.te_id = a.pic
											WHERE idsasaran = '.$new4->tst_id.' '.$wherepk);

				foreach($result6 as $new6){


					if($new4->tst_id == $new6->idsasaran){

						$new4->indikatorsasaran = $result6;
					}
				}

			array_push($hasil, $new4);
		}
		$hasil = (object) $hasil;
		return $hasil;
	}

	public function listprogram($id){
		$where = '';
		if($this->user->role_id == 99 || $this->user->role_id == 97){
			$where = 'AND a.`te_isi` = '.$id.' ';
		}else{
			$where = 'AND a.`te_isi` = '.$this->user->dinas_id.' ';
		}
		$hasil = [];
		$result4 = DB::select('SELECT  a.tps_id, a.tps_program_sasaran, a.tps_keterangan, a.te_isi
		FROM tbl_program_sasaran a
		WHERE a.tahun = '.$_COOKIE['tahun'].' '.$where);

		foreach($result4 as $new4){

			$result6 	= DB::select('SELECT a.*, c.te_jabatan FROM tbl_indikator_program a
										LEFT JOIN tbl_eselon c ON c.te_id = a.pic
										WHERE a.idprogram = '.$new4->tps_id.' AND a.`status` = 0');

			foreach($result6 as $new6){


				if($new4->tps_id == $new6->idprogram){

					$new4->indikatorprogram = $result6;
				}
			}

		array_push($hasil, $new4);
		}
		$hasil = (object) $hasil;

		return $hasil;
	}

	public function listkegiatan($id){
		$where = '';

		if($this->user->role_id == 99 || $this->user->role_id == 97){
			$where = 'AND a.`te_isi` = '.$id.' ';
		}else{
			$where = 'AND a.`te_isi` = '.$this->user->dinas_id.' ';
		}
		$hasil = [];
		$result4 = DB::select('SELECT  a.tkp_id, a.tkp_kegiatan_program, a.tkp_keterangan, a.te_isi
		FROM tbl_kegiatan_program a
		WHERE a.tahun = '.$_COOKIE['tahun'].' '.$where);

		foreach($result4 as $new4){

			$result6 	= DB::select('SELECT a.*, c.te_jabatan, d.ta_anggaran FROM tbl_indikator_kegiatan a
										LEFT JOIN tbl_eselon c ON c.te_id = a.pic
										LEFT JOIN tbl_anggaran d on a.tik_id = d.idindkegiatan
										WHERE a.idkegiatan = '.$new4->tkp_id.' AND a.`tik_status` = 0');

			foreach($result6 as $new6){


				if($new4->tkp_id == $new6->idkegiatan){

					$new4->indikatorkegiatan = $result6;
				}
			}

		array_push($hasil, $new4);
		}
		$hasil = (object) $hasil;

		return $hasil;
	}

	public function saveindikatorsasaranpic(Request $request){
		$input = $request->all();
		DB::beginTransaction();
		try {

			DB::table('tbl_indikator_sasaran')
			->where('tis_id', $input['idindikatorsasaran'])
			->update([
						'pic' => $input['pic']
				]
			);
			 DB::table('tbl_indikator_sasaran_pk')
			->where('tis_id', $input['idindikatorsasaran'])
			->update([
						'pic' => $input['pic']
				]
			);
			DB::commit();
			$result = array( 'success' => true, 'message' => 'PIC Sukses Terpilih!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);
	}
	public function saveindikatorprogrampic(Request $request){
		$input = $request->all();
		// dd($input);
		DB::beginTransaction();
		try {

			DB::table('tbl_indikator_program')
			->where('tip_id', $input['idindikatorsasaran'])
			->update([
						'pic' => $input['pic']
				]
			);
			DB::commit();
			$result = array( 'success' => true, 'message' => 'PIC Sukses Terpilih!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);
	}
	public function saveindikatorkegiatanpic(Request $request){
		$input = $request->all();
		DB::beginTransaction();
		try {

			DB::table('tbl_indikator_kegiatan')
			->where('tik_id', $input['idindikatorsasaran'])
			->update([
						'pic' => $input['pic']
				]
			);
			DB::commit();
			$result = array( 'success' => true, 'message' => 'PIC Sukses Terpilih!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);
	}

    public function printview($id)
    {
        $dinas =   $this->detaildinas($id);
        $listiku = $this->listsasaran($id);
        $sa = 1;
        $isi = '';

        foreach($listiku as $newsasaran){
            $dalem = '';
            if(isset($newsasaran->indikatorsasaran)){
                $sas = 1;
                foreach($newsasaran->indikatorsasaran as $indisasaran){
                 $dalem .= '<tr>'
                            .'<td>'.$sas.'</td>'
                            .'<td>'.$indisasaran->tis_indikator_sasaran.'</td>'
                            .' <td>'.$indisasaran->tis_satuan.'</td>'
                            .'<td>'.$indisasaran->formula.'</td>'
                       .'</tr>';
                $sas++;
                }

            }

        $isi .= '<li style="display:block">'.$sa.' '.$newsasaran->tst_sasaran_tujuan
                .'<ul style="margin-left: 20px">'
                .' <h4 style="margin-left:-30px; color: #336600; margin-top:10px">Indikator Sasaran </h4>'
                .'  </ul>'
                .'  <ol>'
                .'    <li style="display:block">'
                .'        <table class="table table-bordered">'
                .'            <thead>'
                .'                <tr>'
                .'                   <th><center>NO</center></th>'
                .'                   <th><center>Indikator Sasaran Strategis</center></th>'
                .'                   <th><center>Satuan</center></th>'
                .'                   <th><center>Formula</center></th>'
                .'               </tr>'
                .'           </thead>'
                .'           <tbody>'
                .$dalem
                .'</tbody>'
                .'</table>'
                .' </li>'
                .'</ol></li>';

                $sa++;
            }



        $html='<!DOCTYPE html>
        <html lang="en">
        <head>
        	<title>PENGUKURAN KINERJA</title>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
            <style>
            .page-break {
                page-break-after: always;
            }
            </style>
        </head>
        <body>
        <div class="container">
            <center>
                <h3>PENGUKURAN KINERJA TAHUN '.$_COOKIE['tahun'].'</h3>
                <h3>KEPALA '.$dinas[0]->td_dinas.'</h3>
            </center>
            <br/>
            <br/>
            <br/>
            <p>Dalam rangka mewujudkan manajemen Pemerintahan yang efektif, transparan,
            dan akuntabel serta berorientasi pada hasil, kami yang bertandatangan di bawah ini :
            </p>
            <div class="table-responsive">
                <table class="table">
                    <tbody>
                        <tr>
                            <td style="width:25%; text-align:left">Nama</td>
                            <td style="width:50%; text-align:center;">: </td>
                            <td style="width:25%; text-align:right">(Kepala Perangkat Daerah)</td>
                        </tr>
                        <tr>
                            <td style="width:25%; text-align:left">Jabatan</td>
                            <td style="width:50%; text-align:center;">: </td>
                            <td style="width:25%; text-align:right">KEPALA '.$dinas[0]->td_dinas.'</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <p>Selanjutnya disebut pihak pertama</p>

            <div class="table-responsive">
                <table class="table">
                    <tbody>
                        <tr>
                            <td style="width:25%; text-align:left">Nama</td>
                            <td style="width:50%; text-align:center;">: </td>
                            <td style="width:25%; text-align:right">(Wali Kota)</td>
                        </tr>
                        <tr>
                            <td style="width:25%; text-align:left">Jabatan</td>
                            <td style="width:50%; text-align:center;">: </td>
                            <td style="width:25%; text-align:right">WALI KOTA CIMAHI</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <p>Selaku atasan pihak pertama, selanjutnya disebut pihak kedua	</p>
            <br/>
            <br/>
            <p>Pihak pertama berjanji akan mewujudkan target kinerja yang seharusnya sesuai lampiran perjanjian ini,
            dalam rangka mencapai target kinerja jangka menengah seperti yang telah ditetapkan dalam dokumen perencanaan.
            Keberhasilan dan kegagalan pencapaian target kinerja tersebut menjadi tanggungjawab Kami.</p>
            <br/>

            <p>Pihak Kedua akan melakukan supervisi yang diperlukan serta akan melakukan evaluasi
            terhadap capaian kinerja dari perjanjian ini dan mengambil tindakan yang diperlukan
            dalam rangka pemberian penghargaan dan sanksi.</p>
            <br/>
            <br/>
            <table class="table">
                    <tbody>
                        <tr>
                            <td style="width:25%; text-align:left"></td>
                            <td style="width:50%; text-align:center;"> </td>
                            <td style="width:25%; text-align:center">Cimahi,'.date('d/m/Y').'</td>
                        </tr>
                        <tr>
                            <td style="width:25%; text-align:center">Pihak kedua,</td>
                            <td style="width:50%; text-align:center;"> </td>
                            <td style="width:25%; text-align:center">Pihak pertama,</td>
                        </tr>
                        <tr>
                            <td style="width:25%; text-align:center"><b>WALI KOTA CIMAHI,</b></td>
                            <td style="width:50%; text-align:center;"> </td>
                            <td style="width:25%; text-align:center"><b>KEPALA <br/>'.$dinas[0]->td_dinas.'</b></td>
                        </tr>
                        <tr>
                            <td style="width:25%; text-align:center"></td>
                            <td style="width:50%; text-align:center"> </td>
                            <td style="width:25%; text-align:center"></td>
                        </tr>
                        <tr>
                            <td style="width:25%; text-align:center"></td>
                            <td style="width:50%; text-align:center"> </td>
                            <td style="width:25%; text-align:center"></td>
                        </tr>
                        <tr>
                            <td style="width:25%; text-align:center"></td>
                            <td style="width:50%; text-align:center"> </td>
                            <td style="width:25%; text-align:center"></td>
                        </tr>

                        <tr>
                            <td style="width:25%; text-align:center"><b>(Nama Wali Kota)</b></td>
                            <td style="width:50%; text-align:center;"> </td>
                            <td style="width:25%; text-align:center">Nama <b><br/> Pangkat <br/>NIP</b></td>
                        </tr>

                    </tbody>
                </table>
        </div>
        <div class="page-break">
            <div class="container">
                <center>
                    <h1>'.$dinas[0]->td_dinas.' TAHUN '.$_COOKIE['tahun'].'</h1>
                </center>

                <ul style="margin-left: 20px">
                <h4 style="margin-left:-30px; color: #336600; margin-top:10px">Sasaran Strategis </h4>
            </ul>
            <ol  style="margin-left: 20px">
                '.$isi.'
            </ol>
            </div>
        </div>
    </body>
        </html>';

        // $pdf = PDF::loadHTML($html)->setPaper('a4', 'potrait');
        $pdf = PDF::loadHTML($html);
	    return  $pdf->stream('PENGUKURAN KINERJA - '.date('d/m/Y').'.pdf');
	    // return $pdf->download($title.' '.$detail_corporate->name.' - '.date('d/m/Y').'.pdf');
    }

    public function printperjanjiankinerja(Request $request)
    {
        $dinas =   $this->detaildinas($request->id);
        $listiku = $this->listsasaran($request->id);
        $sa = 1;
        $i = 1;
        $isi = '';

        foreach($listiku as $newsasaran){
            $totalindikatorsasaran=0;

            if(isset($newsasaran->tst_sasaran_tujuan)){
                $isi2 = '';
                if(isset($newsasaran->indikatorsasaran)){
                    foreach($newsasaran->indikatorsasaran as $s=>$indikatorsasaran){
                            $totalindikatorsasaran = array_count_values(array_column($newsasaran->indikatorsasaran, 'idsasaran'))[$newsasaran->tst_id];
                            if($s == 0){

                                $isi2 .= '<td>'.$indikatorsasaran->tis_indikator_sasaran.'</td>'
                                .'<td>'.$indikatorsasaran->tis_target.'</td></tr>';
                            }else{
                                $isi2 .= '<tr ><td>'.$indikatorsasaran->tis_indikator_sasaran.'</td>'
                                .'<td>'.$indikatorsasaran->tis_target.'</td></tr>';
                            }
                    }

                }

                $isi .= '<tr>'
                            .'<td rowspan="'.$totalindikatorsasaran.'">'.$i.'</td>'
                            .'<td rowspan="'.$totalindikatorsasaran.'">'.$newsasaran->tst_sasaran_tujuan.' </td>'
                            .$isi2;

            }else{
                $isi .= '<tr>'
                            .'<td>'.$i.'</td>'
                            .'<td>'.$newsasaran->tst_sasaran_tujuan.'</td>'
                            .'<td></td>'
                            .'<td></td>'
                        .'</tr>';
            }
            $i++;
        }

        $isiProgram = '';

        $result1 	= DB::select('SELECT a.*
		FROM tbl_program_sasaran a
		LEFT JOIN tbl_indikator_sasaran_pk b ON a.`idindisasaran` = b.`tis_id`
		LEFT JOIN tbl_sasaran_tujuan c ON b.`idsasaran` = c.`tst_id`
		WHERE a.tahun = '.$_COOKIE['tahun'].' AND c.`tst_id` = '.$request->id.' AND a.status = 0');
        $totalAnggaran = 0;
        $a=1;
        foreach($result1 as $program){
            $result2 	= DB::select('SELECT SUM(`ta_anggaran`) total_anggaran FROM `tbl_anggaran` WHERE idindkegiatan IN (
                SELECT tik_id FROM tbl_indikator_kegiatan
                LEFT JOIN `tbl_kegiatan_program`ON tbl_kegiatan_program.`tkp_id` = tbl_indikator_kegiatan.`idkegiatan`
                LEFT JOIN `tbl_indikator_program` ON tbl_indikator_program.`tip_id` = tbl_kegiatan_program.`idindiprogram`
                LEFT JOIN `tbl_program_sasaran` ON tbl_program_sasaran.`tps_id` = tbl_indikator_program.`idprogram`
                WHERE tbl_program_sasaran.`tps_id` = '.$program->tps_id.')');

            if (isset($result2[0])){
                $totalAnggaran = $result2[0]->total_anggaran;
            }
            $isiProgram .= '<tr>'
                .'<td>'.$a.'</td>'
                .'<td>'.$program->tps_program_sasaran.'</td>'
                .'<td>'.$totalAnggaran.'</td>'
                .'<td>'.$program->tps_keterangan.'</td>'
            .'</tr>';
            $a++;
        }


        if ($request->id == 1){
            $pihak_pertama = 'WALIKOTA CIMAHI';
        }
        $pihak_pertama = str_replace("Dinas","KEPALA DINAS",$dinas[0]->td_dinas);
        $pihak_pertama = str_replace("Sekretariat","SEKERTARIS",$pihak_pertama);
        $pihak_pertama = str_replace("Kecamatan","CAMAT",$pihak_pertama);
        $pihak_pertama = str_replace("Inspektorat","INSPEKTUR",$pihak_pertama);
        $pihak_pertama = str_replace("RSUD","DIREKTUR RSUD",$pihak_pertama);
        $pihak_pertama = str_replace("Sekretariat DPRD","SEKERTARIS DEWAN PERWAKILAN RAKYAT DAERAH",$pihak_pertama);
        $pihak_pertama = str_replace("Badan Penanggulangan Bencana Daerah"," KEPALA PELAKSANA BADAN PENANGGULANGAN BENCANA DAERAH",$pihak_pertama);

        $pihak_pertama = strtoupper($pihak_pertama);


        $pihak_kedua = str_replace("Dinas","KEPALA DINAS",$dinas[0]->td_dinas);
        $pihak_kedua = str_replace("Sekretariat","SEKERTARIS",$pihak_kedua);
        $pihak_kedua = str_replace("Kecamatan","CAMAT",$pihak_kedua);
        $pihak_kedua = str_replace("Inspektorat","INSPEKTUR",$pihak_kedua);
        $pihak_kedua = str_replace("RSUD","DIREKTUR RSUD",$pihak_kedua);
        $pihak_kedua = str_replace("Sekretariat DPRD","SEKERTARIS DEWAN PERWAKILAN RAKYAT DAERAH",$pihak_kedua);
        $pihak_kedua = str_replace("Badan Penanggulangan Bencana Daerah"," KEPALA PELAKSANA BADAN PENANGGULANGAN BENCANA DAERAH",$pihak_kedua);
        $pihak_kedua = strtoupper($request->jabatan_pihak_kedua);


        $isibr = '';

        if($request->id == 1){
            $isibr = '
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>';
        }


        $namespika = '<table class="table borderless" style="margin-left: -7px">
                <tbody>
                    <tr>
                        <td style="width:10%; text-align:left; border: none">Nama</td>
                        <td style="width:10%; text-align:center; border: none">: </td>
                        <td style="width:75%; text-align:left; border: none"><b>'.$request->pihak_pertama.'</b></td>
                    </tr>
                    <tr >
                        <td style="width:10%; text-align:left; border: none">Jabatan</td>
                        <td style="width:10%; text-align:center; border: none">: </td>
                        <td style="width:75%; text-align:left; border: none">'.$pihak_pertama.'</td>
                    </tr>
                </tbody>
            </table>
            <p style="margin-top: -25px">Selanjutnya disebut pihak pertama</p>';
        $isittd = '<table class="table">
                        <tbody>
                            <tr>
                                <td style="width:25%; text-align:left; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none">Cimahi,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$request->bulan.' '.$_COOKIE['tahun'].'</td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none">Pihak kedua,</td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none">Pihak pertama,</td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"><b>'.$pihak_kedua.',</b></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"><b>'.$pihak_pertama.'</b></td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"></td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"></td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"></td>
                            </tr>

                            <tr>
                                <td style="width:60%; text-align:center; border: none"><b>'.$request->pihak_kedua.'</b></td>
                                <td style="width:25%; text-align:center; border: none"> </td>
                                <td style="width:60%; text-align:center; border: none"><b>'.$request->pihak_pertama.'</b> <b><br/> '.$request->jabatan_pihak_pertama.' <br/>NIP. '.$request->nip.'</b></td>
                            </tr>

                        </tbody>
                    </table>';
        if ($dinas[0]->td_id == 1){
            $namespika = '<p >Selanjutnya disebut pihak pertama</p>';
            $isittd = '<table class="table">
            <tbody>
                <tr>
                    <td style="width:25%; text-align:left; border: none"></td>
                    <td style="width:50%; text-align:center; border: none"> </td>
                    <td style="width:25%; text-align:center; border: none">Cimahi,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$request->bulan.' '.$_COOKIE['tahun'].'</td>
                </tr>
                <tr>
                    <td style="width:25%; text-align:center; border: none"></td>
                    <td style="width:50%; text-align:center; border: none"> </td>
                    <td style="width:25%; text-align:center; border: none">Pihak pertama,</td>
                </tr>
                <tr>
                    <td style="width:25%; text-align:center; border: none"></td>
                    <td style="width:50%; text-align:center; border: none"></td>
                    <td style="width:25%; text-align:center; border: none"><b>WALIKOTA CIMAHI</b></td>
                </tr>
                <tr>
                    <td style="width:25%; text-align:center; border: none"></td>
                    <td style="width:50%; text-align:center; border: none"> </td>
                    <td style="width:25%; text-align:center; border: none"></td>
                </tr>
                <tr>
                    <td style="width:25%; text-align:center; border: none"></td>
                    <td style="width:50%; text-align:center; border: none"> </td>
                    <td style="width:25%; text-align:center; border: none"></td>
                </tr>
                <tr>
                    <td style="width:25%; text-align:center; border: none"></td>
                    <td style="width:50%; text-align:center; border: none"> </td>
                    <td style="width:25%; text-align:center; border: none"></td>
                </tr>

                <tr>
                    <td style="width:60%; text-align:center; border: none"></td>
                    <td style="width:25%; text-align:center; border: none"> </td>
                    <td style="width:60%; text-align:center; border: none"><b>'.$request->pihak_pertama.'  <br/>'.$request->nip.'</b></td>
                </tr>

            </tbody>
        </table>';
        }

        $html='<!DOCTYPE html>
        <html lang="en">
        <head>
        	<title>
                PERJANJIAN KINERJA
            </title>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
            <style>
            .page-break {
                page-break-after: always;
            }
            p {
                line-height: 1.5;
            }
            </style>
        </head>
        <body style="font-size: 12px">
            <div class="container" style="font-size: 12px;  font-family: Arial, sans-serif;  line-height: 2.5pt; margin-left: 2.3cm; margin-top: 1.3cm; margin-bottom: 0.5cm; margin-right: 2.3cm">
                <center>
                    <img src="'.public_path('img/Logo-Cimahi.png').'" style="width:auto; height: 100px"/>
                    <h4>
                        PERJANJIAN KINERJA TAHUN '.$_COOKIE['tahun'].'</h4>
                    <h4>'.$pihak_pertama.'</h4>

                </center>
                <p style="padding-top:1cm">
                    Dalam rangka mewujudkan manajemen Pemerintahan yang efektif, transparan, dan akuntabel serta berorientasi pada hasil, kami yang bertandatangan di bawah ini :
                </p>
                '.$namespika.'


                <table class="table borderless" style="margin-left: -7px">
                    <tbody>
                        <tr>
                            <td style="width:10%; text-align:left; border: none">Nama</td>
                            <td style="width:10%; text-align:center; border: none">: </td>
                            <td style="width:25%; text-align:left; border: none"><b>'.$request->pihak_kedua.'</b></td>
                        </tr>
                        <tr>
                            <td style="width:10%; text-align:left; border: none">Jabatan</td>
                            <td style="width:10%; text-align:center; border: none">: </td>
                            <td style="width:75%; text-align:left; border: none">'.$pihak_kedua.'</td>
                        </tr>
                    </tbody>
                </table>
                <p style="margin-top: -25px">Selaku atasan pihak pertama, selanjutnya disebut pihak kedua	</p>
                <br/>
                <br/>
                <p>Pihak pertama berjanji akan mewujudkan target kinerja yang seharusnya sesuai lampiran perjanjian ini, dalam rangka mencapai target kinerja jangka menengah seperti yang telah ditetapkan dalam dokumen perencanaan. Keberhasilan dan kegagalan pencapaian target kinerja tersebut menjadi tanggung jawab kami.</p>
                <br/>

                <p>Pihak kedua akan melakukan supervisi yang diperlukan serta akan melakukan evaluasi terhadap capaian kinerja dari perjanjian ini dan mengambil tindakan yang diperlukan dalam rangka pemberian penghargaan dan sanksi.</p>
                <br/>
                <br/>
                '.$isittd.'
            </div>

            <br/>
            <br/>
            '.$isibr.'
            <div class="page-break">
                <div class="container" class="container" style="font-size: 12px;  font-family: Arial, Helvetica, sans-serif;  line-height: 2.5pt; margin-left: 2.3cm; margin-top: 1.3cm; margin-bottom: 0.5cm; margin-right: 2.3cm">
                    <center>
                        <h4>
                            LAMPIRAN PERJANJIAN KINERJA TAHUN '.$_COOKIE['tahun'].'
                        </h4>
                        <h4>
                            KEPALA '.strtoupper($dinas[0]->td_dinas).'
                        </h4>
                    </center>
                    <table class="table table-bordered" style="padding-top:1cm">
                        <thead>
                            <tr>
                                <th><center>NO</center></th>
                                <th><center>SASARAN STRATEGIS</center></th>
                                <th><center>INDIKATOR KINERJA</center></th>
                                <th><center>TARGET</center></th>
                            </tr>
                        </thead>
                        <tbody>
                            '.$isi.'
                        </tbody>
                    </table>

                    <br/>
                    <br/>
                    <h5>JUMLAH ANGGARAN :</h5>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th><center>NO</center></th>
                                <th><center>PROGRAM</center></th>
                                <th><center>ANGGARAN</center></th>
                                <th><center>KETERANGAN</center></th>
                            </tr>
                        </thead>
                        <tbody>
                            '.$isiProgram.'
                        </tbody>
                    </table>


                    '.$isittd.'
                </div>
            </div>
        </body>
        </html>';

        // $pdf = PDF::loadHTML($html)->setPaper('a4', 'potrait');
        $pdf = PDF::loadHTML($html)->setPaper('f4', 'potrait');
	    return  $pdf->stream('PENGUKURAN KINERJA - '.date('d/m/Y').'.pdf');
	    // return $pdf->download($title.' '.$detail_corporate->name.' - '.date('d/m/Y').'.pdf');
    }


    public function detaildinas($id){
		$hasil = DB::select('SELECT td_id,
									td_dinas,
									td_keterangan
								FROM tbl_dinas
								WHERE
                                    td_id = '.$id.'
                                    AND
									status = 1
								ORDER BY `index`');
		return $hasil;
	}


}
