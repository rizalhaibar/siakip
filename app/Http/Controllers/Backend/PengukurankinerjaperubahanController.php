<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserValidation;
use App\Models\Role;
use Carbon\Carbon;

/**
 * Class UserController.
 */
class PengukurankinerjaperubahanController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
	 
	function __construct()
	{
		$this->middleware(['auth', 'checkMenuRole']);
	}
	
	public function index()
    {
		return view('rpjmd.index');
    }
	
	public function create()
    {
		$data['user_roles'] = Role::all();
		$data['religions']	= [
			'Islam',
			'Katolik',
			'Kristen',
			'Budha',
			'Hindu',
		];
		return view('user.create', compact('data'));
	}
	
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:191',
            'username' => 'required|string|max:191|unique:user_validations',
            'email' => 'required|string|email|max:191|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'role_id' => 'required|integer',
            'birth_date' => 'required|date',
			'birth_location' => 'required|string|max:191',
			'religion' => 'required|string|max:191',
            'address' => 'required|string|max:191',
            'phone_number' => 'required|numeric',
        ]);
	}
	
    public function register(Request $request)
    {
		$this->validator($request->all())->validate();
		$user = User::create([
            'name' => $request->name,
            'email' => $request->email,
			'role_id' => $request->role_id,
			'birth_date' => $request->birth_date,
			'birth_location' => $request->birth_location,
			'religion' => $request->religion,
			'address' => $request->address,
			'phone_number' => $request->phone_number,
		]);
		$user_validation = UserValidation::create([
            'user_id' => $user->id,
            'username' => $request->username,
            'password' => Hash::make($request->password),
		]);
		return back()->with('success', 'User created successfully.');
	}
	
	public function edit($id)
    {
		$data['detail']		= User::with('userValidation')->where('id', $id)->firstOrFail();
		$data['user_roles']	= Role::all();
		$data['religions']	= [
			'Islam',
			'Katolik',
			'Kristen',
			'Budha',
			'Hindu',
		];
		return view('user.edit', compact('data'));
	}

	public function update($id)
	{
		// dd(request()->all());
		$input = request()->validate([
			'name' => 'required|string|max:191',
            'username' => 'required|string|max:191|unique:user_validations,username,'.$id,
            'email' => 'required|string|email|max:191|unique:users,email,'.$id,
            'password' => 'required|string|min:6|confirmed',
            'role_id' => 'required|integer',
            'birth_date' => 'required|date',
			'birth_location' => 'required|string|max:191',
			'religion' => 'required|string|max:191',
            'address' => 'required|string|max:191',
            'phone_number' => 'required|numeric',
		]);
		$input = request()->all();
		$input['password'] = Hash::make($input['password']);
		$user = User::where('id', $id)->update([
            'name' => $input['name'],
            'email' => $input['email'],
			'role_id' => $input['role_id'],
			'birth_date' => $input['birth_date'],
			'birth_location' => $input['birth_location'],
			'religion' => $input['religion'],
			'address' => $input['address'],
			'phone_number' => $input['phone_number'],
		]);
		$user_validation = UserValidation::where('user_id', $id)->update([
            'username' => $input['username'],
            'password' => $input['password'],
		]);

		return back()->with('success', 'User Updated successfully.');
	}

	public function delete($id)
    {
		$user = User::where('id', $id)->delete();
		$userValidation = UserValidation::where('user_id', $id)->delete();
		$users = User::paginate(5);
		return back()->with('success', 'User deleted successfully.')->with('users', $users);
	}
	
}
