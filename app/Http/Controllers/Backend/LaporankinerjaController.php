<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserValidation;
use App\Models\Role;
use Carbon\Carbon;
use DB;
use Redirect;
/**
 * Class UserController.
 */
class LaporankinerjaController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
	 
	function __construct()
	{
		$this->middleware(['auth']);
		
        $this->middleware(function ($request, $next)
        {
            $this->user = session('data')->user;
            return $next($request);
        });
	}
	
	public function index()
    {
		if($this->user->role_id == 99 || $this->user->role_id == 97){
		$data['breadcrumb'] = [
            'parent' => 'Laporan Kinerja',
            'child' => 'List Laporan Kinerja'
		];
		$data['listdinas'] = $this->listdinas();
		return view('backend.laporankinerja.index', $data);

		}else{
			$id = $this->user->dinas_id;
			
			$url = Redirect::route('backend.laporankinerja.detail',array('id' => $id,'idsasaran' => 0));
			return $url;
		}
	}
	
	public function detail($id, $idsasaran)
    {
		$data['breadcrumb'] = [
            'parent' => 'Detail Laporan Kinerja',
            'child' => ''
		];
		$data['id'] = $id;
		$data['idsasaran'] = $idsasaran;
		$data['listsasaran'] = $this->listsasaran($id);
		if($idsasaran != 0){
			$data['detail'] = DB::table('tbl_sasaran_tujuan')->where('tst_id',$idsasaran)->first();
			
			$data['laporan'] = DB::table('tbl_pelaporan')->where('idsasaran',$idsasaran)->first();

		}
		return view('backend.laporankinerja.detail', $data);
	}
	
	
   
	public function listdinas(){ 
		$hasil = DB::select('SELECT td_id, 
									td_dinas, 
									td_keterangan 
								FROM tbl_dinas
								WHERE 
									status = 1 
								ORDER BY `index`');
		return $hasil;
	}
	

	public function listsasaran($id){
		$where = '';
		if($this->user->role_id == 99 || $this->user->role_id == 97 ){
			$where = 'aa.`iddinas` = '.$id.' AND';
		}else{
			$where = 'aa.`iddinas` = '.$this->user->dinas_id.' AND';
		}

		$result4 	= DB::select('SELECT a.* FROM tbl_sasaran_tujuan a
		
								LEFT JOIN tbl_tujuan cc ON cc.tt_id = a.idtujuan
								LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
								LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
								WHERE '.$where.' a.tst_status = 0');
		return $result4;
	}


	public function listlaporan($id){ 
		$hasil = DB::select('SELECT * FROM tbl_pelaporan WHERE idsasaran = '.$id);
		return $hasil;
	}
	

	public function savelaporan(Request $request){ 
		$input = $request->all();
		$sasaran = DB::table('tbl_pelaporan')->where('idsasaran',$input['id'])->first();
		// dd($input);

		DB::beginTransaction();
		try {

			if($sasaran){
				DB::table('tbl_pelaporan')
				->where('idsasaran', $input['id'])
				->update(['tp_judul' => $input['judul'],'tp_content' => $input['isi']]);
				
				$result = array( 'success' => true, 'message' => 'Laporan Sukses Terupdate!');

				
			}else{
				DB::table('tbl_pelaporan')->insert(
					['idsasaran' => $input['id'], 'tp_judul' => $input['judul'],'tp_content' => $input['isi'], 'tahun' => $_COOKIE['tahun']]
				);
				
				$result = array( 'success' => true, 'message' => 'Laporan Sukses Terisi!');
			}
		
			DB::commit();
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);
	}
}
