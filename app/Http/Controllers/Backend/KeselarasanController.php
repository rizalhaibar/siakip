<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserValidation;
use App\Models\Role;
use Carbon\Carbon;
use DB;
use Redirect;
use PDF;
/**
 * Class UserController.
 */
class KeselarasanController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */

	function __construct()
	{
		$this->middleware(['auth']);

        $this->middleware(function ($request, $next)
        {
            $this->user = session('data')->user;
            return $next($request);
        });
	}

	public function index()
    {

		if($this->user->role_id == 99 || $this->user->role_id == 97){
			$data['breadcrumb'] = [
				'parent' => 'Keselarasan',
				'child' => 'List Dinas'
			];
			$data['listdinas'] = $this->listdinas();

			return view('backend.keselarasan.index', $data);
		}else{
			$id = $this->user->dinas_id;
			$id = \Crypt::encrypt($id);
			$url = Redirect::route('backend.keselarasan.home',array('id' => $id));
			return $url;
		}
	}

	public function home($iddinas)
    {

		$iddinas = \Crypt::decrypt($iddinas);
		$data['breadcrumb'] = [
			'parent' => 'Keselarasan',
			'child' => 'List Keselarasan'
		];
		$data['id'] = $iddinas;
		$data['keselarasan'] = $this->keselarasan($iddinas);

		return view('backend.keselarasan.home', $data);
	}


    public function printkesel($id)
    {
		$data['id'] = $id;

		return view('backend.keselarasan.print', $data);
	}


	public function listdinas(){
		$hasil = DB::select('SELECT td_id,
									td_dinas,
									td_keterangan
								FROM tbl_dinas
								WHERE
									status = 1

                                    and deleted_at is null
								ORDER BY `index`');
		return $hasil;
	}

	public function keselarasan($iddinas){


		$hasil = array();

		$result1 	= DB::select('SELECT a.*
		FROM tbl_tujuan a
		LEFT JOIN tbl_misi b ON a.`idmisi` = b.`tm_id`
		LEFT JOIN tbl_visi c ON b.`idvisi` = c.`tv_id`
		WHERE c.`iddinas` = '. $iddinas.'
		AND a.tahun = '.$_COOKIE['tahun'].'
		AND a.`status` = 0');


		foreach($result1 as $tujuan){
			$result2 	= DB::select('SELECT * FROM tbl_sasaran_tujuan WHERE idtujuan = '.$tujuan->tt_id.' AND tst_status = 0');

			foreach($result2 as $sasaran){
				if($tujuan->tt_id == $sasaran->idtujuan){
					$result3 	= DB::select('SELECT * FROM tbl_indikator_sasaran_pk WHERE idsasaran = '.$sasaran->tst_id.' AND tis_status = 0');

					foreach($result3 as $indikatorsasaran){
						if($sasaran->tst_id == $indikatorsasaran->idsasaran){
							$result4 	= DB::select('SELECT a.*
							FROM tbl_program_sasaran a
							WHERE a.tahun = '.$_COOKIE['tahun'].' AND idindisasaran = '.$indikatorsasaran->tis_id.' AND status=0');


							foreach ($result4 as $program) {
                                if($indikatorsasaran->tis_id == $program->idindisasaran){

									$result5 	= DB::select('SELECT a.* FROM tbl_indikator_program a
										WHERE a.idprogram = '.$program->tps_id.' AND a.`status` = 0');

										foreach($result5 as $indikatorprogram){
											if($program->tps_id == $indikatorprogram->idprogram){
												$result6 	= DB::select('SELECT a.*
												FROM tbl_kegiatan_program a
												WHERE a.idindiprogram = '.$indikatorprogram->tip_id.' AND a.`status` = 0');

												foreach($result6 as $kegiatan){
													if($kegiatan->idindiprogram == $indikatorprogram->tip_id){
														$result7 	= DB::select('SELECT a.*
														FROM tbl_indikator_kegiatan a
														WHERE a.idkegiatan = '.$kegiatan->tkp_id.' AND a.`tik_status` = 0');
														foreach($result7 as $indikatorkegiatan){
															if($kegiatan->tkp_id == $indikatorkegiatan->idkegiatan){
																$kegiatan->indikatorkegiatan = $result7;
															}
														}
														$indikatorprogram->kegiatan[] = $kegiatan;
													}
												}

												$program->indikatorprogram[] = $indikatorprogram;
											}
										}
									$indikatorsasaran->program[] = $program;
                                }
                            }



							$sasaran->indikatorsasaran[] = $indikatorsasaran;
						}
					}
					$tujuan->sasaran[] = $sasaran;
				}

			}
			array_push($hasil, $tujuan);
		}
		// dd($hasil);
		$hasil = (object) $hasil;
		return $hasil;
	}

    public function detaildinas($id){
		$hasil = DB::select('SELECT td_id,
									td_dinas,
									td_keterangan
								FROM tbl_dinas
								WHERE
                                    td_id = '.$id.'
                                    AND
									status = 1
								ORDER BY `index`');
		return $hasil;
	}

    public function print(Request $request)
    {
        $keselarasan = $this->keselarasan($request->id);
        $dinas =   $this->detaildinas($request->id);

        $i=1;
        $html = '';
        $isisasaran = '';
        $isiindikatorsasaran = '';
        $isiprogram = '';
        $isiindikatorprogram = '';
        $isikegiatan = '';
        $isiindikatorkegiatan = '';


                                foreach($keselarasan as $tujuan){


                                    $totalsasaran=0;
                                    $totalsasaranbawahmentah=0;
                                    $totalsasaranbawah=0;
                                    if(isset($tujuan->sasaran)){
                                        foreach($tujuan->sasaran as $s=>$sasaran){

                                            $totalindikatorsasaran=0;
                                            $totalindikatorsasaranbawahmentah=0;
                                            $totalindikatorsasaranbawah=0;
                                            if(isset($sasaran->indikatorsasaran)){
                                                foreach($sasaran->indikatorsasaran as $is=>$indikatorsasaran){

                                                    $totalprogram=0;
                                                    $totalprogrambawahmentah=0;
                                                    $totalprogrambawah=0;
                                                    if(isset($indikatorsasaran->program)){
                                                        foreach($indikatorsasaran->program as $p=>$program){
                                                            $totalindikatorprogram=0;
                                                            $totalindikatorprogrambawahmentah=0;
                                                            $totalindikatorprogrambawah=0;
                                                            if(isset($program->indikatorprogram)){
                                                                foreach($program->indikatorprogram as $ip=>$indikatorprogram){
                                                                    $totalkegiatan=0;
                                                                    $totalkegiatanbawah=0;
                                                                    if(isset($indikatorprogram->kegiatan)){
                                                                        $totalindikatorkegiatan=0;
                                                                        $totalindikatorkegiatanbawah=0;
                                                                        foreach($indikatorprogram->kegiatan as $k=>$kegiatan){
                                                                            if(isset($kegiatan->indikatorkegiatan)){
                                                                                foreach($kegiatan->indikatorkegiatan as $ik=>$indikatorkegiatan){

                                                                                    if($ik == 0){
                                                                                        $totalindikatorkegiatan += array_count_values(array_column($kegiatan->indikatorkegiatan, 'idkegiatan'))[$kegiatan->tkp_id];
                                                                                        $totalindikatorkegiatanbawah = 1;
                                                                                        $isiindikatorkegiatan = '<td >'.$indikatorkegiatan->tik_indikator_kegiatan.' </td>'
                                                                                                    .'</tr>';
                                                                                    }else{
                                                                                        $totalindikatorkegiatanbawah += 1;

                                                                                            $isiindikatorkegiatan .= '<tr ><td >'.$indikatorkegiatan->tik_indikator_kegiatan.' </span></td>'
                                                                                                   .'</tr>';

                                                                                    }
                                                                                }

                                                                                if($k == 0){

                                                                                    $totalkegiatan += $totalindikatorkegiatan;
                                                                                    $isikegiatan = '<td rowspan="'.$totalindikatorkegiatan.'">'.$kegiatan->tkp_kegiatan_program.' </td>'
                                                                                                    .$isiindikatorkegiatan
                                                                                                .'</tr>';
                                                                                }else{

                                                                                    $totalkegiatanbawah += $totalindikatorkegiatanbawah;

                                                                                        $isikegiatan .= '<tr ><td rowspan="'.$totalindikatorkegiatanbawah.'">'.$kegiatan->tkp_kegiatan_program.' </span></td>'
                                                                                                    .$isiindikatorkegiatan
                                                                                                .'</tr>';
                                                                                }

                                                                            }else{
                                                                                if($k == 0){
                                                                                    $totalkegiatan+=1;
                                                                                    $isikegiatan = '<td>'.$kegiatan->tkp_kegiatan_program.' </td>'
                                                                                                    .'<td></td>'
                                                                                                .'</tr>';
                                                                                }else{
                                                                                    $totalkegiatanbawah = $totalkegiatanbawah + 1;

                                                                                        $isikegiatan .= '<tr ><td >'.$kegiatan->tkp_kegiatan_program.' </span></td>'
                                                                                                    .'<td></td>'
                                                                                                .'</tr>';
                                                                                }
                                                                            }
                                                                        }

                                                                        if($ip == 0){
                                                                            $totalindikatorprogram = $totalkegiatan + $totalkegiatanbawah;
                                                                            $isiindikatorprogram = '<td rowspan="'.$totalindikatorprogram.'">'.$indikatorprogram->tip_indikator_program.' </td>'
                                                                                            .$isikegiatan
                                                                                        .'</tr>';
                                                                        }else{
                                                                            $totalindikatorprogrambawahmentah = $totalkegiatan + $totalkegiatanbawah;
                                                                            $totalindikatorprogrambawah += $totalindikatorprogrambawahmentah;

                                                                                $isiindikatorprogram .= '<tr ><td rowspan="'.$totalindikatorprogrambawahmentah.'">'.$indikatorprogram->tip_indikator_program.' </span></td>'
                                                                                            .$isikegiatan
                                                                                        .'</tr>';
                                                                        }

                                                                    }else{
                                                                        if($ip == 0){
                                                                            $totalindikatorprogram+=1;
                                                                            $isiindikatorprogram = '<td>'.$indikatorprogram->tip_indikator_program.' </td>'
                                                                                            .'<td></td>'
                                                                                            .'<td></td>'
                                                                                        .'</tr>';
                                                                        }else{
                                                                            $totalindikatorprogrambawah+=1;

                                                                                $isiindikatorprogram .= '<tr ><td >'.$indikatorprogram->tip_indikator_program.' </span></td>'
                                                                                            .'<td></td>'
                                                                                            .'<td></td>'
                                                                                        .'</tr>';
                                                                        }
                                                                    }

                                                                }

                                                                if($p == 0){
                                                                    $totalprogram = $totalindikatorprogram + $totalindikatorprogrambawah;
                                                                    $isiprogram = '<td rowspan="'.$totalprogram.'">'.$program->tps_program_sasaran.' </td>'
                                                                                    .$isiindikatorprogram
                                                                                .'</tr>';
                                                                }else{
                                                                    $totalprogrambawahmentah = $totalindikatorprogram + $totalindikatorprogrambawah;
                                                                    $totalprogrambawah += $totalprogrambawahmentah;

                                                                        $isiprogram .= '<tr ><td rowspan="'.$totalprogrambawahmentah.'">'.$program->tps_program_sasaran.' </span></td>'
                                                                                    .$isiindikatorprogram
                                                                                .'</tr>';
                                                                }

                                                            }else{
                                                                if($p == 0){
                                                                    $totalprogram+=1;
                                                                    $isiprogram = '<td>'.$program->tps_program_sasaran.' </td>'
                                                                                    .'<td></td>'
                                                                                    .'<td></td>'
                                                                                    .'<td></td>'
                                                                                .'</tr>';
                                                                }else{
                                                                    $totalprogrambawah+=1;

                                                                        $isiprogram .= '<tr ><td >'.$program->tps_program_sasaran.' </span></td>'
                                                                                    .'<td></td>'
                                                                                    .'<td></td>'
                                                                                    .'<td></td>'
                                                                                .'</tr>';
                                                                }
                                                            }

                                                        }
                                                        if($is == 0){
                                                            $totalindikatorsasaran = $totalprogram + $totalprogrambawah;
                                                            $isiindikatorsasaran = '<td rowspan="'.$totalindikatorsasaran.'">'.$indikatorsasaran->tis_indikator_sasaran.' </td>'
                                                                            .$isiprogram
                                                                        .'</tr>';
                                                        }else{
                                                            $totalindikatorsasaranmentah = $totalprogram + $totalprogrambawah;
                                                            $totalindikatorsasaranbawah += $totalindikatorsasaranmentah;

                                                                $isiindikatorsasaran .= '<tr ><td rowspan="'.$totalindikatorsasaranmentah.'">'.$indikatorsasaran->tis_indikator_sasaran.' </span></td>'
                                                                            .$isiprogram
                                                                        .'</tr>';
                                                        }

                                                    }else{
                                                        if($is == 0){
                                                            $totalindikatorsasaran+=1;
                                                            $isiindikatorsasaran = '<td>'.$indikatorsasaran->tis_indikator_sasaran.' </td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                        .'</tr>';
                                                        }else{
                                                            $totalindikatorsasaranbawah+=1;

                                                                $isiindikatorsasaran .= '<tr ><td >'.$indikatorsasaran->tis_indikator_sasaran.' </span></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                        .'</tr>';
                                                        }
                                                    }
                                                }

                                                if($s == 0){
                                                    $totalsasaran = $totalindikatorsasaran + $totalindikatorsasaranbawah;
                                                    $isisasaran = '<td rowspan="'.$totalsasaran.'">'.$sasaran->tst_sasaran_tujuan.' </td>'
                                                                        .$isiindikatorsasaran
                                                                .'</tr>';
                                                }else{
                                                    $totalsasaranmentah = $totalindikatorsasaran + $totalindikatorsasaranbawah;
                                                    $totalsasaranbawah += $totalsasaranmentah;

                                                        $isisasaran .= '<tr ><td rowspan="'.$totalsasaranmentah.'">'.$sasaran->tst_sasaran_tujuan.' </span></td>'
                                                                    .$isiindikatorsasaran
                                                                .'</tr>';
                                                }

                                            }else{
                                                if($s == 0){
                                                    $totalsasaran+=1;
                                                    $isisasaran = '<td>'.$sasaran->tst_sasaran_tujuan.' </td>'
                                                                    .'<td></td>'
                                                                    .'<td></td>'
                                                                    .'<td></td>'
                                                                    .'<td></td>'
                                                                    .'<td></td>'
                                                                .'</tr>';
                                                }else{
                                                 $totalsasaranbawah+=1;

                                                        $isisasaran .= '<tr ><td >'.$sasaran->tst_sasaran_tujuan.' </span></td>'
                                                                    .'<td></td>'
                                                                    .'<td></td>'
                                                                    .'<td></td>'
                                                                    .'<td></td>'
                                                                    .'<td></td>'
                                                                .'</tr>';
                                                }
                                            }
                                        }
                                        $totaltujuan = $totalsasaran + $totalsasaranbawah;
                                        $html .= '<tr>'
                                                        .'<td rowspan="'.$totaltujuan.'">'.$i.'</td>'
                                                        .'<td rowspan="'.$totaltujuan.'">'.$tujuan->tt_tujuan.'</td>'
                                                        .$isisasaran
                                                    .'</tr>';
                                    }else{
                                        $html .= '<tr>'
                                                        .'<td>'.$i.'</td>'
                                                        .'<td>'.$tujuan->tt_tujuan.'</td>'
                                                        .'<td></td>'
                                                        .'<td></td>'
                                                        .'<td></td>'
                                                        .'<td></td>'
                                                        .'<td></td>'
                                                        .'<td></td>'
                                                    .'</tr>';
                                    }
                                    $i++;
                                }

        if ($request->id == 1){
            $pihak_pertama = 'WALIKOTA CIMAHI';
        }
        $pihak_pertama = str_replace("Dinas","KEPALA DINAS",$dinas[0]->td_dinas);
        $pihak_pertama = str_replace("Sekretariat","SEKERTARIS",$pihak_pertama);
        $pihak_pertama = str_replace("Kecamatan","CAMAT",$pihak_pertama);
        $pihak_pertama = str_replace("Inspektorat","INSPEKTUR",$pihak_pertama);
        $pihak_pertama = str_replace("RSUD","DIREKTUR RSUD",$pihak_pertama);
        $pihak_pertama = str_replace("Sekretariat DPRD","SEKERTARIS DEWAN PERWAKILAN RAKYAT DAERAH",$pihak_pertama);
        $pihak_pertama = str_replace("Badan Penanggulangan Bencana Daerah"," KEPALA PELAKSANA BADAN PENANGGULANGAN BENCANA DAERAH",$pihak_pertama);

        $pihak_pertama = strtoupper($pihak_pertama);


        $pihak_kedua = str_replace("Dinas","KEPALA DINAS",$dinas[0]->td_dinas);
        $pihak_kedua = str_replace("Sekretariat","SEKERTARIS",$pihak_kedua);
        $pihak_kedua = str_replace("Kecamatan","CAMAT",$pihak_kedua);
        $pihak_kedua = str_replace("Inspektorat","INSPEKTUR",$pihak_kedua);
        $pihak_kedua = str_replace("RSUD","DIREKTUR RSUD",$pihak_kedua);
        $pihak_kedua = str_replace("Sekretariat DPRD","SEKERTARIS DEWAN PERWAKILAN RAKYAT DAERAH",$pihak_kedua);
        $pihak_kedua = str_replace("Badan Penanggulangan Bencana Daerah"," KEPALA PELAKSANA BADAN PENANGGULANGAN BENCANA DAERAH",$pihak_kedua);
        $pihak_kedua = strtoupper($pihak_kedua);






        $htmlNew='<!DOCTYPE html>
        <html lang="en">
        <head>
        	<title>
            KESELARASAN KINERJA PERANGKAT DAERAH TAHUN
            </title>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
            <style>
            .page-break {
                page-break-after: always;
            }
            thead{display: table-header-group;}
            tfoot {display: table-row-group;}
            tr {page-break-inside: avoid;}
            </style>
        </head>
        <body style="page-break-after:auto; font-size: 9px">
            <div class="container" style="font-size: 12px;  font-family: Arial, Helvetica, sans-serif;  line-height: 2.5pt; margin-left: 2.3cm; margin-top: 1.3cm; margin-bottom: 0.5cm; margin-right: 2.5cm">

                <center>
                    <img src="'.public_path('img/Logo-Cimahi.png').'" style="width:auto; height: 100px"/>
                    <h4>
                    KESELARASAN KINERJA PERANGKAT DAERAH TAHUN '.$_COOKIE['tahun'].'</h4>
                    <h4>'.$pihak_pertama.'</h4>

                </center>
                <table class="table table-bordered" style="page-break-inside: avoid;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th width="15%">Tujuan</th>
                            <th width="15%">Sasaran Strategis</th>
                            <th>Indikator Sasaran Strategis</th>
                            <th>Sasaran Program</th>
                            <th>Indikator Sasaran Program</th>
                            <th>Sasaran Kegiatan</th>
                            <th>Indikator Sasaran Kegiatan</th>
                        </tr>
                    </thead>

                    <tbody>
                        '.$html.'
                    </tbody>
                </table>
                <br/>
                <br/>
                <table class="table">
                        <tbody>
                            <tr>
                                <td style="width:25%; text-align:left; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none">Cimahi,_________'.date('Y').'</td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none">Pihak pertama,</td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></b></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"><b>'.$pihak_pertama.'</b></td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"></td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"></td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"></td>
                            </tr>

                            <tr>
                                <td style="width:25%; text-align:center; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none">'.$request->pihak_pertama.' <b><br/> '.$request->jabatan_pihak_pertama.' <br/>'.$request->nip.'</b></td>
                            </tr>

                        </tbody>
                    </table>
            </div>
        </body>
        </html>';

        // $pdf = PDF::loadHTML($html)->setPaper('a4', 'potrait');
        $pdf = PDF::loadHTML($htmlNew)->setPaper('a4', 'landscape');
	    return  $pdf->stream('KESELARASAN - '.date('d/m/Y').'.pdf');
	    // return $pdf->download($title.' '.$detail_corporate->name.' - '.date('d/m/Y').'.pdf');
    }

}
