<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserValidation;
use App\Models\Role;
use Carbon\Carbon;
use DB;
use Redirect;
use PDF;
/**
 * Class UserController.
 */
class RktController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */

	function __construct()
	{
		$this->middleware(['auth']);

        $this->middleware(function ($request, $next)
        {
            $this->user = session('data')->user;
            return $next($request);
        });
	}

	public function index()
    {
		if($this->user->role_id == 99 || $this->user->role_id == 97){
			$data['breadcrumb'] = [
				'parent' => 'RKT',
				'child' => 'List RKT'
			];
			$data['listdinas'] = $this->listdinas();

			return view('backend.rkt.index', $data);
		}else{
			$id = $this->user->dinas_id;

			$url = Redirect::route('backend.rkt.home',array('id' => $id));
			return $url;
		}
	}

	public function home($id)
    {
		$data['breadcrumb'] = [
			'parent' => 'RKT',
			'child' => 'List RKT'
		];
		$data['id'] = $id;
		$data['listsasaran'] = $this->listsasaran($id);

		return view('backend.rkt.home', $data);
	}

	public function indikator()
    {
		$data['breadcrumb'] = [
			'parent' => 'RKT',
			'child' => 'Indikator'
		];
		$data['listindikator'] = $this->listindikator();

		return view('backend.rkt.indikator', $data);
	}


	public function tambahtarget($id, $iddinas)
    {
		$query = DB::select("SELECT * FROM tbl_indikator_sasaran WHERE tis_id = ".$id);
		$data['target'] = $query;

		$data['id'] = $id;
		$data['iddinas'] = $iddinas;

		return view('backend.rkt.detail', $data);
	}

    public function printpk($id)
    {
		$data['id'] = $id;

		return view('backend.rkt.print', $data);
	}


	public function listdinas(){
		$hasil = DB::select('SELECT td_id,
									td_dinas,
									td_keterangan
								FROM tbl_dinas
								WHERE
									status = 1

                                    and deleted_at is null
								ORDER BY `index`');
		return $hasil;
	}

	public function listsasaran($id){
		$hasil = array();
		$where = '';
		if($this->user->role_id == 99 || $this->user->role_id == 97 ){
			$where = 'aa.`iddinas` = '.$id.' AND';
		}else{
			$where = 'aa.`iddinas` = '.$this->user->dinas_id.' AND';
		}

		$result4 	= DB::select('SELECT a.* FROM tbl_sasaran_tujuan a

								LEFT JOIN tbl_tujuan cc ON cc.tt_id = a.idtujuan
								LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
								LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
								WHERE '.$where.' tst_status = 0 and a.tahun = '.$_COOKIE['tahun']);


		// dd($result4);

		foreach($result4 as $new4){

				$result6 	= DB::select('SELECT * FROM tbl_indikator_sasaran a

											LEFT JOIN tbl_iku b ON b.tis_id = a.tis_id
											WHERE idsasaran = '.$new4->tst_id.' AND tis_status = 0');

				foreach($result6 as $new6){


					if($new4->tst_id == $new6->idsasaran){

						$new4->indikatorsasaran = $result6;
					}
				}

			array_push($hasil, $new4);
		}
		$hasil = (object) $hasil;
		return $hasil;
	}

	public function listindikator(){
		$where = '';
		if($this->user->dinas_id != 1){
			$where = 'aa.`iddinas` = '.$this->user->dinas_id.' AND';
		}
		$hasil = DB::select('SELECT a.tis_id, a.tis_indikator_sasaran, a.tis_satuan , b.formula, a.tis_target
		FROM tbl_indikator_sasaran a
		LEFT JOIN tbl_sasaran_tujuan dd ON dd.`tst_id` = a.`idsasaran`
		LEFT JOIN tbl_tujuan cc ON cc.tt_id = dd.idtujuan
		LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
		LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
		LEFT JOIN tbl_iku b ON b.tis_id = a.tis_id
		WHERE '.$where.' aa.tahun = '.$_COOKIE['tahun']);
		return $hasil;
	}


	public function targetedit(Request $request){
		$input = $request->all();
		$data = DB::table('tbl_indikator_sasaran')
			->where('tis_id', $input['idsasaran'])
			->update([
						'tis_target' => $input['target'],
						'tis_satuan' => $input['satuan']
				]
		);
		$data = DB::table('tbl_indikator_sasaran_pk')
			->where('tis_id', $input['idsasaran'])
			->update([
						'tis_target' => $input['target'],
						'tis_satuan' => $input['satuan']
				]
		);
		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'Target Sukses Terupdate!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}
    }
    public function detaildinas($id){
		$hasil = DB::select('SELECT td_id,
									td_dinas,
									td_keterangan
								FROM tbl_dinas
								WHERE
                                    td_id = '.$id.'
                                    AND
									status = 1
								ORDER BY `index`');
		return $hasil;
	}


    public function print(Request $request)
    {
        $dinas =   $this->detaildinas($request->id);
        $listiku = $this->listsasaran($request->id);
        $sa = 1;
        $i = 1;
        $isi = '';

        foreach($listiku as $newsasaran){
            $totalindikatorsasaran=0;

            if(isset($newsasaran->tst_sasaran_tujuan)){
                $isi2 = '';
                if(isset($newsasaran->indikatorsasaran)){
                    foreach($newsasaran->indikatorsasaran as $s=>$indikatorsasaran){
                            $totalindikatorsasaran = array_count_values(array_column($newsasaran->indikatorsasaran, 'idsasaran'))[$newsasaran->tst_id];
                            if($s == 0){

                                $isi2 .= '<td>'.$indikatorsasaran->tis_indikator_sasaran.'</td>'
                                .'<td>'.$indikatorsasaran->tis_target.'</td></tr>';
                            }else{
                                $isi2 .= '<tr ><td>'.$indikatorsasaran->tis_indikator_sasaran.'</td>'
                                .'<td>'.$indikatorsasaran->tis_target.'</td></tr>';
                            }
                    }

                }

                $isi .= '<tr>'
                            .'<td rowspan="'.$totalindikatorsasaran.'">'.$i.'</td>'
                            .'<td rowspan="'.$totalindikatorsasaran.'">'.$newsasaran->tst_sasaran_tujuan.'</td>'
                            .$isi2;

            }else{
                $isi .= '<tr>'
                            .'<td>'.$i.'</td>'
                            .'<td>'.$newsasaran->tst_sasaran_tujuan.'</td>'
                            .'<td></td>'
                            .'<td></td>'
                        .'</tr>';
            }
            $i++;
        }

        if ($request->id == 1){
            $pihak_pertama = 'WALIKOTA CIMAHI';
        }
        $pihak_pertama = str_replace("Dinas","KEPALA DINAS",$dinas[0]->td_dinas);
        $pihak_pertama = str_replace("Sekretariat","SEKERTARIS",$pihak_pertama);
        $pihak_pertama = str_replace("Kecamatan","CAMAT",$pihak_pertama);
        $pihak_pertama = str_replace("Inspektorat","INSPEKTUR",$pihak_pertama);
        $pihak_pertama = str_replace("RSUD","DIREKTUR RSUD",$pihak_pertama);
        $pihak_pertama = str_replace("Sekretariat DPRD","SEKERTARIS DEWAN PERWAKILAN RAKYAT DAERAH",$pihak_pertama);
        $pihak_pertama = str_replace("Badan Penanggulangan Bencana Daerah"," KEPALA PELAKSANA BADAN PENANGGULANGAN BENCANA DAERAH",$pihak_pertama);

        $pihak_pertama = strtoupper($pihak_pertama);

        $html='<!DOCTYPE html>
        <html lang="en">
        <head>
        	<title>
            RENCANA KINERJA TAHUNAN
            </title>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
            <style>
            .page-break {
                page-break-after: always;
            }
            p {
                line-height: 1.5;
            }
            </style>
        </head>
        <body style="font-size: 12px">
            <div class="container" style="font-size: 12px;  font-family: Arial, sans-serif;  line-height: 2.5pt; margin-left: 2.3cm; margin-top: 1.3cm; margin-bottom: 0.5cm; margin-right: 2.5cm">
                    <center>
                        <h4>
                            RENCANA KINERJA TAHUNAN TAHUN '.$_COOKIE['tahun'].'
                        </h4>
                        <h4>
                           '.strtoupper($dinas[0]->td_dinas).'
                        </h4>
                    </center>
                    <table class="table table-bordered"  style="padding-top:1cm">
                        <thead>
                            <tr>
                                <th><center>NO</center></th>
                                <th><center>SASARAN STRATEGIS</center></th>
                                <th><center>INDIKATOR KINERJA</center></th>
                                <th><center>TARGET</center></th>
                            </tr>
                        </thead>
                        <tbody>
                            '.$isi.'
                        </tbody>
                    </table>

                    <br/>
                    <br/>

                    <table class="table">
                        <tbody>
                            <tr>
                                <td style="width:25%; text-align:left; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none">Cimahi,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$request->bulan.' '.$_COOKIE['tahun'].'</td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none">Pihak pertama,</td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></b></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"><b>'.$pihak_pertama.'</b></td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"></td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"></td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"></td>
                            </tr>

                            <tr>
                            <td style="width:60%; text-align:center; border: none"></td>
                            <td style="width:25%; text-align:center; border: none"> </td>
                            <td style="width:80%; text-align:center; border: none"><b>'.strtoupper($request->pihak_pertama).' <br/> '.strtoupper($request->jabatan_pihak_pertama).' <br/>NIP. '.$request->nip.'</b></td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </body>
        </html>';

        // $pdf = PDF::loadHTML($html)->setPaper('a4', 'potrait');
        $pdf = PDF::loadHTML($html)->setPaper('f4', 'potrait');
	    return  $pdf->stream('PENGUKURAN KINERJA - '.date('d/m/Y').'.pdf');
	    // return $pdf->download($title.' '.$detail_corporate->name.' - '.date('d/m/Y').'.pdf');
    }

}
