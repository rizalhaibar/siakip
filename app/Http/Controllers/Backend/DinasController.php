<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserValidation;
use App\Models\Role;
use Carbon\Carbon;
use DB;
/**
 * Class UserController.
 */
class DinasController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */

	function __construct()
	{
		$this->middleware(['auth']);
	}

	public function index()
    {
		$data['breadcrumb'] = [
            'parent' => 'Dinas',
            'child' => ''
		];
		$data['listdinas'] = $this->listdinas();
		return view('backend.dinas.index', $data);
	}

	public function eselon($id)
    {
		$data['breadcrumb'] = [
            'parent' => 'Dinas',
            'child' => 'Perangkat Daerah'
		];
		$data['id'] = $id;
		$data['dinas'] = $this->dinas($id);
		$data['eselon'] = $this->eselons($id);
		$data['eselonisi'] = $this->eselonisi($id);
		return view('backend.dinas.eselon', $data);
	}


	public function tambaheselon($id)
    {
		$data['id'] = $id;
		$data['dinas'] = $this->dinas($id);
		$data['eselon'] = $this->eselons($id);
		$data['eselonisi'] = $this->eselonisi($id);
		return view('backend.dinas.tambaheselon', $data);
	}

	public function addeselon($id, $ids)
    {
		$query = DB::select("SELECT * FROM tbl_eselon WHERE te_id = ".$id." AND status = 1");
		$data['detaileselon'] = $query;
        $data['id'] = $id;
		$data['iddinas'] = $ids;
		return view('backend.dinas.detaileselon', $data);
	}
	public function addeselon3($id, $ids)
    {
		$query = DB::select("SELECT * FROM tbl_eselon WHERE te_id = ".$id." AND status = 1");
		$data['detaileselon'] = $query;
        $data['id'] = $id;
		$data['iddinas'] = $ids;

		return view('backend.dinas.detaileselon3', $data);
	}
	public function addeselon4($id, $ids)
    {
		$query = DB::select("SELECT * FROM tbl_eselon WHERE te_id = ".$id." AND status = 1");
		$data['detaileselon'] = $query;
        $data['id'] = $id;
		$data['iddinas'] = $ids;

		return view('backend.dinas.detaileselon4', $data);
	}
	public function addeselon5($id, $ids)
    {
		$query = DB::select("SELECT * FROM tbl_eselon WHERE te_id = ".$id." AND status = 1");
		$data['detaileselon'] = $query;
        $data['id'] = $id;
		$data['iddinas'] = $ids;

		return view('backend.dinas.detaileselon5', $data);
	}


    public function tambahdinas()
    {
        return view('backend.dinas.create');
	}


	public function editdinas($id)
    {
		$query = DB::select("SELECT * FROM tbl_dinas WHERE td_id = ".$id);
		$data['detail'] = $query;
        $data['id'] = $id;

		return view('backend.dinas.edit', $data);
	}


	public function listdinas(){
		$hasil = DB::select('SELECT td_id,
									td_dinas,
									td_keterangan
								FROM tbl_dinas
								WHERE
									status = 1
                                    and deleted_at is null
								ORDER BY `index` asc');
		return $hasil;
	}

	public function dinas($id){
		$hasil = DB::select('SELECT * FROM tbl_dinas WHERE td_id = '.$id);
		return $hasil;
	}

	public function eselons($id){
		$hasil = DB::select('select * from tbl_eselon where iddinas = '.$id.' and status = 1 and te_eselon in("2A","2B")');
		return $hasil;
	}

	public function eselonisi($id){
		$hasil = DB::select('select * from tbl_eselon where iddinas = '.$id.' and status = 1');
		return $hasil;
	}


	public function saveeselon(Request $request){
		$input = $request->all();
		$aa = explode("_",$input['eselon']);
        $eselon = $aa[0];
        $jabatan = $aa[1];
		$data = DB::table('tbl_eselon')->insert(
			['iddinas' => $input['iddinas'], 'te_eselon' => $eselon, 'te_jabatan' => $jabatan, 'te_isi' => $input['bagian'], 'te_parent' => 1, 'te_child' => 0]
		);
		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'Eselon Sukses Tersimpan!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}
	}

	public function saveeselon3(Request $request){
		$input = $request->all();
		$aa = explode("_",$input['eselon']);
        $eselon = $aa[0];
        $jabatan = $aa[1];
		$data = DB::table('tbl_eselon')->insert(
			['id_parent' => $input['ideselon3'],'iddinas' => $input['iddinas'], 'te_eselon' => $eselon, 'te_jabatan' => $jabatan, 'te_isi' => $input['bagian'], 'te_parent' => 0, 'te_child' => 1]
		);
		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'Eselon Sukses Tersimpan!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}
	}
	public function saveeselon4(Request $request){
		$input = $request->all();
		$aa = explode("_",$input['eselon']);
        $eselon = $aa[0];
        $jabatan = $aa[1];
		$data = DB::table('tbl_eselon')->insert(
			['id_parent' => $input['ideselon3'],'iddinas' => $input['iddinas'], 'te_eselon' => $eselon, 'te_jabatan' => $jabatan, 'te_isi' => $input['bagian'], 'te_parent' => 0, 'te_child' => 2]
		);
		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'Eselon Sukses Tersimpan!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}
	}
	public function saveeselon5(Request $request){
		$input = $request->all();
		$aa = explode("_",$input['eselon']);
        $eselon = $aa[0];
        $jabatan = $aa[1];
		$data = DB::table('tbl_eselon')->insert(
			['id_parent' => $input['ideselon3'],'iddinas' => $input['iddinas'], 'te_eselon' => $eselon, 'te_jabatan' => $jabatan, 'te_isi' => $input['bagian'], 'te_parent' => 0, 'te_child' => 3]
		);
		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'Eselon Sukses Tersimpan!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}
	}


	public function eselondelete(Request $request){
		$input = $request->all();
		$data = DB::table('tbl_eselon')->where('te_id', '=', $input['id'])->delete();
		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'Eselon Sukses Terhapus!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}
	}


	public function eselonedit(Request $request){
		$input = $request->all();
		$data = DB::table('tbl_eselon')
				->where('te_id', $input['ideselon2'])
				->update(['te_isi' => $input['bagian']]);
		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'Dinas Sukses Terupdate!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}
	}

	public function dinastambah(Request $request){
		$input = $request->all();
		$data = DB::table('tbl_dinas')->insert(
			['td_dinas' => $input['dinas'], 'td_keterangan' => $input['ket'], 'index' => $input['index'], 'status' => 1]
		);
		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'Dinas Sukses Tersimpan!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}
	}

	public function dinasdelete(Request $request){
		$input = $request->all();
		$data = DB::table('tbl_dinas')
				->where('td_id', $input['id'])
				->update(['deleted_at' => date('Y-m-d H:i:s')]);
		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'Dinas Sukses Terhapus!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}
	}
	public function dinasedit(Request $request){
		$input = $request->all();
		$data = DB::table('tbl_dinas')
				->where('td_id', $input['iddinas'])
				->update(['td_dinas' => $input['dinas'], 'td_keterangan' => $input['ket'], 'index' => $input['index']]);
		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'Dinas Sukses Terupdate!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}
	}
	public function copydinas(Request $request){
		$input = $request->all();
        $id = $input['id'];
        $tahun = $input['tahun'];
        DB::beginTransaction();
		try {
        $check = DB::select('SELECT * FROM tbl_visi WHERE iddinas = '.$id.' AND tahun ='.$_COOKIE['tahun'].' AND status = 0');
        if ($check){
            $result = DB::select('SELECT * FROM tbl_visi WHERE iddinas = '.$id.' AND tahun ='.$tahun.' AND status = 0');
            foreach($result as $new){
                DB::table('tbl_visi')->insert(
                    ['iddinas' => $new->iddinas, 'tv_visi' => $new->tv_visi, 'tv_keterangan' => $new->tv_keterangan, 'tahun' => $_COOKIE['tahun']]
                );

                $idvisi 		= DB::getPdo()->lastInsertId();

                $result2 = DB::select('SELECT * FROM tbl_misi WHERE idvisi = '.$new->tv_id.' AND status = 0');
                foreach($result2 as $new2){

                    DB::table('tbl_misi')->insert(
                        ['idvisi' => $idvisi, 'tm_misi' => $new2->tm_misi, 'tm_keterangan' => $new2->tm_keterangan, 'tahun' => $_COOKIE['tahun']]
                    );
                    $idmisi 		= DB::getPdo()->lastInsertId();

                    $result3 	= DB::select('SELECT * FROM tbl_tujuan WHERE idmisi = '.$new2->tm_id.' AND status = 0');
                    foreach($result3 as $new3){
                        DB::table('tbl_tujuan')->insert(
                            ['idmisi' => $idmisi, 'tt_tujuan' => $new3->tt_tujuan, 'tt_keterangan' => $new3->tt_keterangan, 'tahun' => $_COOKIE['tahun']]
                        );
                        $idtujuan 		= DB::getPdo()->lastInsertId();
                        $result6 	= DB::select('SELECT * FROM tbl_indikator_tujuan WHERE idtujuan = '.$new3->tt_id.' AND tit_status = 0');

                        foreach($result6 as $new6){
                            DB::table('tbl_indikator_tujuan')->insert(
                                [
                                    'idtujuan' => $idtujuan,
                                    'tit_indikator_tujuan' => $new6->tit_indikator_tujuan,
                                    'tit_keterangan' => $new6->tit_keterangan,
                                    'tit_satuan' => $new6->tit_satuan,
                                    'tit_target' => $new6->tit_target,
                                    'tit_status' => $new6->tit_status,
                                    'tahun' => $_COOKIE['tahun']
                                    ]
                            );
                        }
                    }
                    $result4 	= DB::select('SELECT * FROM tbl_sasaran_tujuan WHERE idtujuan = '.$new3->tt_id.' AND tst_status = 0');

                    foreach($result4 as $new4){

                        DB::table('tbl_sasaran_tujuan')->insert(
                            [
                                'idtujuan' => $new4->idtujuan,
                                'tst_sasaran_tujuan' => $new4->tst_sasaran_tujuan,
                                'tst_keterangan' => $new4->tst_keterangan,
                                'tst_iku' => $new4->tst_iku,
                                'tst_status' => $new4->tst_status,
                                'tahun' => $_COOKIE['tahun']
                                ]
                        );
                        $idsasaran 		= DB::getPdo()->lastInsertId();

                        $result5 	= DB::select('SELECT * FROM tbl_indikator_sasaran WHERE idsasaran = '.$new4->tst_id.' AND tis_status = 0');
                        foreach($result5 as $new5){

                            DB::table('tbl_indikator_sasaran')->insert(
                                [
                                    'idsasaran' => $idsasaran,
                                    'tis_indikator_sasaran' => $new5->tis_indikator_sasaran,
                                    'tis_keterangan' => $new5->tis_keterangan,
                                    'tis_satuan' => $new5->tis_satuan,
                                    'tis_target' => $new5->tis_target,
                                    'tis_iku' => $new5->tis_iku,
                                    'formula' => $new5->formula,
                                    'sumberdata' => $new5->sumberdata,
                                    'tis_tahun' => 1,
                                    'tahun' => $_COOKIE['tahun']
                                    ]
                            );
                            $idindisasaran 		= DB::getPdo()->lastInsertId();
                            $result7 = DB::select("SELECT * FROM tbl_iku WHERE tis_id =".$new5->tis_id);
                            foreach($result7 as $new7){
                                DB::table('tbl_iku')->insert(
                                    [
                                        'tis_id' => $idindisasaran,
                                        'formula' => $new7->formula,
                                        'sumberdata' => $new7->sumberdata,
                                        'tahun' => $_COOKIE['tahun']
                                        ]
                                );
                            }

                            DB::table('tbl_rencana_aksi')->insert(
                                [
                                    'tis_id' => $idindisasaran
                                ]
                            );

                            DB::table('tbl_indikator_sasaran_pk')->insert(
                                [
                                    'idsasaran' => $idsasaran,
                                    'tis_indikator_sasaran' => $new5->tis_indikator_sasaran,
                                    'tis_keterangan' => $new5->tis_keterangan,
                                    'tis_satuan' => $new5->tis_satuan,
                                    'tis_target' => $new5->tis_target,
                                    'tis_iku' => $new5->tis_iku,
                                    'formula' => $new5->formula,
                                    'sumberdata' => $new5->sumberdata,
                                    'tis_tahun' => 1,
                                    'tahun' => $_COOKIE['tahun']
                                    ]
                            );



                            $result9 = DB::select('SELECT a.*
                            FROM tbl_program_sasaran a
                            WHERE a.tahun = '.$_COOKIE['tahun'].' AND idindisasaran = '.$new5->tis_id.' AND status=0');
                            foreach($result9 as $new9){
                                DB::table('tbl_program_sasaran')->insert(
                                    [
                                        'idindisasaran' =>$idindisasaran,
                                        'tps_program_sasaran' => $new9->tps_program_sasaran,
                                        'tps_keterangan' => $new9->tps_keterangan,
                                        'te_isi' => $new9->te_isi,
                                        'tahun' => $_COOKIE['tahun']
                                    ]
                                );

                                $idprogram 		= DB::getPdo()->lastInsertId();
                                DB::table('tbl_program_sasaran_log')->insert(
                                    [
                                        'idindisasaran' => $idindisasaran,
                                        'tps_program_sasaran' => $new9->tps_program_sasaran,
                                        'tps_keterangan' => $new9->tps_keterangan,
                                        'te_isi' => $new9->te_isi,
                                        'tahun' => $_COOKIE['tahun']
                                    ]
                                );
                                $result10 	= DB::select('SELECT a.*, c.te_isi, c.te_jabatan
                                FROM tbl_indikator_program a
                                LEFT JOIN tbl_eselon c ON c.te_id = a.pic AND c.te_eselon = "3A"
                                WHERE a.idprogram = '.$new9->tps_id.' AND a.`status` = 0');
                                foreach($result10 as $new10){

                                    DB::table('tbl_indikator_program')->insert(
                                        [
                                            'idprogram' => $idprogram,
                                            'tip_indikator_program' => $new10->tip_indikator_program,
                                            'tip_keterangan' => $new10->tip_keterangan,
                                            'tip_satuan' => $new10->tip_satuan,
                                            'tip_target' => $new10->tip_target,
                                            'te_isi' => $new10->te_isi,
                                            'tahun' => $_COOKIE['tahun']
                                            ]
                                    );
                                    $idindiprogram 		= DB::getPdo()->lastInsertId();
                                    DB::table('tbl_indikator_program_log')->insert(
                                        [
                                            'idprogram' => $idprogram,
                                            'tip_indikator_program' => $new10->tip_indikator_program,
                                            'tip_keterangan' => $new10->tip_keterangan,
                                            'tip_satuan' => $new10->tip_satuan,
                                            'tip_target' => $new10->tip_target,
                                            'te_isi' => $new10->te_isi,
                                            'tahun' => $_COOKIE['tahun']
                                            ]
                                    );

                                    $result11 	= DB::select('SELECT a.*
                                    FROM tbl_kegiatan_program a
                                    WHERE a.idindiprogram = '.$new10->tip_id.' AND a.`status` = 0');
                                    foreach($result11 as $new11){
                                        DB::table('tbl_kegiatan_program')->insert(
                                            [
                                                'idindiprogram' => $idindiprogram,
                                                'tkp_kegiatan_program' => $new11->tkp_kegiatan_program,
                                                'tkp_keterangan' => $new11->tkp_keterangan,
                                                'te_isi' => $new11->te_isi,
                                                'tahun' => $_COOKIE['tahun']
                                                ]
                                        );
                                        $idkegiatan 		= DB::getPdo()->lastInsertId();
                                        DB::table('tbl_kegiatan_program_log')->insert(
                                            [
                                                'idindiprogram' => $idindiprogram,
                                                'tkp_kegiatan_program' => $new11->tkp_kegiatan_program,
                                                'tkp_keterangan' => $new11->tkp_keterangan,
                                                'te_isi' => $new11->te_isi,
                                                'tahun' => $_COOKIE['tahun']
                                                ]
                                        );

                                        $result12 	= DB::select('SELECT a.*, c.te_jabatan, d.ta_anggaran FROM tbl_indikator_kegiatan a
                                            LEFT JOIN tbl_eselon c ON c.te_id = a.pic
                                            LEFT JOIN tbl_anggaran d on a.tik_id = d.idindkegiatan
                                            WHERE a.idkegiatan = '.$new11->tkp_id.' AND a.`tik_status` = 0');

                                        foreach($result12 as $new12){
                                            DB::table('tbl_indikator_kegiatan')->insert(
                                                [
                                                    'idkegiatan' => $idkegiatan,
                                                    'tik_indikator_kegiatan' => $new12->tik_indikator_kegiatan,
                                                    'tik_satuan' => $new12->tik_satuan,
                                                    'tik_keterangan' => $new12->tik_keterangan,
                                                    'tik_target' => $new12->tik_target,
                                                    'te_isi' => $new12->te_isi,
                                                    'tahun' => $_COOKIE['tahun']
                                                    ]
                                            );
                                            $idindikegiatan 		= DB::getPdo()->lastInsertId();

                                            DB::table('tbl_indikator_kegiatan_log')->insert(
                                                [
                                                    'idkegiatan' => $idkegiatan,
                                                    'tik_indikator_kegiatan' => $new12->tik_indikator_kegiatan,
                                                    'tik_satuan' => $new12->tik_satuan,
                                                    'tik_keterangan' => $new12->tik_keterangan,
                                                    'tik_target' => $new12->tik_target,
                                                    'te_isi' => $new12->te_isi,
                                                    'tahun' => $_COOKIE['tahun']
                                                    ]
                                            );

                                            DB::table('tbl_anggaran')->insert(
                                                [
                                                    'idindkegiatan' => $idindikegiatan,
                                                    'ta_anggaran' => $new12->ta_anggaran,
                                                    'tahun' => $_COOKIE['tahun']
                                                ]
                                            );
                                            DB::table('tbl_anggaran_log')->insert(
                                                [
                                                    'idindkegiatan' => $idindikegiatan,
                                                    'ta_anggaran' => $new12->ta_anggaran,
                                                    'tahun' => $_COOKIE['tahun']
                                                ]
                                            );
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        DB::commit();
        $result = array( 'success' => true, 'message' => 'PIC Sukses Terpilih!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);
	}

}
