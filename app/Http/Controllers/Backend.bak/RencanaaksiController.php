<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserValidation;
use App\Models\Role;
use Carbon\Carbon;
use Redirect;
use DB;
/**
 * Class UserController.
 */
class RencanaaksiController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
	 
	
	function __construct()
	{
		$this->middleware(['auth']);
		
        $this->middleware(function ($request, $next)
        {
            $this->user = session('data')->user;
            return $next($request);
        });
	}
	
	public function index()
    {
		if($this->user->role_id == 99 || $this->user->role_id == 97){
			$data['breadcrumb'] = [
				'parent' => 'Rencana Aksi',
				'child' => 'List Dinas'
			];
			$data['listdinas'] = $this->listdinas();

			return view('backend.rencanaaksi.index', $data);
		}else{
			$id = $this->user->dinas_id;
			
			$url = Redirect::route('backend.rencanaaksi.home',array('id' => $id,'idsasaran' => 0));
			return $url;
		}
	}

	public function home($id, $idsasaran)
    {
		$data['breadcrumb'] = [
			'parent' => 'Rencana Aksi',
			'child' => 'List Rencana Aksi'
		];
		$data['id'] = $id;
		$data['idsasaran'] = $idsasaran;
		$data['listsasaran'] = $this->listsasaran($id);
		if($idsasaran != 0){
			$data['indikatorsasaran'] = $this->listindikatorsasaran($id, $idsasaran);
			$data['program'] = $this->getProgram($idsasaran);
			$data['kegiatan'] = $this->getKegiatan($idsasaran);
		}
		return view('backend.rencanaaksi.home', $data);
	}


	public function tambahtargetsasaran($id, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;
		

		return view('backend.rencanaaksi.tambahtargetsasaran', $data);
	}
	public function edittargetsasaran($id, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;
		$data['detail'] = DB::table('tbl_indikator_sasaran_triwulan')->where('indisasaran',$id)->first();
		

		return view('backend.rencanaaksi.edittargetsasaran', $data);
	}

	public function tambahtargetprogram($id, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;
		

		return view('backend.rencanaaksi.tambahtargetprogram', $data);
	}
	public function edittargetprogram($id, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;
		$data['detail'] = DB::table('tbl_indikator_program_triwulan')->where('indiprogram',$id)->first();
		

		return view('backend.rencanaaksi.edittargetprogram', $data);
	}

	public function tambahtargetkegiatan($id, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;
		

		return view('backend.rencanaaksi.tambahtargetkegiatan', $data);
	}
	public function edittargetkegiatan($id, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;
		$data['detail'] = DB::table('tbl_indikator_kegiatan_triwulan')->where('indikegiatan',$id)->first();
		

		return view('backend.rencanaaksi.edittargetkegiatan', $data);
	}


	public function savetargetindikatorsasaran(Request $request){ 
		$input = $request->all();
		DB::beginTransaction();
		try {
		DB::table('tbl_indikator_sasaran_triwulan')->insert(
			[
				'indisasaran' => $input['idindikatorsasaran'], 
				'twtarget1' => $input['tw1'], 
				'twtarget2' => $input['tw2'], 
				'twtarget3' => $input['tw3'], 
				'twtarget4' => $input['tw4'], 
				'tahun' => $_COOKIE['tahun']
				]
		);

		DB::commit();
		$result = array( 'success' => true, 'message' => 'Target Triwulan Indikator Sasaran Sukses Tersimpan!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);	
	}

	
	public function edittargetindikatorsasaran(Request $request){ 
		$input = $request->all();
		DB::beginTransaction();
		try {
			
			DB::table('tbl_indikator_sasaran_triwulan')
			->where('indisasaran', $input['id'])
			->update([
					'twtarget1' => $input['tw1'], 
					'twtarget2' => $input['tw2'], 
					'twtarget3' => $input['tw3'], 
					'twtarget4' => $input['tw4']
				]
			);
			DB::commit();
			$result = array( 'success' => true, 'message' => 'Target Triwulan Indikator Sasaran Sukses Teredit!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);	
	}

	public function savetargetindikatorprogram(Request $request){ 
		$input = $request->all();
		DB::beginTransaction();
		try {
		DB::table('tbl_indikator_program_triwulan')->insert(
			[
				'indiprogram' => $input['idindikatorsasaran'], 
				'twtarget1' => $input['tw1'], 
				'twtarget2' => $input['tw2'], 
				'twtarget3' => $input['tw3'], 
				'twtarget4' => $input['tw4'], 
				'tahun' => $_COOKIE['tahun']
				]
		);

		DB::commit();
		$result = array( 'success' => true, 'message' => 'Target Triwulan Indikator Program Sukses Tersimpan!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);	
	}

	
	public function edittargetindikatorprogram(Request $request){ 
		$input = $request->all();
		DB::beginTransaction();
		try {
			
			DB::table('tbl_indikator_program_triwulan')
			->where('indiprogram', $input['id'])
			->update([
					'twtarget1' => $input['tw1'], 
					'twtarget2' => $input['tw2'], 
					'twtarget3' => $input['tw3'], 
					'twtarget4' => $input['tw4']
				]
			);
			DB::commit();
			$result = array( 'success' => true, 'message' => 'Target Triwulan Indikator Program Sukses Teredit!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);	
	}

	public function savetargetindikatorkegiatan(Request $request){ 
		$input = $request->all();
		DB::beginTransaction();
		try {
		DB::table('tbl_indikator_kegiatan_triwulan')->insert(
			[
				'indikegiatan' => $input['idindikatorsasaran'], 
				'twtarget1' => $input['tw1'], 
				'twtarget2' => $input['tw2'], 
				'twtarget3' => $input['tw3'], 
				'twtarget4' => $input['tw4'], 
				'tahun' => $_COOKIE['tahun']
				]
		);

		DB::commit();
		$result = array( 'success' => true, 'message' => 'Target Triwulan Indikator Kegiatan Sukses Tersimpan!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);	
	}

	
	public function edittargetindikatorkegiatan(Request $request){ 
		$input = $request->all();
		DB::beginTransaction();
		try {
			
			DB::table('tbl_indikator_kegiatan_triwulan')
			->where('indikegiatan', $input['id'])
			->update([
					'twtarget1' => $input['tw1'], 
					'twtarget2' => $input['tw2'], 
					'twtarget3' => $input['tw3'], 
					'twtarget4' => $input['tw4']
				]
			);
			DB::commit();
			$result = array( 'success' => true, 'message' => 'Target Triwulan Indikator Kegiatan Sukses Teredit!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);	
	}












	public function listdinas(){ 
		$hasil = DB::select('SELECT td_id, 
									td_dinas, 
									td_keterangan 
								FROM tbl_dinas
								WHERE 
									status = 1 
								ORDER BY `index`');
		return $hasil;
	}

	
	public function listsasaran($id){
		$where = '';
		if($this->user->role_id != 99){
			$where = 'aa.`iddinas` = '.$this->user->dinas_id.' AND';
		}else{
			$where = 'aa.`iddinas` = '.$id.' AND';
		}

		$result4 	= DB::select('SELECT a.* FROM tbl_sasaran_tujuan a
		
								LEFT JOIN tbl_tujuan cc ON cc.tt_id = a.idtujuan
								LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
								LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
								WHERE '.$where.' a.tst_status = 0');
		return $result4;
	}

	public function listindikatorsasaran($id, $idsasaran){

		$where = '';
		if($this->user->role_id != 99){
			$where = 'aa.`iddinas` = '.$this->user->dinas_id.' ';
		}else{
			$where = 'aa.`iddinas` = '.$id.' ';
		}
		$result6 	= DB::select('SELECT  aadc.tis_id, aadc.tis_indikator_sasaran, aadc.tis_satuan,  aadc.tis_target,
							bbdc.twtarget1, 
							bbdc.twtarget2, 
							bbdc.twtarget3, 
							bbdc.twtarget4
					FROM
					(
					SELECT a.tis_id, a.tis_indikator_sasaran, a.tis_satuan, a.tis_target, a.idsasaran, a.tis_status
					FROM tbl_indikator_sasaran_pk a
					LEFT JOIN tbl_sasaran_tujuan dd ON a.`idsasaran` = dd.`tst_id`
					LEFT JOIN tbl_tujuan cc ON cc.tt_id = dd.idtujuan
					LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
					LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
					)aadc
					LEFT JOIN
					(

					SELECT a.tis_id, a.tis_indikator_sasaran, a.tis_satuan,  a.idsasaran,
						sa.`tw1`, sa.`twtarget1`, 
						sa.`tw2`, sa.`twtarget2`, 
						sa.`tw3`, sa.`twtarget3`, 
						sa.`tw4`, sa.`twtarget4`
					FROM tbl_indikator_sasaran_pk a
					LEFT JOIN tbl_indikator_sasaran_triwulan sa ON a.tis_id = sa.indisasaran
					LEFT JOIN tbl_sasaran_tujuan dd ON dd.`tst_id` = a.`idsasaran`
					LEFT JOIN tbl_tujuan cc ON cc.tt_id = dd.idtujuan
					LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
					LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
					)bbdc ON aadc.tis_id = bbdc.tis_id 
					WHERE aadc.idsasaran =  '.$idsasaran.' AND aadc.tis_status = 0');
		return $result6;
	}
	public function getKegiatan($id){
		$hasil = array();

		$result1 	= DB::select('SELECT a.* 
		FROM tbl_program_sasaran a
		LEFT JOIN tbl_indikator_sasaran_pk b ON a.`idindisasaran` = b.`tis_id`
		LEFT JOIN tbl_sasaran_tujuan c ON b.`idsasaran` = c.`tst_id`
		WHERE c.`tst_id` = '.$id.' AND a.status = 0');

		foreach($result1 as $program){

			$result2 	= DB::select('SELECT a.* FROM tbl_indikator_program a
										WHERE a.idprogram = '.$program->tps_id.' AND a.`status` = 0');

			foreach($result2 as $indikatorprogram){
				if($program->tps_id == $indikatorprogram->idprogram){
					$result3 	= DB::select('SELECT a.*
					FROM tbl_kegiatan_program a
					WHERE a.idindiprogram = '.$indikatorprogram->tip_id.' AND a.`status` = 0');

					foreach($result3 as $kegiatan){
						if($kegiatan->idindiprogram == $indikatorprogram->tip_id){
							$result4 	= DB::select('SELECT 
							aadc.tik_id, aadc.idkegiatan, aadc.tik_indikator_kegiatan, aadc.tik_satuan, aadc.tik_target, 
							bbdc.twtarget1, bbdc.twtarget2, bbdc.twtarget3, bbdc.twtarget4 
							FROM ( 
								SELECT a.tik_id, a.idkegiatan, a.tik_indikator_kegiatan, a.tik_satuan, a.tik_target, a.`tik_status` 
								FROM tbl_indikator_kegiatan a 
							)aadc 
							LEFT JOIN ( 
								SELECT 
								a.tik_id, a.idkegiatan, a.tik_indikator_kegiatan, a.tik_satuan, a.tik_target, a.`tik_status` ,
								sa.`tw1`, sa.`twtarget1`, sa.`tw2`, sa.`twtarget2`, sa.`tw3`, sa.`twtarget3`, sa.`tw4`, sa.`twtarget4` 
								FROM tbl_indikator_kegiatan a 
								LEFT JOIN tbl_indikator_kegiatan_triwulan sa 
								ON a.tik_id = sa.indikegiatan 
							)bbdc ON aadc.tik_id = bbdc.tik_id 
							WHERE aadc.idkegiatan = '.$kegiatan->tkp_id.' AND aadc.`tik_status` = 0');
							foreach($result4 as $indikatorkegiatan){
								if($kegiatan->tkp_id == $indikatorkegiatan->idkegiatan){
									$kegiatan->indikatorkegiatan = $result4;
								}
							}	
							$indikatorprogram->kegiatan[] = $kegiatan;
						}
					}	

					$program->indikatorprogram[] = $indikatorprogram;
				}
			}
			array_push($hasil, $program);
		}
		$hasil = (object) $hasil;
		return $hasil;
	}
	public function getProgram($id){
		$hasil = array();

		$result1 	= DB::select('SELECT a.* FROM tbl_indikator_sasaran_pk a 
							LEFT JOIN tbl_iku b ON b.tis_id = a.tis_id 
							LEFT JOIN tbl_eselon c ON c.te_id = a.pic 
							WHERE idsasaran = '.$id.' AND tis_status = 0');

		// echo '<pre>';
        foreach ($result1 as $indikatorsasaran) {
			$result2 = DB::select('SELECT a.*
									FROM tbl_program_sasaran a
									WHERE a.tahun = '.$_COOKIE['tahun'].' AND idindisasaran = '.$indikatorsasaran->tis_id.' AND status=0');
            foreach ($result2 as $program) {
				if($indikatorsasaran->tis_id == $program->idindisasaran){
					$result3 	= DB::select('SELECT aadc.tip_id, aadc.idprogram, aadc.tip_indikator_program,  aadc.tip_satuan, aadc.tip_target,
												bbdc.twtarget1, 
												bbdc.twtarget2, 
												bbdc.twtarget3, 
												bbdc.twtarget4
											FROM
											(
												SELECT a.tip_id, a.idprogram, a.tip_indikator_program, a.tip_satuan, a.tip_target, a.`status`
												FROM tbl_indikator_program a
											)aadc
											LEFT JOIN
											(
												SELECT a.tip_id, a.idprogram, a.tip_indikator_program, a.tip_satuan, a.tip_target, a.`status`, 
												sa.`tw1`, sa.`twtarget1`, 
												sa.`tw2`, sa.`twtarget2`, 
												sa.`tw3`, sa.`twtarget3`, 
												sa.`tw4`, sa.`twtarget4`
												FROM tbl_indikator_program a
												LEFT JOIN tbl_indikator_program_triwulan sa ON a.tip_id = sa.indiprogram
											)bbdc ON aadc.tip_id = bbdc.tip_id 
											WHERE aadc.idprogram = '.$program->tps_id.' AND aadc.`status` = 0');


					foreach($result3 as $indikatorprogram){
						
						
						if($program->tps_id == $indikatorprogram->idprogram){
							
							$program->indikatorprogram = $result3;
						}
					}	
					$indikatorsasaran->program[] = $program;
				}
				
            }
			array_push($hasil, $indikatorsasaran);
		}
		// print_r($hasil);
		// die;
		$hasil = (object) $hasil;
		return $hasil;
	}
}
