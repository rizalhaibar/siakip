<?php

namespace App\Repositories\AccessControl;

interface AccessControlRepositoryInterface
{
    
    /**
     * 
     */
    function access($id);
    
    /**
     * 
     */
    function update($attributes);

}