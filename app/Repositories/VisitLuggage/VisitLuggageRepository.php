<?php

namespace App\Repositories\VisitLuggage;

use App\Repositories\VisitLuggage\VisitLuggageRepositoryInterface;
use App\Repositories\BaseRepository;
use App\Traits\FormatMessageTraits;
use App\Models\VisitLuggage;

class VisitLuggageRepository extends BaseRepository implements VisitLuggageRepositoryInterface
{
    use FormatMessageTraits;

    protected $model;

    /**
     * VisitLuggageRepository constructor.
     * @param
     */
    public function __construct(VisitLuggage $visitLuggage)
    {
        //
        $this->model = $visitLuggage;
        $this->__setup();
    }

    /**
     *
     */
    private function __setup()
    {
        $this->VALUE_EXIST = config('setting.value.exist');
        $this->VALUE_ZERO = config('setting.value.zero');
        $this->MESSAGE_VALUE_EXIST = "Cannot processing action. Data %xxx% still used in another table";
        $this->MESSAGE_TOGGLE_SUCCESS = "%xxx% status successfuly updated.";
        $this->MESSAGE_TOGGLE_FAILED = "%xxx% status to update.";
        $this->MESSAGE_CREATE_SUCCESS = "%xxx% successfuly created.";
        $this->MESSAGE_CREATE_FAILED = "%xxx% Failed to create.";
        $this->MESSAGE_UPDATE_SUCCESS = "%xxx% successfuly updated.";
        $this->MESSAGE_UPDATE_FAILED = "%xxx% Failed to update.";
        $this->MESSAGE_ATTACH_SUCCESS = "%xxx% Successfully attached.";
        $this->MESSAGE_DETTACH_SUCCESS = "%xxx% Successfully dettached.";
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function hasRelation(int $id)
    {
        return $this->search('id', $id)->has('visitor')->exists();
    }

    /**
     * @param string $id
     * @param string $message
     * @param object $attributes
     * @return mixed
     */
    public function returnResponse($type, $message, $data)
    {
        return $this->returnMessage($type, $this->format($message, $data->name));
    }

    /**
     *
     */
    public function visitLuggages($status)
    {
        $data = $this->model->query();

        if( $status != config('setting.status.all') ) {
            $data->where('status', $status);
        }

        return $data;
    }

    /**
     * @param int $limit
     * @return mixed
     */
    public function pagination($attributes, int $limit)
    {
        $query = $this->model->with('department','company');

        if(isset($attributes->company_id))
        {
            $query->where('company_id', $attributes->company_id);
        }

        return (object)[
            'datas' => $query->paginate($limit),
            'status' => $this->status(),
        ];
    }


}
