<?php

namespace App\Repositories\Company;

use App\Repositories\Company\CompanyRepositoryInterface;
use App\Repositories\BaseRepository;
use App\Traits\FormatMessageTraits;
use App\Models\Company;

class DinasRepository extends BaseRepository implements CompanyRepositoryInterface
{
    use FormatMessageTraits;

    protected $model;

    /**
     * CompanyRepository constructor.
     * @param
     */
    public function __construct(Company $company)
    {
        //
        $this->model = $company;
        $this->__setup();
    }

    /**
     *
     */
    private function __setup()
    {
        $this->VALUE_EXIST              = config('setting.value.exist');
        $this->VALUE_ZERO               = config('setting.value.zero');
        $this->MESSAGE_VALUE_EXIST      = "Cannot processing action. Data %xxx% still used in another table";
        $this->MESSAGE_TOGGLE_SUCCESS   = "%xxx% status successfuly updated.";
        $this->MESSAGE_TOGGLE_FAILED    = "%xxx% status to update.";
        $this->MESSAGE_CREATE_SUCCESS   = "%xxx% successfuly created.";
        $this->MESSAGE_CREATE_FAILED    = "%xxx% Failed to create.";
        $this->MESSAGE_UPDATE_SUCCESS   = "%xxx% successfuly updated.";
        $this->MESSAGE_UPDATE_FAILED    = "%xxx% Failed to update.";
    }

    /**
     * @param $id
     * @return mixed
     */
    public function hasRelation($id)
    {
        return $this->search('id', $id)->has('users')->has('departments')->exists();
    }

    /**
     * @param string $id
     * @param string $message
     * @param object $attributes
     * @return mixed
     */
    public function returnResponse($type, $message, $data)
    {
        return $this->returnMessage($type, $this->format($message, $data->name));
    }

    /**
     *
     */
    public function companies($status)
    {
        $data = $this->model->query();

        if( $status != config('setting.status.all') ) {
            $data->where('status', $status);
        }

        return $data;
    }

    /**
     * @param none
     * @return mixed
     */
    public function parents()
    {
        return $this->model->where('parent', $this->VALUE_ZERO);
    }

    /**
     * @param int $limit
     * @return mixed
     */
    public function pagination(int $limit)
    {
        return (object)[
            'datas' => $this->model->paginate($limit),
            'status' => $this->status(),
        ];
    }


}
