<?php

namespace App\Repositories\Company;

interface DinasRepositoryInterface
{
    /**
     *
     */
    function status();

    /**
     *
     */
    function find(int $id);

    /**
     *
     */
    function search(string $field, string $value);

    /**
     *
     */
    function store($attributes);

    /**
     *
     */
    function update(int $id, $attributes);

    /**
     *
     */
    function toggle($attributes);

    /**
     *
     */
    function companies($status);

    /**
     *
     */
    function parents();

    /**
     *
     */
    function pagination(int $limit);

}
