<?php

namespace App\Repositories\Visitor;

use App\Repositories\Visitor\VisitorRepositoryInterface;
use App\Repositories\VisitLuggage\VisitLuggageRepositoryInterface;
use App\Repositories\Storage\StorageRepositoryInterface;
use App\Repositories\BaseRepository;
use App\Traits\FormatMessageTraits;
use App\Models\Visitor;

class VisitorRepository extends BaseRepository implements VisitorRepositoryInterface
{
    use FormatMessageTraits;

    protected $model;

    /**
     * VisitorRepository constructor.
     * @param
     */
    public function __construct(
        Visitor $visitor,
        VisitLuggageRepositoryInterface $visitLuggageRepo,
        StorageRepositoryInterface $storageRepo
    ) {
        //
        $this->model = $visitor;

        $this->visitLuggageRepo = $visitLuggageRepo;
        $this->storageRepo = $storageRepo;

        $this->__setup();
    }

    /**
     *
     */
    private function __setup()
    {

        // DISK
        $this->DISK = "public";
        $this->IMAGE_CARD_PATH = config('setting.store-path.visitor.card');
        $this->IMAGE_PERSON_PATH = config('setting.store-path.visitor.person');
        $this->IMAGE_PACKAGE_PATH = config('setting.store-path.visitor.package');

        $this->VALUE_EXIST = config('setting.value.exist');
        $this->VALUE_ZERO = config('setting.value.zero');
        $this->MESSAGE_VALUE_EXIST = "Cannot processing action. Data %xxx% still used in another table";
        $this->MESSAGE_TOGGLE_SUCCESS = "%xxx% status successfuly updated.";
        $this->MESSAGE_TOGGLE_FAILED = "%xxx% status to update.";
        $this->MESSAGE_CREATE_SUCCESS = "%xxx% successfuly created.";
        $this->MESSAGE_CREATE_FAILED = "%xxx% Failed to create.";
        $this->MESSAGE_UPDATE_SUCCESS = "%xxx% successfuly updated.";
        $this->MESSAGE_UPDATE_FAILED = "%xxx% Failed to update.";
        $this->MESSAGE_ATTACH_SUCCESS = "%xxx% Successfully attached.";
        $this->MESSAGE_DETTACH_SUCCESS = "%xxx% Successfully dettached.";

        $this->random_string = date('ymdhis').\Str::random(10);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function hasRelation($id)
    {
        return $this->search('id', $id)->has('visit')->has('visitLuggages')->exists();
    }

    /**
     * @param string $id
     * @param string $message
     * @param object $attributes
     * @return mixed
     */
    public function returnResponse($type, $message, $data)
    {
        return $this->returnMessage($type, $this->format($message, $data->name));
    }

    /**
     *
     */
    public function visitors($status)
    {
        $data = $this->model->query();

        if( $status != config('setting.status.all') ) {
            $data->where('status', $status);
        }

        return $data;
    }

    /**
     * @param int $limit
     * @return mixed
     */

     /**
      * NOTE : SETTING CONSTANT BUAT SETIAP ISI PARAMETER
      */
    public function pagination($attributes)
    {

        // Base Query
        $model = $this->model->query();

        // Visit Join
        $query = $model->whereHas('visit', function($visit) use ($attributes) {

            // Company Filter
            if(isset($attributes->company_id)) {
                $visit->where('company_id', $attributes->company_id);
            }

            // Date Range Filter
            if(isset($attributes->end_date) || isset($attributes->start_date) ) {
                $visit->whereBetween('visit_date', [$attributes->start_date, $attributes->end_date]);
            }

            // Time Range Filter
            if(isset($attributes->end_time) || isset($attributes->start_time) ) {
                $visit->whereBetween('visit_time_in', [$attributes->start_time, $attributes->end_time]);
            }

            // Person Name Filter
            if(isset($attributes->param_filter) && $attributes->param_filter == "persons.name") {
                $visit->whereHas('person', function($person) use($attributes) {
                    $person->where($attributes->param_filter, 'like', '%'.$attributes->value_search.'%');
                });
            }

            // Visitor Name Filter
            if(isset($attributes->param_filter) && $attributes->param_filter == "visitors.name") {
                $visit->where($attributes->param_filter, 'like', '%'.$attributes->value_search.'%');
            }

            // Visitor Name Filter
            if(isset($attributes->param_filter) && $attributes->param_filter == "visitors.visit_card_number") {
                $visit->where($attributes->param_filter, 'like', '%'.$attributes->value_search.'%');
            }

        });

        return $query->paginate($attributes->limit);
    }

    /**
     * @param object $attributes
     * @return mixed
     */
    public function store($attributes)
    {
        if($attributes->has('image_person'))
        {
            $images = (object)[
                'filename' => $this->random_string.'_person_.png',
                'file' => $attributes->image_person
            ];
            $this->storageRepo->store($this->DISK, $this->IMAGE_PERSON_PATH, $images->filename, $images);
            $attributes->merge([ 'image_person' => $images->filename ]);
        }

        if($attributes->has('image_card'))
        {
            $images = (object)[
                'filename' => $this->random_string.'_card_.png',
                'file' => $attributes->image_card
            ];
            $this->storageRepo->store($this->DISK, $this->IMAGE_CARD_PATH, $images->filename, $images);
            $attributes->merge([ 'image_card' => $images->filename ]);
        }

        $data = new $this->model;
        $data->fill( $attributes->toArray() );
        $data->save();

        // Check if posting luggage
        if($attributes->item_name) {
            $this->store_visit_luggage($data->id, $attributes);
        }

        if( !$data->save() ) {
            return $this->returnResponse('warning', $this->MESSAGE_CREATE_FAILED, $data);
        } else {
            return $this->returnResponse('success', $this->MESSAGE_CREATE_SUCCESS, $data);
        }
    }

    /**
     * @param integer $id
     * @param object $attributes
     * @return mixed
     */
    public function update(int $id, $attributes)
    {
        $data = $this->find($id);
        $data->fill( $attributes->toArray() );

        // Check if posting luggage
        if($attributes->item_name) { $this->store_visit_luggage($data->id, $attributes); }

        if( !$data->save() ) {
            return $this->returnResponse('warning', $this->MESSAGE_UPDATE_FAILED, $data);
        } else {
            return $this->returnResponse('success', $this->MESSAGE_UPDATE_SUCCESS, $data);
        }
    }

    /**
     * @param object $visitor
     * @param object $attributes
     * @return mixed
     */
    private function store_visit_luggage($id, $attributes)
    {

        // Request Only
        $attributes = $attributes->only(['item_name', 'total_item', 'item_permissions']);

        $request = new \Illuminate\Http\Request();

        // Visitor ID
        $request->merge(['visitor_id' => $id]);

        // Data Luggage
        for( $i = 0; $i < count($attributes['item_name']); ++$i ) {

            $request->merge([ 'name' => $attributes['item_name'][$i] ]);
            $request->merge([ 'total' => $attributes['total_item'][$i] ]);
            $request->merge([ 'permission' => $attributes['item_permissions'][$i] ]);

            $this->visitLuggageRepo->store($request);
        }
    }




}
