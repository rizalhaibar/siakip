<?php

namespace App\Repositories\MeetingRoom;

interface MeetingRoomRepositoryInterface
{
    /**
     * 
     */
    function status();

    /**
     * 
     */
    function find(int $id);

    /**
     * 
     */
    function search(string $field, string $value);

    /**
     * 
     */
    function store($attributes);

    /**
     * 
     */
    function update(int $id, $attributes);

    /**
     * 
     */
    function toggle($attributes);

    /**
     * 
     */
    function meetingRooms($status);

    /**
     * 
     */
    function pagination($attributes);

}