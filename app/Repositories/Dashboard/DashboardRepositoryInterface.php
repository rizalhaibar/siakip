<?php

namespace App\Repositories\Dashboard;

interface DashboardRepositoryInterface
{
	/**
	 * 
	 */
    function get_total(string $type, string $when, $attributes);

	/**
	 * 
	 */
    function get_top_person(string $type, string $when, $attributes);

	/**
	 * 
	 */
    function get_top_department(string $type, string $when, $attributes);
	
	/**
	 * 
	 */
    function get_top_purpose(string $type, string $when, $attributes);

	/**
	 * 
	 */
	function get_chart(string $type, string $when, $attributes);
	
	/**
	 * 
	 */
    function get_hours(string $type, string $when, $attributes);

}