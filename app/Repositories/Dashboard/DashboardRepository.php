<?php

namespace App\Repositories\Dashboard;

use App\Repositories\BaseRepository;
use App\Repositories\Visit\VisitRepositoryInterface;
use App\Repositories\Visitor\VisitorRepositoryInterface;
use App\Repositories\Courrier\CourrierRepositoryInterface;
use App\Traits\FormatMessageTraits;
use Illuminate\Support\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;

class DashboardRepository extends BaseRepository implements DashboardRepositoryInterface
{
    use FormatMessageTraits;

    /**
     * DashboardRepository constructor.
     * @param
     */
    public function __construct(
        VisitRepositoryInterface $visitRepo,
        VisitorRepositoryInterface $visitorRepo,
        CourrierRepositoryInterface $courrierRepo
    ) {
        //
        $this->visitRepo = $visitRepo;
        $this->visitorRepo = $visitorRepo;
        $this->courrierRepo = $courrierRepo;
    }

    /**
     * 
     */
    public function get_total(string $type, string $when, $attributes)
    {

        if($type == "visitorRepo")
        {
            $query = $this->$type->visitors(config('setting.status.all'));
        } else {
            $query = $this->$type->courriers(config('setting.status.all'));
        }

        $data = $query
            ->whereHas('visit', function($subquery) use($when, $attributes) {
            
                // Filter by Current Month
                if($when == 'current_month') {
                    $subquery->whereMonth('visit_date', Carbon::now()->month);
                }

                // Filter by Today
                if($when == 'today') {
                    $subquery->whereDate('visit_date', Carbon::now()->toDateString());
                }
                
                // Filter by Company
                if(isset($attributes->company_id)) {
                    $subquery->where('company_id', $attributes->company_id);
                }
            })
        ;
        
        return $data->count();
    }

    /**
     * 
     */
    public function get_top_person(string $type, string $when, $attributes)
    {
        if($type == "visitorRepo")
        {
            $query = $this->$type->visitors(config('setting.status.all'));
        } else {
            $query = $this->$type->courriers(config('setting.status.all'));
        }

        $data = $query
            ->select('visit.person_id as person_id','person.name as person_name')
            ->selectRaw('count(*) as person_total')
            ->join('visits as visit', 
                'visit.id', '=', ($type == "visitorRepo" ? 'visitors.visit_id' : 'courriers.visit_id') // Handle Visitor / Courrier
            )
            ->join('persons as person', 'person.id', '=', 'visit.person_id')
            ->groupBy('visit.person_id')
            ->groupBy('person.name')
            ->orderByRaw('COUNT(*) DESC')
        ;
        
        // Filter by Current Month
        if($when == 'current_month') {
            $data->whereMonth('visit.visit_date', Carbon::now()->month);
        }

        // Filter by Today
        if($when == 'today') {
            $data->whereDate('visit.visit_date', Carbon::now()->toDateString());
        }
        
        // Filter by Company
        if(isset($attributes->company_id)) {
            $data->where('visit.company_id', $attributes->company_id);
        }

        return $data->first();
    }

    /**
     * 
     */
    public function get_top_department(string $type, string $when, $attributes)
    {
        if($type == "visitorRepo")
        {
            $query = $this->$type->visitors(config('setting.status.all'));
        } else {
            $query = $this->$type->courriers(config('setting.status.all'));
        }

        $data = $query
            ->select('visit.department_id as department_id','department.name as department_name')
            ->selectRaw('count(*) as department_total')
            ->join('visits as visit', 
                'visit.id', '=', ($type == "visitorRepo" ? 'visitors.visit_id' : 'courriers.visit_id') // Handle Visitor / Courrier
            )
            ->join('departments as department', 'department.id', '=', 'visit.department_id')
            ->groupBy('visit.department_id')
            ->groupBy('department.name')
            ->orderByRaw('COUNT(*) DESC')
        ;
        
        // Filter by Current Month
        if($when == 'current_month') {
            $data->whereMonth('visit.visit_date', Carbon::now()->month);
        }

        // Filter by Today
        if($when == 'today') {
            $data->whereDate('visit.visit_date', Carbon::now()->toDateString());
        }
        
        // Filter by Company
        if(isset($attributes->company_id)) {
            $data->where('visit.company_id', $attributes->company_id);
        }

        return $data->first();
    }

    /**
     * 
     */
    public function get_top_purpose(string $type, string $when, $attributes)
    {
        if($type == "visitorRepo")
        {
            $query = $this->$type->visitors(config('setting.status.all'));
        } else {
            $query = $this->$type->courriers(config('setting.status.all'));
        }

        $data = $query
            ->select('visit.visit_purpose_id as visit_purpose_id','visit_purpose.name as visit_purpose_name')
            ->selectRaw('count(*) as visit_purpose_total')
            ->join('visits as visit', 
                'visit.id', '=', ($type == "visitorRepo" ? 'visitors.visit_id' : 'courriers.visit_id') // Handle Visitor / Courrier
            )
            ->join('visit_purposes as visit_purpose', 'visit_purpose.id', '=', 'visit.visit_purpose_id')
            ->groupBy('visit.visit_purpose_id')
            ->groupBy('visit_purpose.name')
            ->orderByRaw('COUNT(*) DESC')
        ;
        
        // Filter by Current Month
        if($when == 'current_month') {
            $data->whereMonth('visit.visit_date', Carbon::now()->month);
        }

        // Filter by Today
        if($when == 'today') {
            $data->whereDate('visit.visit_date', Carbon::now()->toDateString());
        }
        
        // Filter by Company
        if(isset($attributes->company_id)) {
            $data->where('visit.company_id', $attributes->company_id);
        }

        return $data->first();
    }

    /**
     * 
     */
    public function get_chart(string $type, string $when, $attributes)
    {
        // Year
        if( $when == "current_year" ) { $date = Carbon::now()->toDateString(); }
        else if( $when == "last_year" ) { $date = Carbon::now()->sub(1, 'year')->toDateString(); }
        
        // Table
        if( $type == "visitorRepo" ) { $relation = 'visitors'; }
        else if( $type == "courrierRepo" ) { $relation = "courriers"; }
        
        return \DB::select(
            \DB::raw("SELECT COALESCE(COUNT(visits.id), 0) as total, months.Y as year, MONTHNAME(CONCAT(CONCAT(months.Y,'-'),months.m,'-01')) as month FROM ( SELECT 1 AS m, YEAR('".$date."') AS Y UNION SELECT 2 AS m, YEAR('".$date."') AS Y UNION SELECT 3 AS m, YEAR('".$date."') AS Y UNION SELECT 4 AS m, YEAR('".$date."') AS Y UNION SELECT 5 AS m, YEAR('".$date."') AS Y UNION SELECT 6 AS m, YEAR('".$date."') AS Y UNION SELECT 7 AS m, YEAR('".$date."') AS Y UNION SELECT 8 AS m, YEAR('".$date."') AS Y UNION SELECT 9 AS m, YEAR('".$date."') AS Y UNION SELECT 10 AS m, YEAR('".$date."') AS Y UNION SELECT 11 AS m, YEAR('".$date."') AS Y UNION SELECT 12 AS m, YEAR('".$date."') AS Y ) months LEFT JOIN ( SELECT visit.* FROM visits AS visit JOIN ".$relation." AS related ON related.visit_id = visit.id WHERE visit.company_id = :company_id ) AS visits ON (months.m = MONTH(visits.visit_date) AND months.y = YEAR(visits.visit_date)) GROUP BY months.Y, months.m, YEAR(visits.visit_date)"),
        array(
            'company_id' => $attributes->company_id
        ));

    }

    /**
     * 
     */
    public function get_hours(string $type, string $when, $attributes)
    {
        // Month
        $when = Carbon::now()->toDateString();
        
        // Table
        if( $type == "visitorRepo" ) { $relation = 'visitors'; }
        else if( $type == "courrierRepo" ) { $relation = "courriers"; }
        
        return \DB::select(
            \DB::raw("SELECT GROUP_CONCAT(a.most_hour ORDER BY a.most_hour) AS hours, SUM(a.counts) AS total FROM (SELECT HOUR(visits.visit_time_in) AS most_hour, COUNT(*) AS counts FROM visits JOIN ".$relation." as relation ON relation.visit_id = visits.id WHERE MONTH(visits.visit_date) = MONTH('".$when."') AND visits.company_id = :company_id GROUP BY HOUR(visits.visit_time_in) ORDER BY HOUR(visits.visit_time_in)) AS a GROUP BY a.most_hour ORDER BY a.counts DESC, a.most_hour"),
        array(
            'company_id' => $attributes->company_id
        ));

    }
    
}