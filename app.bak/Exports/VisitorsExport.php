<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Repositories\Visitor\VisitorRepositoryInterface;


class VisitorsExport implements FromCollection, WithHeadings, WithColumnFormatting, WithMapping, ShouldAutoSize
{
    protected $attributes, $visitorRepo;

    function __construct( $attributes ) {

        $this->attributes = $attributes;
        $this->visitorRepo = app(VisitorRepositoryInterface::class);
    }

    public function collection()
    {
        $attributes = $this->attributes;
        $visitors = $this->visitorRepo
            ->visitors( config('setting.status.all') )
            ->with('visit.person', 'visit.department', 'visit.visitPurpose')
            ->whereHas('visit.department')
            ->whereHas('visit.person')
            ->whereHas('visit.visitPurpose')
        ;

        // Visit Join
        $query = $visitors->whereHas('visit', function($visit) use ($attributes) {

            // Company Filter
            if(isset($attributes->company_id)) {
                $visit->where('company_id', $attributes->company_id);
            }

            // Date Range Filter
            if(isset($attributes->end_date) || isset($attributes->start_date) ) {
                $visit->whereBetween('visit_date', [$attributes->start_date, $attributes->end_date]);
            }

            // Time Range Filter
            if(isset($attributes->end_time) || isset($attributes->start_time) ) {
                $visit->whereBetween('visit_time_in', [$attributes->start_time, $attributes->end_time]);
            }

            // Person Name Filter
            if(isset($attributes->param_filter) && $attributes->param_filter == "persons.name") {
                $visit->whereHas('person', function($person) use($attributes) {
                    $person->where($attributes->param_filter, 'like', '%'.$attributes->value_search.'%');
                });
            }

            // Visitor Name Filter
            if(isset($attributes->param_filter) && $attributes->param_filter == "visitors.name") {
                $visit->where($attributes->param_filter, 'like', '%'.$attributes->value_search.'%');
            }

            // Visitor Name Filter
            if(isset($attributes->param_filter) && $attributes->param_filter == "visitors.visit_card_number") {
                $visit->where($attributes->param_filter, 'like', '%'.$attributes->value_search.'%');
            }


        });

        return $query->get();
    }

    public function headings(): array
    {
        return [
            'Id',
            'VC Number',
            'Department',
            'Intended Person',
            'Name',
            'From',
            'Purpose',
            'Date & Time in',
            'Status'
        ];
    }

    public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_TEXT
        ];
    }

    public function map($visitors): array
    {
        return [
            $visitors->visit->id,
            $visitors->visit->visit_card_number,
            $visitors->visit->department->name,
            $visitors->visit->person->name,
            $visitors->name,
            $visitors->from,
            $visitors->visit->visitPurpose->name,
            $visitors->visit->visit_date.' '.$visitors->visit->visit_time_in,
            ($visitors->visit->status == 2 ? 'Checked Out' : 'Checked In'),
        ];
    }

}
