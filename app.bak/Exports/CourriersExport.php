<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Repositories\Courrier\CourrierRepositoryInterface;


class CourriersExport implements FromCollection, WithHeadings, WithColumnFormatting, WithMapping, ShouldAutoSize
{
    protected $attributes, $courrierRepo;

    function __construct( $attributes ) {

        $this->attributes = $attributes;
        $this->courrierRepo = app(CourrierRepositoryInterface::class);
    }

    public function collection()
    {
        $attributes = $this->attributes;
        $courriers = $this->courrierRepo
            ->courriers( config('setting.status.all') )
            ->with('visit.person', 'visit.department')
            ->whereHas('visit.department')
            ->whereHas('visit.person')
        ;

        // Visit Join
        $query = $courriers->whereHas('visit', function($visit) use ($attributes) {

            // Company Filter
            if(isset($attributes->company_id)) {
                $visit->where('company_id', $attributes->company_id);
            }

            // Date Range Filter
            if(isset($attributes->end_date) || isset($attributes->start_date) ) {
                $visit->whereBetween('visit_date', [$attributes->start_date, $attributes->end_date]);
            }

            // Time Range Filter
            if(isset($attributes->end_time) || isset($attributes->start_time) ) {
                $visit->whereBetween('visit_time_in', [$attributes->start_time, $attributes->end_time]);
            }

            // Package Name Filter
            if(isset($attributes->param_filter) && $attributes->param_filter == "package") {
                $visit->where($attributes->param_filter, 'like', '%'.$attributes->value_search.'%');
            }

            // Person Name Filter
            if(isset($attributes->param_filter) && $attributes->param_filter == "persons.name") {
                $visit->whereHas('person', function($person) use($attributes) {
                    $person->where($attributes->param_filter, 'like', '%'.$attributes->value_search.'%');
                });
            }

        });

        return $query->get();
    }

    public function headings(): array
    {
        return [
            'Id',
            'Package',
            'Department',
            'Intended Person',
            'Date & Time in',
            'Status'
        ];
    }

    public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_TEXT
        ];
    }

    public function map($courriers): array
    {
        return [
            $courriers->visit->id,
            $courriers->package,
            $courriers->visit->department->name,
            $courriers->visit->person->name,
            $courriers->visit->visit_date.' '.$courriers->visit->visit_time_in,
            ($courriers->visit->status == 2 ? 'Checked Out' : 'Checked In'),
        ];
    }

}
