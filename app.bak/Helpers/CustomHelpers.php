<?php 

if (! function_exists('csrf_field')) {
    function csrf_field()
    {
        return new HtmlString('<input type="hidden" name="csrf_token" value="'.csrf_token().'">');
    }
}