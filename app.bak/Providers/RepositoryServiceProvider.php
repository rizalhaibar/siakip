<?php
namespace App\Providers;

use App\Repositories\Role\RoleRepository;
use App\Repositories\Role\RoleRepositoryInterface;

use App\Repositories\Menu\MenuRepository;
use App\Repositories\Menu\MenuRepositoryInterface;

use App\Repositories\AccessControl\AccessControlRepository;
use App\Repositories\AccessControl\AccessControlRepositoryInterface;

use App\Repositories\User\UserRepository;
use App\Repositories\User\UserRepositoryInterface;

use App\Repositories\Company\CompanyRepository;
use App\Repositories\Company\CompanyRepositoryInterface;

use App\Repositories\Department\DepartmentRepository;
use App\Repositories\Department\DepartmentRepositoryInterface;

use App\Repositories\Person\PersonRepository;
use App\Repositories\Person\PersonRepositoryInterface;

use App\Repositories\Storage\StorageRepository;
use App\Repositories\Storage\StorageRepositoryInterface;

use App\Repositories\VisitPurpose\VisitPurposeRepository;
use App\Repositories\VisitPurpose\VisitPurposeRepositoryInterface;

use App\Repositories\Visit\VisitRepository;
use App\Repositories\Visit\VisitRepositoryInterface;

use App\Repositories\Visitor\VisitorRepository;
use App\Repositories\Visitor\VisitorRepositoryInterface;

use App\Repositories\VisitLuggage\VisitLuggageRepository;
use App\Repositories\VisitLuggage\VisitLuggageRepositoryInterface;

use App\Repositories\Param\ParamRepository;
use App\Repositories\Param\ParamRepositoryInterface;

use App\Repositories\Courrier\CourrierRepository;
use App\Repositories\Courrier\CourrierRepositoryInterface;

use App\Repositories\MeetingRoom\MeetingRoomRepository;
use App\Repositories\MeetingRoom\MeetingRoomRepositoryInterface;

use App\Repositories\Meeting\MeetingRepository;
use App\Repositories\Meeting\MeetingRepositoryInterface;

use App\Repositories\Dashboard\DashboardRepository;
use App\Repositories\Dashboard\DashboardRepositoryInterface;

use Illuminate\Support\ServiceProvider;
class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bind the interface to an implementation repository class
     */
    public function register()
    {
        $this->app->bind(
            RoleRepositoryInterface::class,
            RoleRepository::class
        );

        $this->app->bind(
            MenuRepositoryInterface::class,
            MenuRepository::class
        );

        $this->app->bind(
            AccessControlRepositoryInterface::class,
            AccessControlRepository::class
        );

        $this->app->bind(
            UserRepositoryInterface::class,
            UserRepository::class
        );

        $this->app->bind(
            CompanyRepositoryInterface::class,
            CompanyRepository::class
        );
        
        $this->app->bind(
            DepartmentRepositoryInterface::class,
            DepartmentRepository::class
        );

        $this->app->bind(
            PersonRepositoryInterface::class,
            PersonRepository::class
        );
        
        $this->app->bind(
            StorageRepositoryInterface::class,
            StorageRepository::class
        );

        $this->app->bind(
            VisitPurposeRepositoryInterface::class,
            VisitPurposeRepository::class
        );

        $this->app->bind(
            VisitRepositoryInterface::class,
            VisitRepository::class
        );

        $this->app->bind(
            VisitorRepositoryInterface::class,
            VisitorRepository::class
        );
        
        $this->app->bind(
            VisitLuggageRepositoryInterface::class,
            VisitLuggageRepository::class
        );

        $this->app->bind(
            ParamRepositoryInterface::class,
            ParamRepository::class
        );

        $this->app->bind(
            CourrierRepositoryInterface::class,
            CourrierRepository::class
        );

        $this->app->bind(
            MeetingRoomRepositoryInterface::class,
            MeetingRoomRepository::class
        );

        $this->app->bind(
            MeetingRepositoryInterface::class,
            MeetingRepository::class
        );

        $this->app->bind(
            DashboardRepositoryInterface::class,
            DashboardRepository::class
        );
        
    }
}