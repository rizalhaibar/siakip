<?php

namespace App\Repositories\VisitLuggage;

interface VisitLuggageRepositoryInterface
{
    /**
     * 
     */
    function status();

    /**
     * 
     */
    function find(int $id);

    /**
     * 
     */
    function search(string $field, string $value);

    /**
     * 
     */
    function store($attributes);

    /**
     * 
     */
    function update(int $id, $attributes);

    /**
     * 
     */
    function toggle($attributes);

    /**
     * 
     */
    function visitLuggages($status);

    /**
     * 
     */
    function pagination($attributes, int $limit);

}