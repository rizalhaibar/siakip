<?php

namespace App\Repositories\Department;

use App\Repositories\Department\DepartmentRepositoryInterface;
use App\Repositories\BaseRepository;
use App\Traits\FormatMessageTraits;
use App\Models\Department;

class DepartmentRepository extends BaseRepository implements DepartmentRepositoryInterface
{
    use FormatMessageTraits;

    protected $model;

    /**
     * DepartmentRepository constructor.
     * @param
     */
    public function __construct(Department $department)
    {
        //
        $this->model = $department;
        $this->__setup();
    }

    /**
     *
     */
    private function __setup()
    {
        $this->VALUE_EXIST              = config('setting.value.exist');
        $this->VALUE_ZERO               = config('setting.value.zero');
        $this->MESSAGE_VALUE_EXIST      = "Cannot processing action. Data %xxx% still used in another table";
        $this->MESSAGE_TOGGLE_SUCCESS   = "%xxx% status successfuly updated.";
        $this->MESSAGE_TOGGLE_FAILED    = "%xxx% status to update.";
        $this->MESSAGE_CREATE_SUCCESS   = "%xxx% successfuly created.";
        $this->MESSAGE_CREATE_FAILED    = "%xxx% Failed to create.";
        $this->MESSAGE_UPDATE_SUCCESS   = "%xxx% successfuly updated.";
        $this->MESSAGE_UPDATE_FAILED    = "%xxx% Failed to update.";
    }

    /**
     * @param $id
     * @return mixed
     */
    public function hasRelation($id)
    {
        return false ;
    }

    /**
     * @param string $id
     * @param string $message
     * @param object $attributes
     * @return mixed
     */
    public function returnResponse($type, $message, $data)
    {
        return $this->returnMessage($type, $this->format($message, $data->name));
    }

    /**
     *
     */
    public function departments($status)
    {
        $data = $this->model->query();

        if( $status != config('setting.status.all') ) {
            $data->where('status', $status);
        }

        return $data;
    }

    /**
     * @param int $limit
     * @return mixed
     */
    public function pagination($attributes)
    {
        $query = $this->model->query();

        // Filter by Company
        if(isset($attributes->company_id))
        {
            $query->where('company_id', $attributes->company_id);
        }

        // Filter by Search Department Name
        if(isset($attributes->param_filter))
        {
            $query->where($attributes->param_filter, 'like', '%'.$attributes->value_search.'%');
        }

        return $query->paginate($attributes->limit);
    }





}
