<?php

namespace App\Repositories\Param;

interface ParamRepositoryInterface
{
    /**
     * 
     */
    function status();

    /**
     * 
     */
    function find(int $id);

    /**
     * 
     */
    function search(string $field, string $value);

    /**
     * 
     */
    function store($attributes);

    /**
     * 
     */
    function update(int $id, $attributes);

    /**
     * 
     */
    function toggle($attributes);

    /**
     * 
     */
    function params($status);

    /**
     * 
     */
    function pagination(int $limit);

}