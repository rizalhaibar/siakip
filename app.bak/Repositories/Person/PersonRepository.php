<?php

namespace App\Repositories\Person;

use App\Repositories\Person\PersonRepositoryInterface;
use App\Repositories\BaseRepository;
use App\Traits\FormatMessageTraits;
use App\Models\Person;
use App\Imports\PersonsImport;
use Excel;
use File;

class PersonRepository extends BaseRepository implements PersonRepositoryInterface
{
    use FormatMessageTraits;

    protected $model;

    /**
     * PersonRepository constructor.
     * @param
     */
    public function __construct(Person $person)
    {
        //
        $this->model = $person;
        $this->__setup();
    }

    /**
     * Private
     * Setting everything of variables
     * in one function
     */
    private function __setup()
    {
        $this->VALUE_EXIST = config('setting.value.exist');
        $this->VALUE_ZERO = config('setting.value.zero');
        $this->MESSAGE_VALUE_EXIST = "Cannot processing action. Data %xxx% still used in another table";
        $this->MESSAGE_TOGGLE_SUCCESS = "%xxx% status successfuly updated.";
        $this->MESSAGE_TOGGLE_FAILED = "%xxx% status to update.";
        $this->MESSAGE_CREATE_SUCCESS = "%xxx% successfuly created.";
        $this->MESSAGE_CREATE_FAILED = "%xxx% Failed to create.";
        $this->MESSAGE_UPDATE_SUCCESS = "%xxx% successfuly updated.";
        $this->MESSAGE_UPDATE_FAILED = "%xxx% Failed to update.";
        $this->MESSAGE_ATTACH_SUCCESS = "%xxx% Successfully attached.";
        $this->MESSAGE_DETTACH_SUCCESS = "%xxx% Successfully dettached.";
        $this->MESSAGE_IMPORT_EXTENSION_SUCCESS = "Success uploading !";
        $this->MESSAGE_IMPORT_EXTENSION_INCORRECT = "Please upload a valid .xlsx/.xls/.csv file !";
    }

    /**
     * @param $id
     * @return mixed
     */
    public function hasRelation($id)
    {
        return $this->search('id', $id)->whereHas('visits')->exists();
    }

    /**
     * @param string $id
     * @param string $message
     * @param object $attributes
     * @return mixed
     */
    public function returnResponse($type, $message, $data)
    {
        return $this->returnMessage($type, $this->format($message, $data->name));
    }

    /**
     *
     */
    public function persons($status)
    {
        $data = $this->model->query();

        if( $status != config('setting.status.all') ) {
            $data->where('status', $status);
        }

        return $data;
    }

    /**
     * @param int $limit
     * @return mixed
     */
    public function pagination($attributes)
    {
        $query = $this->model->with('department','company');

        // Filter by Company
        if(isset($attributes->company_id))
        {
            $query->where('company_id', $attributes->company_id);
        }

        // Filter by Search Person Name
        if(isset($attributes->param_filter))
        {
            $query->where($attributes->param_filter, 'like', '%'.$attributes->value_search.'%');
        }

        return $query->paginate($attributes->limit);
    }

    /**
     * @param object $attributes
     * @return mixed
     */
    public function do($attributes)
    {
        $data = $this->find($attributes->person);

        if($attributes->doing == "attach") {
            $data->fill( $attributes->toArray() );
            $message = $this->MESSAGE_ATTACH_SUCCESS;
        }

        if($attributes->doing == "dettach") {
            $attributes->merge(['department_id' => null ]);
            $data->fill( $attributes->toArray() );
            $message = $this->MESSAGE_DETTACH_SUCCESS;
        }

        if( !$data->save() ) {
            return $this->returnResponse('warning', $this->MESSAGE_UPDATE_FAILED, $data);
        } else {
            return $this->returnResponse('success', $message, $data);
        }
    }

    /**
     * @param object $attributes
     * @return mixed
     */
    public function import($attributes)
    {
        // Validate file
		if($attributes->hasFile('file'))
		{
            $extension = File::extension($attributes->file->getClientOriginalName());
			if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

                $attributes->status = config('setting.status.active');
                Excel::import(new PersonsImport($attributes), $attributes->file('file'));

                return $this->returnMessage('success', $this->format($this->MESSAGE_IMPORT_EXTENSION_SUCCESS, $extension));
            }

            return $this->returnMessage('success', $this->format($this->MESSAGE_IMPORT_EXTENSION_INCORRECT, $extension));
        }
    }


}
