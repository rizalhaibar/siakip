<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use DB;

/**
 * Class UserController.
 */
class PemerintahController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
	 
	function __construct()
	{
	}
	
	public function index()
    {
		return view('backend.pemerintah.index');
    }
	
	public function about()
    {
		return view('backend.pemerintah.about');
    }
	
	public function contact()
    {
		return view('backend.pemerintah.contact');
    }
	
	public function evaluasi()
    {
		$data		 = $this->listcabang();
		return view('backend.pemerintah.evaluasi',
            [
                'data' => $data
            ]);
    }
	
	
   
	public function listcabang(){ 
		$hasil = DB::select('SELECT aa.td_id, aa.td_dinas, aa.td_keterangan , bb.nilai
						FROM
								( 
								 SELECT td_id, td_dinas, td_keterangan 
								 FROM tbl_dinas 
								 WHERE
									status = 1
								 )aa
						LEFT JOIN
								(
								 SELECT tbl_dinas.td_id, tbl_dinas.td_dinas, tbl_dinas.td_keterangan , b.nilai
								 FROM tbl_dinas 
								 LEFT JOIN tbl_evaluasi b on tbl_dinas.td_id = b.iddinas
								 WHERE
									status = 1
								 AND b.tahun = 2020
								 )bb on bb.td_id = aa.td_id

						GROUP BY aa.td_dinas ASC');
		return $hasil;
    }
	
	
	public function program()
    {
		$data		 = $this->listcabang();
		return view('backend.pemerintah.program',
            [
                'data' => $data
            ]);
    }
	
	public function pengukuran()
    {
		$data		 = $this->listcabang();
		return view('backend.pemerintah.pengukuran',
            [
                'data' => $data
            ]);
    }
	
	public function pelaporan()
    {
		$data		 = $this->listcabang();
		return view('backend.pemerintah.pelaporan',
            [
                'data' => $data
            ]);
    }
	
	
	public function detaildinas()
    {
		return view('backend.pemerintah.evaluasi');
    }
	
	
	public function iku()
    {
		return view('backend.pemerintah.iku');
    }
	
	
	public function rkt()
    {
		return view('backend.pemerintah.rkt');
    }
	
	
	public function pk()
    {
		return view('backend.pemerintah.pk');
    }
	
	
	public function pkperubahan()
    {
		return view('backend.pemerintah.pkperubahan');
    }
	
	public function rencanaaksi()
    {
		return view('backend.pemerintah.rencanaaksi');
    }
	
	
	
}
