<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserValidation;
use App\Models\Role;
use Carbon\Carbon;
use DB;
use Redirect;
/**
 * Class UserController.
 */
class KeselarasanController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
	 
	function __construct()
	{
		$this->middleware(['auth']);
		
        $this->middleware(function ($request, $next)
        {
            $this->user = session('data')->user;
            return $next($request);
        });
	}
	
	public function index()
    {
		
		if($this->user->role_id == 99 || $this->user->role_id == 97){
			$data['breadcrumb'] = [
				'parent' => 'Keselarasan',
				'child' => 'List Dinas'
			];
			$data['listdinas'] = $this->listdinas();

			return view('backend.keselarasan.index', $data);
		}else{
			$id = $this->user->dinas_id;
			$id = \Crypt::encrypt($id);
			$url = Redirect::route('backend.keselarasan.home',array('id' => $id));
			return $url;
		}
	}

	public function home($iddinas)
    {
		
		$iddinas = \Crypt::decrypt($iddinas);
		$data['breadcrumb'] = [
			'parent' => 'Keselarasan',
			'child' => 'List Keselarasan'
		];
		$data['id'] = $iddinas;
		$data['keselarasan'] = $this->keselarasan($iddinas);

		return view('backend.keselarasan.home', $data);
	}

	
	public function listdinas(){ 
		$hasil = DB::select('SELECT td_id, 
									td_dinas, 
									td_keterangan 
								FROM tbl_dinas
								WHERE 
									status = 1 
								ORDER BY `index`');
		return $hasil;
	}

	public function keselarasan($iddinas){
		
        
		$hasil = array();
		
		$result1 	= DB::select('SELECT a.* 
		FROM tbl_tujuan a
		LEFT JOIN tbl_misi b ON a.`idmisi` = b.`tm_id`
		LEFT JOIN tbl_visi c ON b.`idvisi` = c.`tv_id`
		WHERE c.`iddinas` = '. $iddinas.'
		AND a.tahun = '.$_COOKIE['tahun'].'
		AND a.`status` = 0');

		
		foreach($result1 as $tujuan){
			$result2 	= DB::select('SELECT * FROM tbl_sasaran_tujuan WHERE idtujuan = '.$tujuan->tt_id.' AND tst_status = 0');
			
			foreach($result2 as $sasaran){
				if($tujuan->tt_id == $sasaran->idtujuan){
					$result3 	= DB::select('SELECT * FROM tbl_indikator_sasaran_pk WHERE idsasaran = '.$sasaran->tst_id.' AND tis_status = 0');
	
					foreach($result3 as $indikatorsasaran){
						if($sasaran->tst_id == $indikatorsasaran->idsasaran){
							$result4 	= DB::select('SELECT a.*
							FROM tbl_program_sasaran a
							WHERE a.tahun = '.$_COOKIE['tahun'].' AND idindisasaran = '.$indikatorsasaran->tis_id.' AND status=0');
							
							
							foreach ($result4 as $program) {
                                if($indikatorsasaran->tis_id == $program->idindisasaran){

									$result5 	= DB::select('SELECT a.* FROM tbl_indikator_program a
										WHERE a.idprogram = '.$program->tps_id.' AND a.`status` = 0');

										foreach($result5 as $indikatorprogram){
											if($program->tps_id == $indikatorprogram->idprogram){
												$result6 	= DB::select('SELECT a.*
												FROM tbl_kegiatan_program a
												WHERE a.idindiprogram = '.$indikatorprogram->tip_id.' AND a.`status` = 0');

												foreach($result6 as $kegiatan){
													if($kegiatan->idindiprogram == $indikatorprogram->tip_id){
														$result7 	= DB::select('SELECT a.*
														FROM tbl_indikator_kegiatan a
														WHERE a.idkegiatan = '.$kegiatan->tkp_id.' AND a.`tik_status` = 0');
														foreach($result7 as $indikatorkegiatan){
															if($kegiatan->tkp_id == $indikatorkegiatan->idkegiatan){
																$kegiatan->indikatorkegiatan = $result7;
															}
														}	
														$indikatorprogram->kegiatan[] = $kegiatan;
													}
												}	

												$program->indikatorprogram[] = $indikatorprogram;
											}
										}
									$indikatorsasaran->program[] = $program;
                                }
                            }
							
	
							
							$sasaran->indikatorsasaran[] = $indikatorsasaran;
						}
					}
					$tujuan->sasaran[] = $sasaran;
				}
				
			}
			array_push($hasil, $tujuan);
		}
		// dd($hasil);
		$hasil = (object) $hasil;
		return $hasil;
	}
	
	
}
