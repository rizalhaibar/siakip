<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserValidation;
use App\Models\Role;
use Carbon\Carbon;
use Redirect;
use DB;
/**
 * Class UserController.
 */
class IkuController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
	 
	function __construct()
	{
		$this->middleware(['auth']);
		
        $this->middleware(function ($request, $next)
        {
            $this->user = session('data')->user;
            return $next($request);
        });
	}
	
	public function index()
    {
		if($this->user->role_id == 99 || $this->user->role_id == 97){
			$data['breadcrumb'] = [
				'parent' => 'IKU',
				'child' => 'List Dinas'
			];
			$data['listdinas'] = $this->listdinas();

			return view('backend.iku.index', $data);
		}else{
			$id = $this->user->dinas_id;
			
			$url = Redirect::route('backend.iku.home',array('id' => $id));
			return $url;
		}
	}

	public function home($id)
    {
		$data['breadcrumb'] = [
			'parent' => 'IKU',
			'child' => 'List IKU'
		];
		$data['id'] = $id;
		$data['listiku'] = $this->listiku($id);

		return view('backend.iku.home', $data);
	}

	
    public function tambahformula($id)
    {
		$query = DB::select("SELECT * FROM tbl_iku WHERE tis_id =".$id);
		$data['formula'] = $query;
        $data['id'] = $id;
		
        return view('backend.iku.tambah',$data);
    }
	
   
	public function listdinas(){ 
		$hasil = DB::select('SELECT td_id, 
									td_dinas, 
									td_keterangan 
								FROM tbl_dinas
								WHERE 
									status = 1 
								ORDER BY `index`');
		return $hasil;
	}
	
   
	public function listiku($id){ 
		$hasil = array();
		$where = '';
		if($this->user->role_id != 99){
			$where = 'aa.`iddinas` = '.$this->user->dinas_id.' AND';
		}else{
			$where = 'aa.`iddinas` = '.$id.' AND';
		}
		$result4 	= DB::select('SELECT a.* FROM tbl_sasaran_tujuan a
		
								LEFT JOIN tbl_tujuan cc ON cc.tt_id = a.idtujuan
								LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
								LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
								WHERE '.$where.' tst_status = 0');
		
		
		
		
		foreach($result4 as $new4){

				$result6 	= DB::select('SELECT * FROM tbl_indikator_sasaran a 
				
											LEFT JOIN tbl_iku b ON b.tis_id = a.tis_id 
											WHERE idsasaran = '.$new4->tst_id.' AND tis_status = 0');

				foreach($result6 as $new6){
					
					
					if($new4->tst_id == $new6->idsasaran){
						
						$new4->indikatorsasaran = $result6;
					}
				}
			
			array_push($hasil, $new4);
		}
		$hasil = (object) $hasil;
		return $hasil;
	}

	

	public function formulaedit(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_iku')
				->where('id', $input['idsasaran'])
				->update(['formula' => $input['formula']]);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'IKU Sukses Terupdate!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}
	
	
	public function formulasave(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_iku')->insert(
			['tis_id' => $input['idsasaran'], 'formula' => $input['formula']]
		);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'IKU Sukses Tersimpan!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}
	
	
}
