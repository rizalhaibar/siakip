<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserValidation;
use App\Models\Role;
use Carbon\Carbon;
use Redirect;
use DB;
use PDF;
/**
 * Class UserController.
 */
class IkuController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */

	function __construct()
	{
		$this->middleware(['auth']);

        $this->middleware(function ($request, $next)
        {
            $this->user = session('data')->user;
            return $next($request);
        });
	}

	public function index()
    {
		if($this->user->role_id == 99 || $this->user->role_id == 97){
			$data['breadcrumb'] = [
				'parent' => 'IKU',
				'child' => 'List Dinas'
			];
			$data['listdinas'] = $this->listdinas();

			return view('backend.iku.index', $data);
		}else{
			$id = $this->user->dinas_id;

			$url = Redirect::route('backend.iku.home',array('id' => $id));
			return $url;
		}
	}

	public function home($id)
    {
		$data['breadcrumb'] = [
			'parent' => 'IKU',
			'child' => 'List IKU'
		];
		$data['id'] = $id;
		$data['listiku'] = $this->listiku($id);

		return view('backend.iku.home', $data);
	}


    public function tambahformula($id)
    {
		$query = DB::select("SELECT * FROM tbl_iku WHERE tis_id =".$id);
		$data['formula'] = $query;
        $data['id'] = $id;

        return view('backend.iku.tambah',$data);
    }



    public function printiku($id)
    {
		$data['id'] = $id;

		return view('backend.iku.print', $data);
	}


	public function listdinas(){
		$hasil = DB::select('SELECT td_id,
									td_dinas,
									td_keterangan
								FROM tbl_dinas
								WHERE
									status = 1
								ORDER BY `index`');
		return $hasil;
	}

	public function detaildinas($id){
		$hasil = DB::select('SELECT td_id,
									td_dinas,
									td_keterangan
								FROM tbl_dinas
								WHERE
                                    td_id = '.$id.'
                                    AND
									status = 1
								ORDER BY `index`');
		return $hasil;
	}


	public function listiku($id){
		$hasil = array();
		$where = '';
		if($this->user->role_id == 99 || $this->user->role_id == 97 ){
			$where = 'aa.`iddinas` = '.$id.' AND';
		}else{
			$where = 'aa.`iddinas` = '.$this->user->dinas_id.' AND';
		}
		$result4 	= DB::select('SELECT a.* FROM tbl_sasaran_tujuan a

								LEFT JOIN tbl_tujuan cc ON cc.tt_id = a.idtujuan
								LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
								LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
								WHERE '.$where.' tst_status = 0');




		foreach($result4 as $new4){

				$result6 	= DB::select('SELECT * FROM tbl_indikator_sasaran a

											LEFT JOIN tbl_iku b ON b.tis_id = a.tis_id
											WHERE idsasaran = '.$new4->tst_id.' AND tis_status = 0');

				foreach($result6 as $new6){


					if($new4->tst_id == $new6->idsasaran){

						$new4->indikatorsasaran = $result6;
					}
				}

			array_push($hasil, $new4);
		}
		$hasil = (object) $hasil;
		return $hasil;
	}



	public function formulaedit(Request $request){
		$input = $request->all();
		$data = DB::table('tbl_iku')
				->where('id', $input['idsasaran'])
				->update(['formula' => $input['formula']]);
		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'IKU Sukses Terupdate!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}
	}


	public function formulasave(Request $request){
		$input = $request->all();
		$data = DB::table('tbl_iku')->insert(
			['tis_id' => $input['idsasaran'], 'formula' => $input['formula']]
		);
		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'IKU Sukses Tersimpan!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}
	}

    public function listsasaran($id){
		$hasil = array();
		$where = '';
		if($this->user->role_id == 99 || $this->user->role_id == 97 ){
			$where = 'aa.`iddinas` = '.$id.' AND';
		}else{
			$where = 'aa.`iddinas` = '.$this->user->dinas_id.' AND';
		}

		$result4 	= DB::select('SELECT a.* FROM tbl_sasaran_tujuan a

								LEFT JOIN tbl_tujuan cc ON cc.tt_id = a.idtujuan
								LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
								LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
								WHERE '.$where.' tst_status = 0');


		// dd($result4);

		foreach($result4 as $new4){

				$result6 	= DB::select('SELECT * FROM tbl_indikator_sasaran a

											LEFT JOIN tbl_iku b ON b.tis_id = a.tis_id
											WHERE idsasaran = '.$new4->tst_id.' AND tis_status = 0');

				foreach($result6 as $new6){


					if($new4->tst_id == $new6->idsasaran){

						$new4->indikatorsasaran = $result6;
					}
				}

			array_push($hasil, $new4);
		}
		$hasil = (object) $hasil;
		return $hasil;
	}

    public function print(Request $request)
    {
        $dinas =   $this->detaildinas($request->id);
        $listiku = $this->listsasaran($request->id);
        $sa = 1;
        $i = 1;
        $isi = '';

        foreach($listiku as $newsasaran){
            $totalindikatorsasaran=0;

            if(isset($newsasaran->tst_sasaran_tujuan)){
                $isi2 = '';
                if(isset($newsasaran->indikatorsasaran)){
                    foreach($newsasaran->indikatorsasaran as $s=>$indikatorsasaran){
                            $totalindikatorsasaran = array_count_values(array_column($newsasaran->indikatorsasaran, 'idsasaran'))[$newsasaran->tst_id];
                            if($s == 0){

                                $isi2 .= '<td>'.$indikatorsasaran->tis_indikator_sasaran.'</td>'
                                .'<td>'.$indikatorsasaran->formula.'</td></tr>';
                            }else{
                                $isi2 .= '<tr ><td>'.$indikatorsasaran->tis_indikator_sasaran.'</td>'
                                .'<td>'.$indikatorsasaran->formula.'</td></tr>';
                            }
                    }

                }

                $isi .= '<tr>'
                            .'<td rowspan="'.$totalindikatorsasaran.'">'.$i.'</td>'
                            .'<td rowspan="'.$totalindikatorsasaran.'">'.$newsasaran->tst_sasaran_tujuan.' '.$totalindikatorsasaran.'</td>'
                            .$isi2;

            }else{
                $isi .= '<tr>'
                            .'<td>'.$i.'</td>'
                            .'<td>'.$newsasaran->tst_sasaran_tujuan.'</td>'
                            .'<td></td>'
                            .'<td></td>'
                        .'</tr>';
            }
            $i++;
        }

        $pihak_pertama = $request->jabatan_pihak_pertama;

        if ($pihak_pertama != 'WALIKOTA CIMAHI'){
            $pihak_pertama = $request->jabatan_pihak_pertama.' '.strtoupper($dinas[0]->td_dinas);
        }else{
            $pihak_kedua = $request->jabatan_pihak_pertama.' '.strtoupper($dinas[0]->td_dinas);
        }

        $pihak_kedua = $request->jabatan_pihak_kedua;

        if ($pihak_kedua == 'WALIKOTA CIMAHI' ){
            $pihak_kedua = $request->jabatan_pihak_kedua;
        }else{
            $pihak_kedua = $request->jabatan_pihak_kedua.' '.strtoupper($dinas[0]->td_dinas);
        }
        $namespika = '<table class="table borderless" style="margin-left: -7px">
                <tbody>
                    <tr>
                        <td style="width:10%; text-align:left; border: none"><b>Nama</b></td>
                        <td style="width:10%; text-align:center; border: none">: </td>
                        <td style="width:75%; text-align:left; border: none">'.$request->pihak_pertama.'</td>
                    </tr>
                    <tr >
                        <td style="width:10%; text-align:left; border: none">Jabatan</td>
                        <td style="width:10%; text-align:center; border: none">: </td>
                        <td style="width:75%; text-align:left; border: none">'.$pihak_pertama.'</td>
                    </tr>
                </tbody>
            </table>
            <p style="margin-top: -25px">Selanjutnya disebut pihak pertama</p>';
        $isittd = '<table class="table">
                        <tbody>
                            <tr>
                                <td style="width:25%; text-align:left; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none">Cimahi,_________'.date('Y').'</td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none">Pihak pertama,</td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></b></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"><b>'.$pihak_pertama.'</b></td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"></td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"></td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"></td>
                            </tr>

                            <tr>
                                <td style="width:25%; text-align:center; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none">'.$request->pihak_pertama.' <b><br/> '.$request->jabatan_pihak_pertama.' <br/>'.$request->nip.'</b></td>
                            </tr>

                        </tbody>
                    </table>';
        if ($dinas[0]->td_id == 1){
            $namespika = '<p >Selanjutnya disebut pihak pertama</p>';
            $isittd = '<table class="table">
            <tbody>
                <tr>
                    <td style="width:25%; text-align:left; border: none"></td>
                    <td style="width:50%; text-align:center; border: none"> </td>
                    <td style="width:25%; text-align:center; border: none">Cimahi,_________'.date('Y').'</td>
                </tr>
                <tr>
                    <td style="width:25%; text-align:center; border: none"></td>
                    <td style="width:50%; text-align:center; border: none"> </td>
                    <td style="width:25%; text-align:center; border: none">Pihak pertama,</td>
                </tr>
                <tr>
                    <td style="width:25%; text-align:center; border: none"></td>
                    <td style="width:50%; text-align:center; border: none"></td>
                    <td style="width:25%; text-align:center; border: none"><b>WALIKOTA CIMAHI</b></td>
                </tr>
                <tr>
                    <td style="width:25%; text-align:center; border: none"></td>
                    <td style="width:50%; text-align:center; border: none"> </td>
                    <td style="width:25%; text-align:center; border: none"></td>
                </tr>
                <tr>
                    <td style="width:25%; text-align:center; border: none"></td>
                    <td style="width:50%; text-align:center; border: none"> </td>
                    <td style="width:25%; text-align:center; border: none"></td>
                </tr>
                <tr>
                    <td style="width:25%; text-align:center; border: none"></td>
                    <td style="width:50%; text-align:center; border: none"> </td>
                    <td style="width:25%; text-align:center; border: none"></td>
                </tr>

                <tr>
                    <td style="width:25%; text-align:center; border: none"></td>
                    <td style="width:50%; text-align:center; border: none"> </td>
                    <td style="width:25%; text-align:center; border: none">'.$request->pihak_pertama.'</td>
                </tr>

            </tbody>
        </table>';
        }

        $html='<!DOCTYPE html>
        <html lang="en">
        <head>
        	<title>
            INDIKATOR KINERJA UTAMA
            </title>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
            <style>
            .page-break {
                page-break-after: always;
            }
            p {
                line-height: 1.5;
            }
            </style>
        </head>
        <body>
            <div class="page-break">
                <div class="container" style="font-size: 12px;  font-family: Arial, Helvetica, sans-serif;  line-height: 2.5pt; margin-left: 2.3cm; margin-top: 1.3cm; margin-bottom: 0.5cm; margin-right: 2.5cm">
                    <center>
                        <h4 style="font-family: Arial, Helvetica, sans-serif;">
                            LAMPIRAN INDIKATOR KINERJA UTAMA TAHUN '.$_COOKIE['tahun'].'
                        </h4>
                        <h4>
                            KEPALA '.strtoupper($dinas[0]->td_dinas).'
                        </h4>
                    </center>
                    <br/>
                    <br/>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th><center>NO</center></th>
                                <th><center>SASARAN STRATEGIS</center></th>
                                <th><center>INDIKATOR KINERJA</center></th>
                                <th><center>SUMBER DATA</center></th>
                            </tr>
                        </thead>
                        <tbody>
                            '.$isi.'
                        </tbody>
                    </table>

                    <br/>
                    <br/>

                    '.$isittd.'
                    </table>
                </div>
            </div>
        </body>
        </html>';

        // $pdf = PDF::loadHTML($html)->setPaper('a4', 'potrait');
        $pdf = PDF::loadHTML($html)->setPaper('f4', 'potrait');
	    return  $pdf->stream('INDIKATOR KINERJA UTAMA - '.date('d/m/Y').'.pdf');
	    // return $pdf->download($title.' '.$detail_corporate->name.' - '.date('d/m/Y').'.pdf');
    }

}
