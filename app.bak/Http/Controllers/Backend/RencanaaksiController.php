<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserValidation;
use App\Models\Role;
use Carbon\Carbon;
use Redirect;
use DB;
use PDF;
use iio\libmergepdf\Merger;
/**
 * Class UserController.
 */
class RencanaaksiController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */


	function __construct()
	{
		$this->middleware(['auth']);

        $this->middleware(function ($request, $next)
        {
            $this->user = session('data')->user;
            return $next($request);
        });
	}

	public function index()
    {
		if($this->user->role_id == 99 || $this->user->role_id == 97){
			$data['breadcrumb'] = [
				'parent' => 'Rencana Aksi',
				'child' => 'List Dinas'
			];
			$data['listdinas'] = $this->listdinas();

			return view('backend.rencanaaksi.index', $data);
		}else{
			$id = $this->user->dinas_id;

			$url = Redirect::route('backend.rencanaaksi.home',array('id' => $id,'idsasaran' => 0));
			return $url;
		}
	}

	public function home($id, $idsasaran)
    {
		$data['breadcrumb'] = [
			'parent' => 'Rencana Aksi',
			'child' => 'List Rencana Aksi'
		];
		$data['id'] = $id;
		$data['idsasaran'] = $idsasaran;
		$data['listsasaran'] = $this->listsasaran($id);
		if($idsasaran != 0){
			$data['indikatorsasaran'] = $this->listindikatorsasaran($id, $idsasaran);
			$data['program'] = $this->getProgram($idsasaran);
			$data['kegiatan'] = $this->getKegiatan($idsasaran);
		}
		return view('backend.rencanaaksi.home', $data);
	}


	public function tambahtargetsasaran($id, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;


		return view('backend.rencanaaksi.tambahtargetsasaran', $data);
	}


    public function printrencanaaksi($id)
    {
		$data['id'] = $id;

		return view('backend.rencanaaksi.print', $data);
	}


    public function printrencanaaksipelaporan($id)
    {
		$data['id'] = $id;

		return view('backend.rencanaaksi.printpelaporan', $data);
	}

	public function edittargetsasaran($id, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;
		$data['detail'] = DB::table('tbl_indikator_sasaran_triwulan')->where('indisasaran',$id)->first();


		return view('backend.rencanaaksi.edittargetsasaran', $data);
	}

	public function tambahtargetprogram($id, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;


		return view('backend.rencanaaksi.tambahtargetprogram', $data);
	}
	public function edittargetprogram($id, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;
		$data['detail'] = DB::table('tbl_indikator_program_triwulan')->where('indiprogram',$id)->first();


		return view('backend.rencanaaksi.edittargetprogram', $data);
	}

	public function tambahtargetkegiatan($id, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;


		return view('backend.rencanaaksi.tambahtargetkegiatan', $data);
	}
	public function edittargetkegiatan($id, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;
		$data['detail'] = DB::table('tbl_indikator_kegiatan_triwulan')->where('indikegiatan',$id)->first();


		return view('backend.rencanaaksi.edittargetkegiatan', $data);
	}


	public function savetargetindikatorsasaran(Request $request){
		$input = $request->all();
		DB::beginTransaction();
		try {
		DB::table('tbl_indikator_sasaran_triwulan')->insert(
			[
				'indisasaran' => $input['idindikatorsasaran'],
				'twtarget1' => $input['tw1'],
				'twtarget2' => $input['tw2'],
				'twtarget3' => $input['tw3'],
				'twtarget4' => $input['tw4'],
				'tahun' => $_COOKIE['tahun']
				]
		);

		DB::commit();
		$result = array( 'success' => true, 'message' => 'Target Triwulan Indikator Sasaran Sukses Tersimpan!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);
	}


	public function edittargetindikatorsasaran(Request $request){
		$input = $request->all();
		DB::beginTransaction();
		try {

			DB::table('tbl_indikator_sasaran_triwulan')
			->where('indisasaran', $input['id'])
			->update([
					'twtarget1' => $input['tw1'],
					'twtarget2' => $input['tw2'],
					'twtarget3' => $input['tw3'],
					'twtarget4' => $input['tw4']
				]
			);
			DB::commit();
			$result = array( 'success' => true, 'message' => 'Target Triwulan Indikator Sasaran Sukses Teredit!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);
	}

	public function savetargetindikatorprogram(Request $request){
		$input = $request->all();
		DB::beginTransaction();
		try {
		DB::table('tbl_indikator_program_triwulan')->insert(
			[
				'indiprogram' => $input['idindikatorsasaran'],
				'twtarget1' => $input['tw1'],
				'twtarget2' => $input['tw2'],
				'twtarget3' => $input['tw3'],
				'twtarget4' => $input['tw4'],
				'tahun' => $_COOKIE['tahun']
				]
		);

		DB::commit();
		$result = array( 'success' => true, 'message' => 'Target Triwulan Indikator Program Sukses Tersimpan!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);
	}


	public function edittargetindikatorprogram(Request $request){
		$input = $request->all();
		DB::beginTransaction();
		try {

			DB::table('tbl_indikator_program_triwulan')
			->where('indiprogram', $input['id'])
			->update([
					'twtarget1' => $input['tw1'],
					'twtarget2' => $input['tw2'],
					'twtarget3' => $input['tw3'],
					'twtarget4' => $input['tw4']
				]
			);
			DB::commit();
			$result = array( 'success' => true, 'message' => 'Target Triwulan Indikator Program Sukses Teredit!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);
	}

	public function savetargetindikatorkegiatan(Request $request){
		$input = $request->all();
		DB::beginTransaction();
		try {
		DB::table('tbl_indikator_kegiatan_triwulan')->insert(
			[
				'indikegiatan' => $input['idindikatorsasaran'],
				'twtarget1' => $input['tw1'],
				'twtarget2' => $input['tw2'],
				'twtarget3' => $input['tw3'],
				'twtarget4' => $input['tw4'],
				'tahun' => $_COOKIE['tahun']
				]
		);

		DB::commit();
		$result = array( 'success' => true, 'message' => 'Target Triwulan Indikator Kegiatan Sukses Tersimpan!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);
	}


	public function edittargetindikatorkegiatan(Request $request){
		$input = $request->all();
		DB::beginTransaction();
		try {

			DB::table('tbl_indikator_kegiatan_triwulan')
			->where('indikegiatan', $input['id'])
			->update([
					'twtarget1' => $input['tw1'],
					'twtarget2' => $input['tw2'],
					'twtarget3' => $input['tw3'],
					'twtarget4' => $input['tw4']
				]
			);
			DB::commit();
			$result = array( 'success' => true, 'message' => 'Target Triwulan Indikator Kegiatan Sukses Teredit!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);
	}












	public function listdinas(){
		$hasil = DB::select('SELECT td_id,
									td_dinas,
									td_keterangan
								FROM tbl_dinas
								WHERE
									status = 1
								ORDER BY `index`');
		return $hasil;
	}


	public function listsasaran($id){
		$where = '';
		if($this->user->role_id == 99 || $this->user->role_id == 97 ){
			$where = 'aa.`iddinas` = '.$id.' AND';
		}else{
			$where = 'aa.`iddinas` = '.$this->user->dinas_id.' AND';
		}

		$result4 	= DB::select('SELECT a.* FROM tbl_sasaran_tujuan a

								LEFT JOIN tbl_tujuan cc ON cc.tt_id = a.idtujuan
								LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
								LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
								WHERE '.$where.' a.tst_status = 0');
		return $result4;
	}

	public function listindikatorsasaran($id, $idsasaran){

		$where = '';
		if($this->user->role_id == 99 || $this->user->role_id == 97 ){
			$where = 'aa.`iddinas` = '.$id.' AND';
		}else{
			$where = 'aa.`iddinas` = '.$this->user->dinas_id.' AND';
		}
		$result6 	= DB::select('SELECT  aadc.tis_id, aadc.tis_indikator_sasaran, aadc.tis_satuan,  aadc.tis_target,
							bbdc.twtarget1,
							bbdc.twtarget2,
							bbdc.twtarget3,
							bbdc.twtarget4
					FROM
					(
					SELECT a.tis_id, a.tis_indikator_sasaran, a.tis_satuan, a.tis_target, a.idsasaran, a.tis_status
					FROM tbl_indikator_sasaran_pk a
					LEFT JOIN tbl_sasaran_tujuan dd ON a.`idsasaran` = dd.`tst_id`
					LEFT JOIN tbl_tujuan cc ON cc.tt_id = dd.idtujuan
					LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
					LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
					)aadc
					LEFT JOIN
					(

					SELECT a.tis_id, a.tis_indikator_sasaran, a.tis_satuan,  a.idsasaran,
						sa.`tw1`, sa.`twtarget1`,
						sa.`tw2`, sa.`twtarget2`,
						sa.`tw3`, sa.`twtarget3`,
						sa.`tw4`, sa.`twtarget4`
					FROM tbl_indikator_sasaran_pk a
					LEFT JOIN tbl_indikator_sasaran_triwulan sa ON a.tis_id = sa.indisasaran
					LEFT JOIN tbl_sasaran_tujuan dd ON dd.`tst_id` = a.`idsasaran`
					LEFT JOIN tbl_tujuan cc ON cc.tt_id = dd.idtujuan
					LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
					LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
					)bbdc ON aadc.tis_id = bbdc.tis_id
					WHERE aadc.idsasaran =  '.$idsasaran.' AND aadc.tis_status = 0');
		return $result6;
	}
	public function getKegiatanAll($id){
		$hasil = array();



        $where = '';
		if($this->user->role_id == 99 || $this->user->role_id == 97 ){
			$where = 'aa.`iddinas` = 5 AND';
			// $where = 'aa.`iddinas` = '.$id.' AND';
		}else{
			$where = 'aa.`iddinas` = '.$this->user->dinas_id.' AND';
		}
		$result1 	= DB::select('SELECT a.*
		FROM tbl_program_sasaran a
		LEFT JOIN tbl_indikator_sasaran_pk b ON a.`idindisasaran` = b.`tis_id`
		LEFT JOIN tbl_sasaran_tujuan c ON b.`idsasaran` = c.`tst_id`
        LEFT JOIN tbl_tujuan cc ON cc.tt_id = c.idtujuan
        LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
        LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
		WHERE '.$where.' a.status = 0');

		foreach($result1 as $program){

			$result2 	= DB::select('SELECT a.* FROM tbl_indikator_program a
										WHERE a.idprogram = '.$program->tps_id.' AND a.`status` = 0');

			foreach($result2 as $indikatorprogram){
				if($program->tps_id == $indikatorprogram->idprogram){
					$result3 	= DB::select('SELECT a.*
					FROM tbl_kegiatan_program a
					WHERE a.idindiprogram = '.$indikatorprogram->tip_id.' AND a.`status` = 0');

					foreach($result3 as $kegiatan){
						if($kegiatan->idindiprogram == $indikatorprogram->tip_id){
							$result4 	= DB::select('SELECT
							aadc.tik_id, aadc.idkegiatan, aadc.tik_indikator_kegiatan, aadc.tik_satuan, aadc.tik_target,
							bbdc.twtarget1, bbdc.twtarget2, bbdc.twtarget3, bbdc.twtarget4
							FROM (
								SELECT a.tik_id, a.idkegiatan, a.tik_indikator_kegiatan, a.tik_satuan, a.tik_target, a.`tik_status`
								FROM tbl_indikator_kegiatan a
							)aadc
							LEFT JOIN (
								SELECT
								a.tik_id, a.idkegiatan, a.tik_indikator_kegiatan, a.tik_satuan, a.tik_target, a.`tik_status` ,
								sa.`tw1`, sa.`twtarget1`, sa.`tw2`, sa.`twtarget2`, sa.`tw3`, sa.`twtarget3`, sa.`tw4`, sa.`twtarget4`
								FROM tbl_indikator_kegiatan a
								LEFT JOIN tbl_indikator_kegiatan_triwulan sa
								ON a.tik_id = sa.indikegiatan
							)bbdc ON aadc.tik_id = bbdc.tik_id
							WHERE aadc.idkegiatan = '.$kegiatan->tkp_id.' AND aadc.`tik_status` = 0');
							foreach($result4 as $indikatorkegiatan){
								if($kegiatan->tkp_id == $indikatorkegiatan->idkegiatan){
									$kegiatan->indikatorkegiatan = $result4;
								}
							}
							$indikatorprogram->kegiatan[] = $kegiatan;
						}
					}

					$program->indikatorprogram[] = $indikatorprogram;
				}
			}
			array_push($hasil, $program);
		}
		$hasil = (object) $hasil;
		return $hasil;
	}

    public function getKegiatan($id){
		$hasil = array();

		$result1 	= DB::select('SELECT a.*
		FROM tbl_program_sasaran a
		LEFT JOIN tbl_indikator_sasaran_pk b ON a.`idindisasaran` = b.`tis_id`
		LEFT JOIN tbl_sasaran_tujuan c ON b.`idsasaran` = c.`tst_id`
		WHERE c.`tst_id` = '.$id.' AND a.status = 0');

		foreach($result1 as $program){

			$result2 	= DB::select('SELECT a.* FROM tbl_indikator_program a
										WHERE a.idprogram = '.$program->tps_id.' AND a.`status` = 0');

			foreach($result2 as $indikatorprogram){
				if($program->tps_id == $indikatorprogram->idprogram){
					$result3 	= DB::select('SELECT a.*
					FROM tbl_kegiatan_program a
					WHERE a.idindiprogram = '.$indikatorprogram->tip_id.' AND a.`status` = 0');

					foreach($result3 as $kegiatan){
						if($kegiatan->idindiprogram == $indikatorprogram->tip_id){
							$result4 	= DB::select('SELECT
							aadc.tik_id, aadc.idkegiatan, aadc.tik_indikator_kegiatan, aadc.tik_satuan, aadc.tik_target,
							bbdc.twtarget1, bbdc.twtarget2, bbdc.twtarget3, bbdc.twtarget4
							FROM (
								SELECT a.tik_id, a.idkegiatan, a.tik_indikator_kegiatan, a.tik_satuan, a.tik_target, a.`tik_status`
								FROM tbl_indikator_kegiatan a
							)aadc
							LEFT JOIN (
								SELECT
								a.tik_id, a.idkegiatan, a.tik_indikator_kegiatan, a.tik_satuan, a.tik_target, a.`tik_status` ,
								sa.`tw1`, sa.`twtarget1`, sa.`tw2`, sa.`twtarget2`, sa.`tw3`, sa.`twtarget3`, sa.`tw4`, sa.`twtarget4`
								FROM tbl_indikator_kegiatan a
								LEFT JOIN tbl_indikator_kegiatan_triwulan sa
								ON a.tik_id = sa.indikegiatan
							)bbdc ON aadc.tik_id = bbdc.tik_id
							WHERE aadc.idkegiatan = '.$kegiatan->tkp_id.' AND aadc.`tik_status` = 0');
							foreach($result4 as $indikatorkegiatan){
								if($kegiatan->tkp_id == $indikatorkegiatan->idkegiatan){
									$kegiatan->indikatorkegiatan = $result4;
								}
							}
							$indikatorprogram->kegiatan[] = $kegiatan;
						}
					}

					$program->indikatorprogram[] = $indikatorprogram;
				}
			}
			array_push($hasil, $program);
		}
		$hasil = (object) $hasil;
		return $hasil;
	}
	public function getProgram($id){
		$hasil = array();

		$result1 	= DB::select('SELECT a.* FROM tbl_indikator_sasaran_pk a
							LEFT JOIN tbl_iku b ON b.tis_id = a.tis_id
							LEFT JOIN tbl_eselon c ON c.te_id = a.pic
							WHERE idsasaran = '.$id.' AND tis_status = 0');

		// echo '<pre>';
        foreach ($result1 as $indikatorsasaran) {
			$result2 = DB::select('SELECT a.*
									FROM tbl_program_sasaran a
									WHERE a.tahun = '.$_COOKIE['tahun'].' AND idindisasaran = '.$indikatorsasaran->tis_id.' AND status=0');
            foreach ($result2 as $program) {
				if($indikatorsasaran->tis_id == $program->idindisasaran){
					$result3 	= DB::select('SELECT aadc.tip_id, aadc.idprogram, aadc.tip_indikator_program,  aadc.tip_satuan, aadc.tip_target,
												bbdc.twtarget1,
												bbdc.twtarget2,
												bbdc.twtarget3,
												bbdc.twtarget4
											FROM
											(
												SELECT a.tip_id, a.idprogram, a.tip_indikator_program, a.tip_satuan, a.tip_target, a.`status`
												FROM tbl_indikator_program a
											)aadc
											LEFT JOIN
											(
												SELECT a.tip_id, a.idprogram, a.tip_indikator_program, a.tip_satuan, a.tip_target, a.`status`,
												sa.`tw1`, sa.`twtarget1`,
												sa.`tw2`, sa.`twtarget2`,
												sa.`tw3`, sa.`twtarget3`,
												sa.`tw4`, sa.`twtarget4`
												FROM tbl_indikator_program a
												LEFT JOIN tbl_indikator_program_triwulan sa ON a.tip_id = sa.indiprogram
											)bbdc ON aadc.tip_id = bbdc.tip_id
											WHERE aadc.idprogram = '.$program->tps_id.' AND aadc.`status` = 0');


					foreach($result3 as $indikatorprogram){


						if($program->tps_id == $indikatorprogram->idprogram){

							$program->indikatorprogram = $result3;
						}
					}
					$indikatorsasaran->program[] = $program;
				}

            }
			array_push($hasil, $indikatorsasaran);
		}
		// print_r($hasil);
		// die;
		$hasil = (object) $hasil;
		return $hasil;
	}

    public function detaildinas($id){
		$hasil = DB::select('SELECT td_id,
									td_dinas,
									td_keterangan
								FROM tbl_dinas
								WHERE
                                    td_id = '.$id.'
                                    AND
									status = 1
								ORDER BY `index`');
		return $hasil;
	}



    public function print(Request $request)
    {
        $dinas =   $this->detaildinas($request->id);
        $kegiatan = $this->getKegiatanAll($request->id);

        $ik=1;

        $htmlk = '';
        $isik1 = '';
        $isik2 = '';
        $isik3 = '';
        $isik4 = '';
        $isik5 = '';
        $isik5s = '';
        $isik6 = '';
        $isik7 = '';
        $akhirtotala = 0;

        foreach($kegiatan as $keg){

            $totalprogramk=0;
            $totalatasprogram=0;
            $testatas=0;
            $alltotalkegiatan=0;
            $alltotalkegiatanbawah=0;

            $totalbawahprogram=0;
            $totalbawahprogram2=0;
            if(isset($keg->indikatorprogram)){
                foreach($keg->indikatorprogram as $m=>$indikatorprogram){

                    $totalkegiatan=0;
                    $totalkegiatanbawah=0;
                    $totalindikatorkegiatank=0;
                    $allataskegiatan=0;
                    $allbawahkegiatan=0;

                    if(isset($indikatorprogram->kegiatan)){

                        $totalindikatorkegiatan=0;
                        $totalindikatorkegiatanbawah=0;

                        foreach($indikatorprogram->kegiatan as $as=>$kga){

                            if(isset($kga->indikatorkegiatan)){


                                foreach($kga->indikatorkegiatan as $inkas=>$indikatorkegiatan){
                                    if($inkas == 0){
                                        $totalindikatorkegiatan += array_count_values(array_column($kga->indikatorkegiatan, 'idkegiatan'))[$kga->tkp_id];

                                        $totalindikatorkegiatanbawah = 1;
                                        $isik5 = '<td > '.$indikatorkegiatan->tik_indikator_kegiatan.'  </td>'
                                                            .'<td>'.$indikatorkegiatan->tik_satuan.'</td>'
                                                            .'<td>'.$indikatorkegiatan->tik_target.'</td>'
                                                            .'<td>'.$indikatorkegiatan->twtarget1.'</td>'
                                                            .'<td><b>'.@$indikatorkegiatan->tw1.'</b></td>'
                                                            .'<td>'.$indikatorkegiatan->twtarget2.'</td>'
                                                            .'<td><b>'.@$indikatorkegiatan->tw2.'</b></td>'
                                                            .'<td>'.$indikatorkegiatan->twtarget3.'</td>'
                                                            .'<td><b>'.@$indikatorkegiatan->tw3.'</b></td>'
                                                            .'<td>'.$indikatorkegiatan->twtarget4.'</td>'
                                                            .'<td><b>'.@$indikatorkegiatan->tw4.'</b></td>'
                                                        .'</tr>';
                                    }else{

                                        $totalindikatorkegiatanbawah += 1;


                                            $isik5 .= '<tr >'
                                                                .'<td >'.$indikatorkegiatan->tik_indikator_kegiatan.' </td>'
                                                                .'<td>'.$indikatorkegiatan->tik_satuan.'</td>'
                                                                .'<td>'.$indikatorkegiatan->tik_target.'</td>'
                                                                .'<td>'.$indikatorkegiatan->twtarget1.'</td>'
                                                                .'<td><b>'.@$indikatorkegiatan->tw1.'</b></td>'
                                                                .'<td>'.$indikatorkegiatan->twtarget2.'</td>'
                                                                .'<td><b>'.@$indikatorkegiatan->tw2.'</b></td>'
                                                                .'<td>'.$indikatorkegiatan->twtarget3.'</td>'
                                                                .'<td><b>'.@$indikatorkegiatan->tw3.'</b></td>'
                                                                .'<td>'.$indikatorkegiatan->twtarget4.'</td>'
                                                                .'<td><b>'.@$indikatorkegiatan->tw4.'</b></td>'
                                                            .'</tr>';
                                    }
                                }

                                if($as == 0){

                                    $allataskegiatan+=$totalindikatorkegiatan;
                                    $totalkegiatan += array_count_values(array_column($indikatorprogram->kegiatan, 'idindiprogram'))[$indikatorprogram->tip_id];
                                    $isik2 = '<td rowspan="'.$totalindikatorkegiatan.'">'.$kga->tkp_kegiatan_program.'  </td>'
                                                        .$isik5;
                                }else{
                                    $totalkegiatan += 1;


                                    $allbawahkegiatan += $totalindikatorkegiatanbawah;


                                        $isik2 .= '<tr ><td rowspan="'.$totalindikatorkegiatanbawah.'"> '.$kga->tkp_kegiatan_program.'</td>'
                                                            .$isik5;
                                }

                            }else{
                                if($as == 0){
                                    $allataskegiatan+=1;
                                    $totalkegiatan += array_count_values(array_column($indikatorprogram->kegiatan, 'idindiprogram'))[$indikatorprogram->tip_id];
                                    $isik2 = '<td>'.$kga->tkp_kegiatan_program.' </td>'
                                                        .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                        .'</tr>';
                                }else{

                                    $allbawahkegiatan = $allbawahkegiatan + 1;

                                        $isik2 .= '<tr ><td > '.$kga->tkp_kegiatan_program.'</td>'
                                                    .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                            .'<td></td>'
                                                        .'</tr>';
                                }
                            }
                        }


                        if($m == 0){
                            $alltotalkegiatan = $allataskegiatan + $allbawahkegiatan;
                            $isik1 = '<td rowspan="'.$alltotalkegiatan.'"> '.$indikatorprogram->tip_indikator_program.'  </td>'
                                            .$isik2;
                        }else{
                            $alltotalkegiatanbawah = $allataskegiatan + $allbawahkegiatan;
                            $testatas += $alltotalkegiatanbawah;

                                $isik1 .= '<tr>'
                                            .'<td rowspan="'.$alltotalkegiatanbawah.'">'.$indikatorprogram->tip_indikator_program.'   </td>'
                                            .$isik2;
                        }

                    }else{


                        if($m == 0){

                        $alltotalkegiatan = $alltotalkegiatan + 1;
                            $isik1 = '<td>'.$indikatorprogram->tip_indikator_program.'</td>'
                                            .'<td></td>'
                                            .'<td></td>'
                                            .'<td></td>'
                                            .'<td></td>'
                                            .'<td></td>'
                                            .'<td></td>'
                                            .'<td></td>'
                                            .'<td></td>'
                                            .'<td></td>'
                                            .'<td></td>'
                                            .'<td></td>'
                                            .'<td></td>'
                                        .'</tr>';
                        }else{

                        $testatas += 1;

                                $isik1 .= '<tr ><td >'.$indikatorprogram->tip_indikator_program.' </td>'
                                            .'<td></td>'
                                            .'<td></td>'
                                            .'<td></td>'
                                            .'<td></td>'
                                            .'<td></td>'
                                            .'<td></td>'
                                            .'<td></td>'
                                            .'<td></td>'
                                            .'<td></td>'
                                            .'<td></td>'
                                            .'<td></td>'
                                            .'<td></td>'
                                        .'</tr>';
                        }

                    }
                }
                $akhirtotal =  $alltotalkegiatan + $testatas;
                $htmlk .= '<tr>'
                                    .'<td rowspan="'.$akhirtotal.'">'.$ik.'</td>'
                                    .'<td rowspan="'.$akhirtotal.'">'.$keg->tps_program_sasaran.' </td>'
                                    .$isik1;
            }else{

                $htmlk .= '<tr>'
                                .'<td>'.$ik.'</td>'
                                .'<td>'.$keg->tps_program_sasaran.'</td>'
                                .'<td></td>'
                                .'<td></td>'
                                .'<td></td>'
                                .'<td></td>'
                                .'<td></td>'
                                .'<td></td>'
                                .'<td></td>'
                                .'<td></td>'
                                .'<td></td>'
                                .'<td></td>'
                                .'<td></td>'
                                .'<td></td>'
                                .'<td></td>'
                                .'<td></td>'
                            .'</tr>';
            }
            $ik++;
        }


        $html='<!DOCTYPE html>
        <html lang="en">
        <head>
        	<title>
            RENCANA AKSI KINERJA TAHUNAN
            </title>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
            <style>
            .page-break {
                page-break-after: always;
            }
            </style>
        </head>
        <body style="font-size: 12px">
            <div class="container">
                <center>
                    <img src="'.public_path('img/Logo-Cimahi.png').'" style="width:auto; height: 100px"/>
                    <h4>
                        RENCANA AKSI KINERJA TAHUNAN TAHUN '.$_COOKIE['tahun'].'</h4>
                    <h4>KEPALA '.strtoupper($dinas[0]->td_dinas).'</h4>

                </center>
                <br/>
                <br/>
                <br/>
                <p>
                    Dalam rangka mewujudkan manajemen Pemerintahan yang efektif, transparan, dan akuntabel serta berorientasi pada hasil, kami yang bertandatangan di bawah ini :
                </p>
                <table class="table borderless">
                    <tbody>
                        <tr>
                            <td style="width:10%; text-align:left; border: none">Nama</td>
                            <td style="width:10%; text-align:center; border: none">: </td>
                            <td style="width:75%; text-align:left; border: none">'.$request->pihak_pertama.'</td>
                        </tr>
                        <tr>
                            <td style="width:10%; text-align:left; border: none">Jabatan</td>
                            <td style="width:10%; text-align:center; border: none">: </td>
                            <td style="width:75%; text-align:left; border: none">KEPALA '.$dinas[0]->td_dinas.'</td>
                        </tr>
                    </tbody>
                </table>
                <p>Selanjutnya disebut pihak pertama</p>

                <table class="table borderless">
                    <tbody>
                        <tr>
                            <td style="width:10%; text-align:left; border: none">Nama</td>
                            <td style="width:10%; text-align:center; border: none">: </td>
                            <td style="width:25%; text-align:left; border: none">'.$request->pihak_kedua.'</td>
                        </tr>
                        <tr>
                            <td style="width:10%; text-align:left; border: none">Jabatan</td>
                            <td style="width:10%; text-align:center; border: none">: </td>
                            <td style="width:75%; text-align:left; border: none">WALI KOTA CIMAHI</td>
                        </tr>
                    </tbody>
                </table>
                <p>Selaku atasan pihak pertama, selanjutnya disebut pihak kedua	</p>
                <br/>
                <br/>
                <p>Pihak pertama berjanji akan mewujudkan target kinerja yang seharusnya sesuai lampiran perjanjian ini, dalam rangka mencapai target kinerja jangka menengah seperti yang telah ditetapkan dalam dokumen perencanaan. Keberhasilan dan kegagalan pencapaian target kinerja tersebut menjadi tanggung jawab kami.</p>
                <br/>

                <p>Pihak kedua akan melakukan supervisi yang diperlukan serta akan melakukan evaluasi terhadap capaian kinerja dari perjanjian ini dan mengambil tindakan yang diperlukan dalam rangka pemberian penghargaan dan sanksi.</p>
                <br/>
                <br/>
                <table class="table">
                        <tbody>
                            <tr>
                                <td style="width:25%; text-align:left; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none">Cimahi, '.date('d-m-Y').'</td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none">Pihak kedua,</td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none">Pihak pertama,</td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"><b>WALI KOTA CIMAHI,</b></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"><b>KEPALA <br/>'.$dinas[0]->td_dinas.'</b></td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"></td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"></td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"></td>
                            </tr>

                            <tr>
                                <td style="width:25%; text-align:center; border: none">'.$request->pihak_kedua.'</td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none">'.$request->pihak_pertama.' <b><br/> Pangkat <br/>NIP</b></td>
                            </tr>

                        </tbody>
                    </table>
            </div>
        </body>
        </html>';


        $html2='<!DOCTYPE html>
        <html lang="en">
        <head>
        	<title>
            RENCANA AKSI KINERJA TAHUNAN
            </title>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
            <style>
            .page-break {
                page-break-after: always;
            }
            </style>
        </head>
        <body style="font-size: 9px">
        <div class="container">
            <center>
                <h4>
                    LAMPIRAN RENCANA AKSI KINERJA TAHUNAN TAHUN '.$_COOKIE['tahun'].'
                </h4>
                <h4>
                    KEPALA '.strtoupper($dinas[0]->td_dinas).'
                </h4>
            </center>
            <br/>
            <br/>
            <table class="table table-bordered page-break" style="font-size: 9px; ">
                <thead>
                    <tr>

                        <tr>
                            <th rowspan="2">No</th>
                            <th rowspan="2">Sasaran Program</th>
                            <th rowspan="2">Indikator Sasaran Program</th>
                            <th rowspan="2">Sasaran Kegiatan</th>
                            <th rowspan="2">Indikator Sasaran Kegiatan</th>
                            <th rowspan="2">Satuan</th>
                            <th rowspan="2">Target Per Tahun</th>
                            <th colspan="2"><center>Triwulan 1</center></th>
                            <th colspan="2"><center>Triwulan 2</center></th>
                            <th colspan="2"><center>Triwulan 3</center></th>
                            <th colspan="2"><center>Triwulan 4</center></th>
                        </tr>
                        <tr>
                            <th>Target</th>
                            <th>Realisasi</th>
                            <th>Target</th>
                            <th>Realisasi</th>
                            <th>Target</th>
                            <th>Realisasi</th>
                            <th>Target</th>
                            <th>Realisasi</th>

                        </tr>
                    </tr>
                </thead>
                <tbody>
                    '.$htmlk.'
                </tbody>
            </table>

            <br/>
            <br/>

            <table class="table">
                <tbody>
                    <tr>
                        <td style="width:25%; text-align:left; border: none"></td>
                        <td style="width:50%; text-align:center; border: none"> </td>
                        <td style="width:25%; text-align:center; border: none">Cimahi, '.date('d-m-Y').'</td>
                    </tr>
                    <tr>
                        <td style="width:25%; text-align:center; border: none">Pihak kedua,</td>
                        <td style="width:50%; text-align:center; border: none"> </td>
                        <td style="width:25%; text-align:center; border: none">Pihak pertama,</td>
                    </tr>
                    <tr>
                        <td style="width:25%; text-align:center; border: none"><b>WALI KOTA CIMAHI,</b></td>
                        <td style="width:50%; text-align:center; border: none"> </td>
                        <td style="width:25%; text-align:center; border: none"><b>KEPALA <br/>'.$dinas[0]->td_dinas.'</b></td>
                    </tr>
                    <tr>
                        <td style="width:25%; text-align:center; border: none"></td>
                        <td style="width:50%; text-align:center; border: none"> </td>
                        <td style="width:25%; text-align:center; border: none"></td>
                    </tr>
                    <tr>
                        <td style="width:25%; text-align:center; border: none"></td>
                        <td style="width:50%; text-align:center; border: none"> </td>
                        <td style="width:25%; text-align:center; border: none"></td>
                    </tr>
                    <tr>
                        <td style="width:25%; text-align:center; border: none"></td>
                        <td style="width:50%; text-align:center; border: none"> </td>
                        <td style="width:25%; text-align:center; border: none"></td>
                    </tr>

                    <tr>
                        <td style="width:25%; text-align:center; border: none">'.$request->pihak_kedua.'</td>
                        <td style="width:50%; text-align:center; border: none"> </td>
                        <td style="width:25%; text-align:center; border: none">'.$request->pihak_pertama.' <b><br/> Pangkat <br/>NIP</b></td>
                    </tr>

                </tbody>
            </table>
        </div>
        </body>
        </html>';
        $m = new Merger();

        // $pdf = PDF::loadHTML($html)->setPaper('f4', 'potrait');
        // $pdf = $pdf->stream();
        $pdf = PDF::loadHTML($html2)->setPaper('f4', 'landscape');
        // $pdf = $pdf->stream();

        // $pdf = PDF::loadHTML($html2, [], [
        //     'title'      => 'RENCANA AKSI',
        //     'format'      => 'f4',
        //     'orientation'      => 'L',
        //     'default_font_size'      => '12',
        //     'margin_top' => 0
        // ]);

        $pdf = $pdf->stream();

        return $pdf;
	    // return  $pdf->stream('RENCANA AKSI - '.date('d/m/Y').'.pdf');
	    // return $pdf->download($title.' '.$detail_corporate->name.' - '.date('d/m/Y').'.pdf');
    }


    public function printlaporan(Request $request)
    {
        $dinas =   $this->detaildinas($request->id);

        $html='<!DOCTYPE html>
        <html lang="en">
        <head>
        	<title>
            RENCANA AKSI KINERJA TAHUNAN
            </title>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
            <style>
            .page-break {
                page-break-after: always;
            }
            </style>
        </head>
        <body style="font-size: 12px">
            <div class="container">
                <center>
                    <img src="'.public_path('img/Logo-Cimahi.png').'" style="width:auto; height: 100px"/>
                    <h4>
                        RENCANA AKSI KINERJA TAHUNAN TAHUN '.$_COOKIE['tahun'].'</h4>
                    <h4>KEPALA '.strtoupper($dinas[0]->td_dinas).'</h4>

                </center>
                <br/>
                <br/>
                <br/>
                <p>
                    Dalam rangka mewujudkan manajemen Pemerintahan yang efektif, transparan, dan akuntabel serta berorientasi pada hasil, kami yang bertandatangan di bawah ini :
                </p>
                <table class="table borderless">
                    <tbody>
                        <tr>
                            <td style="width:10%; text-align:left; border: none">Nama</td>
                            <td style="width:10%; text-align:center; border: none">: </td>
                            <td style="width:75%; text-align:left; border: none">'.$request->pihak_pertama.'</td>
                        </tr>
                        <tr>
                            <td style="width:10%; text-align:left; border: none">Jabatan</td>
                            <td style="width:10%; text-align:center; border: none">: </td>
                            <td style="width:75%; text-align:left; border: none">KEPALA '.$dinas[0]->td_dinas.'</td>
                        </tr>
                    </tbody>
                </table>
                <p>Selanjutnya disebut pihak pertama</p>

                <table class="table borderless">
                    <tbody>
                        <tr>
                            <td style="width:10%; text-align:left; border: none">Nama</td>
                            <td style="width:10%; text-align:center; border: none">: </td>
                            <td style="width:25%; text-align:left; border: none">'.$request->pihak_kedua.'</td>
                        </tr>
                        <tr>
                            <td style="width:10%; text-align:left; border: none">Jabatan</td>
                            <td style="width:10%; text-align:center; border: none">: </td>
                            <td style="width:75%; text-align:left; border: none">WALI KOTA CIMAHI</td>
                        </tr>
                    </tbody>
                </table>
                <p>Selaku atasan pihak pertama, selanjutnya disebut pihak kedua	</p>
                <br/>
                <br/>
                <p>Pihak pertama berjanji akan mewujudkan target kinerja yang seharusnya sesuai lampiran perjanjian ini, dalam rangka mencapai target kinerja jangka menengah seperti yang telah ditetapkan dalam dokumen perencanaan. Keberhasilan dan kegagalan pencapaian target kinerja tersebut menjadi tanggung jawab kami.</p>
                <br/>

                <p>Pihak kedua akan melakukan supervisi yang diperlukan serta akan melakukan evaluasi terhadap capaian kinerja dari perjanjian ini dan mengambil tindakan yang diperlukan dalam rangka pemberian penghargaan dan sanksi.</p>
                <br/>
                <br/>
                <table class="table">
                        <tbody>
                            <tr>
                                <td style="width:25%; text-align:left; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none">Cimahi, '.date('d-m-Y').'</td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none">Pihak kedua,</td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none">Pihak pertama,</td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"><b>WALI KOTA CIMAHI,</b></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"><b>KEPALA <br/>'.$dinas[0]->td_dinas.'</b></td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"></td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"></td>
                            </tr>
                            <tr>
                                <td style="width:25%; text-align:center; border: none"></td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none"></td>
                            </tr>

                            <tr>
                                <td style="width:25%; text-align:center; border: none">'.$request->pihak_kedua.'</td>
                                <td style="width:50%; text-align:center; border: none"> </td>
                                <td style="width:25%; text-align:center; border: none">'.$request->pihak_pertama.' <b><br/> Pangkat <br/>NIP</b></td>
                            </tr>

                        </tbody>
                    </table>
            </div>
        </body>
        </html>';

        $pdf = PDF::loadHTML($html)->setPaper('f4', 'potrait');

        $pdf = $pdf->stream('RENCANA AKSI - '.date('d/m/Y').'.pdf');

        return $pdf;
    }
}
