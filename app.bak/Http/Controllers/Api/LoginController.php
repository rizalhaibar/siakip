<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\API\BaseController as BaseController;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Role\RoleRepositoryInterface;
use App\Repositories\Company\CompanyRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class LoginController extends BaseController
{
    //
    function __construct(
        UserRepositoryInterface $userRepo,
        RoleRepositoryInterface $roleRepo,
        CompanyRepositoryInterface $companyRepo
    ) {
        $this->userRepo     = $userRepo;
        $this->roleRepo     = $roleRepo;
        $this->companyRepo  = $companyRepo;
    }

    public $successStatus = 200;

    public function login()
    {
        if( Auth::attempt([
                'email' => request('email'),
                'password' => request('password'),
                'status' => config('setting.status.active')
            ])
        ) {

            $user = Auth::user();
            $user = $this->userRepo->find($user->id);
            $user->role; // Getting Role Relation
            $token = $user->createToken('vms-token')->accessToken;
            $result = collect([
                'token' => $token,
                'user' => $user,
                
            ]);

            return $this->sendResponse($result->toArray(), 'Authorised Access');
        } else {
            return $this->sendError('Error', 'Unauthorised Access');
        }
    }

    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }
}
