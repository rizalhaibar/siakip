@extends('backend.layouts.landing')
@section('title', 'Pelaporan')

@section('content')
	<section id="about" style="margin-top: 20px">      
        <div class="container" style="width: auto !important">
			<div class="col-md-12" style="margin-top: 10px" >

			<div class="row">
					<div class="col-md-12">
					<h3>Perencanaan Kinerja <span id="initahun"></span></h3>
					<br/>
					<br/>
					</div>
				</div>
				<div class="row">
				 
				  <div class="col-md-3">
					  <div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 " style="margin-top:5px">Tahun</label>
						<div class="col-sm-10">
						<select class="form-control right" id="tahunini" name="tahunini" onChange="tahunnya(this.value)">

						<option disabled selected>Pilih Tahun..</option>
						 <?php
							$already_selected_value = date("Y");
							$earliest_year = 2013;

							foreach (range(date('Y'), $earliest_year) as $x) {
								if($_COOKIE['tahun'] == $x){
									echo '<option value="'.$x.'" selected>'.$x.'</option>';
								}else{
									echo '<option value="'.$x.'">'.$x.'</option>';
								}
								
							}


							?>
						</select>
						</div>
					  </div>
					  
				 </div>
				 <div class="col-md-8">
					<a href="{{ route('pemerintah.perencanaan',['id'=>Crypt::encryptString(0), 'idsasaran'=>Crypt::encryptString(0), 'idjabatan'=> $idjabatan]) }}" class="btn btn-default active"><i class="fa fa-cogs"></i> Perencanaan Kinerja</a>
					<a href="{{ route('pemerintah.pengukuran',['id'=>Crypt::encryptString(0), 'idsasaran'=>Crypt::encryptString(0)]) }}" class="btn btn-default "><i class="fa fa-dashboard"></i> Pengukuran Kinerja</a>
					<a href="{{ route('pemerintah.pelaporan',['id'=>Crypt::encryptString(0), 'idsasaran'=>Crypt::encryptString(0)] ) }}" class="btn btn-default "><i class="fa fa-clipboard"></i> Pelaporan Kinerja</a>
					<a href="{{ route('pemerintah.evaluasi') }}" class="btn btn-default "><i class="fa fa-check"></i> Evaluasi Kinerja</a>
			
				 </div>
			 </div> 
			
				<div class="row">
					<div class="col-md-3">
					<table id="example" class="table " style="font-size: 12px">
							<thead>
								<tr>
									<th></th>
									<th>Instansi</th>
									<th>Action</th>
								</tr>
							</thead>

							<tbody>
							
								@php($sl=1)
								 @foreach($data as $dinas)

								 @if(Crypt::decryptString($id) ==$dinas->td_id )
								 <tr style="background: #ccc">
								 
								 @else
								 
								 <tr>
								 @endif
									<td>{{$sl}}</td>
									<td>{{$dinas->td_dinas}}</td>
									<td><a href="{{route('pemerintah.perencanaan',['id'=>Crypt::encryptString($dinas->td_id), 'idsasaran'=>Crypt::encryptString(0), 'idjabatan'=> $idjabatan])}}" type="button" class="btn btn-block btn-success" ><i class="fa fa-search"></i></a></td>
								 </tr>
								@php($sl++)
								@endforeach
							</tbody>
						</table>
					</div>
					<div class="col-md-9">
						<ul class="nav nav-tabs  nav-tabs-line nav-tabs-line-2x nav-tabs-line-success" role="tablist">
							<li class="nav-item ">
								<a class="nav-link active" data-toggle="tab" href="#iku" role="tab">IKU</a>
							</li>
							<li class="nav-item">
								<a class="nav-link " data-toggle="tab" href="#rkt" role="tab">RKT</a>
							</li>
							<li class="nav-item ">
								<a class="nav-link " data-toggle="tab" href="#pk" role="tab">PK</a>
							</li>
							<li class="nav-item">
								<a class="nav-link " data-toggle="tab" href="#ra" role="tab">Rencana Aksi</a>
							</li>
							<li class="nav-item">
								<a class="nav-link " data-toggle="tab" href="#keselarasan" role="tab">Keselarasan</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="iku" role="tabpanel">
								@if(!empty($listiku))
								<ul style="margin-left: 20px">
									<h4 style="margin-left:-30px; color: #336600; margin-top:10px">Sasaran Strategis </h4>
								</ul>


								<ol  style="margin-left: 20px">
									@php($sa = 1)
									@foreach((object)$listiku as $newsasaran)
										
									
									<li style="display:block">
										{{$sa}}. {{$newsasaran->tst_sasaran_tujuan}}
									
											<ul style="margin-left: 20px">
												<h4 style="margin-left:-30px; color: #336600; margin-top:10px">Indikator Sasaran </h4>
											</ul>
												<ol>
												<li style="display:block">
													<table class="table table-bordered">
														<thead>
															<tr>
																<th><center>NO</center></th>
																<th><center>Indikator Sasaran Strategis</center></th>
																<th><center>Satuan</center></th>
																<th><center>Formula</center></th>
															</tr>
														</thead>
														<tbody>
														@if(isset($newsasaran->indikatorsasaran))
														@php($sas = 1)
														@foreach($newsasaran->indikatorsasaran as $indisasaran)
															
															<tr>
																<td>{{$sas}}</td>
																<td>{{$indisasaran->tis_indikator_sasaran}}</td>
																<td>{{$indisasaran->tis_satuan}}</td>
																<td>{{$indisasaran->formula}}</td>
															</tr>
															@php($sas++) 
															@endforeach
															
																	
														@endif       
														</tbody>
													</table>
												</li>
											</ol>

										
									</li>
												
									@php($sa++)
									@endforeach
								</ol>
								@endif
							</div>
							<div class="tab-pane " id="rkt" role="tabpanel">
								<ul style="margin-left: 20px">
									<h4 style="margin-left:-30px; color: #336600; margin-top:10px">Sasaran Strategis </h4>
								</ul>
								<ol  style="margin-left: 20px">
									@php($si = 1)
									@foreach((object)$listrkt as $rkt)
										
									
									<li style="display:block">
										{{$si}}. {{$rkt->tst_sasaran_tujuan}}
									
											<ul style="margin-left: 20px">
												<h4 style="margin-left:-30px; color: #336600; margin-top:10px">Indikator Sasaran </h4>
											</ul>
												<ol>
												<li style="display:block">
													<table class="table table-bordered">
														<thead>
															<tr>
																<th><center>NO</center></th>
																<th><center>Indikator Sasaran Strategis</center></th>
																<th><center>Satuan</center></th>
																<th><center>Target</center></th>
															</tr>
														</thead>
														<tbody>
														@if(isset($rkt->indikatorsasaran))
														@php($sasi = 1)
														@foreach($rkt->indikatorsasaran as $rktindisasaran)
															
															<tr>
																<td>{{$sasi}}</td>
																<td>{{$rktindisasaran->tis_indikator_sasaran}}</td>
																<td>{{$rktindisasaran->tis_satuan}}</td>
																<td>{{$rktindisasaran->tis_target}}</td>
															</tr>
															@php($sasi++) 
															@endforeach
															
																	
														@endif       
														</tbody>
													</table>
												</li>
											</ol>

										
									</li>
												
									@php($si++)
									@endforeach
								</ol>
							</div>
							<div class="tab-pane " id="pk" role="tabpanel">
							@if(Crypt::decryptString($id) != 0)
								<div class="form-group">
									<label for="exampleSelect1">Pilih Jabatan Utama : </label>
									<select class="form-control"  onChange="cariJabatan(this.value)">
										<option selected disabled>Pilih Jabatan Utama ....</option>
										@if(isset($listjabatan[0]))
                                        @foreach($listjabatan as $new)
                                            @if($new->te_parent == 1)
                                                <option value="{{$new->te_id.'_'.$new->te_eselon}}" @if($idjabatan != 0 ) {{$new->te_id == $idjabatan ? 'selected' : ''}} @endif >{{$new->te_jabatan}} : {{$new->te_isi}}</option>
                                            @endif
                                                @foreach($listjabatan as $anak)
                                                    @if($anak->id_parent == $new->te_id)
                                                        @if($anak->te_child == 1)
																<option value="{{$anak->te_id.'_'.$anak->te_eselon}}" @if($idjabatan != 0 ) {{$anak->te_id == $idjabatan ? 'selected' : ''}} @endif >&nbsp;&nbsp;{{$anak->te_jabatan}} : {{$anak->te_isi}}</option>
                                     
                                                                    @foreach($listjabatan as $anak2)
                                                                        @if($anak2->id_parent == $anak->te_id)
                                                                            @if($anak2->te_child == 2)
																				<option value="{{$anak2->te_id.'_'.$anak2->te_eselon}}" @if($idjabatan != 0 ) {{$anak2->te_id == $idjabatan ? 'selected' : ''}} @endif >&nbsp;&nbsp;&nbsp;&nbsp;{{$anak2->te_jabatan}} : {{$anak2->te_isi}}</option>
                                     
                                                                                    @foreach($listjabatan as $anak3)
                                                                                        @if($anak3->id_parent == $anak2->te_id)
                                                                                      
                                                                                            @if($anak3->te_child == 3)
																								<option value="{{$anak3->te_id.'_'.$anak3->te_eselon}}" @if($idjabatan != 0 ) {{$anak3->te_id == $idjabatan ? 'selected' : ''}} @endif >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$anak3->te_jabatan}} : {{$anak3->te_isi}}</option>
                                     
                                                                                            @endif
                                                                                        @endif
                                                                                    @endforeach
                                                                            @endif
                                                                        @endif
                                                                    @endforeach    
                                                        @endif
                                                    @endif
                                                @endforeach
                                        @endforeach
                                    @endif
									</select>
								</div>





									

								@if($ideselon == '2A')
								<ul style="margin-left: 20px">
									<h4 style="margin-left:-30px; color: #336600; margin-top:10px">Indikator Sasaran Strategis</h4>
								</ul>
								<div class="row">
									<div class="col-12">
										<table class="table table-bordered" style="font-size: 10.5px">
											<thead>
												<tr>
													<th rowspan="2">No</th>
													<th rowspan="2">Sasaran Strategis</th>
													<th rowspan="2">Indikator Sasaran Strategis</th>
													<th rowspan="2">Satuan</th>
													<th rowspan="2">Target</th>
												</tr>
											</thead>
											<tbody>
											<?php 
												$htmlpk = '';  
												$isipk1 = '';   
											?>
											<tbody>
												@php($sas = 1)
												@foreach($pk_indikatorsasaranra as $sasaran)
													@php($alltotalindikatorprogram=0)
													@if(isset($sasaran->indikator_sasaran))
														@foreach($sasaran->indikator_sasaran as $s=>$indikator_sasaran)

															@if($s == 0)
																@php($alltotalindikatorprogram = array_count_values(array_column($sasaran->indikator_sasaran, 'idsasaran'))[$sasaran->tst_id])
															
																@php($isipk1 = '<td> '.$indikator_sasaran->tis_indikator_sasaran.' </td>'
																				.'<td>'.$indikator_sasaran->tis_satuan.'</td>'
																				.'<td>'.$indikator_sasaran->tis_target.'</td>'
																			.'</tr>'
																			)

																
															@else
															
																@php(
																	$isipk1 .= '<tr ><td > '.$indikator_sasaran->tis_indikator_sasaran.'</td>'
																				.'<td>'.$indikator_sasaran->tis_satuan.'</td>'
																				.'<td>'.$indikator_sasaran->tis_target.'</td>'
																			.'</tr>'
																)
															@endif
														@endforeach

														@php($htmlpk .= '<tr>'
																		.'<td rowspan="'.$alltotalindikatorprogram.'">'.$sas.'</td>'
																		.'<td rowspan="'.$alltotalindikatorprogram.'">'.$sasaran->tst_sasaran_tujuan.'</td>'
																		.$isipk1
																	.'</tr>')
													@else

													@php($htmlpk .= '<tr>'
																	.'<td>'.$sas.'</td>'
																	.'<td>'.$sasaran->tst_sasaran_tujuan.'</td>'
																	.'<td></td>'
																	.'<td></td>'
																	.'<td></td>'
																.'</tr>')
													@endif
												@php($sas++) 
												@endforeach
												
												@php(print_r($htmlpk))
											</tbody>
										</table>
									</div>
								</div>

								@elseif($ideselon == '3A')
								<ul style="margin-left: 20px">
									<h4 style="margin-left:-30px; color: #336600; margin-top:10px">Sasaran Program </h4>
								</ul>
								<div class="row">
									<div class="col-12">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th>No</th>  
													<th>Sasaran Program</th>
													<th>Indikator Sasaran Program</th>
													<th>Satuan</th>
													<th>Target</th>
												</tr>
											</thead>
											<tbody>
											
											<?php 
												$htmlpk = '';  
												$isipk1 = '';   
											?>
												@php($sa = 1)
												@foreach($pk_programs as $programs)
													@php($alltotalindikatorprogram=0)
													@if(isset($programs->indikatorprogram))
														@foreach($programs->indikatorprogram as $s=>$indikatorprogram)

															@if($s == 0)
																@php($alltotalindikatorprogram = array_count_values(array_column($programs->indikatorprogram, 'idprogram'))[$programs->tps_id])
															
																@php($isipk1 = '<td> '.$indikatorprogram->tip_indikator_program.' </td>'
																				.'<td>'.$indikatorprogram->tip_satuan.'</td>'
																				.'<td>'.$indikatorprogram->tip_target.'</td>'
																			.'</tr>'
																			)

																
															@else
															
																@php(
																	$isipk1 .= '<tr ><td > '.$indikatorprogram->tip_indikator_program.'</td>'
																				.'<td>'.$indikatorprogram->tip_satuan.'</td>'
																				.'<td>'.$indikatorprogram->tip_target.'</td>'
																			.'</tr>'
																)
															@endif
														@endforeach

														@php($htmlpk .= '<tr>'
																		.'<td rowspan="'.$alltotalindikatorprogram.'">'.$sa.'</td>'
																		.'<td rowspan="'.$alltotalindikatorprogram.'">'.$programs->tps_program_sasaran.'</td>'
																		.$isipk1
																	.'</tr>')
													@else

													@php($htmlpk .= '<tr>'
																	.'<td>'.$sa.'</td>'
																	.'<td>'.$programs->tps_program_sasaran.'</td>'
																	.'<td></td>'
																	.'<td></td>'
																	.'<td></td>'
																.'</tr>')
													@endif
												@php($sa++) 
												@endforeach
												
												@php(print_r($htmlpk))
											</tbody>
										</table>
									</div>
								</div>
								@elseif($ideselon == '4A')
								<ul style="margin-left: 20px">
									<h4 style="margin-left:-30px; color: #336600; margin-top:10px">Sasaran Kegiatan </h4>
								</ul>
								<div class="row">
									<div class="col-12">
										<table class="table table-bordered" style="font-size: 10.5px">
											<thead>
												<tr>
													<th rowspan="2">No</th>
													<th rowspan="2">Sasaran Kegiatan</th>
													<th rowspan="2">Indikator Sasaran Kegiatan</th>
													<th rowspan="2">Satuan</th>
													<th rowspan="2">Target</th>
												</tr>
											</thead>
											<tbody>
											
											<?php 
												$htmlpk = '';  
												$isipk1 = '';   
											?>
												@php($sa = 1)
												@foreach($pk_kegiatans as $kegiatans)
													@php($alltotalindikatorprogram=0)
													@if(isset($kegiatans->indikatorkegiatan))
														@foreach($kegiatans->indikatorkegiatan as $s=>$indikatorkegiatan)

															@if($s == 0)
																@php($alltotalindikatorprogram = array_count_values(array_column($kegiatans->indikatorkegiatan, 'idkegiatan'))[$kegiatans->tkp_id])
															
																@php($isipk1 = '<td> '.$indikatorkegiatan->tik_indikator_kegiatan.' </td>'
																				.'<td>'.$indikatorkegiatan->tik_satuan.'</td>'
																				.'<td>'.$indikatorkegiatan->tik_target.'</td>'
																			.'</tr>'
																			)

																
															@else
															
																@php(
																	$isipk1 .= '<tr ><td > '.$indikatorkegiatan->tik_indikator_kegiatan.'</td>'
																				.'<td>'.$indikatorkegiatan->tik_satuan.'</td>'
																				.'<td>'.$indikatorkegiatan->tik_target.'</td>'
																			.'</tr>'
																)
															@endif
														@endforeach

														@php($htmlpk .= '<tr>'
																		.'<td rowspan="'.$alltotalindikatorprogram.'">'.$sa.'</td>'
																		.'<td rowspan="'.$alltotalindikatorprogram.'">'.$kegiatans->tkp_kegiatan_program.'</td>'
																		.$isipk1
																	.'</tr>')
													@else

													@php($htmlpk .= '<tr>'
																	.'<td>'.$sa.'</td>'
																	.'<td>'.$kegiatans->tkp_kegiatan_program.'</td>'
																	.'<td></td>'
																	.'<td></td>'
																	.'<td></td>'
																.'</tr>')
													@endif
												@php($sa++) 
												@endforeach
												
												@php(print_r($htmlpk))
											</tbody>
										</table>
									</div>
								</div>
								@endif

								

								
								
								
								
							@endif
							</div>


							<div class="tab-pane " id="ra" role="tabpanel">
								@if(Crypt::decryptString($id) != 0)
									<div class="form-group">
										<label for="exampleSelect1">Pilih Sasaran Strategis : </label>
										<select class="form-control"  onChange="cariData(this.value)">
											<option selected disabled>Pilih Sasaran Strategis ....</option>
											@foreach($listsasaran as $sasaran)
												<option value="{{Crypt::encryptString($sasaran->tst_id)}}" @if(Crypt::decryptString($idsasaran) != 0 ) {{$sasaran->tst_id == Crypt::decryptString($idsasaran) ? 'selected' : ''}} @endif >{{$sasaran->tst_sasaran_tujuan}}</option>
											@endforeach
										</select>
									</div>


									@if(Crypt::decryptString($idsasaran) != 0)
										<ul class="nav nav-tabs  nav-tabs-line nav-tabs-line-2x nav-tabs-line-success" role="tablist">
											<li class="nav-item active">
												<a class="nav-link " data-toggle="tab" href="#sasaranra" role="tab">Sasaran Strategis</a>
											</li>
											<li class="nav-item">
												<a class="nav-link " data-toggle="tab" href="#programra" role="tab">Sasaran Program</a>
											</li>
											<li class="nav-item">
												<a class="nav-link " data-toggle="tab" href="#kegiatanra" role="tab">Sasaran Kegiatan</a>
											</li>
										</ul>
										<div class="tab-content">
											<div class="tab-pane active" id="sasaranra" role="tabpanel">
												<ul style="margin-left: 20px">
													<h4 style="margin-left:-30px; color: #336600; margin-top:10px">Indikator Sasaran Strategis</h4>
												</ul>
												<div class="row">
													<div class="col-12">
													<table class="table table-bordered" style="font-size: 10.5px">
													 <thead>
														<tr>
															<th rowspan="2">No</th>
															<th rowspan="2">Indikator Sasaran Strategis</th>
															<th rowspan="2">Satuan</th>
															<th rowspan="2">Target</th>
															<th colspan="4"><center>Target <span id="tahun"></span></center></th>
															<th rowspan="2">Action</th>
														</tr>
														<tr>
															<th>Triwulan I</th>
															<th>Triwulan II</th>
															<th>Triwulan III</th>
															<th>Triwulan IV</th>
														</tr>
													</thead>
													<tbody>
													@php($sas = 1)
													@foreach($indikatorsasaranra as $indisasaran)
														
														<tr>
															<td>{{$sas}}</td>
															<td>{{$indisasaran->tis_indikator_sasaran}}</td>
															<td>{{$indisasaran->tis_satuan}}</td>
															<td>{{$indisasaran->tis_target}}</td>
															<td>{{$indisasaran->twtarget1}}</td>
															<td>{{$indisasaran->twtarget2}}</td>
															<td>{{$indisasaran->twtarget3}}</td>
															<td>{{$indisasaran->twtarget4}}</td>
															<td>
																<center>
																	<div class="btn-group">
																	@if($indisasaran->twtarget1)
																		@if($indisasaran->tw1)
																			<a href="javascript:void(0)"  onClick="edittargetsasaran({{$indisasaran->tis_id}})" type="button" class="btn btn-info btn-icon"><i class="fa fa-edit"></i></a>
																		@else
																			<a href="javascript:void(0)"  onClick="tambahtargetsasaran({{$indisasaran->tis_id}})" type="button" class="btn btn-success btn-icon"><i class="fa fa-search"></i></a>
																		@endif
																	@endif
																	</div>
																</center>
															</td>
														</tr>
														@php($sas++) 
														@endforeach
													</tbody>
												</table>
													</div>
												</div>
												
											</div>
											<div class="tab-pane " id="programra" role="tabpanel">

												<ul style="margin-left: 20px">
													<h4 style="margin-left:-30px; color: #336600; margin-top:10px">SasaranProgram </h4>
												</ul>
												<table class="table table-bordered" style="font-size: 10.5px">
													<thead>
														<tr>
															<th rowspan="2">No</th>
															<th rowspan="2">Indikator Sasaran Strategis</th>
															<th rowspan="2">Sasaran Program</th> 
															<th rowspan="2">Indikator Sasaran Program</th>
															<th rowspan="2">Satuan</th>
															<th rowspan="2">Target</th>
															<th colspan="4"><center>Target <span id="tahun"></span></center></th>
														</tr>
														<tr>
															<th>Triwulan I</th>
															<th>Triwulan II</th>
															<th>Triwulan III</th>
															<th>Triwulan IV</th>
														</tr>
													</thead>
															<?php 
																	$i=1; 
																	
																	$html = '';                                  
																	$isi1 = '';                                  
																	$isi2 = '';  
																	$isi3 = '';  
																	$isi4 = '';  
																?>
															<tbody>
															@foreach($programs as $new)
															@php($totalprogram=0)
															
																	@if(isset($new->program))

																	@php($alltotalindikatorprogram=0)
																	@php($allatas=0)
																	@php($allbawah=0)
																	@foreach($new->program as $k=>$prg)
																		@php($totalindikatorprogram=0)
																		@php($totalindikatorprogrambawah=0)
																		@php($x=0)
																		@if(isset($prg->indikatorprogram))
																		



																		
																			@foreach($prg->indikatorprogram as $s=>$indprg)  
																				@php($actionhtml = '')
																				@if($indprg->twtarget1)
																					@if($indprg->tw1)
																						@php($actionhtml = '<a href="javascript:void(0)" class="btn btn-info btn-sm btn-icon" onClick="edittargetprogram('.$indprg->tip_id.')"><i class="fa fa-edit"></i></a>')
																					@else
																						@php($actionhtml = '<a href="javascript:void(0)" class="btn btn-success btn-sm btn-icon" onClick="tambahtargetprogram('.$indprg->tip_id.')"><i class="fa fa-search"></i></a>')
																					@endif
																				@endif
																				@if($s == 0)
																					@php($totalindikatorprogram += array_count_values(array_column($prg->indikatorprogram, 'idprogram'))[$prg->tps_id])

																				
																					@php($totalindikatorprogrambawah = 1)
																					@php($isi2 = '<td>'.$indprg->tip_indikator_program.' </td>'
																									.'<td>'.$indprg->tip_satuan.'</td>'
																									.'<td>'.$indprg->tip_target.'</td>'
																									.'<td>'.$indprg->twtarget1.'</td>'
																									.'<td>'.$indprg->twtarget2.'</td>'
																									.'<td>'.$indprg->twtarget3.'</td>'
																									.'<td>'.$indprg->twtarget4.'</b></td>'
																								.'</tr>'
																								)

																					
																				@else
																					@php($totalindikatorprogrambawah += array_count_values(array_column($prg->indikatorprogram, 'idprogram'))[$prg->tps_id])

																					@php(
																						$isi2 .= '<tr ><td >'.$indprg->tip_indikator_program.' </td>'
																									.'<td>'.$indprg->tip_satuan.'</td>'
																									.'<td>'.$indprg->tip_target.'</td>'
																									.'<td>'.$indprg->twtarget1.'</td>'
																									.'<td>'.$indprg->twtarget2.'</td>'
																									.'<td>'.$indprg->twtarget3.'</td>'
																									.'<td>'.$indprg->twtarget4.'</b></td>'
																								.'</tr>'
																					)
																				@endif

																			@endforeach



																			
																			@if($k == 0)
																				@php($totalprogram += array_count_values(array_column($new->program, 'idindisasaran'))[$new->tis_id])
																				
																				@php($allatas = $totalindikatorprogram)
																				
																				@php($isi1 = '<td rowspan="'.$totalindikatorprogram.'"> '.$prg->tps_program_sasaran.' </td>'
																								.$isi2)
																			@else
																				@php($allbawah += $totalindikatorprogrambawah)
																				@php(
																					$isi1 .= '<tr ><td rowspan="'.$totalindikatorprogrambawah.'"> '.$prg->tps_program_sasaran.'</td>'
																								.$isi2
																				)
																			@endif

																			
																			
																		@else

																		
																			@php($alltotalindikatorprogram = $alltotalindikatorprogram+1)
																			@if($k == 0)
																				@php($totalprogram += array_count_values(array_column($new->program, 'idindisasaran'))[$new->tis_id])
																				@php($isi1 = '<td>'.$prg->tps_program_sasaran.' </td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																							.'</tr>')
																			@else
																				@php(
																					$isi1 .= '<tr ><td >'.$prg->tps_program_sasaran.' </span></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																							.'</tr>'
																				)
																			@endif
																		@endif
																	@endforeach

																			
																		
																	@php($alltotalindikatorprogram = $totalindikatorprogram+$totalindikatorprogrambawah)
																	@php($totalkeseluruhan = $allatas+$allbawah)
																	@php($html .= '<tr>'
																					.'<td rowspan="'.$totalkeseluruhan.'">'.$i.'</td>'
																					.'<td rowspan="'.$totalkeseluruhan.'">'.$new->tis_indikator_sasaran.' </td>'
																					.$isi1)
																	@else
																	
																		@php($html .= '<tr>'
																					.'<td>'.$i.'</td>'
																					.'<td>'.$new->tis_indikator_sasaran.'</td>'
																					.'<td></td>'
																					.'<td></td>'
																					.'<td></td>'
																					.'<td></td>'
																					.'<td></td>'
																					.'<td></td>'
																					.'<td></td>'
																					.'<td></td>'
																				.'</tr>')
																	@endif
																	@php($i++)
															@endforeach
															
															@php(print_r($html))
															</tbody>
												</table>
											
											</div>
											<div class="tab-pane " id="kegiatanra" role="tabpanel">
												<ul style="margin-left: 20px">
													<h4 style="margin-left:-30px; color: #336600; margin-top:10px">Kegiatan </h4>
												</ul>
												<table class="table table-bordered" style="font-size: 10.5px">
												<thead>
													<tr>
														<th rowspan="2">No</th>
														<th rowspan="2">Sasaran Program</th> 
														<th rowspan="2">Indikator Sasaran Program</th>
														<th rowspan="2">Sasaran Kegiatan</th>
														<th rowspan="2">Indikator Sasaran Kegiatan</th>
														<th rowspan="2">Satuan</th>
														<th rowspan="2">Target</th>
														<th colspan="4"><center>Target <span id="tahun"></span></center></th>
													</tr>
													<tr>
														<th>Triwulan I</th>
														<th>Triwulan II</th>
														<th>Triwulan III</th>
														<th>Triwulan IV</th>
													</tr>
												</thead>
													<tbody>
														<?php 
															$ik=1; 
															
															$htmlk = '';                                  
															$isik1 = '';                                  
															$isik2 = '';  
															$isik3 = '';  
															$isik4 = '';  
															$isik5 = '';  
															$isik5s = '';  
															$isik6 = '';  
															$isik7 = '';  
															$akhirtotala = 0;  
														?>

															@foreach($kegiatans as $keg)
																
																@php($totalprogramk=0)
																@php($totalatasprogram=0)
																@php($testatas=0)
																@php($alltotalkegiatan=0)
																@php($alltotalkegiatanbawah=0)
																
																@php($totalbawahprogram=0)
																@php($totalbawahprogram2=0)
																@if(isset($keg->indikatorprogram))
																	@foreach($keg->indikatorprogram as $m=>$indikatorprogram)
																		<!-- param -->
																		
																		@php($totalkegiatan=0)
																		@php($totalkegiatanbawah=0)
																		@php($totalindikatorkegiatank=0)
																		@php($allataskegiatan=0)
																		@php($allbawahkegiatan=0)

																		@if(isset($indikatorprogram->kegiatan))
																			<!-- INDIKATOR KEGIATAN -->
																			
																			@php($totalindikatorkegiatan=0)
																			@php($totalindikatorkegiatanbawah=0)

																			@foreach($indikatorprogram->kegiatan as $as=>$kga)
																			
																				@if(isset($kga->indikatorkegiatan))

																				
																					@foreach($kga->indikatorkegiatan as $inkas=>$indikatorkegiatan)
																					@php($actionhtml = '')
																						@if($indikatorkegiatan->twtarget1)
																							@if($indikatorkegiatan->tw1)
																								@php($actionhtml = '<a href="javascript:void(0)" class="btn btn-info btn-sm btn-icon" onClick="edittargetkegiatan('.$indikatorkegiatan->tik_id.')"><i class="fa fa-edit"></i></a>')
																							@else
																								@php($actionhtml = '<a href="javascript:void(0)" class="btn btn-success btn-sm btn-icon" onClick="tambahtargetkegiatan('.$indikatorkegiatan->tik_id.')"><i class="fa fa-search"></i></a>')
																							@endif
																						@endif
																						@if($inkas == 0)
																							@php($totalindikatorkegiatan += array_count_values(array_column($kga->indikatorkegiatan, 'idkegiatan'))[$kga->tkp_id])
																							
																							@php($totalindikatorkegiatanbawah = 1)
																							@php($isik5 = '<td > '.$indikatorkegiatan->tik_indikator_kegiatan.'  </td>'
																												.'<td>'.$indikatorkegiatan->tik_satuan.'</td>'
																												.'<td>'.$indikatorkegiatan->tik_target.'</td>'
																												.'<td>'.$indikatorkegiatan->twtarget1.'</td>'
																												.'<td>'.$indikatorkegiatan->twtarget2.'</td>'
																												.'<td>'.$indikatorkegiatan->twtarget3.'</td>'
																												.'<td>'.$indikatorkegiatan->twtarget4.'</td>'
																											.'</tr>'
																										)
																						@else
																						
																							@php($totalindikatorkegiatanbawah += 1)
																							
																							@php(
																								$isik5 .= '<tr >'
																													.'<td >'.$indikatorkegiatan->tik_indikator_kegiatan.' </td>'
																													.'<td>'.$indikatorkegiatan->tik_satuan.'</td>'
																													.'<td>'.$indikatorkegiatan->tik_target.'</td>'
																													.'<td>'.$indikatorkegiatan->twtarget1.'</td>'
																													.'<td>'.$indikatorkegiatan->twtarget2.'</td>'
																													.'<td>'.$indikatorkegiatan->twtarget3.'</td>'
																													.'<td>'.$indikatorkegiatan->twtarget4.'</td>'
																												.'</tr>'
																							)
																						@endif
																					@endforeach

																					@if($as == 0)

																						@php($allataskegiatan+=$totalindikatorkegiatan)
																						@php($totalkegiatan += array_count_values(array_column($indikatorprogram->kegiatan, 'idindiprogram'))[$indikatorprogram->tip_id])
																						@php($isik2 = '<td rowspan="'.$totalindikatorkegiatan.'">'.$kga->tkp_kegiatan_program.'  </td>'
																											.$isik5
																											
																										
																										)
																					@else
																						@php($totalkegiatan += 1)
																						
																						
																						@php($allbawahkegiatan += $totalindikatorkegiatanbawah)
																						
																						@php(
																							$isik2 .= '<tr ><td rowspan="'.$totalindikatorkegiatanbawah.'"> '.$kga->tkp_kegiatan_program.'</td>'
																												.$isik5
																												
																												
																						)
																					@endif

																				@else
																					@if($as == 0)
																						@php($allataskegiatan+=1)
																						@php($totalkegiatan += array_count_values(array_column($indikatorprogram->kegiatan, 'idindiprogram'))[$indikatorprogram->tip_id])
																						@php($isik2 = '<td>'.$kga->tkp_kegiatan_program.' </td>'
																											.'<td></td>'
																												.'<td></td>'
																												.'<td></td>'
																												.'<td></td>'
																												.'<td></td>'
																												.'<td></td>'
																												.'<td></td>'
																											.'</tr>'
																										)
																					@else
																					
																						@php($allbawahkegiatan = $allbawahkegiatan + 1)
																						@php(
																							$isik2 .= '<tr ><td > '.$kga->tkp_kegiatan_program.'</td>'
																										.'<td></td>'
																												.'<td></td>'
																												.'<td></td>'
																												.'<td></td>'
																												.'<td></td>'
																												.'<td></td>'
																												.'<td></td>'
																											.'</tr>'
																										
																						)
																					@endif
																				@endif
																			@endforeach

																			<!-- KEGIATAN ISI -->
																		
																			@if($m == 0)
																				@php($alltotalkegiatan = $allataskegiatan + $allbawahkegiatan)
																				@php($isik1 = '<td rowspan="'.$alltotalkegiatan.'"> '.$indikatorprogram->tip_indikator_program.'  </td>'
																								.$isik2
																					)
																			@else
																				@php($alltotalkegiatanbawah = $allataskegiatan + $allbawahkegiatan)
																				@php($testatas += $alltotalkegiatanbawah)
																				@php(
																					$isik1 .= '<tr>'
																								.'<td rowspan="'.$alltotalkegiatanbawah.'">'.$indikatorprogram->tip_indikator_program.'   </td>'
																								.$isik2
																				)
																			@endif

																		@else


																			@if($m == 0)
																				
																			@php($alltotalkegiatan = $alltotalkegiatan + 1)
																				@php($isik1 = '<td>'.$indikatorprogram->tip_indikator_program.'</td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																							.'</tr>')
																			@else
																			
																			@php($testatas += 1)
																				@php(
																					$isik1 .= '<tr ><td >'.$indikatorprogram->tip_indikator_program.' </td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																								.'<td></td>'
																							.'</tr>'
																				)
																			@endif
																		
																		@endif
																	@endforeach
																	@php($akhirtotal =  $alltotalkegiatan + $testatas)
																<!-- Program ISI -->
																	@php($htmlk .= '<tr>'
																						.'<td rowspan="'.$akhirtotal.'">'.$ik.'</td>'
																						.'<td rowspan="'.$akhirtotal.'">'.$keg->tps_program_sasaran.' </td>'
																						.$isik1
																						)
																@else
																
																<!-- Program Kosong -->
																	@php($htmlk .= '<tr>'
																					.'<td>'.$ik.'</td>'
																					.'<td>'.$keg->tps_program_sasaran.'</td>'
																					.'<td></td>'
																					.'<td></td>'
																					.'<td></td>'
																					.'<td></td>'
																					.'<td></td>'
																					.'<td></td>'
																					.'<td></td>'
																					.'<td></td>'
																					.'<td></td>'
																				.'</tr>')
																@endif
																@php($ik++)    
															@endforeach

															@php(print_r($htmlk))
													</tbody>
												</table>
											</div>
										</div>
									@endif
								@endif
							</div>
							<div class="tab-pane " id="keselarasan" role="tabpanel">
								@if(Crypt::decryptString($id) != 0)
									<div class="form-group">
										<ul style="margin-left: 20px">
											<h4 style="margin-left:-30px; color: #336600; margin-top:10px">Keselarasan </h4>
										</ul>
										<br/>												
										@if(!empty($keselarasan))
										<table id="listkota" class="table table-bordered">
												<thead>
													<tr>
														<th>No</th>
														<th width="15%">Tujuan</th>
														<th width="15%">Sasaran Strategis</th>
														<th>Indikator Sasaran Strategis</th>
														<th>Sasaran Program</th> 
														<th>Indikator Sasaran Program</th>
														<th>Sasaran Kegiatan</th>
														<th>Indikator Sasaran Kegiatan</th>
													</tr>
												</thead>
												
												<tbody>
													<?php 
															$i=1; 
															$html = '';                                  
															$isisasaran = '';                                  
															$isiindikatorsasaran = '';  
															$isiprogram = '';  
															$isiindikatorprogram = '';  
															$isikegiatan = '';  
															$isiindikatorkegiatan = '';  
														?>
														
														@foreach((object)$keselarasan as $tujuan)
														
																
															@php($totalsasaran=0)
															@php($totalsasaranbawahmentah=0)
															@php($totalsasaranbawah=0)
															@if(isset($tujuan->sasaran))
																@foreach($tujuan->sasaran as $s=>$sasaran)
																
																	@php($totalindikatorsasaran=0)
																	@php($totalindikatorsasaranbawahmentah=0)
																	@php($totalindikatorsasaranbawah=0)
																	@if(isset($sasaran->indikatorsasaran))
																		@foreach($sasaran->indikatorsasaran as $is=>$indikatorsasaran)
																		
																			@php($totalprogram=0)
																			@php($totalprogrambawahmentah=0)
																			@php($totalprogrambawah=0)
																			@if(isset($indikatorsasaran->program))
																				@foreach($indikatorsasaran->program as $p=>$program)
																					@php($totalindikatorprogram=0)
																					@php($totalindikatorprogrambawahmentah=0)
																					@php($totalindikatorprogrambawah=0)
																					@if(isset($program->indikatorprogram))
																						@foreach($program->indikatorprogram as $ip=>$indikatorprogram)
																							@php($totalkegiatan=0)
																							@php($totalkegiatanbawah=0)
																							@if(isset($indikatorprogram->kegiatan))
																								@php($totalindikatorkegiatan=0)
																								@php($totalindikatorkegiatanbawah=0)
																								@foreach($indikatorprogram->kegiatan as $k=>$kegiatan)
																									@if(isset($kegiatan->indikatorkegiatan))
																										@foreach($kegiatan->indikatorkegiatan as $ik=>$indikatorkegiatan)

																											@if($ik == 0)
																												@php($totalindikatorkegiatan += array_count_values(array_column($kegiatan->indikatorkegiatan, 'idkegiatan'))[$kegiatan->tkp_id])
																												@php($totalindikatorkegiatanbawah = 1)
																												@php($isiindikatorkegiatan = '<td>'.$indikatorkegiatan->tik_indikator_kegiatan.' </td>'
																															.'</tr>')
																											@else
																												@php($totalindikatorkegiatanbawah += 1)
																												@php(
																													$isiindikatorkegiatan .= '<tr ><td >'.$indikatorkegiatan->tik_indikator_kegiatan.' </span></td>'
																														.'</tr>'
																												)
																											@endif
																										@endforeach

																										@if($k == 0)
																										
																											@php($totalkegiatan+=$totalindikatorkegiatan)
																											@php($isikegiatan = '<td rowspan="'.$totalindikatorkegiatan.'">'.$kegiatan->tkp_kegiatan_program.' </td>'
																															.$isiindikatorkegiatan
																														.'</tr>')
																										@else
																										
																											@php($totalkegiatanbawah += $totalindikatorkegiatanbawah)
																											@php(
																												$isikegiatan .= '<tr ><td rowspan="'.$totalindikatorkegiatanbawah.'">'.$kegiatan->tkp_kegiatan_program.' </span></td>'
																															.$isiindikatorkegiatan
																														.'</tr>'
																											)
																										@endif

																									@else
																										@if($k == 0)
																											@php($totalkegiatan+=1)
																											@php($isikegiatan = '<td>'.$kegiatan->tkp_kegiatan_program.' </td>'
																															.'<td></td>'
																														.'</tr>')
																										@else
																											@php($totalkegiatanbawah = $totalkegiatanbawah + 1)
																											@php(
																												$isikegiatan .= '<tr ><td >'.$kegiatan->tkp_kegiatan_program.' </span></td>'
																															.'<td></td>'
																														.'</tr>'
																											)
																										@endif
																									@endif
																								@endforeach

																								@if($ip == 0)
																									@php($totalindikatorprogram = $totalkegiatan + $totalkegiatanbawah)
																									@php($isiindikatorprogram = '<td rowspan="'.$totalindikatorprogram.'">'.$indikatorprogram->tip_indikator_program.' </td>'
																													.$isikegiatan
																												.'</tr>')
																								@else
																									@php($totalindikatorprogrambawahmentah = $totalkegiatan + $totalkegiatanbawah)
																									@php($totalindikatorprogrambawah += $totalindikatorprogrambawahmentah)
																									@php(
																										$isiindikatorprogram .= '<tr ><td rowspan="'.$totalindikatorprogrambawahmentah.'">'.$indikatorprogram->tip_indikator_program.' </span></td>'
																													.$isikegiatan
																												.'</tr>'
																									)
																								@endif

																							@else
																								@if($ip == 0)
																									@php($totalindikatorprogram+=1)
																									@php($isiindikatorprogram = '<td>'.$indikatorprogram->tip_indikator_program.' </td>'
																													.'<td></td>'
																													.'<td></td>'
																												.'</tr>')
																								@else
																									@php($totalindikatorprogrambawah+=1)
																									@php(
																										$isiindikatorprogram .= '<tr ><td >'.$indikatorprogram->tip_indikator_program.' </span></td>'
																													.'<td></td>'
																													.'<td></td>'
																												.'</tr>'
																									)
																								@endif
																							@endif

																						@endforeach

																						@if($p == 0)
																							@php($totalprogram = $totalindikatorprogram + $totalindikatorprogrambawah)
																							@php($isiprogram = '<td rowspan="'.$totalprogram.'">'.$program->tps_program_sasaran.' </td>'
																											.$isiindikatorprogram
																										.'</tr>')
																						@else
																							@php($totalprogrambawahmentah = $totalindikatorprogram + $totalindikatorprogrambawah)
																							@php($totalprogrambawah += $totalprogrambawahmentah)
																							@php(
																								$isiprogram .= '<tr ><td rowspan="'.$totalprogrambawahmentah.'">'.$program->tps_program_sasaran.' </span></td>'
																											.$isiindikatorprogram
																										.'</tr>'
																							)
																						@endif

																					@else
																						@if($p == 0)
																							@php($totalprogram+=1)
																							@php($isiprogram = '<td>'.$program->tps_program_sasaran.' </td>'
																											.'<td></td>'
																											.'<td></td>'
																											.'<td></td>'
																										.'</tr>')
																						@else
																							@php($totalprogrambawah+=1)
																							@php(
																								$isiprogram .= '<tr ><td >'.$program->tps_program_sasaran.' </span></td>'
																											.'<td></td>'
																											.'<td></td>'
																											.'<td></td>'
																										.'</tr>'
																							)
																						@endif
																					@endif

																				@endforeach
																				@if($is == 0)
																					@php($totalindikatorsasaran = $totalprogram + $totalprogrambawah)
																					@php($isiindikatorsasaran = '<td rowspan="'.$totalindikatorsasaran.'">'.$indikatorsasaran->tis_indikator_sasaran.' </td>'
																									.$isiprogram
																								.'</tr>')
																				@else
																					@php($totalindikatorsasaranmentah = $totalprogram + $totalprogrambawah)
																					@php($totalindikatorsasaranbawah += $totalindikatorsasaranmentah)
																					@php(
																						$isiindikatorsasaran .= '<tr ><td rowspan="'.$totalindikatorsasaranmentah.'">'.$indikatorsasaran->tis_indikator_sasaran.' </span></td>'
																									.$isiprogram
																								.'</tr>'
																					)
																				@endif

																			@else
																				@if($is == 0)
																					@php($totalindikatorsasaran+=1)
																					@php($isiindikatorsasaran = '<td>'.$indikatorsasaran->tis_indikator_sasaran.' </td>'
																									.'<td></td>'
																									.'<td></td>'
																									.'<td></td>'
																									.'<td></td>'
																								.'</tr>')
																				@else
																					@php($totalindikatorsasaranbawah+=1)
																					@php(
																						$isiindikatorsasaran .= '<tr ><td >'.$indikatorsasaran->tis_indikator_sasaran.' </span></td>'
																									.'<td></td>'
																									.'<td></td>'
																									.'<td></td>'
																									.'<td></td>'
																								.'</tr>'
																					)
																				@endif
																			@endif
																		@endforeach
																		
																		@if($s == 0)
																			@php($totalsasaran = $totalindikatorsasaran + $totalindikatorsasaranbawah)
																			@php($isisasaran = '<td rowspan="'.$totalsasaran.'">'.$sasaran->tst_sasaran_tujuan.' </td>'
																								.$isiindikatorsasaran
																						.'</tr>')
																		@else
																			@php($totalsasaranmentah = $totalindikatorsasaran + $totalindikatorsasaranbawah)
																			@php($totalsasaranbawah += $totalsasaranmentah)
																			@php(
																				$isisasaran .= '<tr ><td rowspan="'.$totalsasaranmentah.'">'.$sasaran->tst_sasaran_tujuan.' </span></td>'
																							.$isiindikatorsasaran
																						.'</tr>'
																			)
																		@endif

																	@else
																		@if($s == 0)
																			@php($totalsasaran+=1)
																			@php($isisasaran = '<td>'.$sasaran->tst_sasaran_tujuan.' </td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																						.'</tr>')
																		@else
																		@php($totalsasaranbawah+=1)
																			@php(
																				$isisasaran .= '<tr ><td >'.$sasaran->tst_sasaran_tujuan.' </span></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																							.'<td></td>'
																						.'</tr>'
																			)
																		@endif
																	@endif
																@endforeach
																@php($totaltujuan = $totalsasaran + $totalsasaranbawah)
																@php($html .= '<tr>'
																				.'<td rowspan="'.$totaltujuan.'">'.$i.'</td>'
																				.'<td rowspan="'.$totaltujuan.'">'.$tujuan->tt_tujuan.'</td>'
																				.$isisasaran
																			.'</tr>')
															@else
																@php($html .= '<tr>'
																				.'<td>'.$i.'</td>'
																				.'<td>'.$tujuan->tt_tujuan.'</td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																				.'<td></td>'
																			.'</tr>')
															@endif
															@php($i++)
														@endforeach
														
														@php(print_r($html))
												</tbody>
											</table>
										
										@endif
									</div>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('onpage-js')
@include('backend.layouts.message')
<script> 	
	function cariData(id){
		var url = '{{ route("pemerintah.perencanaan",["id"=> $id, "idsasaran"=> ":idsasaran", "idjabatan"=> $idjabatan ]) }}';
		url = url.replace(':idsasaran', id);
		window.location.href =  url;
	}
	function cariJabatan(id){
		var url = '{{ route("pemerintah.perencanaan",["id"=> $id, "idsasaran"=> $idsasaran, "idjabatan"=> ":idjabatan" ]) }}';
		url = url.replace(':idjabatan', id);
		window.location.href =  url;
	}
</script> 	
@endsection