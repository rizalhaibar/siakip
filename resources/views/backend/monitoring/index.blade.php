@extends('backend.layouts.app')

@section('title', 'Monitoring')

@section('content')
                

     <table id="listkota" class="table ">
	  <thead>
		<tr>
			<th>No</th>
			<th>Instansi</th>
			<th>Action</th>
		</tr>
	  </thead>
	  
		<tbody>
			@php($sl=1)
				@foreach($listdinas as $dinas)
				 <tr>
					<td>{{$sl}}</td>
					<td>{{$dinas->td_dinas}}</td>
					<td>
						<div class="btn-group">
							<a href="{{ route('backend.monitoring.home', ['id'=>$dinas->td_id, 'idsasaran'=>0]) }}" type="button" class="btn btn-success btn-icon"><i class="fa fa-search"></i></a>
						</div>
					</td>
				 </tr>
				 
				@php($sl++)
			@endforeach
		
		</tbody>

	</table>

@endsection

@section('customjs')
<script>
    $( document ).ready(function() {
		
    });
   
</script>
@endsection


