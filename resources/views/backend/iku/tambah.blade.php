
@if(isset($formula[0]))
            
            <div class="form-group">
              <label class="col-md-2 control-label">Formula*</label>
              <div class="col-md-10">
                  <textarea class="form-control" rows="5" id="formula" misi="formula" placeholder="formula..." required><?php if(isset($formula[0])){ echo $formula[0]['formula']; };?></textarea>
              </div>
          </div>
      <div class="form-group">
          <div class="col-md-offset-10 col-md-8">
              <button type="button" onClick="formulaEdit()" class="btn btn-primary waves-effect waves-light btn-rounded">
                  Edit
              </button>
          </div>
      </div>
          
@else
            <div class="form-group">
              <label class="col-md-2 control-label">Formula*</label>
              <div class="col-md-10">
                  <textarea class="form-control" rows="5" id="formula" misi="formula" placeholder="formula..." required></textarea>
              </div>
          </div>
          <div class="form-group">
          <div class="col-md-offset-10 col-md-8">
              <button type="button" onClick="formulaSave()" class="btn btn-primary waves-effect waves-light btn-rounded">
                  Simpan
              </button>
          </div>
      </div>
@endif


<script>
   
    
    function formulaSave(){
       
         var formula   = $('#formula').val();
        
                if(formula){
                    var datapost={
                        "idsasaran"  :   "{{$id}}",
                        "formula"  :   formula
                      };
                      $.ajax({
                        type: "POST",
                        url: "{{route('backend.iku.formulasave')}}",
                        data : JSON.stringify(datapost),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        success: function(response) {
                          if (response.success == true) {

                              swal({
                                    title: 'Success!',
                                    text: response.message,
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonText: 'Ok'
                                }).then(function () {
                                   window.location.href = "{{route('backend.iku.home')}}";
                                })



                          } else{
                            swal("Failed!", response.message, "error");
                          }
                        }
                      });
                }else{
                    swal("Failed!", "Formula Belum Terisi!", "error");
                }
          
    }
    
    
    function formulaEdit(){
       
         var formula   = $('#formula').val();
        
                if(formula){
                    var datapost={
                        "idsasaran"  :   "{{@$formula[0]->id}}",
                        "formula"  :   formula
                      };
                      $.ajax({
                        type: "POST",
                        url: "{{route('backend.iku.formulaedit')}}",
                        data : JSON.stringify(datapost),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        success: function(response) {
                          if (response.success == true) {

                              swal({
                                    title: 'Success!',
                                    text: response.message,
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonText: 'Ok'
                                }).then(function () {
                                   window.location.href = "{{route('backend.iku.home')}}";
                                })



                          } else{
                            swal("Failed!", response.message, "error");
                          }
                        }
                      });
                }else{
                    swal("Failed!", "Formula Belum Terisi!", "error");
                }
          
    }
</script>
     