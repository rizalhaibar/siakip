<!DOCTYPE html>
<html lang="en">
<head>

<meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ config('app.name', 'Siakip Cimahi') }}</title>
<!--
Hydro Template
http://www.templatemo.com/tm-509-hydro
-->
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <meta name="description" content="">
     <meta name="keywords" content="">
     <meta name="author" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

     <link rel="stylesheet" href="{{ asset('templatemo/css/bootstrap.min.css') }}"  >
     <link rel="stylesheet" href="{{ asset('templatemo/css/magnific-popup.css') }}">
     <link rel="stylesheet" href="{{ asset('templatemo/css/font-awesome.min.css') }}">

     <!-- MAIN CSS -->
     <link rel="stylesheet" href="{{ asset('templatemo/css/templatemo-style.css') }}">
     <style>
     .center {
          display: block;
          margin-left: auto;
          margin-right: auto;
          margin-top: 30%;
          width: 50%;
     }
     </style>
</head>
<body>

     <!-- PRE LOADER -->
     <section class="preloader">
          <div class="spinner">
               <span class="spinner-rotate"></span>
          </div>
     </section>


     <!-- MENU -->
     <section class="navbar custom-navbar navbar-fixed-top" role="navigation">
          <div class="container">

               <div class="navbar-header">
                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                    </button>

                    <!-- lOGO TEXT HERE -->
                    <a href="{{ route('pemerintah.index') }}"><img src="{{ asset('images/logosiakipnew.png') }}" class="navbar-brand" style="width: 150px; height: auto"></a>
               </div>

               <!-- MENU LINKS -->
               <div class="collapse navbar-collapse">
               @if (Request::path() == 'index')

                  <ul class="nav navbar-nav navbar-nav-first">
                        <li><a href="{{ route('pemerintah.index') }}" class="smoothScroll">Beranda</a></li>
                        <!-- <li><a href="#fitur" class="smoothScroll">Fitur Siakip</a></li> -->
                        <li><a href="#blog" class="smoothScroll">Fitur Siakip</a></li>
                        <li><a href="#info" class="smoothScroll">Info Siakip</a></li>
                        <li><a href="#contact" class="smoothScroll">Contacts</a></li>
                  </ul>
              @endif

                    <ul class="nav navbar-nav navbar-right">
                      @if(Session::get('data'))
                        <li  class="section-btn"><a style="color: #ffffff;"  href="{{ route('backend.home') }}" >Back To Admin</a></li>
                      @else
                        <li class="section-btn"><a href="{{ route('login') }}" >Login</a></li>
                      @endif

                    </ul>
               </div>

          </div>
     </section>

     @yield('content')
     <!-- FOOTER -->
     <footer id="contact"  data-stellar-background-ratio="0.5">
          <div class="container">
               <div class="row">

                    <div class="col-md-4 col-sm-12">
                         <div class="footer-thumb footer-info">
                              <h2>Kontak Kami</h2>
							  <ul class="footer-link">
                                   <li><a href="javascript:void(0)">Pemerintah Kota Cimahi</a></li>
                                   <li><a href="javascript:void(0)">Jl. Rd. Demang Hardjakusumah</a></li>
                                   <li><a href="javascript:void(0)">Blok Jati, Cihanjuang, Cimahi</a></li>
                                   <li><a href="javascript:void(0)">Telp. (022) 6654274 | Fax: (022) 6654274</a></li>
                              </ul>
                         </div>
                    </div>

                    <div class="col-md-5 col-sm-8">
                         <div class="footer-thumb">
                              <h2>Layanan </h2>
                              <ul class="footer-link">
                                   <li><a href="javascript:void(0)">Pesan Singkat Penduduk : <b>081221700800<b/></a></li>
                                   <li><a href="javascript:void(0)">Email Layanan Informasi (PPID) :<b> humas@cimahikota.go.id</b></a></li>
                                   <li><a href="javascript:void(0)">Pesan Singkat Penduduk : <b>081221700800</b></a></li>
                              </ul>
                         </div>
                    </div>

                    <div class="col-md-3 col-sm-4">
                         <div class="footer-thumb">
                              <h2></h2>
                         </div>
                    </div>

                    <div class="col-md-12 col-sm-12">
                         <div class="footer-bottom">
                              <div class="col-md-6 col-sm-5">
                                   <div class="copyright-text">
                                        <p>Copyright &copy; 2020 Siakip Cimahi</p>
                                   </div>
                              </div>
                              <div class="col-md-4 col-sm-5">
                                   <div class="phone-contact">

                                   </div>
                              </div>
                              <div class="col-md-2 col-sm-2">
                                   <div class="phone-contact">

                                   </div>
                                   <ul class="social-icon">
                                        <li><img src="{{ asset('images/logo_cimahi2.png') }}" class="img-responsive" alt=""></li>
                                   </ul>
                              </div>
                         </div>
                    </div>

               </div>
          </div>
     </footer>

     <!-- SCRIPTS -->
     <script src="{{ asset('templatemo/js/jquery.js') }}"></script>
     <script src="{{ asset('templatemo/js/bootstrap.min.js') }}" ></script>
     <script src="{{ asset('templatemo/js/jquery.stellar.min.js') }}"></script>
     <script src="{{ asset('templatemo/js/jquery.magnific-popup.min.js') }}"></script>
     <script src="{{ asset('templatemo/js/smoothscroll.js') }}"></script>
     <script src="{{ asset('templatemo/js/custom.js') }}"></script>

     <script>
        var tahun  = localStorage.getItem("tahun");
        $( document ).ready(function() {
          if(!tahun){
            var d = new Date();
            var n = d.getFullYear();
            localStorage.setItem("tahun", n);
            setCookie("tahun", n, 30000000);
          }

        var pkperubahan=getCookie("pkperubahan");
        if (pkperubahan == "") {
            setCookie("pkperubahan", 'normal', 30000000);
        }

          var user=getCookie("tahun");
          if (user == "") {
            var d = new Date();
            var n = d.getFullYear();
            setCookie("tahun", n, 30);
          }

          $('#initahun').html('<span id="initahun">Data Tahun : '+tahun+'</span>');
        });

        function tahunnya(id){
            localStorage.setItem("tahun", id);
             setCookie("tahun", id, 30000000);
            window.location.href = "{{url()->current()}}";
        }

        function getCookie(cname) {
          var name = cname + "=";
          var decodedCookie = decodeURIComponent(document.cookie);
          var ca = decodedCookie.split(';');
          for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
              c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
              return c.substring(name.length, c.length);
            }
          }
          return "";
        }

        function setCookie(cname,cvalue,exdays) {
          var d = new Date();
          d.setTime(d.getTime() + (exdays*24*60*60*1000));
          var expires = "expires=" + d.toGMTString();
          document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }
    </script>

@yield('onpage-js')
</body>
</html>
