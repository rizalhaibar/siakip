@extends('backend.layouts.app')

@section('title', 'IKU')

@section('content')

<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <div class="row">
                        <div class="col-md-11">
                            <h3 class="kt-portlet__head-title">
                                List Misi Keselarasan
                            </h3>
                        </div>
                        <div class="col-md-1" >
                            <a href="javascript:void(0)" onclick="printAwal({{$id}})" class="btn btn-primary waves-effect waves-light btn-rounded">
                                <i class="fas fa-print"></i> Print
                            </a>
                        </div>
                    </div>
                    <table id="listkota" class="table table-bordered">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td width="15%">Tujuan</td>
                                <td width="15%">Sasaran Strategis</td>
                                <td>Indikator Sasaran Strategis</td>
                                <th>Sasaran Program</th>
                                <th>Indikator Sasaran Program</th>
                                <th>Sasaran Kegiatan</th>
                                <th>Indikator Sasaran Kegiatan</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                                    $i=1;
                                    $html = '';
                                    $isisasaran = '';
                                    $isiindikatorsasaran = '';
                                    $isiprogram = '';
                                    $isiindikatorprogram = '';
                                    $isikegiatan = '';
                                    $isiindikatorkegiatan = '';
                                ?>

                                @foreach($keselarasan as $tujuan)


                                    @php($totalsasaran=0)
                                    @php($totalsasaranbawahmentah=0)
                                    @php($totalsasaranbawah=0)
                                    @if(isset($tujuan->sasaran))
                                        @foreach($tujuan->sasaran as $s=>$sasaran)

                                            @php($totalindikatorsasaran=0)
                                            @php($totalindikatorsasaranbawahmentah=0)
                                            @php($totalindikatorsasaranbawah=0)
                                            @if(isset($sasaran->indikatorsasaran))
                                                @foreach($sasaran->indikatorsasaran as $is=>$indikatorsasaran)

                                                    @php($totalprogram=0)
                                                    @php($totalprogrambawahmentah=0)
                                                    @php($totalprogrambawah=0)
                                                    @if(isset($indikatorsasaran->program))
                                                        @foreach($indikatorsasaran->program as $p=>$program)
                                                            @php($totalindikatorprogram=0)
                                                            @php($totalindikatorprogrambawahmentah=0)
                                                            @php($totalindikatorprogrambawah=0)
                                                            @if(isset($program->indikatorprogram))
                                                                @foreach($program->indikatorprogram as $ip=>$indikatorprogram)
                                                                    @php($totalkegiatan=0)
                                                                    @php($totalkegiatanbawah=0)
                                                                    @if(isset($indikatorprogram->kegiatan))
                                                                        @php($totalindikatorkegiatan=0)
                                                                        @php($totalindikatorkegiatanbawah=0)
                                                                        @foreach($indikatorprogram->kegiatan as $k=>$kegiatan)
                                                                            @if(isset($kegiatan->indikatorkegiatan))
                                                                                @foreach($kegiatan->indikatorkegiatan as $ik=>$indikatorkegiatan)

                                                                                    @if($ik == 0)
                                                                                        @php($totalindikatorkegiatan += array_count_values(array_column($kegiatan->indikatorkegiatan, 'idkegiatan'))[$kegiatan->tkp_id])
                                                                                        @php($totalindikatorkegiatanbawah = 1)
                                                                                        @php($isiindikatorkegiatan = '<td>'.$indikatorkegiatan->tik_indikator_kegiatan.' </td>'
                                                                                                    .'</tr>')
                                                                                    @else
                                                                                        @php($totalindikatorkegiatanbawah += 1)
                                                                                        @php(
                                                                                            $isiindikatorkegiatan .= '<tr ><td >'.$indikatorkegiatan->tik_indikator_kegiatan.' </span></td>'
                                                                                                   .'</tr>'
                                                                                        )
                                                                                    @endif
                                                                                @endforeach

                                                                                @if($k == 0)

                                                                                    @php($totalkegiatan+=$totalindikatorkegiatan)
                                                                                    @php($isikegiatan = '<td rowspan="'.$totalindikatorkegiatan.'">'.$kegiatan->tkp_kegiatan_program.' </td>'
                                                                                                    .$isiindikatorkegiatan
                                                                                                .'</tr>')
                                                                                @else

                                                                                    @php($totalkegiatanbawah += $totalindikatorkegiatanbawah)
                                                                                    @php(
                                                                                        $isikegiatan .= '<tr ><td rowspan="'.$totalindikatorkegiatanbawah.'">'.$kegiatan->tkp_kegiatan_program.' </span></td>'
                                                                                                    .$isiindikatorkegiatan
                                                                                                .'</tr>'
                                                                                    )
                                                                                @endif

                                                                            @else
                                                                                @if($k == 0)
                                                                                    @php($totalkegiatan+=1)
                                                                                    @php($isikegiatan = '<td>'.$kegiatan->tkp_kegiatan_program.' </td>'
                                                                                                    .'<td></td>'
                                                                                                .'</tr>')
                                                                                @else
                                                                                    @php($totalkegiatanbawah = $totalkegiatanbawah + 1)
                                                                                    @php(
                                                                                        $isikegiatan .= '<tr ><td >'.$kegiatan->tkp_kegiatan_program.' </span></td>'
                                                                                                    .'<td></td>'
                                                                                                .'</tr>'
                                                                                    )
                                                                                @endif
                                                                            @endif
                                                                        @endforeach

                                                                        @if($ip == 0)
                                                                            @php($totalindikatorprogram = $totalkegiatan + $totalkegiatanbawah)
                                                                            @php($isiindikatorprogram = '<td rowspan="'.$totalindikatorprogram.'">'.$indikatorprogram->tip_indikator_program.' </td>'
                                                                                            .$isikegiatan
                                                                                        .'</tr>')
                                                                        @else
                                                                            @php($totalindikatorprogrambawahmentah = $totalkegiatan + $totalkegiatanbawah)
                                                                            @php($totalindikatorprogrambawah += $totalindikatorprogrambawahmentah)
                                                                            @php(
                                                                                $isiindikatorprogram .= '<tr ><td rowspan="'.$totalindikatorprogrambawahmentah.'">'.$indikatorprogram->tip_indikator_program.' </span></td>'
                                                                                            .$isikegiatan
                                                                                        .'</tr>'
                                                                            )
                                                                        @endif

                                                                    @else
                                                                        @if($ip == 0)
                                                                            @php($totalindikatorprogram+=1)
                                                                            @php($isiindikatorprogram = '<td>'.$indikatorprogram->tip_indikator_program.' </td>'
                                                                                            .'<td></td>'
                                                                                            .'<td></td>'
                                                                                        .'</tr>')
                                                                        @else
                                                                            @php($totalindikatorprogrambawah+=1)
                                                                            @php(
                                                                                $isiindikatorprogram .= '<tr ><td >'.$indikatorprogram->tip_indikator_program.' </span></td>'
                                                                                            .'<td></td>'
                                                                                            .'<td></td>'
                                                                                        .'</tr>'
                                                                            )
                                                                        @endif
                                                                    @endif

                                                                @endforeach

                                                                @if($p == 0)
                                                                    @php($totalprogram = $totalindikatorprogram + $totalindikatorprogrambawah)
                                                                    @php($isiprogram = '<td rowspan="'.$totalprogram.'">'.$program->tps_program_sasaran.' </td>'
                                                                                    .$isiindikatorprogram
                                                                                .'</tr>')
                                                                @else
                                                                    @php($totalprogrambawahmentah = $totalindikatorprogram + $totalindikatorprogrambawah)
                                                                    @php($totalprogrambawah += $totalprogrambawahmentah)
                                                                    @php(
                                                                        $isiprogram .= '<tr ><td rowspan="'.$totalprogrambawahmentah.'">'.$program->tps_program_sasaran.' </span></td>'
                                                                                    .$isiindikatorprogram
                                                                                .'</tr>'
                                                                    )
                                                                @endif

                                                            @else
                                                                @if($p == 0)
                                                                    @php($totalprogram+=1)
                                                                    @php($isiprogram = '<td>'.$program->tps_program_sasaran.' </td>'
                                                                                    .'<td></td>'
                                                                                    .'<td></td>'
                                                                                    .'<td></td>'
                                                                                .'</tr>')
                                                                @else
                                                                    @php($totalprogrambawah+=1)
                                                                    @php(
                                                                        $isiprogram .= '<tr ><td >'.$program->tps_program_sasaran.' </span></td>'
                                                                                    .'<td></td>'
                                                                                    .'<td></td>'
                                                                                    .'<td></td>'
                                                                                .'</tr>'
                                                                    )
                                                                @endif
                                                            @endif

                                                        @endforeach
                                                        @if($is == 0)
                                                            @php($totalindikatorsasaran = $totalprogram + $totalprogrambawah)
                                                            @php($isiindikatorsasaran = '<td rowspan="'.$totalindikatorsasaran.'">'.$indikatorsasaran->tis_indikator_sasaran.' </td>'
                                                                            .$isiprogram
                                                                        .'</tr>')
                                                        @else
                                                            @php($totalindikatorsasaranmentah = $totalprogram + $totalprogrambawah)
                                                            @php($totalindikatorsasaranbawah += $totalindikatorsasaranmentah)
                                                            @php(
                                                                $isiindikatorsasaran .= '<tr ><td rowspan="'.$totalindikatorsasaranmentah.'">'.$indikatorsasaran->tis_indikator_sasaran.' </span></td>'
                                                                            .$isiprogram
                                                                        .'</tr>'
                                                            )
                                                        @endif

                                                    @else
                                                        @if($is == 0)
                                                            @php($totalindikatorsasaran+=1)
                                                            @php($isiindikatorsasaran = '<td>'.$indikatorsasaran->tis_indikator_sasaran.' </td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                        .'</tr>')
                                                        @else
                                                            @php($totalindikatorsasaranbawah+=1)
                                                            @php(
                                                                $isiindikatorsasaran .= '<tr ><td >'.$indikatorsasaran->tis_indikator_sasaran.' </span></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                        .'</tr>'
                                                            )
                                                        @endif
                                                    @endif
                                                @endforeach

                                                @if($s == 0)
                                                    @php($totalsasaran = $totalindikatorsasaran + $totalindikatorsasaranbawah)
                                                    @php($isisasaran = '<td rowspan="'.$totalsasaran.'">'.$sasaran->tst_sasaran_tujuan.' </td>'
                                                                        .$isiindikatorsasaran
                                                                .'</tr>')
                                                @else
                                                    @php($totalsasaranmentah = $totalindikatorsasaran + $totalindikatorsasaranbawah)
                                                    @php($totalsasaranbawah += $totalsasaranmentah)
                                                    @php(
                                                        $isisasaran .= '<tr ><td rowspan="'.$totalsasaranmentah.'">'.$sasaran->tst_sasaran_tujuan.' </span></td>'
                                                                    .$isiindikatorsasaran
                                                                .'</tr>'
                                                    )
                                                @endif

                                            @else
                                                @if($s == 0)
                                                    @php($totalsasaran+=1)
                                                    @php($isisasaran = '<td>'.$sasaran->tst_sasaran_tujuan.' </td>'
                                                                    .'<td></td>'
                                                                    .'<td></td>'
                                                                    .'<td></td>'
                                                                    .'<td></td>'
                                                                    .'<td></td>'
                                                                .'</tr>')
                                                @else
                                                 @php($totalsasaranbawah+=1)
                                                    @php(
                                                        $isisasaran .= '<tr ><td >'.$sasaran->tst_sasaran_tujuan.' </span></td>'
                                                                    .'<td></td>'
                                                                    .'<td></td>'
                                                                    .'<td></td>'
                                                                    .'<td></td>'
                                                                    .'<td></td>'
                                                                .'</tr>'
                                                    )
                                                @endif
                                            @endif
                                        @endforeach
                                        @php($totaltujuan = $totalsasaran + $totalsasaranbawah)
                                        @php($html .= '<tr>'
                                                        .'<td rowspan="'.$totaltujuan.'">'.$i.'</td>'
                                                        .'<td rowspan="'.$totaltujuan.'">'.$tujuan->tt_tujuan.'</td>'
                                                        .$isisasaran
                                                    .'</tr>')
                                    @else
                                        @php($html .= '<tr>'
                                                        .'<td>'.$i.'</td>'
                                                        .'<td>'.$tujuan->tt_tujuan.'</td>'
                                                        .'<td></td>'
                                                        .'<td></td>'
                                                        .'<td></td>'
                                                        .'<td></td>'
                                                        .'<td></td>'
                                                        .'<td></td>'
                                                    .'</tr>')
                                    @endif
                                    @php($i++)
                                @endforeach

                                @php(print_r($html))
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="panel-modal" class="modal fade" id="kt_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

@endsection
@section('onpage-js')
@include('backend.layouts.message')
<script>


    function printAwal(id){
        var url = '{{ route("backend.keselarasan.printkesel",["id"=>":id"]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Print Keselarasan');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }

</script>
@endsection


