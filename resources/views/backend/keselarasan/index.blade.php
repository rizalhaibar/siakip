@extends('backend.layouts.app')

@section('title', 'Keselarasan')

@section('content')
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                List Dinas
                            </h3>
                        </div>
                </div>
                <div class="kt-portlet__body">
                    <table id="listkota" class="table ">
                        <thead>
                            <tr>
                            <th>No</th>
                            <th>Instansi</th>
                            <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            @php($sl=1)
                                @foreach($listdinas as $dinas)
                                    <tr>
                                    <td>{{$sl}}</td>
                                    <td>{{$dinas->td_dinas}}</td>
                                    <td>
                                        <div class="btn-group">           
                                            <a href="{{ route('backend.keselarasan.home', ['id'=>Crypt::encrypt($dinas->td_id)]) }}" type="button" class="btn btn-success btn-icon"><i class="fa fa-search"></i></a>
                                 
                                        </div>
                                    </td>
                                    </tr>
                                    
                                @php($sl++)
                            @endforeach
                        </tbody>
                    </table>
                </div>              
            </div>              
        </div>              
    </div>              
</div>              


@endsection

@section('customjs')
<script>
    $( document ).ready(function() {
		
    });
      
</script>
@endsection


