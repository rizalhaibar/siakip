<form class="kt-form" method="post" action="{{route('backend.keselarasan.print')}}">
    @csrf

    @if($id == 1)
    <div class="form-group">
        <label class="col-md-2 control-label">Pihak Pertama*</label>
        <div class="col-md-12">
            <input type="hidden" class="form-control" id="id" name="id" value="{{$id}}">
            <input type="text" class="form-control" id="pihak_pertama" name="pihak_pertama" placeholder="Pihak Pertama..." required>
        </div>
    </div>

    @if($id != 1)
        <div class="form-group">
            <label class="col-md-2 control-label">Pangkat/Golongan Ruang*</label>
            <div class="col-md-12">
                <input type="text" class="form-control" id="jabatan_pihak_pertama" name="jabatan_pihak_pertama" placeholder="Pangkat/Golongan Ruang..." required>
            </div>
        </div>
    @else
        <input type="hidden" class="form-control" id="jabatan_pihak_pertama" name="jabatan_pihak_pertama" value="WALIKOTA" required>
    @endif
    @else
    <div class="form-group">
        <label class="col-md-2 control-label">Pihak Pertama*</label>
        <div class="col-md-12">
            <input type="hidden" class="form-control" id="id" name="id" value="{{$id}}">
            <input type="text" class="form-control" id="pihak_pertama" name="pihak_pertama" placeholder="Pihak Pertama..." required>
        </div>
    </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Pangkat/Golongan Ruang*</label>
            <div class="col-md-12">
                <input type="text" class="form-control" id="jabatan_pihak_pertama" name="jabatan_pihak_pertama" placeholder="Pangkat/Golongan Ruang..." required>
            </div>
        </div>
    <div class="form-group">
        <label class="col-md-2 control-label">Pihak Kedua*</label>
        <div class="col-md-12">
            <input type="text" class="form-control" id="pihak_kedua" name="pihak_kedua" placeholder="Pihak Kedua..." required>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-2 control-label">Pangkat/Golongan Ruang*</label>
        <div class="col-md-12">
            <input type="text" class="form-control" id="jabatan_pihak_kedua" name="jabatan_pihak_kedua" placeholder="Pangkat/Golongan Ruang..." required>
        </div>
    </div>
    @endif
    <div class="form-group">
        <label class="col-md-2 control-label">NIP*</label>
        <div class="col-md-12">
            <input type="text" class="form-control" id="nip" name="nip" placeholder="NIP..." required>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-offset-10 col-md-8">
            <button type="submit" class="btn btn-primary waves-effect waves-light btn-rounded">
                Print
            </button>
        </div>
    </div>
</form>
