@extends('backend.layouts.app')

@section('title', 'Detail Laporan Kinerja')

@section('content')
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                List Sasaran : @if($idsasaran != 0){{ @$detail->tst_sasaran_tujuan}}@endif
                            </h3>
                        </div>
                </div>
                <div class="kt-portlet__body">
                <div class="form-group">
                    <label for="exampleSelect1">Pilih Sasaran : </label>
                    <select class="form-control"  onChange="cariData(this.value)">
                        <option selected disabled>Pilih Sasaran ....</option>
                        @foreach($listsasaran as $sasaran)
                        <option value="{{$sasaran->tst_id}}" @if($idsasaran != 0 ) {{$sasaran->tst_id == $idsasaran ? 'selected' : ''}} @endif >{{$sasaran->tst_sasaran_tujuan}}</option>
                        @endforeach
                    </select>
                </div>

                @if($idsasaran != 0)
                        
                <div class="form-horizontal" >
                    <div class="form-group">
                        <label class="col-md-2 control-label">Judul*</label>
                        <div class="col-md-12">
                            <input type="text" value="{{@$laporan->tp_judul}}" class="form-control" id="judul" name="judul" placeholder="Judul..." required>
                        </div>
                    </div>

                    <div id="editor">
                        <textarea name="isieditor" id="isieditor">{{@$laporan->tp_content}}</textarea>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-11 col-md-8">
                            <button type="button" id="send" class="btn btn-primary waves-effect waves-light btn-rounded">
                                Simpan
                            </button>
                        </div>
                    </div>
                </div>
                            
                @endif  
                   
                </div>                
            </div>                
        </div>                
    </div>                
</div>


@endsection

@section('onpage-js')
@include('backend.layouts.message')
<script src="{{ asset('themes/metronic/assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js') }}" type="text/javascript"></script>
<script> 	

// Initialization
jQuery(document).ready(function() {
    var myEditor;
    editor = ClassicEditor
        .create( document.querySelector( '#isieditor' ) )
        .then( editor => {
            console.log( editor );
            myEditor = editor;
        } )
        .catch( error => {
            console.error( error );
        } );

            
        $('#send').click(function() {
            var editorData = myEditor.getData();
            var judul       = $('#judul').val();
            console.log(editorData)
            swal({
                title: 'Apa anda yakin?',
                    text: "Anda akan menyimpan laporan ini!",
                    showCancelButton: true,
                    confirmButtonText: 'Ok'
            }).then(function () {
                var datapost={
                    "id"  :   '{{$idsasaran}}',
                    "isi" : editorData,
                    "judul" : judul,
                    "tahun" : tahun
                    };
                    $.ajax({
                    type: "POST",
                    url: "{{route('backend.laporankinerja.savelaporan')}}",
                    data : JSON.stringify(datapost),
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    success: function(response) {
                        if (response.success == true) {

                            swal({
                                title: 'Success!',
                                text: response.message,
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonText: 'Ok'
                            }).then(function () {
                                
                                var url = '{{ route("backend.laporankinerja.detail",["id"=>"$id", "idsasaran"=> "$idsasaran" ]) }}';
                       
                                window.location.href =  url;
                            })
                        } else{
                        swal("Failed!", response.message, "error");
                        }
                    }
                    });
            })
        });
});

$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});

function cariData(id){
    var url = '{{ route("backend.laporankinerja.detail",["id"=>"$id", "idsasaran"=> ":idsasaran" ]) }}';
    url = url.replace(':idsasaran', id);
    window.location.href =  url;
}

</script> 	
@endsection