@extends('backend.layouts.app')

@section('title', 'Laporan Kinerja')

@section('content')
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                List Dinas
                            </h3>
                        </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="listkota" class="table ">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Instansi</th>
                                            <th><Action</th>
                                        </tr>
                                        <tbody>
                                            @php($sl=1)
                                                @foreach($listdinas as $dinas)
                                                <tr>
                                                    <td>{{$sl}}</td>
                                                    <td>{{$dinas->td_dinas}}</td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <a href="{{ route('backend.laporankinerja.detail', ['id'=>$dinas->td_id, 'idsasaran'=>0]) }}" type="button" class="btn btn-success btn-icon"><i class="fa fa-search"></i></a>
                                                        </div> 
                                                        <div class="btn-group">
                                                            <a href="javascript:void(0)" onClick="print({{$dinas->td_id}})" class="btn btn-info btn-icon" ><i class="fa fa-print"></i></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                
                                                @php($sl++)
                                            @endforeach
                                        
                                        </tbody>
                                    </thead>
                            </table>
                        </div>                
                    </div>                
                </div>                
            </div>                
        </div>                
    </div>                
</div>


@endsection

@section('onpage-js')
@include('backend.layouts.message')
<script>
   
</script>
@endsection