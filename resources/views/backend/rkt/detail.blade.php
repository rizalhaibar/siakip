
<div class="form-group">
    <label class="col-md-2 control-label">Satuan*</label>
    <div class="col-md-10">
        <input class="form-control" value="{{@$target[0]->tis_satuan}}" id="satuan" misi="satuan" placeholder="satuan..." required>
    </div>
</div>
<div class="form-group">
    <label class="col-md-2 control-label">Target*</label>
    <div class="col-md-10">
        <input class="form-control" value="{{@$target[0]->tis_target}}" id="target" misi="target" placeholder="target..." required>
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-10 col-md-8">
        <button type="button" onClick="targetEdit()" class="btn btn-primary waves-effect waves-light btn-rounded">
            Simpan
        </button>
    </div>
</div>


<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    
    
    function targetEdit(){
       
         var target   = $('#target').val();
         var satuan   = $('#satuan').val();
        
                if(satuan){
                    if(target){
                        var datapost={
                            "idsasaran"  :   "{{$id}}",
                            "satuan"  :   satuan,
                            "target"  :   target
                          };
                          $.ajax({
                            type: "POST",
                            url: "{{route('backend.rkt.targetedit')}}",
                            data : JSON.stringify(datapost),
                            dataType: 'json',
                            contentType: 'application/json; charset=utf-8',
                            success: function(response) {
                              if (response.success == true) {
                                $("#panel-modal .close").click();
                                  swal({
                                        title: 'Success!',
                                        text: response.message,
                                        type: 'success',
                                        showCancelButton: false,
                                        confirmButtonText: 'Ok'
                                    }).then(function () {
                                       window.location.href = "{{route('backend.rkt.home', $iddinas)}}";
                                    })



                              } else{
                                swal("Failed!", response.message, "error");
                              }
                            }
                          });
                    }else{
						$("#panel-modal .close").click();
                        swal("Failed!", "Target Belum Terisi!", "error");
                    }  
                
                
                }else{
					$("#panel-modal .close").click();
                    swal("Failed!", "Satuan Belum Terisi!", "error");
                }
          
    }
</script>