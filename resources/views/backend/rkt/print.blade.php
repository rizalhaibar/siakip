<form class="kt-form" method="post" action="{{route('backend.rkt.print')}}">
    @csrf
    <div class="form-group">
        <label class="col-md-2 control-label">Bulan*</label>
        <div class="col-md-12">
            <select class="form-control" id="bulan" name="bulan">
                <option selected disabled>Select</option>
                <option value="Januari">Januari</option>
                <option value="Februari">Februari</option>
                <option value="Maret">Maret</option>
                <option value="April">April</option>
                <option value="Mei">Mei</option>
                <option value="Juni">Juni</option>
                <option value="Juli">Juli</option>
                <option value="Agustus">Agustus</option>
                <option value="September">September</option>
                <option value="Oktober">Oktober</option>
                <option value="November">November</option>
                <option value="Desember">Desember</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2 control-label">Pihak Pertama*</label>
        <div class="col-md-12">
            <input type="hidden" class="form-control" id="id" name="id" value="{{$id}}">
            <input type="text" class="form-control" id="pihak_pertama" name="pihak_pertama" placeholder="Pihak Pertama..." required>
        </div>
    </div>

    @if($id != 1)
        <div class="form-group">
            <label class="col-md-2 control-label">Pangkat/Golongan Ruang*</label>
            <div class="col-md-12">
                <input type="text" class="form-control" id="jabatan_pihak_pertama" name="jabatan_pihak_pertama" placeholder="Pangkat/Golongan Ruang..." required>
            </div>
        </div>
    @endif

    <div class="form-group">
        <label class="col-md-2 control-label">NIP*</label>
        <div class="col-md-12">
            <input type="text" class="form-control" id="nip" name="nip" placeholder="NIP..." required>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-offset-10 col-md-8">
            <button type="submit" class="btn btn-primary waves-effect waves-light btn-rounded">
                Print
            </button>
        </div>
    </div>
</form>
