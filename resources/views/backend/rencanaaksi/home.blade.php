@extends('backend.layouts.app')

@section('title', 'Rencana Aksi')

@section('content')

<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__body">


                    <div class="row">
                        <div class="col-md-11">
                            <h3 class="kt-portlet__head-title">
                                List Sasaran
                            </h3>
                        </div>
                        <div class="col-md-1" >
                            <a href="javascript:void(0)" onclick="printAwal({{$id}})" class="btn btn-primary waves-effect waves-light btn-rounded">
                                <i class="fas fa-print"></i> Print
                            </a>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect1">Pilih Sasaran : </label>
                        <select class="form-control"  onChange="cariData(this.value)">
                            <option selected disabled>Pilih Sasaran ....</option>
                            @foreach($listsasaran as $sasaran)
                            <option value="{{$sasaran->tst_id}}" @if($idsasaran != 0 ) {{$sasaran->tst_id == $idsasaran ? 'selected' : ''}} @endif >{{$sasaran->tst_sasaran_tujuan}}</option>
                            @endforeach
                        </select>
                    </div>

                    @if($idsasaran != 0)
                    <ul class="nav nav-tabs  nav-tabs-line nav-tabs-line-2x nav-tabs-line-success" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#sasaran" role="tab">Sasaran Strategis</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " data-toggle="tab" href="#program" role="tab">Sasaran Program</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " data-toggle="tab" href="#kegiatan" role="tab">Sasaran Kegiatan</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="sasaran" role="tabpanel">
                            <ul style="margin-left: 20px">
                                <h4 style="margin-left:-30px; color: #336600; margin-top:10px">Indikator Sasaran Strategis</h4>
                            </ul>

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2">Indikator Sasaran Strategis</th>
                                        <th rowspan="2">Satuan</th>
                                        <th rowspan="2">Target</th>
                                        <th colspan="4"><center>Target <span id="tahun"></span></center></th>
                                        <th rowspan="2">Action</th>
                                    </tr>
                                    <tr>
                                        <th>Triwulan I</th>
                                        <th>Triwulan II</th>
                                        <th>Triwulan III</th>
                                        <th>Triwulan IV</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php($sas = 1)
                                @foreach($indikatorsasaran as $indisasaran)

                                    <tr>
                                        <td>{{$sas}}</td>
                                        <td>{{$indisasaran->tis_indikator_sasaran}}</td>
                                        <td>{{$indisasaran->tis_satuan}}</td>
                                        <td>{{$indisasaran->tis_target}}</td>
                                        <td>{{$indisasaran->twtarget1}}</td>
                                        <td>{{$indisasaran->twtarget2}}</td>
                                        <td>{{$indisasaran->twtarget3}}</td>
                                        <td>{{$indisasaran->twtarget4}}</td>
                                        <td>
                                            <center>
                                                <div class="btn-group">
                                                @if($indisasaran->twtarget1)
                                                    <a href="javascript:void(0)"  onClick="edittargetsasaran({{$indisasaran->tis_id}})" type="button" class="btn btn-info btn-icon"><i class="fa fa-edit"></i></a>
                                                @else
                                                    <a href="javascript:void(0)"  onClick="tambahtargetsasaran({{$indisasaran->tis_id}})" type="button" class="btn btn-success btn-icon"><i class="fa fa-search"></i></a>
                                                @endif
                                                </div>
                                            </center>
                                        </td>
                                    </tr>
                                    @php($sas++)
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane " id="program" role="tabpanel">

                        <ul style="margin-left: 20px">
                            <h4 style="margin-left:-30px; color: #336600; margin-top:10px">Sasaran Program </h4>
                        </ul>

                            <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">No</th>
                                                <th rowspan="2">Indikator Sasaran Strategis</th>
                                                <th rowspan="2">Sasaran Program</th>
                                                <th rowspan="2">Indikator Sasaran Program</th>
                                                <th rowspan="2">Satuan</th>
                                                <th rowspan="2">Target</th>
                                                <th colspan="4"><center>Target <span id="tahun"></span></center></th>
                                                <th rowspan="2">Action</th>
                                            </tr>
                                            <tr>
                                                <th>Triwulan I</th>
                                                <th>Triwulan II</th>
                                                <th>Triwulan III</th>
                                                <th>Triwulan IV</th>
                                            </tr>
                                        </thead>
                                        <?php
                                                $i=1;

                                                $html = '';
                                                $isi1 = '';
                                                $isi2 = '';
                                                $isi3 = '';
                                                $isi4 = '';
                                            ?>
                                        <tbody>
                                        @foreach($program as $new)
                                        @php($totalprogram=0)

                                                @if(isset($new->program))

                                                @php($alltotalindikatorprogram=0)
                                                @php($allatas=0)
                                                @php($allbawah=0)
                                                @foreach($new->program as $k=>$prg)
                                                    @php($totalindikatorprogram=0)
                                                    @php($totalindikatorprogrambawah=0)
                                                    @php($x=0)
                                                    @if(isset($prg->indikatorprogram))




                                                        @php($actionhtml = '')
                                                        @foreach($prg->indikatorprogram as $s=>$indprg)
                                                            @if($indprg->twtarget1)
                                                                @php($actionhtml = '<a href="javascript:void(0)" class="btn btn-info btn-sm btn-icon" onClick="edittargetprogram('.$indprg->tip_id.')"><i class="fa fa-edit"></i></a>')
                                                            @else
                                                                @php($actionhtml = '<a href="javascript:void(0)" class="btn btn-success btn-sm btn-icon" onClick="tambahtargetprogram('.$indprg->tip_id.')"><i class="fa fa-search"></i></a>')
                                                            @endif
                                                            @if($s == 0)
                                                                @php($totalindikatorprogram += array_count_values(array_column($prg->indikatorprogram, 'idprogram'))[$prg->tps_id])


                                                                @php($totalindikatorprogrambawah = 1)
                                                                @php($isi2 = '<td> '.$indprg->tip_indikator_program.' </td>'
                                                                                .'<td>'.$indprg->tip_satuan.'</td>'
                                                                                .'<td>'.$indprg->tip_target.'</td>'
                                                                                .'<td>'.$indprg->twtarget1.'</td>'
                                                                                .'<td>'.$indprg->twtarget2.'</td>'
                                                                                .'<td>'.$indprg->twtarget3.'</td>'
                                                                                .'<td>'.$indprg->twtarget4.'</td>'
                                                                                .'<td>'.$actionhtml.'</td>'
                                                                            .'</tr>'
                                                                            )


                                                            @else

                                                                @php(
                                                                    $isi2 .= '<tr ><td >'.$indprg->tip_indikator_program.' </td>'
                                                                                .'<td>'.$indprg->tip_satuan.'</td>'
                                                                                .'<td>'.$indprg->tip_target.'</td>'
                                                                                .'<td>'.$indprg->twtarget1.'</td>'
                                                                                .'<td>'.$indprg->twtarget2.'</td>'
                                                                                .'<td>'.$indprg->twtarget3.'</td>'
                                                                                .'<td>'.$indprg->twtarget4.'</td>'
                                                                                .'<td>'.$actionhtml.'</td>'
                                                                            .'</tr>'
                                                                )
                                                            @endif

                                                        @endforeach




                                                        @if($k == 0)
                                                            @php($totalprogram += array_count_values(array_column($new->program, 'idindisasaran'))[$new->tis_id])

                                                            @php($allatas = $totalindikatorprogram)

                                                            @php($isi1 = '<td rowspan="'.$totalindikatorprogram.'">'.$prg->tps_program_sasaran.' </td>'
                                                                            .$isi2)
                                                        @else
                                                            @php($allbawah += $totalindikatorprogrambawah)
                                                            @php(
                                                                $isi1 .= '<tr ><td rowspan="'.$totalindikatorprogrambawah.'"> '.$prg->tps_program_sasaran.'</td>'
                                                                            .$isi2
                                                            )
                                                        @endif



                                                    @else


                                                        @php($alltotalindikatorprogram = $alltotalindikatorprogram+1)
                                                        @if($k == 0)
                                                            @php($totalprogram += array_count_values(array_column($new->program, 'idindisasaran'))[$new->tis_id])
                                                            @php($isi1 = '<td>'.$prg->tps_program_sasaran.' </td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                        .'</tr>')
                                                        @else
                                                            @php(
                                                                $isi1 .= '<tr ><td >'.$prg->tps_program_sasaran.' </span></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                        .'</tr>'
                                                            )
                                                        @endif
                                                    @endif
                                                @endforeach



                                                @php($alltotalindikatorprogram = $totalindikatorprogram+$totalindikatorprogrambawah)
                                                @php($totalkeseluruhan = $allatas+$allbawah)
                                                @php($html .= '<tr>'
                                                                .'<td rowspan="'.$totalkeseluruhan.'">'.$i.'</td>'
                                                                .'<td rowspan="'.$totalkeseluruhan.'">'.$new->tis_indikator_sasaran.' </td>'
                                                                .$isi1)
                                                @else

                                                    @php($html .= '<tr>'
                                                                .'<td>'.$i.'</td>'
                                                                .'<td>'.$new->tis_indikator_sasaran.'</td>'
                                                                .'<td></td>'
                                                                .'<td></td>'
                                                                .'<td></td>'
                                                                .'<td></td>'
                                                                .'<td></td>'
                                                                .'<td></td>'
                                                                .'<td></td>'
                                                                .'<td></td>'
                                                                .'<td></td>'
                                                            .'</tr>')
                                                @endif
                                                @php($i++)
                                        @endforeach

                                        @php(print_r($html))
                                        </tbody>
                            </table>

                        </div>
                        <div class="tab-pane " id="kegiatan" role="tabpanel">
                        <ul style="margin-left: 20px">
                            <h4 style="margin-left:-30px; color: #336600; margin-top:10px">Sasaran Kegiatan </h4>
                        </ul>

                            <table class="table table-bordered">
                                <thead>
                                    <tr>

                                        <tr>
                                            <th rowspan="2">No</th>
                                            <th rowspan="2">Sasaran Program</th>
                                            <th rowspan="2">Indikator Sasaran Program</th>
                                            <th rowspan="2">Sasaran Kegiatan</th>
                                            <th rowspan="2">Indikator Sasaran Kegiatan</th>
                                            <th rowspan="2">Satuan</th>
                                            <th rowspan="2">Target</th>
                                            <th colspan="4"><center>Target <span id="tahun"></span></center></th>
                                            <th rowspan="2">Action</th>
                                        </tr>
                                        <tr>
                                            <th>Triwulan I</th>
                                            <th>Triwulan II</th>
                                            <th>Triwulan III</th>
                                            <th>Triwulan IV</th>
                                        </tr>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $ik=1;

                                        $htmlk = '';
                                        $isik1 = '';
                                        $isik2 = '';
                                        $isik3 = '';
                                        $isik4 = '';
                                        $isik5 = '';
                                        $isik5s = '';
                                        $isik6 = '';
                                        $isik7 = '';
                                        $akhirtotala = 0;
                                    ?>

                                        @foreach($kegiatan as $keg)

                                            @php($totalprogramk=0)
                                            @php($totalatasprogram=0)
                                            @php($testatas=0)
                                            @php($alltotalkegiatan=0)
                                            @php($alltotalkegiatanbawah=0)

                                            @php($totalbawahprogram=0)
                                            @php($totalbawahprogram2=0)
                                            @if(isset($keg->indikatorprogram))
                                                @foreach($keg->indikatorprogram as $m=>$indikatorprogram)
                                                    <!-- param -->

                                                    @php($totalkegiatan=0)
                                                    @php($totalkegiatanbawah=0)
                                                    @php($totalindikatorkegiatank=0)
                                                    @php($allataskegiatan=0)
                                                    @php($allbawahkegiatan=0)

                                                    @if(isset($indikatorprogram->kegiatan))
                                                        <!-- INDIKATOR KEGIATAN -->

                                                        @php($totalindikatorkegiatan=0)
                                                        @php($totalindikatorkegiatanbawah=0)

                                                        @foreach($indikatorprogram->kegiatan as $as=>$kga)

                                                            @if(isset($kga->indikatorkegiatan))


                                                                @foreach($kga->indikatorkegiatan as $inkas=>$indikatorkegiatan)
                                                                @php($actionhtml = '')
                                                                    @if($indikatorkegiatan->twtarget1)
                                                                        @php($actionhtml = '<a href="javascript:void(0)" class="btn btn-info btn-sm btn-icon" onClick="edittargetkegiatan('.$indikatorkegiatan->tik_id.')"><i class="fa fa-edit"></i></a>')
                                                                    @else
                                                                        @php($actionhtml = '<a href="javascript:void(0)" class="btn btn-success btn-sm btn-icon" onClick="tambahtargetkegiatan('.$indikatorkegiatan->tik_id.')"><i class="fa fa-search"></i></a>')
                                                                    @endif
                                                                    @if($inkas == 0)
                                                                        @php($totalindikatorkegiatan += array_count_values(array_column($kga->indikatorkegiatan, 'idkegiatan'))[$kga->tkp_id])

                                                                        @php($totalindikatorkegiatanbawah = 1)
                                                                        @php($isik5 = '<td > '.$indikatorkegiatan->tik_indikator_kegiatan.'  </td>'
                                                                                            .'<td>'.$indikatorkegiatan->tik_satuan.'</td>'
                                                                                            .'<td>'.$indikatorkegiatan->tik_target.'</td>'
                                                                                            .'<td>'.$indikatorkegiatan->twtarget1.'</td>'
                                                                                            .'<td>'.$indikatorkegiatan->twtarget2.'</td>'
                                                                                            .'<td>'.$indikatorkegiatan->twtarget3.'</td>'
                                                                                            .'<td>'.$indikatorkegiatan->twtarget4.'</td>'
                                                                                            .'<td>'.$actionhtml.'</td>'
                                                                                        .'</tr>'
                                                                                    )
                                                                    @else

                                                                        @php($totalindikatorkegiatanbawah += 1)

                                                                        @php(
                                                                            $isik5 .= '<tr >'
                                                                                                .'<td >'.$indikatorkegiatan->tik_indikator_kegiatan.' </td>'
                                                                                                .'<td>'.$indikatorkegiatan->tik_satuan.'</td>'
                                                                                                .'<td>'.$indikatorkegiatan->tik_target.'</td>'
                                                                                                .'<td>'.$indikatorkegiatan->twtarget1.'</td>'
                                                                                                .'<td>'.$indikatorkegiatan->twtarget2.'</td>'
                                                                                                .'<td>'.$indikatorkegiatan->twtarget3.'</td>'
                                                                                                .'<td>'.$indikatorkegiatan->twtarget4.'</td>'
                                                                                                .'<td>'.$actionhtml.'</td>'
                                                                                            .'</tr>'
                                                                        )
                                                                    @endif
                                                                @endforeach

                                                                @if($as == 0)

                                                                    @php($allataskegiatan+=$totalindikatorkegiatan)
                                                                    @php($totalkegiatan += array_count_values(array_column($indikatorprogram->kegiatan, 'idindiprogram'))[$indikatorprogram->tip_id])
                                                                    @php($isik2 = '<td rowspan="'.$totalindikatorkegiatan.'">'.$kga->tkp_kegiatan_program.'  </td>'
                                                                                        .$isik5


                                                                                    )
                                                                @else
                                                                    @php($totalkegiatan += 1)


                                                                    @php($allbawahkegiatan += $totalindikatorkegiatanbawah)

                                                                    @php(
                                                                        $isik2 .= '<tr ><td rowspan="'.$totalindikatorkegiatanbawah.'"> '.$kga->tkp_kegiatan_program.'</td>'
                                                                                            .$isik5


                                                                    )
                                                                @endif

                                                            @else
                                                                @if($as == 0)
                                                                    @php($allataskegiatan+=1)
                                                                    @php($totalkegiatan += array_count_values(array_column($indikatorprogram->kegiatan, 'idindiprogram'))[$indikatorprogram->tip_id])
                                                                    @php($isik2 = '<td>'.$kga->tkp_kegiatan_program.' </td>'
                                                                                        .'<td></td>'
                                                                                            .'<td></td>'
                                                                                            .'<td></td>'
                                                                                            .'<td></td>'
                                                                                            .'<td></td>'
                                                                                            .'<td></td>'
                                                                                            .'<td></td>'
                                                                                            .'<td></td>'
                                                                                        .'</tr>'
                                                                                    )
                                                                @else

                                                                    @php($allbawahkegiatan = $allbawahkegiatan + 1)
                                                                    @php(
                                                                        $isik2 .= '<tr ><td > '.$kga->tkp_kegiatan_program.'</td>'
                                                                                    .'<td></td>'
                                                                                            .'<td></td>'
                                                                                            .'<td></td>'
                                                                                            .'<td></td>'
                                                                                            .'<td></td>'
                                                                                            .'<td></td>'
                                                                                            .'<td></td>'
                                                                                            .'<td></td>'
                                                                                        .'</tr>'

                                                                    )
                                                                @endif
                                                            @endif
                                                        @endforeach

                                                        <!-- KEGIATAN ISI -->

                                                        @if($m == 0)
                                                            @php($alltotalkegiatan = $allataskegiatan + $allbawahkegiatan)
                                                            @php($isik1 = '<td rowspan="'.$alltotalkegiatan.'"> '.$indikatorprogram->tip_indikator_program.'  </td>'
                                                                            .$isik2
                                                                )
                                                        @else
                                                            @php($alltotalkegiatanbawah = $allataskegiatan + $allbawahkegiatan)
                                                            @php($testatas += $alltotalkegiatanbawah)
                                                            @php(
                                                                $isik1 .= '<tr>'
                                                                            .'<td rowspan="'.$alltotalkegiatanbawah.'">'.$indikatorprogram->tip_indikator_program.'   </td>'
                                                                            .$isik2
                                                            )
                                                        @endif

                                                    @else


                                                        @if($m == 0)

                                                        @php($alltotalkegiatan = $alltotalkegiatan + 1)
                                                            @php($isik1 = '<td>'.$indikatorprogram->tip_indikator_program.'</td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                        .'</tr>')
                                                        @else

                                                        @php($testatas += 1)
                                                            @php(
                                                                $isik1 .= '<tr ><td >'.$indikatorprogram->tip_indikator_program.' </td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                            .'<td></td>'
                                                                        .'</tr>'
                                                            )
                                                        @endif

                                                    @endif
                                                @endforeach
                                                @php($akhirtotal =  $alltotalkegiatan + $testatas)
                                            <!-- Program ISI -->
                                                @php($htmlk .= '<tr>'
                                                                    .'<td rowspan="'.$akhirtotal.'">'.$ik.'</td>'
                                                                    .'<td rowspan="'.$akhirtotal.'">'.$keg->tps_program_sasaran.' </td>'
                                                                    .$isik1
                                                                    )
                                            @else

                                            <!-- Program Kosong -->
                                                @php($htmlk .= '<tr>'
                                                                .'<td>'.$ik.'</td>'
                                                                .'<td>'.$keg->tps_program_sasaran.'</td>'
                                                                .'<td></td>'
                                                                .'<td></td>'
                                                                .'<td></td>'
                                                                .'<td></td>'
                                                                .'<td></td>'
                                                                .'<td></td>'
                                                                .'<td></td>'
                                                                .'<td></td>'
                                                                .'<td></td>'
                                                                .'<td></td>'
                                                            .'</tr>')
                                            @endif
                                            @php($ik++)
                                        @endforeach

                                        @php(print_r($htmlk))
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>


<div id="panel-modal" class="modal fade" id="kt_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

@endsection

@section('onpage-js')
@include('backend.layouts.message')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function tambahtargetsasaran(id){
        var url = '{{ route("backend.rencanaaksi.tambahtargetsasaran",["id"=>":id", "iddinas"=> $id, "idsasaran"=> $idsasaran]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Tambah Target Triwulan Sasaran');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    function edittargetsasaran(id){
        var url = '{{ route("backend.rencanaaksi.edittargetsasaran",["id"=>":id", "iddinas"=> $id, "idsasaran"=> $idsasaran]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Edit Target Triwulan Sasaran');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    function tambahtargetprogram(id){
        var url = '{{ route("backend.rencanaaksi.tambahtargetprogram",["id"=>":id", "iddinas"=> $id, "idsasaran"=> $idsasaran]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Tambah Target Triwulan Program');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    function edittargetprogram(id){
        var url = '{{ route("backend.rencanaaksi.edittargetprogram",["id"=>":id", "iddinas"=> $id, "idsasaran"=> $idsasaran]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Edit Target Triwulan Program');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    function tambahtargetkegiatan(id){
        var url = '{{ route("backend.rencanaaksi.tambahtargetkegiatan",["id"=>":id", "iddinas"=> $id, "idsasaran"=> $idsasaran]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Tambah Target Triwulan Kegiatan');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    function edittargetkegiatan(id){
        var url = '{{ route("backend.rencanaaksi.edittargetkegiatan",["id"=>":id", "iddinas"=> $id, "idsasaran"=> $idsasaran]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Edit Target Triwulan Kegiatan');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }

    function cariData(id){
        var url = '{{ route("backend.rencanaaksi.home",["id"=>"$id", "idsasaran"=> ":idsasaran" ]) }}';
        url = url.replace(':idsasaran', id);
        window.location.href =  url;
    }



    function printAwal(id){
        var url = '{{ route("backend.rencanaaksi.printrencanaaksipelaporan",["id"=>":id"]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Print Rencana Aksi Pelaporan');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }


    function printBaru(id){
        var url = '{{ route("backend.rencanaaksi.printrencanaaksi",["id"=>":id"]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Print Rencana Aksi Tabel');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }

</script>
@endsection


