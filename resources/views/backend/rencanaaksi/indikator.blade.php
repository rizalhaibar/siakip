@extends('backend.layouts.app')

@section('title', 'IKU')

@section('content')
                
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                List Indikator Sasaran Rencana Aksi
                            </h3>
                        </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-portlet kt-portlet--tabs">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-toolbar">
                                <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-danger nav-tabs-line-2x nav-tabs-line-right" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#kt_portlet_base_demo_2_3_tab_content" role="tab">
                                            <i class="fa fa-calendar-check-o" aria-hidden="true"></i>Indikator Sasaran
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_2_2_tab_content" role="tab">
                                            <i class="fa fa-bar-chart" aria-hidden="true"></i>Program
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_2_3_tab_content" role="tab">
                                            <i class="fa fa-tags" aria-hidden="true"></i>Kegiatan
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="kt_portlet_base_demo_2_3_tab_content" role="tabpanel">
                                    <table  id="example" class="table ">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">No</th>
                                                <th rowspan="2">Indikator Sasaran</th>
                                                <th rowspan="2">Satuan</th>
                                                <th rowspan="2">Target</th>
                                                <th colspan="4"><center>Target <span id="tahun"></span></center></th>
                                                <th rowspan="2">Action</th>
                                            </tr>
                                            <tr>
                                                
                                                <th>Triwulan I</th>
                                                <th>Triwulan II</th>
                                                <th>Triwulan III</th>
                                                <th>Triwulan IV</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="tab-pane" id="kt_portlet_base_demo_2_2_tab_content" role="tabpanel">
                                    <table  id="example2" class="table " style="width:100%">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">No</th>
                                                <th rowspan="2">Program</th>
                                                <th rowspan="2">Indikator Program</th>
                                                <th rowspan="2">Satuan</th>
                                                <th rowspan="2">Target</th>
                                                <th colspan="4"><center>Target <span id="tahun"></span></center></th>
                                                <th rowspan="2">Action</th>
                                            </tr>
                                            <tr>
                                                
                                                <th>Triwulan I</th>
                                                <th>Triwulan II</th>
                                                <th>Triwulan III</th>
                                                <th>Triwulan IV</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="tab-pane" id="kt_portlet_base_demo_2_3_tab_content" role="tabpanel">
                                    <table  id="example3" class="table " style="width:100%">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">No</th>
                                                <th rowspan="2">Kegiatan</th>
                                                <th rowspan="2">Indikator Kegiatan</th>
                                                <th rowspan="2">Satuan</th>
                                                <th rowspan="2">Target</th>
                                                <th colspan="4"><center>Target <span id="tahun"></span></center></th>
                                                <th rowspan="2">Action</th>
                                            </tr>
                                            <tr>
                                                
                                                <th>Triwulan I</th>
                                                <th>Triwulan II</th>
                                                <th>Triwulan III</th>
                                                <th>Triwulan IV</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>              
            </div>              
        </div>              
    </div>              
</div>         

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                        <div class="row">
                            <div class="col-md-12">
                                    <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
	
@endsection

@section('customjs')
<script>
    $( document ).ready(function() {
		
    });
      
    
    function tambahFormula(id){
        var url = '{{ route("backend.rencanaaksi.tambahtarget",["id"=>":id"]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .panel-body').load(url);
        $('#panel-modal  .panel-title').html('<i class="fa fa-user"></i> Tambah/Edit Target');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
</script>
@endsection


