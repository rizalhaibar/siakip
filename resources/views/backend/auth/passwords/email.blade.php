@extends('backend.layouts.app-auth')

@section('content')

<div class="kt-grid kt-grid--ver kt-grid--root kt-page">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v1" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">

            <!--begin::Aside-->
            <div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside" style="background-image: url({{ asset('images/bg.png') }});">
                <div class="kt-grid__item">
                    <a href="#" class="kt-login__logo">

                    </a>
                </div>
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
                    <div class="kt-grid__item kt-grid__item--middle">
                        <h3 class="kt-login__title">Welcome to
                        <img src="{{ asset('images/logosiakip.png') }}" style="width: 250px; height: auto; margin-top: -46px"></h3>
                        <h4 class="kt-login__subtitle">Web apps.</h4>
                    </div>
                </div>
                <div class="kt-grid__item">
                    <div class="kt-login__info">
                        <div class="kt-login__copyright" style="margin-top: 30px">
                            &copy {{ date('Y').' '.env('APP_NAME') }}
                        </div>
                        <div class="kt-login__menu">
							<img src="{{ asset('images/logo_cimahi2.png') }}" style="width: 80px; height: auto; margin-top: 20px">
                        </div>
                    </div>
                </div>
            </div>

            <!--begin::Aside-->

            <!--begin::Content-->
            <div class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">

                <!--begin::Head-->
                <div class="kt-login__head">
                </div>

                <!--end::Head-->

                <!--begin::Body-->
                <div class="kt-login__body">

                    <!--begin::Signin-->
                    <div class="kt-login__form">
                        <div class="kt-login__title">
                            <h2 class="font-bold">Forgot password</h2>

                            <p>
                                Enter your email address and your password will be reset and emailed to you.
                            </p>
                        </div>

						@if(Session::has('status'))
							<div class="alert alert-success" role="alert">
								{{ Session::get('status') }}
							</div>
							@php
								Session::forget('status');
							@endphp
						@endif
                        <!--begin::Form-->
                        <form class="kt-form" method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
                           @csrf
                            <div class="form-group">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="{{ __('E-Mail Address') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback text-xs text-danger" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <button type="submit" class="btn btn-success block full-width m-b">
                                {{ __('Send Password Reset Link') }}
                            </button>

                        </form>

                        <!--end::Form-->
                    </div>

                    <!--end::Signin-->
                </div>

                <!--end::Body-->
            </div>

            <!--end::Content-->
        </div>
    </div>
</div>

@endsection
