<div class="form-horizontal" >
    <div class="form-group">
        <label class="col-md-12 control-label">Nama Perangkat Daerah*</label>
        <div class="col-md-12">
            <input class="form-control" value="{{$detail[0]->td_dinas}}" id="dinas" name="dinas" placeholder="dinas..." required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-12 control-label">Keterangan*</label>
        <div class="col-md-12">
            <textarea class="form-control" rows="5" id="ket" name="ket" placeholder="ket..." required>{{$detail[0]->td_keterangan}}</textarea>
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-md-12 control-label">Index</label>
        <div class="col-md-12">
            <input class="form-control" id="index" misi="index" placeholder="index..." value="{{$detail[0]->index}}" required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-10 col-md-8">
            <button type="button" onClick="targetEdit()" class="btn btn-primary waves-effect waves-light btn-rounded">
                Simpan
            </button>
        </div>
    </div>
    
</div>

<script>

function targetEdit(){
       
       var dinas   = $('#dinas').val();
       var ket   = $('#ket').val();
       var index   = $('#index').val();
      
              if(dinas){
                  
                      var datapost={
                          "iddinas"  :   "{{$id}}",
                          "dinas"  :   dinas,
                          "index"  :   index,
                          "ket"  :   ket
                        };
                        $.ajax({
                          type: "POST",
                          url: '{{ route("backend.dinas.dinasedit") }}',
                          data : JSON.stringify(datapost),
                          dataType: 'json',
                          contentType: 'application/json; charset=utf-8',
                          success: function(response) {
                            if (response.success == true) {
                                $("#panel-modal .close").click();
                                swal({
                                      title: 'Success!',
                                      text: response.message,
                                      type: 'success',
                                      showCancelButton: false,
                                      confirmButtonText: 'Ok'
                                  }).then(function () {
                                        window.location.href = '{{ route("backend.dinas.index") }}';
                                  })



                            } else{
                                $("#panel-modal .close").click();
                              swal("Failed!", response.message, "error");
                            }
                          }
                        });
                 
              
              
              }else{
                  $("#panel-modal .close").click();
                  swal("Failed!", "Eselon Belum Terisi!", "error");
              }
        
  }
</script>