
<div class="form-horizontal" >
    <div class="form-group">
        <label class="col-md-12 control-label">Nama Perangkat Daerah*</label>
        <div class="col-md-12">
            <input class="form-control" id="dinas" misi="dinas" placeholder="dinas..." required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-12 control-label">Keterangan*</label>
        <div class="col-md-12">
            <textarea class="form-control" rows="5" id="ket" misi="ket" placeholder="keterangan..." required></textarea>
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-md-12 control-label">Index</label>
        <div class="col-md-12">
            <input class="form-control" id="index" misi="index" placeholder="index..." required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-12 col-md-8">
            <button type="button" onClick="tambahDinas()" class="btn btn-primary waves-effect waves-light btn-rounded">
                Simpan
            </button>
        </div>
    </div>
</div>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function tambahDinas(){
        var dinas   = $('#dinas').val();
        var ket   = $('#ket').val();
        var index   = $('#index').val();
        
        if(dinas){
            if(ket){
                    var datapost={
                        "dinas"  :   dinas,
                        "ket"  :   ket,
                        "index"  :   index
                    };
                    $.ajax({
                        type: "POST",
                        url: '{{ route("backend.dinas.dinastambah") }}',
                        data : JSON.stringify(datapost),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        success: function(response) {
                            if (response.success == true) {
                                $("#panel-modal .close").click();
                                swal({
                                    title: 'Success!',
                                    text: response.message,
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonText: 'Ok'
                                }).then(function () {
                                    window.location.href = '{{ route("backend.dinas.index") }}';
                                })



                            } else{
                            swal("Failed!", response.message, "error");
                            }
                        }
                    });
            }else{
                swal("Failed!", "Keterangan Belum Terisi!", "error");
            }  
        
        
        }else{
            swal("Failed!", "Dinas Belum Terisi!", "error");
        }
          
    }
</script>