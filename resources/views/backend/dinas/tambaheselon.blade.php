<div class="form-horizontal" >
    <div class="form-group">
        <label class="col-md-2 control-label">Jabatan*</label>
        <div class="col-md-12">
            <select class="form-control" id="eselons" name="eselons">
                <option disabled selected>Pilih Jabatan...</option>
                <option value='2A_Jabatan Pimpinan Tinggi' @if(isset($eselon[0]))@if($eselon[0]->te_eselon == '2A') disabled @endif @endif>Jabatan Pimpinan Tinggi</option>
                <option value='2B_Administrator' @if(isset($eselon[1]))@if($eselon[1]->te_eselon == '2B') disabled @endif @endif >Administrator</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2 control-label">Nama Jabatan*</label>
        <div class="col-md-12">
        <input class="form-control" id="bagian" name="bagian" placeholder="nama jabatan..." required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-12 col-md-2" >
            <button type="button" onClick="eselonSave()" style="width:100%" class="btn btn-primary waves-effect waves-light btn-rounded">
                Simpan
            </button>
        </div>
    </div>
</div>

<script>
   function eselonSave(){
       
       var eselon   = $('#eselons').val();
       var bagian   = $('#bagian').val();
      
              if(eselon){
                  if(bagian){
                      var datapost={
                          "iddinas"  :   "{{$id}}",
                          "eselon"  :   eselon,
                          "bagian"  :   bagian
                        };
                        $.ajax({
                          type: "POST",
                          url: '{{ route("backend.dinas.saveeselon") }}',
                          data : JSON.stringify(datapost),
                          dataType: 'json',
                          contentType: 'application/json; charset=utf-8',
                          success: function(response) {
                            if (response.success == true) {
                                $("#panel-modal .close").click();
                                swal({
                                      title: 'Success!',
                                      text: response.message,
                                      type: 'success',
                                      showCancelButton: false,
                                      confirmButtonText: 'Ok'
                                  }).then(function () {
                                     window.location.href = "{{route('backend.dinas.eselon', $id)}}";
                                  })



                            } else{
                                $("#panel-modal .close").click();
                              swal("Failed!", response.message, "error");
                            }
                          }
                        });
                  }else{
                      $("#panel-modal .close").click();
                      swal("Failed!", "Keterangan Belum Terisi!", "error");
                  }  
              
              
              }else{
                  $("#panel-modal .close").click();
                  swal("Failed!", "Dinas Belum Terisi!", "error");
              }
        
  }
</script>