@extends('backend.layouts.landing')

@section('title', 'About')

@section('content')
	<section id="about" style="margin-top: 180px">      
        <div class="container">
            <div class="row">
                <section class="page col-sm-9">
                    <h2 class="page-title">Tentang Kami</h2>
                    <div class="entry">

                        <p>Kewenangan Kota Cimahi sebagai Daerah Otonom mencakup seluruh kewenangan bidang pemerintahan, termasuk kewenangan wajib yaitu pekerjaan umum, kesehatan, pendidikan dan kebudayaan, perhubungan, industri dan perdagangan, penanaman modal, lingkungan hidup, pertahanan, koperasi dan tenaga kerja kecuali bidang politik luar negeri, pertahanan keamanan, peradilan, moneter fisikal, agama serta kewenangan bidang lain sesuai dengan peraturan Perundang-undangan Nomor I tahun 2003 tentang Kewenangan Kota Cimahi sebagai Daerah Otonom.</p>                        
                    </div>
                </section>
            </div>
        </div>
    </section>
@endsection

@section('customjs')
@endsection