
<div class="form-group">
    <label class="col-md-2 control-label">Penanggung Jawab*</label>
    <div class="col-md-12">
        <select class="form-control" id="pic" name="pic">
            <option selected disabled>Select</option>
            @foreach($listpic as $pic)
                <option value="{{$pic->te_id}}" {{$detail->pic == $pic->te_id ? 'selected' : ''}}>{{$pic->te_isi}} ({{$pic->te_jabatan}})</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-10 col-md-8">
        <button type="button" onClick="targetEdit()" class="btn btn-primary waves-effect waves-light btn-rounded">
            Simpan
        </button>
    </div>
</div>


<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });



    function targetEdit(){

        var pic   = $('#pic').val();

                if(pic){
                        var datapost={
                            "idindikatorsasaran"  :   "{{$id}}",
                            "pic"  :   pic
                            };
                            $.ajax({
                            type: "POST",
                            url: "{{route('backend.pengukurankinerja.saveindikatorsasaranpic')}}",
                            data : JSON.stringify(datapost),
                            dataType: 'json',
                            contentType: 'application/json; charset=utf-8',
                            success: function(response) {
                                if (response.success == true) {
                                    swal({
                                        title: 'Success!',
                                        text: response.message,
                                        type: 'success',
                                        showCancelButton: false,
                                        confirmButtonText: 'Ok'
                                    }).then(function () {
                                        location.reload();
                                        // var url = '{{ route("backend.pengukurankinerja.home",["id"=>"$iddinas", "idsasaran"=> "$idsasaran" ]) }}';
                                        // window.location.href =  url;
                                    })
                                } else{
                                swal("Failed!", response.message, "error");
                                }
                            }
                            });


                }else{
                    swal("Failed!", "PIC Belum Dipilih!", "error");
                }

    }
</script>
