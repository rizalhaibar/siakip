
<div class="row">
    <div class="col-md-12">
        <div class="form-horizontal" >
            <div class="form-group">
                <label class="col-md-2 control-label">Indikator Kegiatan*</label>
                <div class="col-md-12">
                    <input type="text" value="{{@$detail->tik_indikator_kegiatan}}" class="form-control" id="kegiatan_program" name="kegiatan_program" placeholder="Indikator Kegiatan..." required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Deskripsi</label>
                <div class="col-md-12">
                    <textarea class="form-control" rows="5" id="desc" name="desc"  placeholder="Deskripsi..." >{{@$detail->tik_keterangan}}</textarea>
                </div>
            </div>


            <div class="form-group">
                <label class="col-md-2 control-label">Satuan*</label>
                <div class="col-md-12">
                    <input type="text" value="{{@$detail->tik_satuan}}"  class="form-control" id="satuan" name="satuan" placeholder="Satuan..." required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Target*</label>
                <div class="col-md-12">
                    <input type="text" value="{{@$detail->tik_target}}"  class="form-control" id="target" name="target" placeholder="Target..." required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Anggaran*</label>
                <div class="col-md-12">
                    <input type="number" value="{{@$anggaran->ta_anggaran}}"  class="form-control" id="anggaran" name="anggaran" placeholder="Anggaran..." required>
                </div>
            </div>


        <div class="form-group">
            <div class="col-md-offset-10 col-md-8">
                <button type="button" onClick="saveProgram()" class="btn btn-primary waves-effect waves-light btn-rounded">
                    Simpan
                </button>
            </div>
        </div>
    </div>
</div>

<script>
function saveProgram(){

       var kegiatan      = $('#kegiatan_program').val();
       var desc         = $('#desc').val();
       var satuan       = $('#satuan').val();
       var target       = $('#target').val();
       var anggaran       = $('#anggaran').val();
    //   console.log(program);
    //   console.log(desc);
    //   return false;
        if(kegiatan){
            if(satuan){
                if(target){

                    var datapost={
                        "idkegiatan" : "{{$id}}",
                        "kegiatan"   : kegiatan,
                        "iddinas"   : "{{$iddinas}}",
                        "desc"   : desc,
                        "satuan"   : satuan,
                        "target"   : target,
                        "anggaran"   : anggaran,
                        "tahun"  : tahun
                        };
                        $.ajax({
                        type: "POST",
                        url: "{{ route('backend.pengukurankinerja.indikatorkegiatanedit') }}",
                        data : JSON.stringify(datapost),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        success: function(response) {
                            if (response.success == true) {
                                $("#panel-modal .close").click();
                                swal({
                                    title: 'Success!',
                                    text: response.message,
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonText: 'Ok'
                                }).then(function () {
                                    location.reload();
                                    // window.location.href = "{{ route('backend.pengukurankinerja.home', ['id'=>$iddinas, 'ids'=>$idsasaran]) }}";
                                })



                            } else{
                            swal("Failed!", response.message, "error");
                            }
                        }
                        });

                }else{
                    swal("Failed!", "Target Belum Terisi!", "error");
                }
            }else{
                swal("Failed!", "Satuan Belum Terisi!", "error");
            }
        }else{
              swal("Failed!", "Program Belum Terisi!", "error");
        }

  }
</script>
