@extends('backend.layouts.app')

@section('title', 'Evaluasi Mandiri')

@section('content')
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                List Dinas
                            </h3>
                        </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="row">
                        <div class="col-lg-12">

                        <div id="container"></div>

                        <br/>
                        <br/>
                            <table id="listkota" class="table ">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Instansi</th>
                                            <th>Nilai</th>
                                            <th>Kategori</th>
                                            <th>Action</th>
                                        </tr>
                                        <tbody>
                                            @php($sl=1)
                                                @foreach($listdinas as $dinas)
                                                <tr>
                                                    <td>{{$sl}}</td>
                                                    <td>{{$dinas->td_dinas}}</td>
                                                    <td>{{$dinas->nilai}}</td>
                                                    <td>@if($dinas->nilai)
                                                            @if($dinas->nilai <= 30)
                                                                D
                                                            @elseif($dinas->nilai <= 50)
                                                                C
                                                            @elseif($dinas->nilai <= 60)
                                                                CC
                                                            @elseif($dinas->nilai <= 70)
                                                                B
                                                            @elseif($dinas->nilai <= 80)
                                                                BB
                                                            @elseif($dinas->nilai <= 90)
                                                                A
                                                            @elseif($dinas->nilai <= 100)
                                                                AA
                                                            @endif
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <a href="javascript:void(0)" onclick="tambahTarget({{$dinas->td_id}})" type="button" class="btn btn-success btn-icon"><i class="fa fa-search"></i></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                
                                                @php($sl++)
                                            @endforeach
                                        
                                        </tbody>
                                    </thead>
                            </table>
                        </div>                
                    </div>                
                </div>                
            </div>                
        </div>                
    </div>                
</div>




<div id="panel-modal" class="modal fade" id="kt_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
@endsection

@section('onpage-js')
@include('backend.layouts.message')
<script>
    

    $(document).ready(function(){
        demohigh();


        
    });



    var demohigh = function() {
        var data = [
            @foreach($listdinas as $k=>$dinas)
            {  
                name:"{{$dinas->td_dinas}}",
                y: {{$dinas->nilai ? $dinas->nilai : 0}},
                kategori: @if($dinas->nilai <= 30)
                            "D"
                        @elseif($dinas->nilai <= 50)
                            "C"
                        @elseif($dinas->nilai <= 60)
                            "CC"
                        @elseif($dinas->nilai <= 70)
                            "B"
                        @elseif($dinas->nilai <= 80)
                            "BB"
                        @elseif($dinas->nilai <= 90)
                            "A"
                        @elseif($dinas->nilai <= 100)
                            "AA"
                        @endif,
                drilldown: "{{$dinas->td_dinas}}"
            },
            @endforeach
        ];

        // Create the chart
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Evaluasi Kinerja {{$_COOKIE["tahun"]}}'
            },
            subtitle: {
                text: 'Data penilaian pertahun'
            },
            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Nilai Evaluasi '
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span>{point.name}</span>: <b>{point.y}</b>' 
                                +'<br/> '
                                +'<span>Kategori</span>: <b>{point.kategori}</b>'
            },

            series: [
                {
                    name: "Detail : ",
                    colorByPoint: true,
                    data: data
                }
            ]
        });

    };
   
    function tambahTarget(id){
        var url = "{{ route('backend.evaluasikinerja.detail', ['id'=>':id', 'tahun'=>$_COOKIE['tahun']]) }}";
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Nilai Evaluasi Kinerja');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
</script>
@endsection