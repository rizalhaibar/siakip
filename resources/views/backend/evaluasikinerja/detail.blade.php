<div class="form-horizontal" >

    <div class="form-group">
        <label class="col-md-2 control-label">Nilai Evaluasi*</label>
        <div class="col-md-12">
                <input class="form-control" type="number" min="0" max="100" value="{{ @$evaluasi[0]->nilai}}" id="target" name="target" placeholder="nilai..." required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-10 col-md-8">
            <button type="button" onClick="targetEdit()" class="btn btn-primary waves-effect waves-light btn-rounded">
                Simpan
            </button>
        </div>
    </div>
    
</div>

<script>
    
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

@if(isset($evaluasi[0]->nilai))
    function targetEdit(){
            var target   = $('#target').val();
        
                if(target){
                    var datapost={
                        "idsasaran"  :   "{{$id}}",
                        "target"  :   target,
                        "tahun"  :   tahun
                        };
                        $.ajax({
                        type: "POST",
                        url: "{{route('backend.evaluasikinerja.nilaievaluasiedit')}}",
                        data : JSON.stringify(datapost),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        success: function(response) {
                            if (response.success == true) {
                                $("#panel-modal .close").click();
                                swal({
                                    title: 'Success!',
                                    text: response.message,
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonText: 'Ok'
                                }).then(function () {
                                    window.location.href = "{{route('backend.evaluasikinerja.index')}}";
                                })



                            } else{
                                $("#panel-modal .close").click();
                            swal("Failed!", response.message, "error");
                            }
                        }
                        });
                }else{
                    $("#panel-modal .close").click();
                    swal("Failed!", "Target Belum Terisi!", "error");
                }

    }

    
@else
    function targetEdit(){
         var target   = $('#target').val();
        
                if(target){
                    var datapost={
                        "idsasaran"  :   "{{$id}}",
                        "target"  :   target,
                        "tahun"  :   tahun
                      };
                      $.ajax({
                        type: "POST",
                        url: "{{route('backend.evaluasikinerja.nilaievaluasi')}}",
                        data : JSON.stringify(datapost),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        success: function(response) {
                          if (response.success == true) {

                              swal({
                                    title: 'Success!',
                                    text: response.message,
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonText: 'Ok'
                                }).then(function () {
                                    window.location.href = "{{route('backend.evaluasikinerja.index')}}";
                                })



                          } else{
                            swal("Failed!", response.message, "error");
                          }
                        }
                      });
                }else{
                    swal("Failed!", "Target Belum Terisi!", "error");
                }
          
    }
@endif

</script>