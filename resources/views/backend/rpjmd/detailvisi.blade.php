<div class="row">
    <div class="col-md-12">
        <h3>Edit Visi</h3>
        <div class="form-horizontal" >
            <div class="form-group">
                <label class="col-md-2 control-label">Visi*</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" id="visiedit" misi="visiedit" value="{{$visidetail[0]->tv_visi}}" placeholder="Visi..." required>
                    <input type="hidden" class="form-control" id="descedit" name="descedit" placeholder="Deskripsi..." >
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-10 col-md-8">
                    <button type="button" onClick="editVisi()" class="btn btn-primary waves-effect waves-light btn-rounded">
                        Simpan
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$.ajaxSetup({
    headers: {

        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }
});

    function editVisi(){

         var visi   = $('#visiedit').val();
         var desc   = $('#descedit').val();


            if(visi){

					$("#panel-modal .close").click();
					swal({
						title: 'Apa anda yakin?',
						  text: "Apakah anda yakin akan mengubah Visi "+visi,
						  type: 'success',
						  showCancelButton: true,
						  confirmButtonText: 'Ok'
					}).then(function () {

						var datapost={
							"idvisi"  :   "{{$id}}",
							"visi"  :   visi,
							"desc"  :   desc
						  };
						  $.ajax({
							type: "POST",
                            url: "{{ route('backend.rpjmd.editvisi') }}",
							data : JSON.stringify(datapost),
							dataType: 'json',
							contentType: 'application/json; charset=utf-8',
							success: function(response) {
							  if (response.success == true) {

								  swal({
										title: 'Success!',
										text: response.message,
										type: 'success',
										showCancelButton: false,
										confirmButtonText: 'Ok'
									}).then(function () {
                                        location.reload();
									})



							  } else{
								swal("Failed!", response.message, "error");
							  }
							}
						  });
					})




            }else{
                swal("Failed!", "Visi Belum Terisi!", "error");
            }

    }

    // function saveMisi(){

    //      var misi   = $('#misis').val();
    //      var desc   = $('#descmisi').val();

    //         if(misi){

    //                 var datapost={
    //                     "idvisi" : "{{$id}}",
    //                     "misi"   : misi,
    //                     "desc"   : desc,
	// 					"tahun"  : tahun
    //                   };
    //                   $.ajax({
    //                     type: "POST",
    //                     url: "{{ route('backend.rpjmd.savemisi') }}",
    //                     data : JSON.stringify(datapost),
    //                     dataType: 'json',
    //                     contentType: 'application/json; charset=utf-8',
    //                     success: function(response) {
    //                       if (response.success == true) {
    //                           $("#panel-modal .close").click();
    //                           swal({
    //                                 title: 'Success!',
    //                                 text: response.message,
    //                                 type: 'success',
    //                                 showCancelButton: false,
    //                                 confirmButtonText: 'Ok'
    //                             }).then(function () {
    //                               window.location.href = "{{ route('backend.rpjmd.masuk', ['id'=>$iddinas, 'tahun'=>$_COOKIE['tahun']]) }}";;


    //                             })



    //                       } else{
    //                         swal("Failed!", response.message, "error");
    //                       }
    //                     }
    //                   });

    //         }else{
    //             swal("Failed!", "Misi Belum Terisi!", "error");
    //         }

    // }
</script>
