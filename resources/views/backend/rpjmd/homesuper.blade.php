@extends('backend.layouts.app')

@section('title', 'RPJMD')

@section('content')
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
													<span class="kt-portlet__head-icon kt-hide">
														<i class="la la-gear"></i>
													</span>
                            <h3 class="kt-portlet__head-title">
                                Input Visi
                            </h3>
                        </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="form-horizontal" >
                        <div class="form-group">
                            <label class="col-md-2 control-label">Visi*</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="visi" name="visi" placeholder="Visi..." required>
                                <input type="hidden" class="form-control" id="desc" name="desc" placeholder="Deskripsi..." >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2" >
                                <button type="button" onClick="saveVisi()" class="btn btn-primary waves-effect waves-light btn-rounded" style="width:100%">
                                    Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <div class="row">
                        <div class="col-md-11">
                            <h3 class="kt-portlet__head-title">
                                List {{$id == 1 ? 'RPJMD' : 'RENSTRA' }}
                            </h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <ol style="margin-top: 40px;">
                                @foreach($visi as $newvisi)
                                    <h4>Visi</h4>
                                    <li>
                                        {{$newvisi->tv_visi}}

                                        @php($totalmisi = 0)
                                        @if(isset($newvisi->misi))
                                            @if (count($newvisi->misi) > 0)
                                                @php($totalmisi = count($newvisi->misi))
                                            @endif
                                        @endif
                                        &nbsp; <a href="javascript:void(0)"  onClick="editvisi({{$newvisi->tv_id}})"><i class="fa fa-edit"></i></a>
                                        &nbsp;<a href="javascript:void(0)" onClick="hapusvisi({{$newvisi->tv_id}}, {{$totalmisi}})"> <i class="fa fa-trash"></i></a>
                                        <ol style="margin-top:10px">
                                            <h4>Misi <a href="javascript:void(0)" class="btn btn-danger btn-icon btn-sm" onClick="tambahmisi({{$newvisi->tv_id}})"><i class="fa fa-plus"></i></a></h4>
                                            @if(isset($newvisi->misi))
                                                @foreach($newvisi->misi as $misi)

                                                    @php($totaltujuan = 0)
                                                    @if(isset($misi->tujuan ))
                                                        @if (count($misi->tujuan) > 0)
                                                            @php($totaltujuan = count($misi->tujuan))
                                                        @endif
                                                    @endif

                                                    <li style="margin-top:10px">
                                                        {{$misi->tm_misi}}
                                                        &nbsp;<a href="javascript:void(0)"  onClick="editmisi({{$misi->tm_id}})"><i class="fa fa-edit"></i></a>
                                                        <a href="javascript:void(0)"  onClick="hapusmisi({{$misi->tm_id}}, {{$totaltujuan}})"><i class="fa fa-trash" id="danger-alert"></i></a>
                                                    </li>
                                                    <ol>
                                                    <h4 style="margin-top: 10px">Tujuan <a href="javascript:void(0)" class="btn btn-danger btn-icon btn-sm" onClick="tambahtujuan({{$misi->tm_id}})"><i class="fa fa-plus"></i></a></h4>

                                                                @if(isset($misi->tujuan ))

                                                                    @foreach($misi->tujuan as $tujuan)
                                                                    @php($s = 1)
                                                                        @if($tujuan->idmisi == $misi->tm_id)

                                                                        @php($totalindikatortujuan = 0)
                                                                        @if(isset($tujuan->indikator))
                                                                            @if (count($tujuan->indikator) > 0)
                                                                                @php($totalindikatortujuan = count($tujuan->indikator))
                                                                            @endif
                                                                        @endif

                                                                        @php($totalsasaran = 0)
                                                                        @if(isset($tujuan->sasaran))
                                                                            @if (count($tujuan->sasaran) > 0)
                                                                                @php($totalsasaran = count($tujuan->sasaran))
                                                                            @endif
                                                                        @endif
                                                                            <li style="display:block; ">
                                                                                {{$s}}.  {{$tujuan->tt_tujuan}}
                                                                                <span>
                                                                                    <a href="javascript:void(0)" onClick="edittujuan({{$tujuan->tt_id}},{{$id}})"><i class="fa fa-edit" ></i></a>
                                                                                    <a href="javascript:void(0)"  onClick="hapustujuan({{$tujuan->tt_id}}, {{$totalindikatortujuan}}, {{$totalsasaran}})"><i class="fa fa-trash" id="danger-alert"></i></a>
                                                                                </span>
                                                                                <ul style="margin-left: 20px">
                                                                                    <h4 style="margin-left:-30px; color: #336600; margin-top:10px">Indikator Tujuan <a href="javascript:void(0)" class="btn btn-danger btn-icon btn-sm" onClick="tambahindikatortujuan({{$tujuan->tt_id}})"><i class="fa fa-plus"></i></a></h4>
                                                                                </ul>
                                                                                <ol style="margin-left: 20px">
                                                                                    <table class="table table-bordered">
                                                                                        <thead>
                                                                                            <tr>
                                                                                                <th><center>NO</center></th>
                                                                                                <th><center>Indikator Tujuan</center></th>
                                                                                                <th><center>Satuan</center></th>
                                                                                                <th><center>Target</center></th>
                                                                                                <th></th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                            @php($sasssas=1)
                                                                                            @if(isset($tujuan->indikator))
                                                                                                @foreach($tujuan->indikator as $indi)
                                                                                                    @if($tujuan->tt_id == $indi->idtujuan )
                                                                                                    <tr>
                                                                                                        <td><?php echo $sasssas;?></td>
                                                                                                        <td><?php echo $indi->tit_indikator_tujuan;?></td>
                                                                                                        <td><?php echo $indi->tit_satuan;?></td>
                                                                                                        <td><?php echo $indi->tit_target?></td>
                                                                                                        <td>
                                                                                                            <span>
                                                                                                                <a href="javascript:void(0)" onClick="detailtujuanindikator({{$indi->tit_id}},{{$id}})"><i class="fa fa-edit" style="color:blue"></i></a>
                                                                                                                <a href="javascript:void(0)" onClick="hapustujuanindikator({{$indi->tit_id}},{{$id}})"><i class="fa fa-trash" style="color:red" id="danger-alert"></i></a>
                                                                                                            </span>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    @php($sasssas++)
                                                                                                    @endif
                                                                                                @endforeach
                                                                                            @endif
                                                                                        </tbody>
                                                                                    </table>
                                                                                </ol>
                                                                                <ul style="margin-left: 20px">
                                                                                    <h4 style="margin-left:-30px; color: #336600; margin-top:10px">Sasaran Strategis <a href="javascript:void(0)" class="btn btn-danger btn-icon btn-sm" onClick="tambahsasaran({{$tujuan->tt_id}})"><i class="fa fa-plus"></i></a></h4>
                                                                                </ul>
                                                                                <ol  style="margin-left: 20px">
                                                                                    @if(isset($tujuan->sasaran))

                                                                                            @php($sa = 1)
                                                                                            @foreach($tujuan->sasaran as $newsasaran)

                                                                                            @if($tujuan->tt_id == $newsasaran->idtujuan)

                                                                                            @php($totalindikatorsasaran = 0)
                                                                                                @if(isset($newsasaran->indikatorsasaran))
                                                                                                    @if (count($newsasaran->indikatorsasaran) > 0)
                                                                                                        @php($totalindikatorsasaran = count($newsasaran->indikatorsasaran))
                                                                                                    @endif
                                                                                                @endif

                                                                                                        <li style="display:block">
                                                                                                            {{$sa}}. {{$newsasaran->tst_sasaran_tujuan}}
                                                                                                            <span>
                                                                                                                <a href="javascript:void(0)"  onClick="editsasaran({{$newsasaran->tst_id}},{{$id}})"><i class="fa fa-edit" style="color:blue"></i></a>

                                                                                                                <a href="javascript:void(0)"  onClick="hapussasaran({{$newsasaran->tst_id}}, {{$totalindikatorsasaran}})"><i class="fa fa-trash" id="danger-alert"></i></a>

                                                                                                            </span>

                                                                                                                <ul style="margin-left: 20px">
                                                                                                                    <h4 style="margin-left:-30px; color: #336600; margin-top:10px">Indikator Sasaran <a href="javascript:void(0)" class="btn btn-danger btn-icon btn-sm" onClick="tambahindikatorsasaran({{$newsasaran->tst_id}})"><i class="fa fa-plus"></i></a></h4>
                                                                                                                </ul>
                                                                                                                    <ol>
                                                                                                                    <li style="display:block">
                                                                                                                        <table class="table table-bordered">
                                                                                                                            <thead>
                                                                                                                                <tr>
                                                                                                                                    <th><center>NO</center></th>
                                                                                                                                    <th><center>Indikator Sasaran Strategis</center></th>
                                                                                                                                    <th><center>Formula</center></th>
                                                                                                                                    <th><center>Sumber Data</center></th>
                                                                                                                                    <th><center>Satuan</center></th>
                                                                                                                                    <th><center>IKU</center></th>
                                                                                                                                    <th>Action</th>
                                                                                                                                </tr>
                                                                                                                            </thead>
                                                                                                                            <tbody>
                                                                                                                            @if(isset($newsasaran->indikatorsasaran))
                                                                                                                            @php($sas = 1)
                                                                                                                            @foreach($newsasaran->indikatorsasaran as $indisasaran)

                                                                                                                                <tr>
                                                                                                                                    <td>{{$sas}}</td>
                                                                                                                                    <td>{{$indisasaran->tis_indikator_sasaran}}</td>
                                                                                                                                    <td>{{$indisasaran->formula}}</td>
                                                                                                                                    <td>{{$indisasaran->sumberdata}}</td>
                                                                                                                                    <td>{{$indisasaran->tis_satuan}}</td>
                                                                                                                                    <td style="text-align:center">
                                                                                                                                        @if($indisasaran->tis_iku == "T")
                                                                                                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm btn-icon" onClick="rubahIKU({{$indisasaran->tis_id}},&#x27;F&#x27;)" placeholder="Aktif?"><i class="fa fa-check"></i></a>
                                                                                                                                        @else
                                                                                                                                            <a href="javascript:void(0)" class="btn btn-danger  btn-sm btn-icon" onClick="rubahIKU({{$indisasaran->tis_id}},&#x27;T&#x27;)" placeholder="Tidak Aktif?"><i class="fa fa-times"></i></a>
                                                                                                                                        @endif
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <center>
                                                                                                                                            <span>
                                                                                                                                                <a href="javascript:void(0)" class="btn btn-primary btn-sm btn-icon" onClick="detailsasaranindikator({{$indisasaran->tis_id}},{{$id}})"><i class="fa fa-edit" ></i></a>

                                                                                                                                                <a href="javascript:void(0)" class="btn btn-primary btn-sm btn-icon" onClick="hapussasaranindikator({{$indisasaran->tis_id}})"><i class="fa fa-trash" ></i></a>
                                                                                                                                            </span>
                                                                                                                                        </center>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                @php($sas++)
                                                                                                                                @endforeach


                                                                                                                            @endif
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </li>
                                                                                                                </ol>


                                                                                                        </li>

                                                                                            @php($sa++)

                                                                                            @endif
                                                                                            @endforeach

                                                                                    @endif
                                                                                </ol>
                                                                            </li>
                                                                        @endif
                                                                        @php($s++)
                                                                    @endforeach

                                                                @endif
                                                    </ol>
                                                @endforeach
                                            @endif
                                        </ol>
                                    </li>
                                @endforeach

                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="panel-modal" class="modal fade" id="kt_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

@endsection

@section('onpage-js')
@include('backend.layouts.message')

    <script>

    $.ajaxSetup({
        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }
    });
        $(document).ready(function () {

     //Danger
        $('#danger-alert').click(function () {
            swal({
                title: "Apa anda yakin?",
                text: "Anda akan menghapus ini yang berdampak pada semua segment!",
                type: "error",
                showCancelButton: true,
                confirmButtonClass: 'btn-danger waves-effect waves-light',
                confirmButtonText: 'Hapus!'
            });
        });
    });

    function editvisi(id){
        var url = '{{ route("backend.rpjmd.visidetail",["id"=>":id", "ids"=> $id]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Edit Visi');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    function tambahmisi(id){
        var url = '{{ route("backend.rpjmd.tambahmisi",["id"=>":id", "ids"=> $id]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Tambah Misi');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    function tambahtujuan(id){
        var url = '{{ route("backend.rpjmd.tambahtujuan",["id"=>":id", "ids"=> $id]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Tambah Tujuan');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    function tambahindikatortujuan(id){
        var url = '{{ route("backend.rpjmd.tambahindikatortujuan",["id"=>":id", "ids"=> $id]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Tambah Indikator Tujuan');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    function tambahindikatorsasaran(id){
        var url = '{{ route("backend.rpjmd.tambahindikatorsasaran",["id"=>":id", "ids"=> $id]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Tambah Indikator Sasaran');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    function tambahsasaran(id){
        var url = '{{ route("backend.rpjmd.tambahsasaran",["id"=>":id", "ids"=> $id]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Tambah Sasaran');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    function editmisi(id){
        var url = '{{ route("backend.rpjmd.misidetail",["id"=>":id", "ids"=> $id]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Edit Misi');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }

    function hapusvisi(id, totalmisi){
        if(totalmisi > 0){
            swal("Failed!", 'Anda masih memiliki Misi!', "error");
        }else{
              swal({
				title: 'Apa anda yakin?',
				  text: "Anda akan menghapus visi!",
				  type: 'error',
				  showCancelButton: true,
				  confirmButtonText: 'Ok'
			}).then(function () {
				  var datapost={
					"id"  :   id
                  };
				  $.ajax({
					type: "POST",
					url: "{{ route('backend.rpjmd.deletevisi') }}",
					data : JSON.stringify(datapost),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(response) {
					  if (response.success == true) {

						  swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () {
								location.reload();
								//window.location.href =  "{{ route('backend.rpjmd.masuk', ['id'=>$id, 'tahun'=>$_COOKIE['tahun']]) }}";
							})



					  } else{
						swal("Failed!", response.message, "error");
					  }
					}
				  });
            })
        }
    }

    function hapusmisi(id, total){

        if(total > 0){
            swal("Failed!", 'Anda masih memiliki Tujuan!', "error");
        }else{
            swal({
				title: 'Apa anda yakin?',
				  text: "Anda akan menghapus misi!",
				  type: 'error',
				  showCancelButton: true,
				  confirmButtonText: 'Ok'
			    }).then(function () {
				var datapost={
					"id"  :   id
				  };
				  $.ajax({
					type: "POST",
					url: "{{ route('backend.rpjmd.deletemisi') }}",
					data : JSON.stringify(datapost),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(response) {
					  if (response.success == true) {

						  swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () {
								location.reload();
								//window.location.href =  "{{ route('backend.rpjmd.masuk', ['id'=>$id, 'tahun'=>$_COOKIE['tahun']]) }}";
							})



					  } else{
						swal("Failed!", response.message, "error");
					  }
					}
				  });
			})
        }

    }
    function hapustujuan(id, totalindikator, totalsasaran){

        if(totalindikator > 0){
            swal("Failed!", 'Anda masih memiliki Indikator Tujuan!', "error");
        }else{

            if(totalsasaran > 0){
                swal("Failed!", 'Anda masih memiliki Sasaran!', "error");
            }else{
                swal({
                    title: 'Apa anda yakin?',
                    text: "Anda akan menghapus tujuan!",
                    type: 'error',
                    showCancelButton: true,
                    confirmButtonText: 'Ok'
                }).then(function () {
                    var datapost={
                        "id"  :   id
                    };
                    $.ajax({
                        type: "POST",
                        url: "{{ route('backend.rpjmd.deletetujuan') }}",
                        data : JSON.stringify(datapost),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        success: function(response) {
                        if (response.success == true) {

                            swal({
                                    title: 'Success!',
                                    text: response.message,
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonText: 'Ok'
                                }).then(function () {
                                    location.reload();
								//window.location.href =  "{{ route('backend.rpjmd.masuk', ['id'=>$id, 'tahun'=>$_COOKIE['tahun']]) }}";
                                })



                        } else{
                            swal("Failed!", response.message, "error");
                        }
                        }
                    });
                })
            }
        }
    }

    function hapussasaran(id, total){


        var ktitle = 'Apa anda yakin?';
        var ktext = 'Anda akan menghapus sasaran!';
        var ktype = '';
        var kbutton = '';
            if(total > 0){
                 ktitle = 'Anda masih memiliki indikator sasaran';
                 ktext = 'Anda Yakin Akan Menghapus Sasaran Dan Indikator tersebut?';

                 kbutton = 'Hapus Semua!';
            }else{
                 ktype = 'error';
                 kbutton = 'Ok';
            }

              swal({
				  title: ktitle,
				  text: ktext,
				  type: ktype,
				  showCancelButton: true,
				  confirmButtonText: kbutton
			}).then(function () {
				var datapost={
					"id"  :   id
				  };
				  $.ajax({
					type: "POST",
					url: "{{ route('backend.rpjmd.deletesasaran') }}",
					data : JSON.stringify(datapost),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(response) {
					  if (response.success == true) {

						  swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () {
								location.reload();
								//window.location.href =  "{{ route('backend.rpjmd.masuk', ['id'=>$id, 'tahun'=>$_COOKIE['tahun']]) }}";
							})



					  } else{
						swal("Failed!", response.message, "error");
					  }
					}
				  });
			})
    }

    function hapussasaranindikator(id){
              swal({
				title: 'Apa anda yakin?',
				  text: "Anda akan menghapus indikator sasaran!",
				  type: 'error',
				  showCancelButton: true,
				  confirmButtonText: 'Ok'
			}).then(function () {
				var datapost={
					"id"  :   id
				  };
				  $.ajax({
					type: "POST",
					url: "{{ route('backend.rpjmd.deleteindikatorsasaran') }}",
					data : JSON.stringify(datapost),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(response) {
					  if (response.success == true) {

						  swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () {
								location.reload();
								//window.location.href =  "{{ route('backend.rpjmd.masuk', ['id'=>$id, 'tahun'=>$_COOKIE['tahun']]) }}";
							})



					  } else{
						swal("Failed!", response.message, "error");
					  }
					}
				  });
			})
    }


    function saveVisi(){
         var visi   = $('#visi').val();
          var desc   = $('#desc').val();
            if(visi){

                    var datapost={
                        "iddinas"  :   "{{$id}}",
                        "visi"  :   visi,
                        "desc"  :   desc,
						"tahun" : tahun
                      };
                      $.ajax({
                        type: "POST",
                        url: "{{ route('backend.rpjmd.savevisi') }}",
                        data : JSON.stringify(datapost),
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        success: function(response) {
                          if (response.success == true) {

                              swal({
                                    title: 'Success!',
                                    text: response.message,
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonText: 'Ok'
                                }).then(function () {
                                   location.reload();
								//window.location.href = "{{ route('backend.rpjmd.masuk', ['id'=>$id, 'tahun'=>$_COOKIE['tahun']]) }}";
                                })



                          } else{
                            swal("Failed!", response.message, "error");
                          }
                        }
                      });

            }else{
                swal("Failed!", "Visi Belum Terisi!", "error");
            }

    }




    function edittujuan(id, idmisi){
        var url = '{{ route("backend.rpjmd.tujuandetail",["id"=>":id", "ids"=> $id]) }}';
        url = url.replace(':id', id);
        url = url.replace(':ids', idmisi);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('<i class="fa fa-edit"></i> Edit Tujuan');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }

    function editsasaran(id, idmisi){
        var url = '{{ route("backend.rpjmd.sasarandetail",["id"=>":id", "ids"=> $id]) }}';;
        url = url.replace(':id', id);
        url = url.replace(':ids', idmisi);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('<i class="fa fa-useeditr"></i> Edit Sasaran');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }

    function detailsasaranindikator(id, idmisi){
        var url = '{{ route("backend.rpjmd.detailsasaranindikator",["id"=>":id", "ids"=> $id]) }}';
        url = url.replace(':id', id);
        url = url.replace(':ids', idmisi);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('<i class="fa fa-edit"></i> Edit Indikator Sasaran');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    function detailtujuanindikator(id, idmisi){
        var url = '{{ route("backend.rpjmd.detailtujuanindikator",["id"=>":id", "ids"=> $id]) }}';
        url = url.replace(':id', id);
        url = url.replace(':ids', idmisi);
	    $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('<i class="fa fa-edit"></i> Edit Indikator Tujuan');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }


    function tujuanSave(){

       var tujuan   = $('#tujuan').val();
        var desc   = $('#desc').val();
          if(tujuan){

                  var datapost={
                      "idmisi"  :   "{{$id}}",
                      "tujuan"  :   tujuan,
                      "desc"  :   desc
                    };
                    $.ajax({
                      type: "POST",
                      url: '{{ route("backend.rpjmd.savetujuan") }}',
                      data : JSON.stringify(datapost),
                      dataType: 'json',
                      contentType: 'application/json; charset=utf-8',
                      success: function(response) {
                        if (response.success == true) {

                            swal({
                                  title: 'Success!',
                                  text: response.message,
                                  type: 'success',
                                  showCancelButton: false,
                                  confirmButtonText: 'Ok'
                              }).then(function () {
                                 location.reload();
								//window.location.href = '{{ route("backend.rpjmd.masuk",["id"=>$id, "tahun"=> $tahun]) }}';
                              })



                        } else{
                          swal("Failed!", response.message, "error");
                        }
                      }
                    });

          }else{
              swal("Failed!", "Tujuan Belum Terisi!", "error");
          }

  }
  function rubahIKU(id, status){


        var datapost={
          "id"  :  id,
          "status"  :   status
        };
        $.ajax({
          type: "POST",
          url: '{{ route("backend.rpjmd.rubahiku") }}',
          data : JSON.stringify(datapost),
          dataType: 'json',
          contentType: 'application/json; charset=utf-8',
          success: function(response) {
            if (response.success == true) {

                swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                  }).then(function () {
                                 location.reload();
								//window.location.href = '{{ route("backend.rpjmd.masuk",["id"=>$id, "tahun"=> $tahun]) }}';
                  })



            } else{
              swal("Failed!", response.message, "error");
            }
          }
        });



  }


  function hapustujuanindikator(id, ids){
              swal({
				title: 'Apa anda yakin?',
				  text: "Anda akan menghapus indikator tujuan!",
				  type: 'error',
				  showCancelButton: true,
				  confirmButtonText: 'Ok'
			}).then(function () {
				  var datapost={
					"id"  :   id,
					"ids"  :   ids
                  };
				  $.ajax({
					type: "POST",
					url: "{{ route('backend.rpjmd.deleteinditujuan') }}",
					data : JSON.stringify(datapost),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(response) {
					  if (response.success == true) {

						  swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () {
							    location.reload();
								//window.location.href =  "{{ route('backend.rpjmd.masuk', ['id'=>$id, 'tahun'=>$_COOKIE['tahun']]) }}";
							})



					  } else{
						swal("Failed!", response.message, "error");
					  }
					}
				  });
			})
    }
</script>
@endsection
