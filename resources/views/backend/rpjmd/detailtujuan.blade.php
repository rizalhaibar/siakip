<div class="form-horizontal" >
    <div class="form-group">
        <label class="col-md-2 control-label">Tujuan*</label>
        <div class="col-md-10">
            <input type="text" class="form-control" id="tujuanedit" misi="tujuanedit" value="{{@$tujuandetail[0]->tt_tujuan}}" placeholder="Tujuan..." required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2 control-label">Deskripsi*</label>
        <div class="col-md-10">
            <textarea class="form-control" rows="5" id="desctujuan" misi="desctujuan" placeholder="Deskripsi..." >{{@$tujuandetail[0]->tt_keterangan}}</textarea>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-10 col-md-8">
            <button type="button" onClick="editTujuan()" class="btn btn-primary waves-effect waves-light btn-rounded">
                Simpan
            </button>
        </div>
    </div>
</div>


<script>
function editTujuan(){
       var tujuan   = $('#tujuanedit').val();
       var desc   = $('#desctujuan').val();

          if(tujuan){

                  var datapost={
                      "idmisi" : "{{$id}}",
                      "tujuan"   : tujuan,
                      "desc"   : desc,
                      "tahun"  : tahun
                    };
                    $.ajax({
                      type: "POST",
                      url: "{{ route('backend.rpjmd.edittujuan') }}",
                      data : JSON.stringify(datapost),
                      dataType: 'json',
                      contentType: 'application/json; charset=utf-8',
                      success: function(response) {
                        if (response.success == true) {
                            $("#panel-modal .close").click();
                            swal({
                                  title: 'Success!',
                                  text: response.message,
                                  type: 'success',
                                  showCancelButton: false,
                                  confirmButtonText: 'Ok'
                              }).then(function () {
                                location.reload();
                              })



                        } else{
                          swal("Failed!", response.message, "error");
                        }
                      }
                    });

          }else{
              swal("Failed!", "Misi Belum Terisi!", "error");
          }

  }
</script>
