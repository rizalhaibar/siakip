<div class="form-horizontal" >
	<div class="form-group">
		<label class="col-md-2 control-label">Sasaran*</label>
		<div class="col-md-12">
			<input type="text" class="form-control" id="sasarans" misi="sasarans" placeholder="Sasaran..." required>
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-2 control-label">Deskripsi*</label>
		<div class="col-md-12">
			<textarea class="form-control" rows="5" id="descsasaran" misi="descsasaran"  placeholder="Deskripsi..." ></textarea>
		</div>
	</div>


	<div class="form-group">
		<div class="col-md-offset-10 col-md-8">
			<button type="button" onClick="saveSasaran()" class="btn btn-primary waves-effect waves-light btn-rounded">
				Simpan
			</button>
		</div>
	</div>
</div>

<script>
	function saveSasaran(){
       
	   	var sasaran   = $('#sasarans').val();
		var desc   = $('#descsasaran').val();
	  
	  
		  if(sasaran){
			 
				  var datapost={
					  "idtujuan"  :   "{{$id}}",
					  "sasaran"  :   sasaran,
					  "tahun"  :   tahun,
					  "desc"  :   desc
					};
					$.ajax({
					  type: "POST",
					  url: "{{ route('backend.rpjmd.savesasaran') }}",
					  data : JSON.stringify(datapost),
					  dataType: 'json',
					  contentType: 'application/json; charset=utf-8',
					  success: function(response) {
						if (response.success == true) {
							$("#panel-modal .close").click();
							swal({
								  title: 'Success!',
								  text: response.message,
								  type: 'success',
								  showCancelButton: false,
								  confirmButtonText: 'Ok'
							  }).then(function () {
								location.reload();
							  })



						} else{
						  swal("Failed!", response.message, "error");
						}
					  }
					});
			 
		  }else{
			  swal("Failed!", "Visi Belum Terisi!", "error");
		  }
		
  	}
</script>