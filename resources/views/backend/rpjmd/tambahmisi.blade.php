<div class="row">
    <div class="col-md-12">
        <div class="form-horizontal" >
            <div class="form-group">
                <label class="col-md-2 control-label">Misi*</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" id="misis" misi="misis" placeholder="Misi..." required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Deskripsi*</label>
                <div class="col-md-10">
                    <textarea class="form-control" rows="5" id="descmisi" misi="descmisi"  placeholder="Deskripsi..." ></textarea>
                </div>
            </div>


        <div class="form-group">
            <div class="col-md-offset-10 col-md-8">
                <button type="button" onClick="saveMisi()" class="btn btn-primary waves-effect waves-light btn-rounded">
                    Simpan
                </button>
            </div>
        </div>
    </div>
</div>

<script>
function saveMisi(){
       var misi   = $('#misis').val();
       var desc   = $('#descmisi').val();

          if(misi){

                  var datapost={
                      "idvisi" : "{{$id}}",
                      "misi"   : misi,
                      "desc"   : desc,
                      "tahun"  : tahun
                    };
                    $.ajax({
                      type: "POST",
                      url: "{{ route('backend.rpjmd.savemisi') }}",
                      data : JSON.stringify(datapost),
                      dataType: 'json',
                      contentType: 'application/json; charset=utf-8',
                      success: function(response) {
                        if (response.success == true) {
                            swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () {
                                location.reload();
							})


                        } else{
                          swal("Failed!", response.message, "error");
                        }
                      }
                    });

          }else{
              swal("Failed!", "Misi Belum Terisi!", "error");
          }

  }
</script>
