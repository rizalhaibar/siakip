<div class="form-horizontal" >
    <div class="form-group">
        <label class="col-md-2 control-label">Indikator Sasaran*</label>
        <div class="col-md-10">
            <input type="text" class="form-control" id="sasaranedit" misi="sasaranedit" value="{{@$indikatorsasarandetail[0]->tis_indikator_sasaran}}" placeholder="Indikator Sasaran..." required>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-2 control-label">Deskripsi*</label>
        <div class="col-md-10">
            <textarea class="form-control" rows="5" id="descsasaran" misi="descsasaran" placeholder="Deskripsi..." >{{@$indikatorsasarandetail[0]->tis_keterangan}}</textarea>
        </div>
    </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Formula*</label>
            <div class="col-md-10">
                <textarea class="form-control" rows="5" id="descindiformula" name="descindiformula"  placeholder="Formula..." >{{@$indikatorsasarandetail[0]->formula}}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Sumber Data*</label>
            <div class="col-md-10">
                <textarea class="form-control" rows="5" id="sumberdata" name="sumberdata"  placeholder="Sumber Data..." >{{@$indikatorsasarandetail[0]->sumberdata}}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Satuan*</label>
            <div class="col-md-10">
                <input type="text" class="form-control" id="indisatuan" name="indisatuan" placeholder="Indikator Sasaran Satuan..." value="{{@$indikatorsasarandetail[0]->tis_satuan}}" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">IKU*</label>
            <div class="col-md-10">
                <select class="form-control" id="indiiku" name="indiiku">
                    <option value="T" {{@$indikatorsasarandetail[0]->tis_iku == "T" ? 'selected' : ''}} > Ya</option>
                    <option value="F" {{@$indikatorsasarandetail[0]->tis_iku == "F" ? 'selected' : ''}} > Tidak</option>
                </select>
            </div>
        </div>


<div class="form-group">
    <div class="col-md-offset-10 col-md-8">
        <button type="button" onClick="editIndiSasaran()" class="btn btn-primary waves-effect waves-light btn-rounded">
            Simpan
        </button>
    </div>
</div>
</div>


<script>

function editIndiSasaran(){

       var sasaran   = $('#sasaranedit').val();
        var desc   = $('#descsasaran').val();
        var formula   = $('#descindiformula').val();
        var satuan   = $('#indisatuan').val();
        var iku   = $('#indiiku').val();
        var sumberdata   = $('#sumberdata').val();


          if(sasaran){

                    var datapost={
                      "idsasaran"  :   "{{$id}}",
                      "indisasaran"  :   sasaran,
                      "formula"  :   formula,
                      "sumberdata"  :   sumberdata,
                      "satuan"  :   satuan,
                      "iku"  :   iku,
                      "desc"  :   desc
                    };
                    $.ajax({
                      type: "POST",
                      url: "{{ route('backend.rpjmd.editindikatorsasaran') }}",
                      data : JSON.stringify(datapost),
                      dataType: 'json',
                      contentType: 'application/json; charset=utf-8',
                      success: function(response) {
                        if (response.success == true) {
                            $("#panel-modal .close").click();
                            swal({
                                  title: 'Success!',
                                  text: response.message,
                                  type: 'success',
                                  showCancelButton: false,
                                  confirmButtonText: 'Ok'
                              }).then(function () {
                                location.reload();
                              })



                        } else{
                          swal("Failed!", response.message, "error");
                        }
                      }
                    });

          }else{
              swal("Failed!", "Sasaran Belum Terisi!", "error");
          }

  }
</script>
