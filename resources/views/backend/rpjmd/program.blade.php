@extends('backend.layouts.app')

@section('title', 'RPJMD | Program')

@section('content')

<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
													<span class="kt-portlet__head-icon kt-hide">
														<i class="la la-gear"></i>
													</span>
                            <h3 class="kt-portlet__head-title">
                               Program
                            </h3>
                        </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="form-horizontal" >
                        <ul style="list-style-type: none;">
                            <li><b>Visi</b> : {{$visi->tv_visi}}</li>
                            <li>
                                <ol>
                                    <b>Misi</b> : {{$misi->tm_misi}}
                                </ol>
                            </li>
                            <li>
                                <ol>
                                    <ol>
                                        <b>Tujuan</b> : {{$tujuan->tt_tujuan}}
                                    </ol>
                                </ol>
                            </li>
                            <li>
                                <ol>
                                    <ol>
                                        <ol>
                                            <b>Sasaran</b> : {{$sasaran->tst_sasaran_tujuan}}
                                        </ol>
                                    </ol>
                                </ol>
                            </li>
                            <li>
                                <ol>
                                    <ol>
                                        <ol>
                                            <ol>
                                                <b>Indikator Sasaran</b> : {{$indisasaran->tis_indikator_sasaran}}
                                            </ol>
                                        </ol>
                                    </ol>
                                </ol>
                            </li>                           
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>     

<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="header-title m-t-0 m-b-30">Program <a href="javascript:void(0)" class="btn btn-danger btn-icon btn-sm" onClick="tambahprogram({{$id}})"><i class="fa fa-plus"></i></a></h4>
                        <ol style="margin-top: 40px;">
                            @foreach($program as $newprogram)
                                <li>
                                    {{$newprogram->tps_program_sasaran}}
                                    &nbsp; <a href="javascript:void(0)"  onClick="editprogram({{$newprogram->tps_id}})"><i class="fa fa-edit"></i></a>
                                    &nbsp;<a href="javascript:void(0)" onClick="hapusprogram({{$newprogram->tps_id}})"> <i class="fa fa-trash"></i></a>
                                    <ol style="margin-top:10px">
                                        <h4>Indikator Program <a href="javascript:void(0)" class="btn btn-danger btn-icon btn-sm" onClick="tambahindikatorprogram({{$newprogram->tps_id}})"><i class="fa fa-plus"></i></a></h4>
                                        @if(isset($newprogram->indikatorprogram))
                                            @foreach($newprogram->indikatorprogram as $indikatorprogram)
                                                <li style="margin-top:10px">
                                                    {{$indikatorprogram->tip_indikator_program}}
                                                    &nbsp;<a href="javascript:void(0)"  onClick="editindikatorprogram({{$indikatorprogram->tip_id}})"><i class="fa fa-edit"></i></a>
                                                    <a href="javascript:void(0)"  onClick="hapusindikatorprogram({{$indikatorprogram->tip_id}})"><i class="fa fa-trash" id="danger-alert"></i></a>
                                                </li>
                                                <ol>
                                                    <h4 style="margin-top: 10px">Kegiatan <a href="javascript:void(0)" class="btn btn-danger btn-icon btn-sm" onClick="tambahkegiatan({{$indikatorprogram->tip_id}})"><i class="fa fa-plus"></i></a></h4>
                                                        @php($s = 1)
                                                        @if(isset($indikatorprogram->kegiatan ))
                                                            @foreach($indikatorprogram->kegiatan as $kegiatan)
                                                                <li style="display:block; ">
                                                                    {{$s}}. {{$kegiatan->tkp_kegiatan_program}}
                                                                    <span> 
                                                                        <a href="javascript:void(0)" onClick="editkegiatan({{$kegiatan->tkp_id}},{{$id}})"><i class="fa fa-edit" ></i></a> 
                                                                        <a href="javascript:void(0)"  onClick="hapuskegiatan({{$kegiatan->tkp_id}})"><i class="fa fa-trash" id="danger-alert"></i></a> 
                                                                    </span>
                                                                    <ul style="margin-left: 20px">
                                                                        <h4 style="margin-left:-30px; color: #336600; margin-top:10px">Indikator Kegiatan <a href="javascript:void(0)" class="btn btn-danger btn-icon btn-sm" onClick="tambahindikatorkegiatan({{$kegiatan->tkp_id}})"><i class="fa fa-plus"></i></a></h4>
                                                                    </ul>
                                                                    <ol style="margin-left: 20px">
                                                                        <table class="table table-bordered">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th><center>NO</center></th>
                                                                                    <th><center>Indikator Kegiatan</center></th>
                                                                                    <th><center>Satuan</center></th>
                                                                                    <th><center>Target</center></th>
                                                                                    <th></th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                @php($sasssas=1)
                                                                                @if(isset($kegiatan->indikatorkegiatan))
                                                                                @foreach($kegiatan->indikatorkegiatan as $indi)
                                                                                <tr>
                                                                                    <td><?php echo $sasssas;?></td>
                                                                                    <td><?php echo $indi->tik_indikator_kegiatan;?></td>
                                                                                    <td><?php echo $indi->tik_satuan;?></td>
                                                                                    <td><?php echo $indi->tik_target?></td>
                                                                                    <td> 
                                                                                        <span> 
                                                                                            <a href="javascript:void(0)" onClick="detailtujuanindikator({{$indi->tik_id}},{{$id}})"><i class="fa fa-edit" style="color:blue"></i></a> 
                                                                                            <a href="javascript:void(0)" onClick="hapustujuanindikator({{$indi->tik_id}},{{$id}})"><i class="fa fa-trash" style="color:red" id="danger-alert"></i></a> 
                                                                                        </span>
                                                                                    </td>
                                                                                </tr>
                                                                                @php($sasssas++)
                                                                                @endforeach
                                                                                @endif
                                                                            </tbody>
                                                                        </table>
                                                                    </ol>
                                                                </li>
                                                            @php($s++)
                                                            @endforeach
                                                        @endif
                                                </ol>
                                            @endforeach
                                        @endif
                                    </ol>
                                </li>
                            @endforeach
                        </ol>
                </div>
            </div>
        </div>
    </div> 
</div>   
<div id="panel-modal" class="modal fade" id="kt_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

@endsection

@section('onpage-js')
@include('backend.layouts.message')
    
    <script>

    $.ajaxSetup({
        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }
    });
    $(document).ready(function () {
           
    });
      
    function tambahprogram(id){
        var url = '{{ route("backend.rpjmd.tambahprogram",["id"=>":id", "iddinas"=> $iddinas]) }}'
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Tambah Program');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
</script>
@endsection