<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        
        // Index Composer
        View::composer(
            [
                'backend.role.index',
                'backend.menu.index',
                'backend.user.index',
                'backend.company.index',
                'backend.department.index',
                'backend.person.index',
                'backend.visit.index',
                'backend.meeting.room.index',
            ],
            'App\Composers\IndexViewComposer'
        );

    }
}
