<?php

namespace App\Repositories\Visitor;

interface VisitorRepositoryInterface
{

    /**
     * 
     */
    function status();

    /**
     * 
     */
    function find(int $id);

    /**
     * 
     */
    function search(string $field, string $value);

    /**
     * 
     */
    function store($attributes);

    /**
     * 
     */
    function update(int $id, $attributes);

    /**
     * 
     */
    function visitors($status);

    /**
     * 
     */
    function pagination($attributes);
 

}