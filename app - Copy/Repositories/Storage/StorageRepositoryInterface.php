<?php

namespace App\Repositories\Storage;

interface StorageRepositoryInterface
{
    /**
     * 
     */
    function store($disk, $path, $curr, $new);

}