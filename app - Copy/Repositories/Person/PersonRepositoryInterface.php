<?php

namespace App\Repositories\Person;

interface PersonRepositoryInterface
{

    /**
     *
     */
    function status();

    /**
     *
     */
    function find(int $id);

    /**
     *
     */
    function search(string $field, string $value);

    /**
     *
     */
    function store($attributes);

    /**
     *
     */
    function update(int $id, $attributes);

    /**
     *
     */
    function toggle($attributes);

    /**
     *
     */
    function persons($status);

    /**
     *
     */
    function pagination($attributes);

    /**
     *
     */
    function do($attributes);

    /**
     *
     */
    function import($attributes);

}
