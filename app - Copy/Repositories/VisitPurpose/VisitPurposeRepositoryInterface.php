<?php

namespace App\Repositories\VisitPurpose;

interface VisitPurposeRepositoryInterface
{
    
    /**
     * 
     */
    function status();

    /**
     * 
     */
    function find(int $id);

    /**
     * 
     */
    function search(string $field, string $value);

    /**
     * 
     */
    function store($attributes);

    /**
     * 
     */
    function update(int $id, $attributes);

    /**
     * 
     */
    function visitPurposes($status);

    /**
     * 
     */
    function toggle($attributes);

    /**
     * 
     */
    function pagination($attributes, int $limit);

}