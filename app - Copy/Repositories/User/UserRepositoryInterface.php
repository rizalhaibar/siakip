<?php

namespace App\Repositories\User;

interface UserRepositoryInterface
{

    /**
     * 
     */
    function status();

    /**
     * 
     */
    function find(int $id);

    /**
     * 
     */
    function search(string $field, string $value);

    /**
     * 
     */
    function store($attributes);

    /**
     * 
     */
    function update(int $id, $attributes);

    /**
     * 
     */
    function toggle($attributes);

    /**
     * 
     */
    function pagination(int $limit);
    
    /**
     * 
     */
    function updateProfile(int $id, $attributes);

    /**
     * 
     */
    function changePassword(int $id, $attributes);


}