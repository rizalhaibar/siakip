<?php

namespace App\Repositories\Visit;

use App\Repositories\Visit\VisitRepositoryInterface;
use App\Repositories\Storage\StorageRepositoryInterface;
use App\Repositories\Visitor\VisitorRepositoryInterface;
use App\Repositories\Courrier\CourrierRepositoryInterface;
use App\Repositories\VisitPurpose\VisitPurposeRepositoryInterface;
use App\Repositories\Meeting\MeetingRepositoryInterface;
use App\Repositories\BaseRepository;
use App\Traits\FormatMessageTraits;
use App\Models\Visit;

class VisitRepository extends BaseRepository implements VisitRepositoryInterface
{
    use FormatMessageTraits;

    protected $model;

    /**
     * VisitRepository constructor.
     * @param
     */
    public function __construct(
        Visit $visit,
        StorageRepositoryInterface $storageRepo,
        VisitorRepositoryInterface $visitorRepo,
        CourrierRepositoryInterface $courrierRepo,
        VisitPurposeRepositoryInterface $visitPurposeRepo,
        MeetingRepositoryInterface $meetingRepo
    ) {
        //
        $this->model = $visit;
        $this->storageRepo = $storageRepo;
        $this->visitorRepo = $visitorRepo;
        $this->courrierRepo = $courrierRepo;
        $this->visitPurposeRepo = $visitPurposeRepo;
        $this->meetingRepo = $meetingRepo;
        $this->__setup();
    }

    /**
     *
     */
    private function __setup()
    {
        $this->VALUE_EXIST = config('setting.value.exist');
        $this->VALUE_ZERO = config('setting.value.zero');

        $this->MESSAGE_VALUE_EXIST = "Cannot processing action. Data %xxx% still used in another table";

        $this->MESSAGE_CREATE_SUCCESS = "%xxx% successfuly created.";
        $this->MESSAGE_CREATE_FAILED = "%xxx% Failed to create.";

        $this->MESSAGE_UPDATE_SUCCESS = "%xxx% successfuly updated.";
        $this->MESSAGE_UPDATE_FAILED = "%xxx% Failed to update.";
    }

    /**
     * @param $id
     * @return mixed
     */
    public function hasRelation($id)
    {
        return $this->search('id', $id)
        ->has('company')
        ->has('department')
        ->has('person')
        ->has('visitPurpose')
        ->has('visitor')
        ->exists();
    }

    /**
     * @param string $id
     * @param string $message
     * @param object $attributes
     * @return mixed
     */
    public function returnResponse($type, $message, $data)
    {
        return $this->returnMessage($type, $this->format($message, $data->visit_card_number));
    }

    /**
     *
     */
    public function visits($status)
    {
        $data = $this->model->query();

        if( $status != config('setting.status.all') ) {
            $data->where('status', $status);
        }

        return $data;
    }

    /**
     * @param object $attributes
     * @return mixed
     */
    public function store($attributes)
    {
        if( isset($attributes->courrier) ) {
            for ($i = 0; $i < count($attributes->image_package); $i++) {

                // CREATE NEW Request FOR VISIT ATTRIBUTES
                $attrVisit = new \Illuminate\Http\Request([
                    'person_id'  => $attributes->person_id[$i],
                    'department_id'  => $attributes->department_id[$i],
                    'company_id'  => (is_array($attributes->company_id) ? $attributes->company_id[$i] : $attributes->company_id),
                    'visit_date'  => $attributes->visit_date,
                    'visit_time_in'  => $attributes->visit_time_in
                ]);

                // PROCESS INPUT TO TABEL VISIT
                $data = new $this->model;
                $data->fill($attrVisit->toArray());
                $data->save();

                // CREATE NEW Request FOR COURRIER ATTRIBUTES
                $attrCourrier = new \Illuminate\Http\Request([
                    'visit_id'   => $data->id,
                    'image_person'  => $attributes->image_person,
                    'image_card'  => $attributes->image_card,
                    'package'  => $attributes->package[$i],
                    'image_package'  => $attributes->image_package[$i]
                ]);

                // STORE NEW ATRIBUTES TO COURRIER
                $this->courrierRepo->store($attrCourrier);
            }
        } else {

            $data = new $this->model;
            $data->fill( $attributes->toArray() );
            $data->save();

            //
            $attributes->merge([ 'visit_id' => $data->id ]);
            $this->visitorRepo->store($attributes);

            //
            $meeting = $this->visitPurposeRepo->search('name', 'Meeting')->first()->id;
            if ($attributes->visit_purpose_id == $meeting) {
                $this->meetingRepo->store($attributes);
            }
        }

        //
        if( !$data->save() ) {
            return $this->returnResponse('warning', $this->MESSAGE_CREATE_FAILED, $data);
        } else {
            return $this->returnResponse('success', $this->MESSAGE_CREATE_SUCCESS, $data);
        }
    }

}
