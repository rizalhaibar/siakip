<?php

namespace App\Repositories\Role;

interface RoleRepositoryInterface
{
    /**
     * 
     */
    function status();

    /**
     * 
     */
    function find(int $id);

    /**
     * 
     */
    function search(string $field, string $value);

    /**
     * 
     */
    function store($attributes);

    /**
     * 
     */
    function update(int $id, $attributes);

    /**
     * 
     */
    function toggle($attributes);


    /**
     * 
     */
    function roles($status);

    /**
     * 
     */
    function pagination(int $limit);

}