<?php

namespace App\Repositories\Courrier;

use App\Repositories\Courrier\CourrierRepositoryInterface;
use App\Repositories\Storage\StorageRepositoryInterface;
use App\Repositories\BaseRepository;
use App\Traits\FormatMessageTraits;
use App\Models\Courrier;

class CourrierRepository extends BaseRepository implements CourrierRepositoryInterface
{
    use FormatMessageTraits;

    protected $model;

    /**
     * CourrierRepository constructor.
     * @param
     */
    public function __construct(
        Courrier $courrier,
        StorageRepositoryInterface $storageRepo
    ) {
        //
        $this->model = $courrier;
        $this->storageRepo = $storageRepo;
        $this->__setup();
    }

    /**
     *
     */
    private function __setup()
    {

        $this->DISK = "public";
        $this->IMAGE_CARD_PATH = config('setting.store-path.courrier.card');
        $this->IMAGE_PERSON_PATH = config('setting.store-path.courrier.person');
        $this->IMAGE_PACKAGE_PATH = config('setting.store-path.courrier.package');
        $this->MESSAGE_VALUE_EXIST = "Cannot processing action. Data %xxx% still used in another table";
        $this->MESSAGE_TOGGLE_SUCCESS = "%xxx% status successfuly updated.";
        $this->MESSAGE_TOGGLE_FAILED = "%xxx% status to update.";
        $this->MESSAGE_CREATE_SUCCESS = "%xxx% successfuly created.";
        $this->MESSAGE_CREATE_FAILED = "%xxx% Failed to create.";
        $this->MESSAGE_UPDATE_SUCCESS = "%xxx% successfuly updated.";
        $this->MESSAGE_UPDATE_FAILED = "%xxx% Failed to update.";

        $this->random_string = date('ymdhis').\Str::random(10);

    }

    /**
     * @param $id
     * @return mixed
     */
    public function hasRelation($id)
    {
        return $this->search('id', $id)->has('visit')->exists();
    }

    /**
     * @param string $type
     * @param string $message
     * @param object $attributes
     * @return mixed
     */
    public function returnResponse($type, $message, $data)
    {
        return $this->returnMessage($type, $this->format($message, $data->package));
    }

    /**
     *
     */
    public function courriers($status)
    {
        $data = $this->model->query();

        if( $status != config('setting.status.all') ) {
            $data->where('status', $status);
        }

        return $data;
    }

    /**
     * @param Object $attributes
     * @return mixed
     */
    public function pagination($attributes)
    {
        // Base Query
        $model = $this->model->query();

        // Visit Join
        $query = $model->whereHas('visit', function($visit) use ($attributes) {

            // Company Filter
            if(isset($attributes->company_id)) {
                $visit->where('company_id', $attributes->company_id);
            }

            // Date Range Filter
            if(isset($attributes->end_date) || isset($attributes->start_date) ) {
                $visit->whereBetween('visit_date', [$attributes->start_date, $attributes->end_date]);
            }

            // Time Range Filter
            if(isset($attributes->end_time) || isset($attributes->start_time) ) {
                $visit->whereBetween('visit_time_in', [$attributes->start_time, $attributes->end_time]);
            }

            // Package Name Filter
            if(isset($attributes->param_filter) && $attributes->param_filter == "package") {
                $visit->where($attributes->param_filter, 'like', '%'.$attributes->value_search.'%');
            }

            // Person Name Filter
            if(isset($attributes->param_filter) && $attributes->param_filter == "persons.name") {
                $visit->whereHas('person', function($person) use($attributes) {
                    $person->where($attributes->param_filter, 'like', '%'.$attributes->value_search.'%');
                });
            }

        });

        return $query->paginate($attributes->limit);
    }

    /**
     * @param object $attributes
     * @return mixed
     */
    public function store($attributes)
    {
        if($attributes->has('image_person'))
        {
            $images = (object)[
                'filename' => $this->random_string.'_person_.png',
                'file' => $attributes->image_person
            ];
            $this->storageRepo->store($this->DISK, $this->IMAGE_PERSON_PATH, $images->filename, $images);
            $attributes->merge([ 'image_person' => $images->filename ]);
        }

        if($attributes->has('image_card'))
        {
            $images = (object)[
                'filename' => $this->random_string.'_card_.png',
                'file' => $attributes->image_card
            ];
            $this->storageRepo->store($this->DISK, $this->IMAGE_CARD_PATH, $images->filename, $images);
            $attributes->merge([ 'image_card' => $images->filename ]);
        }

        if($attributes->has('image_package'))
        {
            $images = (object)[
                'filename' => $this->random_string.'_package_.png',
                'file' => $attributes->image_package
            ];
            $this->storageRepo->store($this->DISK, $this->IMAGE_PACKAGE_PATH, $images->filename, $images);
            $attributes->merge([ 'image_package' => $images->filename ]);
        }

        $data = new $this->model;
        $data->fill( $attributes->toArray() );
        $data->save();

        /* if( !$data->save() ) {
            return $this->returnResponse('warning', $this->MESSAGE_CREATE_FAILED, $data);
        } else {
            return $this->returnResponse('success', $this->MESSAGE_CREATE_SUCCESS, $data);
        } */
    }

    /**
     * @param integer $id
     * @param object $attributes
     * @return mixed
     */
    public function update(int $id, $attributes)
    {
        // Request Only
        $attributes = $attributes->only(['package']);
        $data = $this->find($id);
        $data->fill( $attributes );
        if( !$data->save() ) {
            return $this->returnResponse('warning', $this->MESSAGE_UPDATE_FAILED, $data);
        } else {
            return $this->returnResponse('success', $this->MESSAGE_UPDATE_SUCCESS, $data);
        }
    }

}
