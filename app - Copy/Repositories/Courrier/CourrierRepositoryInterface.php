<?php

namespace App\Repositories\Courrier;

interface CourrierRepositoryInterface
{
    /**
     * 
     */
    function status();

    /**
     * 
     */
    function find(int $id);

    /**
     * 
     */
    function search(string $field, string $value);

    /**
     * 
     */
    function store($attributes);

    /**
     * 
     */
    function update(int $id, $attributes);

    /**
     * 
     */
    function toggle($attributes);

    /**
     * 
     */
    function courriers($status);

    /**
     * 
     */
    function pagination($attributes);

    

}