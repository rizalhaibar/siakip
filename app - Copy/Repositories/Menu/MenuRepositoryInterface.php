<?php

namespace App\Repositories\Menu;

interface MenuRepositoryInterface
{

    /**
     * 
     */
    function status();

    /**
     * 
     */
    function find(int $id);

    /**
     * 
     */
    function search(string $field, string $value);

    /**
     * 
     */
    function store($attributes);

    /**
     * 
     */
    function update(int $id, $attributes);

    /**
     * 
     */
    function toggle($attributes);

    /**
     * 
     */
    function menus($status);

    /**
     * 
     */
    function parents();

    /**
     * 
     */
    function pagination(int $limit);

}