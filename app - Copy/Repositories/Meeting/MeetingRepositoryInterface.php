<?php

namespace App\Repositories\Meeting;

interface MeetingRepositoryInterface
{
    
    /**
     * 
     */
    function status();

    /**
     * 
     */
    function find(int $id);

    /**
     * 
     */
    function search(string $field, string $value);

    /**
     * 
     */
    function store($attributes);

    /**
     * 
     */
    function update(int $id, $attributes);

    /**
     * 
     */
    function toggle($attributes);

    /**
     * 
     */
    function meetings($status);

    /**
     * 
     */
    function pagination($attributes, int $limit);

}