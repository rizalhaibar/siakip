<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $company_id
 * @property integer $department_id
 * @property integer $person_id
 * @property integer $visit_purpose_id
 * @property integer $visit_code
 * @property string $visit_card_number
 * @property string $visit_date
 * @property string $visit_time_in
 * @property string $visit_time_out
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Company $company
 * @property Department $department
 * @property Person $person
 * @property VisitPurpose $visitPurpose
 * @property Courrier[] $courriers
 * @property Meeting[] $meetings
 * @property Visitor[] $visitors
 */
class Visit extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['company_id', 'department_id', 'person_id', 'visit_purpose_id', 'visit_code', 'visit_card_number', 'visit_date', 'visit_time_in', 'visit_time_out', 'status', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function department()
    {
        return $this->belongsTo('App\Models\Department');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function person()
    {
        return $this->belongsTo('App\Models\Person');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function visitPurpose()
    {
        return $this->belongsTo('App\Models\VisitPurpose');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function courriers()
    {
        return $this->hasOne('App\Models\Courrier');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function meetings()
    {
        return $this->hasOne('App\Models\Meeting');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function visitors()
    {
        return $this->hasOne('App\Models\Visitor');
    }
}
