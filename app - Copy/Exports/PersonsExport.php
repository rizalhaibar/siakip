<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Repositories\Person\PersonRepositoryInterface;


class PersonsExport implements FromCollection, WithHeadings, WithColumnFormatting, WithMapping, ShouldAutoSize
{
    protected $attributes, $personRepo;

    function __construct( $attributes ) {

        $this->attributes = $attributes;
        $this->personRepo = app(PersonRepositoryInterface::class);
    }

    public function collection()
    {
        $attributes = $this->attributes;
        $persons = $this->personRepo
            ->courriers( config('setting.status.all') )
            ->with('department','company')
            ->whereHas('department')
            ->whereHas('company')
        ;

        // Filter by Company
        if(isset($attributes->company_id))
        {
            $persons->where('company_id', $attributes->company_id);
        }

        // Filter by Search Person Name
        if(isset($attributes->param_filter))
        {
            $persons->where($attributes->param_filter, 'like', '%'.$attributes->value_search.'%');
        }

        return $persons->get();
    }

    public function headings(): array
    {
        return [
            'Id',
            'Package',
            'Department',
            'Intended Person',
            'Date & Time in',
            'Status'
        ];
    }

    public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_TEXT
        ];
    }

    public function map($courriers): array
    {
        return [
            $courriers->visit->id,
            $courriers->package,
            $courriers->visit->department->name,
            $courriers->visit->person->name,
            $courriers->visit->visit_date.' '.$courriers->visit->visit_time_in,
            ($courriers->visit->status == 2 ? 'Checked Out' : 'Checked In'),
        ];
    }

}
