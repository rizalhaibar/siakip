<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserValidation;
use App\Models\Role;
use Carbon\Carbon;
use DB;
/**
 * Class UserController.
 */
class EvaluasikinerjaController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
	 
	function __construct()
	{
		$this->middleware(['auth']);
	}
	
	public function index()
    {
		$data['breadcrumb'] = [
            'parent' => 'Evaluasi Kinerja',
            'child' => 'List Evaluasi Mandiri'
		];
		
		$data['listdinas'] = $this->listdinas();
		return view('backend.evaluasikinerja.index', $data);
	}

	
	public function detail($id, $tahun)
    {
		$query = DB::select("SELECT * FROM tbl_evaluasi WHERE iddinas = ".$id);
		$data['evaluasi'] = $query;
        $data['id'] = $id;

		return view('backend.evaluasikinerja.detail', $data);
	}
   
	public function listdinas(){ 
		$hasil = DB::select('SELECT aa.td_id, aa.td_dinas, aa.td_keterangan , bb.nilai
								FROM
										( 
										SELECT a.td_id, a.td_dinas, a.td_keterangan 
										FROM tbl_dinas a
										WHERE
											a.status = 1
										)aa
								LEFT JOIN
										(
										SELECT a.td_id, a.td_dinas, a.td_keterangan , b.nilai
										FROM tbl_dinas a
										LEFT JOIN tbl_evaluasi b on a.td_id = b.iddinas
										where
											status = 1
											AND b.tahun = '.$_COOKIE['tahun'].'
										)bb on bb.td_id = aa.td_id ');
		return $hasil;
	}

	
	public function nilaievaluasiedit(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_evaluasi')
				->where('iddinas', $input['idsasaran'])
				->update(['nilai' => $input['target']]);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Nilai Sukses Terupdate!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}
	
	
	public function nilaievaluasi(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_visi')->insert(
			['iddinas' => $input['idsasaran'], 'nilai' => $input['target'], 'tahun' => $input['tahun']]
		);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Nilai Sukses Tersimpan!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}
	
}
