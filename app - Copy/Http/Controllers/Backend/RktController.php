<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserValidation;
use App\Models\Role;
use Carbon\Carbon;
use DB;
/**
 * Class UserController.
 */
class RktController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
	 
	function __construct()
	{
		$this->middleware(['auth']);
		
        $this->middleware(function ($request, $next)
        {
            $this->user = session('data')->user;
            return $next($request);
        });
	}
	
	public function index()
    {
		if($this->user->role_id == 99 || $this->user->role_id == 97){
			$data['breadcrumb'] = [
				'parent' => 'RKT',
				'child' => 'List RKT'
			];
			$data['listdinas'] = $this->listdinas();

			return view('backend.rkt.index', $data);
		}else{
			$data['breadcrumb'] = [
				'parent' => 'RKT',
				'child' => 'List RKT'
			];
			
			$data['id'] = $this->user->dinas_id;
			$data['listsasaran'] = $this->listsasaran($this->user->dinas_id);

			return view('backend.rkt.home', $data);
		}
	}

	public function home($id)
    {
		$data['breadcrumb'] = [
			'parent' => 'RKT',
			'child' => 'List RKT'
		];	
		$data['id'] = $id;
		$data['listsasaran'] = $this->listsasaran($id);

		return view('backend.rkt.home', $data);
	}

	public function indikator()
    {
		$data['breadcrumb'] = [
			'parent' => 'RKT',
			'child' => 'Indikator'
		];
		$data['listindikator'] = $this->listindikator();

		return view('backend.rkt.indikator', $data);
	}

	
	public function tambahtarget($id, $iddinas)
    {
		$query = DB::select("SELECT * FROM tbl_indikator_sasaran WHERE tis_id = ".$id);
		$data['target'] = $query;
       
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;

		return view('backend.rkt.detail', $data);
	}

	
	public function listdinas(){ 
		$hasil = DB::select('SELECT td_id, 
									td_dinas, 
									td_keterangan 
								FROM tbl_dinas
								WHERE 
									status = 1 
								ORDER BY `index`');
		return $hasil;
	}
	
	public function listsasaran($id){ 
		$hasil = array();
		$where = '';
		if($this->user->role_id != 99){
			$where = 'aa.`iddinas` = '.$this->user->dinas_id.' AND';
		}else{
			$where = 'aa.`iddinas` = '.$id.' AND';
		}

		$result4 	= DB::select('SELECT a.* FROM tbl_sasaran_tujuan a
		
								LEFT JOIN tbl_tujuan cc ON cc.tt_id = a.idtujuan
								LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
								LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
								WHERE '.$where.' tst_status = 0');
		
		
		// dd($result4);
		
		foreach($result4 as $new4){

				$result6 	= DB::select('SELECT * FROM tbl_indikator_sasaran a 
				
											LEFT JOIN tbl_iku b ON b.tis_id = a.tis_id 
											WHERE idsasaran = '.$new4->tst_id.' AND tis_status = 0');

				foreach($result6 as $new6){
					
					
					if($new4->tst_id == $new6->idsasaran){
						
						$new4->indikatorsasaran = $result6;
					}
				}
			
			array_push($hasil, $new4);
		}
		$hasil = (object) $hasil;
		return $hasil;
	}
	
	public function listindikator(){ 
		$where = '';
		if($this->user->dinas_id != 1){
			$where = 'aa.`iddinas` = '.$this->user->dinas_id.' AND';
		}
		$hasil = DB::select('SELECT a.tis_id, a.tis_indikator_sasaran, a.tis_satuan , b.formula, a.tis_target
		FROM tbl_indikator_sasaran a
		LEFT JOIN tbl_sasaran_tujuan dd ON dd.`tst_id` = a.`idsasaran`
		LEFT JOIN tbl_tujuan cc ON cc.tt_id = dd.idtujuan
		LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
		LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
		LEFT JOIN tbl_iku b ON b.tis_id = a.tis_id
		WHERE '.$where.' aa.tahun = '.$_COOKIE['tahun']);
		return $hasil;
	}

	
	public function targetedit(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_indikator_sasaran')
			->where('tis_id', $input['idsasaran'])
			->update([
						'tis_target' => $input['target'], 
						'tis_satuan' => $input['satuan']
				]
		);
		$data = DB::table('tbl_indikator_sasaran_pk')
			->where('tis_id', $input['idsasaran'])
			->update([
						'tis_target' => $input['target'], 
						'tis_satuan' => $input['satuan']
				]
		);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Target Sukses Terupdate!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
    }
	
}
