<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserValidation;
use App\Models\Role;
use Carbon\Carbon;

use DB;
/**
 * Class UserController.
 */
class PengukurankinerjaController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
	 
	
	function __construct()
	{
		$this->middleware(['auth']);
		
        $this->middleware(function ($request, $next)
        {
            $this->user = session('data')->user;
            return $next($request);
        });
	}
	
	
	public function index()
    {
		if($this->user->role_id == 99 || $this->user->role_id == 97){
			$data['breadcrumb'] = [
				'parent' => 'Perjanjian Kinerja',
				'child' => 'List Dinas'
			];
			$data['listdinas'] = $this->listdinas();

			return view('backend.pengukurankinerja.index', $data);
		}else{
			$data['breadcrumb'] = [
				'parent' => 'Perjanjian Kinerja',
				'child' => 'List Rencana Aksi'
			];
			$data['listsasaran'] = $this->listsasaran();
			$data['listprogram'] = $this->listprogram();
			$data['listkegiatan'] = $this->listkegiatan();

			return view('backend.pengukurankinerja.home', $data);
		}
	}

	public function home($id, $idsasaran)
    {
		$data['breadcrumb'] = [
			'parent' => 'Perjanjian Kinerja',
			'child' => 'List Pengukuran Kinerja'
		];
		$data['id'] = $id;
		$data['idsasaran'] = $idsasaran;
		$data['dinas'] = $this->getDinas($id);
		$data['listsasaran'] = $this->listsasaran($id);
		// dd($data);
		if($idsasaran != 0){
			$data['indikatorsasaran'] = $this->listindikatorsasaran($idsasaran);
			$data['program'] = $this->getProgram($idsasaran);
			$data['data_program'] = json_encode($this->getProgramJson($idsasaran));
			$data['kegiatan'] = $this->getKegiatan($idsasaran);
		}
		
		// $data['listprogram'] = $this->listprogram($id);
		// $data['listkegiatan'] = $this->listkegiatan($id);
		// dd($data);
		return view('backend.pengukurankinerja.home', $data);
	}
	public function tambahindikatorsasaranpic($id, $idsasaran, $ids)
    {
		$data['id'] = $id;
		$data['iddinas'] = $ids;
		$data['idsasaran'] = $idsasaran;
		$data['listpic'] = $this->listpic($ids, '2A');
		$data['detail'] = DB::table('tbl_indikator_sasaran_pk')->where('tis_id',$id)->first();
		

		return view('backend.pengukurankinerja.tambahindikatorsasaranpic', $data);
	}
	public function tambahindikatorprogrampic($id, $idsasaran, $ids)
    {
		$data['id'] = $id;
		$data['iddinas'] = $ids;
		$data['idsasaran'] = $idsasaran;
		$data['listpic'] = $this->listpic($ids, '3A');
		$data['detail'] = DB::table('tbl_indikator_program')->where('tip_id',$id)->first();
		

		return view('backend.pengukurankinerja.tambahindikatorprogrampic', $data);
	}
	public function tambahindikatorkegiatanpic($id, $idsasaran, $ids)
    {
		$data['id'] = $id;
		$data['iddinas'] = $ids;
		$data['idsasaran'] = $idsasaran;
		$data['listpic'] = $this->listpic($ids, '4A');
		$data['detail'] = DB::table('tbl_indikator_kegiatan')->where('tik_id',$id)->first();
		

		return view('backend.pengukurankinerja.tambahindikatorkegiatanpic', $data);
	}

	
    public function tambahprogram($id, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;
		
        return view('backend.pengukurankinerja.tambahprogram',$data);
	}

    public function tambahindikatorprogram($id, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;
		
        return view('backend.pengukurankinerja.tambahindikatorprogram',$data);
	}
	
    public function editprogram($id, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;
		$data['detail'] = DB::table('tbl_program_sasaran')->where('tps_id',$id)->first();
		// dd($data);
        return view('backend.pengukurankinerja.editprogram',$data);
	}
    public function editindikatorprogram($id, $idprogram, $iddinas, $idsasaran)
    {
		$data['id'] = $id;
		$data['idprogram'] = $idprogram;
		$data['iddinas'] = $iddinas;
		$data['idsasaran'] = $idsasaran;
		$data['detail'] = DB::table('tbl_indikator_program')->where('tip_id',$id)->first();
		// dd($data);
        return view('backend.pengukurankinerja.editindikatorprogram',$data);
	}
	

	// CURD

	public function saveprogram(Request $request){ 
		$input = $request->all();
		DB::beginTransaction();
		try {
		DB::table('tbl_program_sasaran')->insert(
			[
				'idindisasaran' => $input['idindikatorsasaran'], 
				'tps_program_sasaran' => $input['program'], 
				'tps_keterangan' => $input['desc'], 
				'te_isi' => $input['iddinas'], 
				'tahun' => $input['tahun']
				]
		);
		DB::table('tbl_program_sasaran_log')->insert(
			[
				'idindisasaran' => $input['idindikatorsasaran'], 
				'tps_program_sasaran' => $input['program'], 
				'tps_keterangan' => $input['desc'], 
				'te_isi' => $input['iddinas'], 
				'tahun' => $input['tahun']
				]
		);

		DB::commit();
		$result = array( 'success' => true, 'message' => 'Program Sukses Tersimpan!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);	
	}

	public function saveindikatorprogram(Request $request){ 
		$input = $request->all();
		DB::beginTransaction();
		try {
		DB::table('tbl_indikator_program')->insert(
			[
				'idprogram' => $input['idprogram'], 
				'tip_indikator_program' => $input['program'], 
				'tip_keterangan' => $input['desc'], 
				'tip_satuan' => $input['satuan'], 
				'tip_target' => $input['target'], 
				'te_isi' => $input['iddinas'], 
				'tahun' => $input['tahun']
				]
		);
		DB::table('tbl_indikator_program_log')->insert(
			[
				'idprogram' => $input['idprogram'], 
				'tip_indikator_program' => $input['program'], 
				'tip_keterangan' => $input['desc'], 
				'tip_satuan' => $input['satuan'], 
				'tip_target' => $input['target'], 
				'te_isi' => $input['iddinas'], 
				'tahun' => $input['tahun']
				]
		);

		DB::commit();
		$result = array( 'success' => true, 'message' => 'Indikator Program Sukses Tersimpan!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);	
	}

	


	public function programedit(Request $request){ 
		$input = $request->all();
		DB::beginTransaction();
		try {
			
			DB::table('tbl_program_sasaran')
			->where('tps_id', $input['idprogram'])
			->update([
				'tps_program_sasaran' => $input['program'], 
				'tps_keterangan' => $input['desc'], 
			]);

			DB::table('tbl_program_sasaran_log')
			->where('tps_id', $input['idprogram'])
			->update([
				'tps_program_sasaran' => $input['program'], 
				'tps_keterangan' => $input['desc'], 
			]);

			DB::commit();
			$result = array( 'success' => true, 'message' => 'Program Sukses Teredit!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);	
	}
	
	
	public function indikatorprogramedit(Request $request){ 
		$input = $request->all();
		DB::beginTransaction();
		try {
			
			DB::table('tbl_indikator_program')
			->where('tip_id', $input['idprogram'])
			->update([
				'tip_indikator_program' => $input['program'], 
				'tip_keterangan' => $input['desc'], 
				'tip_satuan' => $input['desc'], 
				'tip_target' => $input['target'], 
			]);

			DB::table('tbl_indikator_program_log')
			->where('tip_id', $input['idprogram'])
			->update([
				'tip_indikator_program' => $input['program'], 
				'tip_keterangan' => $input['desc'], 
				'tip_satuan' => $input['desc'], 
				'tip_target' => $input['target'], 
			]);

			DB::commit();
			$result = array( 'success' => true, 'message' => 'Indikator Program Sukses Teredit!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);	
	}
	
	
	public function deleteprogram(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_program_sasaran')
				->where('tps_id', $input['id'])
				->update(['status' => 1]);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Program Sukses Terdelete!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
    }




	// LIST DATA


	public function listpic($id, $code){ 
		$hasil = DB::select('SELECT te_id, 
									te_jabatan,
									te_isi
								FROM tbl_eselon
								WHERE 
								iddinas = '.$id.'
								AND te_eselon = "'.$code.'"');
		return $hasil;
	}

	public function getDinas($id){ 
		if($this->user->role_id != 99){
			$id = $this->user->dinas_id;
		}
		$hasil = DB::table('tbl_dinas')->where('td_id',$id)->first();
		return $hasil;
	}


	public function listdinas(){ 
		$hasil = DB::select('SELECT td_id, 
									td_dinas, 
									td_keterangan 
								FROM tbl_dinas
								WHERE 
									status = 1 
								ORDER BY `index`');
		return $hasil;
	}
	public function listsasaran($id){
		$where = '';
		if($this->user->role_id != 99){
			$where = 'aa.`iddinas` = '.$this->user->dinas_id.' AND';
		}else{
			$where = 'aa.`iddinas` = '.$id.' AND';
		}

		$result4 	= DB::select('SELECT a.* FROM tbl_sasaran_tujuan a
		
								LEFT JOIN tbl_tujuan cc ON cc.tt_id = a.idtujuan
								LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
								LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
								WHERE '.$where.' tst_status = 0');
		return $result4;
	}
	public function listindikatorsasaran($id){
		$result6 	= DB::select('SELECT *, c.te_isi, c.te_jabatan FROM tbl_indikator_sasaran_pk a 
				
							LEFT JOIN tbl_iku b ON b.tis_id = a.tis_id 
							
							LEFT JOIN tbl_eselon c ON c.te_id = a.pic 
							WHERE idsasaran = '.$id.' AND tis_status = 0');
		return $result6;
	}
	public function getKegiatan($id){
		$hasil = array();

		$result1 	= DB::select('SELECT a.* 
		FROM tbl_program_sasaran a
		LEFT JOIN tbl_indikator_sasaran_pk b ON a.`idindisasaran` = b.`tis_id`
		LEFT JOIN tbl_sasaran_tujuan c ON b.`idsasaran` = c.`tst_id`
		WHERE c.`tst_id` = '.$id.' AND a.status = 0');

		foreach($result1 as $program){

			$result2 	= DB::select('SELECT a.* FROM tbl_indikator_program a
										WHERE a.idprogram = '.$program->tps_id.' AND a.`status` = 0');

			foreach($result2 as $indikatorprogram){
				if($program->tps_id == $indikatorprogram->idprogram){
					$result3 	= DB::select('SELECT a.*
					FROM tbl_kegiatan_program a
					WHERE a.idindiprogram = '.$indikatorprogram->tip_id.' AND a.`status` = 0');

					foreach($result3 as $kegiatan){
						if($kegiatan->idindiprogram == $indikatorprogram->tip_id){
							$result4 	= DB::select('SELECT a.*
							FROM tbl_indikator_kegiatan a
							WHERE a.idkegiatan = '.$kegiatan->tkp_id.' AND a.`tik_status` = 0');
							foreach($result4 as $indikatorkegiatan){
								if($kegiatan->tkp_id == $indikatorkegiatan->idkegiatan){
									$kegiatan->indikatorkegiatan = $result4;
								}
							}	
							$indikatorprogram->kegiatan[] = $kegiatan;
						}
					}	

					$program->indikatorprogram[] = $indikatorprogram;
				}
			}
			array_push($hasil, $program);
		}
		$hasil = (object) $hasil;
		return $hasil;
    }
	public function getProgram($id){
		$hasil = array();

		$result1 	= DB::select('SELECT a.* FROM tbl_indikator_sasaran_pk a 
							LEFT JOIN tbl_iku b ON b.tis_id = a.tis_id 
							LEFT JOIN tbl_eselon c ON c.te_id = a.pic 
							WHERE idsasaran = '.$id.' AND tis_status = 0');

		// echo '<pre>';
        foreach ($result1 as $indikatorsasaran) {
			$result2 = DB::select('SELECT a.*
									FROM tbl_program_sasaran a
									WHERE a.tahun = '.$_COOKIE['tahun'].' AND idindisasaran = '.$indikatorsasaran->tis_id.' AND status=0');
            foreach ($result2 as $program) {
				if($indikatorsasaran->tis_id == $program->idindisasaran){
					$result3 	= DB::select('SELECT a.*, c.te_isi, c.te_jabatan 
												FROM tbl_indikator_program a
												LEFT JOIN tbl_eselon c ON c.te_id = a.pic AND c.te_eselon = "3A" 
												WHERE a.idprogram = '.$program->tps_id.' AND a.`status` = 0');


					foreach($result3 as $indikatorprogram){
						
						
						if($program->tps_id == $indikatorprogram->idprogram){
							
							$program->indikatorprogram = $result3;
						}
					}	
					$indikatorsasaran->program[] = $program;
				}
				
            }
			array_push($hasil, $indikatorsasaran);
		}
		// print_r($hasil);
		// die;
		$hasil = (object) $hasil;
		return $hasil;
	}

	public function getProgramJson($id){
		$hasil = array();

		$result1 	= DB::select('SELECT a.* FROM tbl_indikator_sasaran_pk a 
							LEFT JOIN tbl_iku b ON b.tis_id = a.tis_id 
							LEFT JOIN tbl_eselon c ON c.te_id = a.pic 
							WHERE idsasaran = '.$id.' AND tis_status = 0');

        foreach ($result1 as $indikatorsasaran) {
			$result2 = DB::select('SELECT a.*
									FROM tbl_program_sasaran a
									WHERE a.tahun = '.$_COOKIE['tahun'].' AND idindisasaran = '.$indikatorsasaran->tis_id.' AND status=0');
            foreach ($result2 as $program) {
				if($indikatorsasaran->tis_id == $program->idindisasaran){
					$result3 	= DB::select('SELECT a.*, c.te_isi, c.te_jabatan 
												FROM tbl_indikator_program a
												LEFT JOIN tbl_eselon c ON c.te_id = a.pic AND c.te_eselon = "3A" 
												WHERE a.idprogram = '.$program->tps_id.' AND a.`status` = 0');


					foreach($result3 as $indikatorprogram){
						
						
						if($program->tps_id == $indikatorprogram->idprogram){
							
							$program->indikatorprogram = $result3;
						}
					}	
					$indikatorsasaran->program[] = $program;
				}
				
            }
			array_push($hasil, $indikatorsasaran);
		}
		$hasil = (object) $hasil;
		return $hasil;
	}

	public function listsasaranbak($id){ 
		$hasil = array();
		$where = '';
		if($this->user->role_id != 99){
			$where = 'aa.`iddinas` = '.$this->user->dinas_id.' AND';
		}else{
			$where = 'aa.`iddinas` = '.$id.' AND';
		}

		$result4 	= DB::select('SELECT a.* FROM tbl_sasaran_tujuan a
		
								LEFT JOIN tbl_tujuan cc ON cc.tt_id = a.idtujuan
								LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
								LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
								WHERE '.$where.' tst_status = 0');
		
		
		// dd($result4);
		
		foreach($result4 as $new4){

				$result6 	= DB::select('SELECT *, c.te_isi, c.te_jabatan FROM tbl_indikator_sasaran a 
				
											LEFT JOIN tbl_iku b ON b.tis_id = a.tis_id 
											
	 										LEFT JOIN tbl_eselon c ON c.te_id = a.pic 
											WHERE idsasaran = '.$new4->tst_id.' AND tis_status = 0');

				foreach($result6 as $new6){
					
					
					if($new4->tst_id == $new6->idsasaran){
						
						$new4->indikatorsasaran = $result6;
					}
				}
			
			array_push($hasil, $new4);
		}
		$hasil = (object) $hasil;
		return $hasil;
	}

	public function listprogram($id){ 
		$where = '';
		if($this->user->role_id != 99){
			$where = 'AND a.`te_isi` = '.$this->user->dinas_id.' ';
		}else{
			$where = 'AND a.`te_isi` = '.$id.' ';
		}
		$hasil = [];
		$result4 = DB::select('SELECT  a.tps_id, a.tps_program_sasaran, a.tps_keterangan, a.te_isi
		FROM tbl_program_sasaran a
		WHERE a.tahun = '.$_COOKIE['tahun'].' '.$where);

		foreach($result4 as $new4){

			$result6 	= DB::select('SELECT a.*, c.te_jabatan FROM tbl_indikator_program a
										LEFT JOIN tbl_eselon c ON c.te_id = a.pic 
										WHERE a.idprogram = '.$new4->tps_id.' AND a.`status` = 0');

			foreach($result6 as $new6){
				
				
				if($new4->tps_id == $new6->idprogram){
					
					$new4->indikatorprogram = $result6;
				}
			}

		array_push($hasil, $new4);
		}
		$hasil = (object) $hasil;

		return $hasil;
	}

	public function listkegiatan($id){ 
		$where = '';
		if($this->user->role_id != 99){
			$where = 'AND a.`te_isi` = '.$this->user->dinas_id.' ';
		}else{
			$where = 'AND a.`te_isi` = '.$id.' ';
		}
		$hasil = [];
		$result4 = DB::select('SELECT  a.tkp_id, a.tkp_kegiatan_program, a.tkp_keterangan, a.te_isi
		FROM tbl_kegiatan_program a
		WHERE a.tahun = '.$_COOKIE['tahun'].' '.$where);

		foreach($result4 as $new4){

			$result6 	= DB::select('SELECT a.*, c.te_jabatan, d.ta_anggaran FROM tbl_indikator_kegiatan a
										LEFT JOIN tbl_eselon c ON c.te_id = a.pic 
										LEFT JOIN tbl_anggaran d on a.tik_id = d.idindkegiatan
										WHERE a.idkegiatan = '.$new4->tkp_id.' AND a.`tik_status` = 0');

			foreach($result6 as $new6){
				
				
				if($new4->tkp_id == $new6->idkegiatan){
					
					$new4->indikatorkegiatan = $result6;
				}
			}

		array_push($hasil, $new4);
		}
		$hasil = (object) $hasil;

		return $hasil;
	}

	public function saveindikatorsasaranpic(Request $request){ 
		$input = $request->all();
		DB::beginTransaction();
		try {
			
			DB::table('tbl_indikator_sasaran')
			->where('tis_id', $input['idindikatorsasaran'])
			->update([
						'pic' => $input['pic']
				]
			);
			 DB::table('tbl_indikator_sasaran_pk')
			->where('tis_id', $input['idindikatorsasaran'])
			->update([
						'pic' => $input['pic']
				]
			);
			DB::commit();
			$result = array( 'success' => true, 'message' => 'PIC Sukses Terpilih!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);	
	}
	public function saveindikatorprogrampic(Request $request){ 
		$input = $request->all();
		// dd($input);
		DB::beginTransaction();
		try {
			
			DB::table('tbl_indikator_program')
			->where('tip_id', $input['idindikatorsasaran'])
			->update([
						'pic' => $input['pic']
				]
			);
			DB::commit();
			$result = array( 'success' => true, 'message' => 'PIC Sukses Terpilih!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);	
	}
	public function saveindikatorkegiatanpic(Request $request){ 
		$input = $request->all();
		DB::beginTransaction();
		try {
			
			DB::table('tbl_indikator_kegiatan')
			->where('tik_id', $input['idindikatorsasaran'])
			->update([
						'pic' => $input['pic']
				]
			);
			DB::commit();
			$result = array( 'success' => true, 'message' => 'PIC Sukses Terpilih!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);	
	}
	
	
}
