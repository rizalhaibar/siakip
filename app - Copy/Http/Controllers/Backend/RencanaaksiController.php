<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserValidation;
use App\Models\Role;
use Carbon\Carbon;

use DB;
/**
 * Class UserController.
 */
class RencanaaksiController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
	 
	
	function __construct()
	{
		$this->middleware(['auth']);
		
        $this->middleware(function ($request, $next)
        {
            $this->user = session('data')->user;
            return $next($request);
        });
	}
	
	public function index()
    {
		if($this->user->role_id == 99 || $this->user->role_id == 97){
			$data['breadcrumb'] = [
				'parent' => 'Rencana Aksi',
				'child' => 'List Dinas'
			];
			$data['listdinas'] = $this->listdinas();

			return view('backend.rencanaaksi.index', $data);
		}else{
			$data['breadcrumb'] = [
				'parent' => 'Rencana Aksi',
				'child' => 'List Rencana Aksi'
			];
			$data['listsasaran'] = $this->listsasaran();

			return view('backend.rencanaaksi.home', $data);
		}
	}

	public function home()
    {
		$data['breadcrumb'] = [
			'parent' => 'Rencana Aksi',
			'child' => 'List Rencana Aksi'
		];
		$data['listsasaran'] = $this->listsasaran();

		return view('backend.rencanaaksi.home', $data);
	}

	public function listdinas(){ 
		$hasil = DB::select('SELECT td_id, 
									td_dinas, 
									td_keterangan 
								FROM tbl_dinas
								WHERE 
									status = 1 
								ORDER BY `index`');
		return $hasil;
	}

	
	public function listsasaran(){ 
		$where = '';
		if($this->user->dinas_id != 1){
			$where = 'aa.`iddinas` = '.$this->user->dinas_id.' AND';
		}
		$hasil = DB::select('SELECT  a.tst_id, a.tst_sasaran_tujuan, a.tst_keterangan
		FROM tbl_sasaran_tujuan a
		LEFT JOIN tbl_tujuan cc ON cc.tt_id = a.idtujuan
		LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
		LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
		WHERE '.$where.' aa.tahun = '.$_COOKIE['tahun']);
		return $hasil;
	}
}
