<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Person\PersonRepositoryInterface;
use App\Repositories\Department\DepartmentRepositoryInterface;
use App\Repositories\Company\CompanyRepositoryInterface;
use App\Repositories\Param\ParamRepositoryInterface;
use App\Traits\FlashMessageTraits;

class PersonController extends Controller
{
    use FlashMessageTraits;

    function __construct(
        PersonRepositoryInterface $personRepo,
        DepartmentRepositoryInterface $departmentRepo,
        CompanyRepositoryInterface $companyRepo,
        ParamRepositoryInterface $paramRepo
    ) {
        // REPO
        $this->personRepo = $personRepo;
        $this->departmentRepo = $departmentRepo;
        $this->companyRepo = $companyRepo;
        $this->paramRepo = $paramRepo;
        
        // SESSION
        $this->middleware(function ($request, $next)
        {
            $this->user = session('data')->user;
            return $next($request);
        });

        $this->__setup();
    }

    /**
     * Setting everything of variables
     * in one function
     */
    private function __setup()
    {   
        // VAR
        $this->PAGE_LIMIT = config('setting.pagination.limit');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $request = request();

        // If limit doesnt set
        if(!$request->limit) { $request->limit = $this->PAGE_LIMIT; }

        // If this wasnt Admin
        if( !$this->isAdmin($this->user->role_id) )
        {
            $request->company_id = $this->user->company_id;
        }
        
        $persons = $this->personRepo->pagination($request);
        $status = $this->personRepo->status();
        $searchBy = $this->paramRepo->search('codename', 'SEARCH_BY_IN_PERSON')->get();

        return
            view('backend.person.index')
            ->with('persons', $persons)
            ->with('status', $status)
            ->with('searchBy', $searchBy)
        ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = $this->companyRepo->companies( config('setting.status.active') )->get();
        $departments = $this->departmentRepo->departments( config('setting.status.active') );

        // If this wasnt Admin
        if( !$this->isAdmin($this->user->role_id) ) {
            $departments = $departments->where('company_id', $this->user->company_id);
        }
        
        $departments = $departments->get();
        
        return
            view('backend.person.create')
            ->with('companies', $companies)
            ->with('departments', $departments)
            ->with('admin', $this->isAdmin($this->user->role_id))
        ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        // Check if company id sent by request or not
        if( !isset($request->company_id) ) { $request->merge(['company_id' => $this->user->company_id ]); }

        $response = $this->personRepo->store($request);
        $this->message($response->level, $response->message);

        if(isset($request->cookie_key)) {
            return redirect()->route('backend.person.attach', $request->cookie_key);
        }
        
        return redirect()->route('backend.person.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $id = $this->decryptingId($id);

        $detail = $this->personRepo->find($id);
        $companies = $this->companyRepo->companies( config('setting.status.active') )->get();
        $departments = $this->departmentRepo->departments( config('setting.status.active') );

        // If this wasnt Admin
        if( !$this->isAdmin($this->user->role_id) ) {
            $departments = $departments->where('company_id', $this->user->company_id);
        }

        $departments = $departments->get();

        return
            view('backend.person.edit')
            ->with('detail', $detail)
            ->with('companies', $companies)
            ->with('departments', $departments)
            ->with('admin', $this->isAdmin($this->user->role_id))
        ;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $id = $this->decryptingId($id);

        // Check if company id sent by request or not
        if( !isset($request->company_id) ) { $request->merge(['company_id' => $this->user->company_id ]); }
        
        $response = $this->personRepo->update($id, $request);
        $this->message($response->level, $response->message);
        return redirect()->route('backend.person.index');
    }

    /**
     * Toggle Status.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function toggle(Request $request)
    {
        //
        $response = $this->personRepo->toggle($request);
        $this->message($response->level, $response->message);
        return back();
    }

    /**
     * Show the form for attaching person to the department.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function attach($id)
    {
        //
        $id = $this->decryptingId($id);
        
        $department = $this->departmentRepo->find($id);
        $persons = $this->personRepo->search("department_id", $id)->paginate(config('setting.paging.limit'));
        $available_persons = $this->personRepo->persons( config('setting.status.active') )->whereNull('department_id')->where('company_id', $department->company_id)->get();
        
        return
            view('backend.person.attach')
            ->with('department', $department)
            ->with('persons', $persons)
            ->with('available_persons', $available_persons)
        ;
    }

    /**
     * Show the form for attach or dettach person to the department.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function do(Request $request)
    {
        //
        $response = $this->personRepo->do($request);
        $this->message($response->level, $response->message);
        return back();
    }

    /**
     * Import person from xls, csv.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        // Validate Data Request
        $request->validate([
            'file' => 'required|file',
            'department_id' => 'required',
            'company_id' => 'required'
        ]);

        $response = $this->personRepo->import($request);
        $this->message($response->level, $response->message);
        return back();
    }

}
