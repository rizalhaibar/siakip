<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserValidation;
use App\Models\Role;
use Carbon\Carbon;
use DB;
/**
 * Class UserController.
 */
class RpjmdController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
	 
	function __construct()
	{
		$this->middleware(['auth']);
		
        $this->middleware(function ($request, $next)
        {
            $this->user = session('data')->user;
            return $next($request);
        });
	}
	
	public function index()
    {
		if($this->user->role_id == 99 || $this->user->role_id == 97){
			$data['breadcrumb'] = [
				'parent' => 'Perencanaan Kinerja',
				'child' => 'RPJMD'
			];
			$data['listdinas'] = $this->listdinas();

			return view('backend.rpjmd.index', $data);
		}else{
			if($this->user->dinas_id == 1){
				$data['breadcrumb'] = [
					'parent' => 'RPJMD',
					'child' => 'List Visi & Misi'
				];
			}else{
				$data['breadcrumb'] = [
					'parent' => 'Renstra',
					'child' => 'List Visi & Misi'
				];
			}
			$data['listdinas'] = $this->listdinas();
			$id = $this->user->dinas_id;
			$tahun = $_COOKIE['tahun'];
			$data['id'] = $this->user->dinas_id;
			$data['tahun'] = $_COOKIE['tahun'];
		
			$data['id_role'] = $this->user->role_id;
			$data['visi'] = $this->detailvisi($id,$tahun);
			$data['dinas'] = $this->detaildinas($id);

			return view('backend.rpjmd.homesuper', $data);
		}
    }
	
	public function masuk($id, $tahun)
    {
			if($id == 1){
				$data['breadcrumb'] = [
					'parent' => 'RPJMD',
					'child' => 'Detail'
				];
			}else{
				$data['breadcrumb'] = [
					'parent' => 'RENSTRA',
					'child' => 'Detail'
				];
			}
			
			$data['listdinas'] = $this->listdinas();
			
			$data['id'] = $id;
			$data['tahun'] = $tahun;
		
			$data['id_role'] = $this->user->role_id;
			$data['visi'] = $this->detailvisi($id,$tahun);
			$data['dinas'] = $this->detaildinas($id);
			return view('backend.rpjmd.homesuper', $data);
	
    }
	
	
	public function tujuan($id, $tahun, $ids)
    {
		$data['breadcrumb'] = [
            'parent' => 'Perencanaan Kinerja - RPJMD',
            'child' => 'Tujuan'
        ];
		$data['id'] = $id;
		$data['tahun'] = $tahun;
		$data['ids'] = $ids;
		$data['listdinas'] = $this->listdinas();

		$data['sasaran'] = $this->sasaran($id,$tahun);
		$data['indikatortujuan'] = $this->indikatortujuan($id,$tahun);
		$data['misidetail'] = $this->misi($id,$tahun);
		return view('backend.rpjmd.tujuan', $data);
    }
	
    public function tujuandetail($id, $iddinas)
    {
		$query = DB::select("SELECT * FROM tbl_tujuan WHERE tt_id = ".$id);
		$query2 = DB::select("SELECT SQL_CALC_FOUND_ROWS tit_id, tit_indikator_tujuan, tit_keterangan, tit_satuan, tit_target 
		FROM tbl_indikator_tujuan
		  WHERE idtujuan =  ".$id);
		$data['tujuandetail'] = $query;
		$data['listindikator'] = $query2;
        $data['id'] = $id;
		$data['iddinas'] = $iddinas;
        return view('backend.rpjmd.detailtujuan',$data);
    }
    public function visidetail($id, $iddinas)
    {
		$query = DB::select("SELECT * FROM tbl_visi WHERE tv_id = ".$id);
		$data['visidetail'] = $query;
        $data['id'] = $id;
		$data['iddinas'] = $iddinas;
		
        return view('backend.rpjmd.detailvisi',$data);
    }
    public function sasarandetail($id, $iddinas)
    {
		$query = DB::select("SELECT * FROM tbl_sasaran_tujuan WHERE tst_id = ".$id);
		$data['sasarandetail'] = $query;
        $data['id'] = $id;
		$data['iddinas'] = $iddinas;

        return view('backend.rpjmd.detailsasaran',$data);
    }
    public function tambahmisi($id, $iddinas)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		
        return view('backend.rpjmd.tambahmisi',$data);
    }
	
    public function tambahtujuan($id, $iddinas)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		
        return view('backend.rpjmd.tambahtujuan',$data);
    }
	
    public function tambahindikatortujuan($id, $iddinas)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		
        return view('backend.rpjmd.tambahindikatortujuan',$data);
	}
	
    public function tambahsasaran($id, $iddinas)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		
        return view('backend.rpjmd.tambahsasaran',$data);
    }
    public function tambahindikatorsasaran($id, $iddinas)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		
        return view('backend.rpjmd.tambahindikatorsasaran',$data);
    }
    public function tambahprogram($id, $iddinas)
    {
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		
        return view('backend.rpjmd.tambahprogram',$data);
    }
    public function detailtujuanindikator($id, $iddinas)
    {
		$query = DB::select("SELECT * FROM tbl_indikator_tujuan WHERE tit_id = ".$id);
		$data['indikatortujuandetail'] = $query;
		$data['id'] = $id;
		$data['iddinas'] = $iddinas;
		
        return view('backend.rpjmd.detailtujuanindikator',$data);
    }
	
	public function misidetail($id, $iddinas)
    {
		$query = DB::select("SELECT * FROM tbl_misi WHERE tm_id = ".$id);
		$data['misidetail'] = $query;
        $data['id'] = $id;
		$data['iddinas'] = $iddinas;

		return view('backend.rpjmd.detailmisi', $data);
	}
	public function detailsasaranindikator($id, $iddinas)
    {
		$query = DB::select("SELECT * FROM tbl_indikator_sasaran WHERE tis_id = ".$id);
		$data['indikatorsasarandetail'] = $query;
        $data['id'] = $id;
		$data['iddinas'] = $iddinas;

		return view('backend.rpjmd.detailsasaranindikator', $data);
	}
	public function program($id, $iddinas)
    {
		$data['breadcrumb'] = [
			'parent' => 'Perncanaan Kinerja',
			'child' => 'List Program & Kegiatan'
		];
		$indisasaran = DB::table("tbl_indikator_sasaran")->where('tis_id', $id)->first();
		$sasaran = DB::table("tbl_sasaran_tujuan")->where('tst_id', $indisasaran->idsasaran)->first();
		$tujuan = DB::table("tbl_tujuan")->where('tt_id', $sasaran->idtujuan)->first();
		$misi = DB::table("tbl_misi")->where('tm_id', $tujuan->idmisi)->first();
		$visi = DB::table("tbl_visi")->where('tv_id', $misi->idvisi)->first();
		$data['program'] = $this->detailprogram($id,$_COOKIE['tahun']);
		$data['indisasaran'] = $indisasaran;
		$data['sasaran'] = $sasaran;
		$data['tujuan'] = $tujuan;
		$data['misi'] = $misi;
		$data['visi'] = $visi;
        $data['id'] = $id;
		$data['iddinas'] = $iddinas;

		return view('backend.rpjmd.program', $data);
	}
	
	
   
	public function sasaran($id,$tahun){ 
		$result = DB::select('SELECT * FROM tbl_sasaran_tujuan a
								LEFT JOIN tbl_tujuan b ON b.`tt_id` = a.`idtujuan`
								WHERE a.`tahun` = "'.$tahun.'"
								AND b.`idmisi` = '.$id);
		
		$hasil = array();
		foreach($result as $new){
            $result2 = DB::select('SELECT * FROM tbl_indikator_sasaran WHERE idsasaran ='.$new->tst_id);
            foreach($result2 as $new2){
                
                if($new->tst_id == $new2->idsasaran){
                    $new->indikatorsasaran = $result2;
                }
            }
            array_push($hasil, $new);
		}
		$hasil = (object) $hasil;
		return $hasil;
	}
   
	public function indikatortujuan($id,$tahun){ 
		$hasil = DB::select('SELECT * FROM tbl_indikator_tujuan a
							LEFT JOIN tbl_tujuan b ON b.`tt_id` = a.`idtujuan`
							WHERE a.`tahun` = "'.$tahun.'"
							AND b.`idmisi` = '.$id);
		return $hasil;
	}
	public function misi($id,$tahun){ 
		$result5 = DB::select('SELECT * FROM tbl_misi WHERE tm_id =  "'.$id.'" AND tahun = '.$tahun);
		$hasil = array();
        $tujuan = array();
        foreach($result5 as $new){
            $result2 	= DB::select('SELECT * FROM tbl_tujuan WHERE idmisi = '.$new->tm_id);
            foreach($result2 as $new2){
				$result4 	= DB::select('SELECT * FROM tbl_sasaran_tujuan WHERE idtujuan = '.$new2->tt_id);

                foreach($result4 as $new4){

                    if($new2->tt_id == $new4->idtujuan){
                        $new2->sasaran = $result4;
                    }
                }
                
                 array_push($tujuan, $new2);
                
                if($new->tm_id == $new2->idmisi){
                    $new->tujuan = $tujuan;
                }
            }
            array_push($hasil, $new);
        }
        return $hasil;
	}
	public function listdinas(){ 
		$hasil = DB::select('SELECT td_id, 
									td_dinas, 
									td_keterangan 
								FROM tbl_dinas
								WHERE 
									status = 1 
								
									ORDER BY `index` asc');
		return $hasil;
	}
	
	public function listmisi($id){ 
		$hasil = DB::select('SELECT * 
								FROM tbl_misi
								WHERE idvisi = '.$id.' AND status = 1');
		return $hasil;
    }
	
   
	public function detailvisi($id,$tahun){ 
		$result = DB::select('SELECT * FROM tbl_visi WHERE iddinas = '.$id.' AND tahun ='.$tahun.' AND status = 0');
		$tujuan = array();
		$misi = array();
		$sasaran = array();
		$hasil = array();
		foreach($result as $new){
            $result2 = DB::select('SELECT * FROM tbl_misi WHERE idvisi = '.$new->tv_id.' AND status = 0');
            foreach($result2 as $new2){
                
                if($new->tv_id == $new2->idvisi){
					
					$result3 	= DB::select('SELECT * FROM tbl_tujuan WHERE idmisi = '.$new2->tm_id.' AND status = 0');
					foreach($result3 as $new3){
						$result4 	= DB::select('SELECT * FROM tbl_sasaran_tujuan WHERE idtujuan = '.$new3->tt_id.' AND tst_status = 0');
		
						foreach($result4 as $new4){
		
							if($new3->tt_id == $new4->idtujuan){
								$result6 	= DB::select('SELECT * FROM tbl_indikator_sasaran WHERE idsasaran = '.$new4->tst_id.' AND tis_status = 0');
				
								foreach($result6 as $new6){
									
									
									if($new4->tst_id == $new6->idsasaran){
										
										$new4->indikatorsasaran = $result6;
									}
								}
								
								array_push($sasaran, $new4);
								$new3->sasaran = $sasaran;
							}
						}
						$result5 	= DB::select('SELECT * FROM tbl_indikator_tujuan WHERE idtujuan = '.$new3->tt_id.' AND tit_status = 0');
		
						foreach($result5 as $new5){
		
							if($new3->tt_id == $new5->idtujuan){
								$new3->indikator = $result5;
							}
						}
						
							array_push($tujuan, $new3);
						
						if($new2->tm_id == $new3->idmisi){
							$new2->tujuan = $tujuan;
						}
					}

					array_push($misi, $new2);
					
					$new->misi = $misi;
					
                }
            }
            array_push($hasil, $new);
		}
		$hasil = (object) $hasil;
		return $hasil;
    }
	public function detailprogram($id,$tahun){ 
		$result = DB::select('SELECT * FROM tbl_program_sasaran WHERE idindisasaran = '.$id.' AND tahun ='.$tahun.' AND status = 0');
		
		$indikatorprogram = array();
		$kegiatan = array();
		$indikatorkegiatan = array();
		$hasil = array();
		foreach($result as $new){
            $result2 = DB::select('SELECT * FROM tbl_indikator_program WHERE idprogram = '.$new->tps_id.' AND status = 0');
			
			foreach($result2 as $new2){
                
                if($new->tps_id == $new2->idprogram){
					
					
					
					$result3 	= DB::select('SELECT * FROM tbl_kegiatan_program WHERE idindiprogram = '.$new2->tip_id.' AND status = 0');
					foreach($result3 as $new3){
						$result5 	= DB::select('SELECT * FROM tbl_indikator_kegiatan WHERE idkegiatan = '.$new3->tkp_id.' AND tik_status = 0');
		
						foreach($result5 as $new5){
		
							if($new3->tkp_id == $new5->idkegiatan){
								$new3->indikatorkegiatan = $result5;
							}
						}
						
						
						if($new2->tip_id == $new3->idindiprogram){
							$new2->kegiatan[] = $new3;
						}
					}

					
					$new->indikatorprogram[] = $new2;
				}
			}
			
            array_push($hasil, $new);
		}
		$hasil = (object) $hasil;
		return $hasil;
    }
	
   
	public function detaildinas($id){ 
		$hasil = DB::select('SELECT * FROM tbl_dinas WHERE td_id = '.$id);
		return $hasil;
	}
	

	public function deletevisi(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_visi')
				->where('tv_id', $input['id'])
				->update(['status' => 1]);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Visi Sukses Terdelete!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
    }

	public function deletemisi(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_misi')
				->where('tm_id', $input['id'])
				->update(['status' => 1]);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Misi Sukses Terdelete!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}
	public function deletetujuan(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_tujuan')
				->where('tt_id', $input['id'])
				->update(['status' => 1]);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Tujuan Sukses Terdelete!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}
	public function deleteinditujuan(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_indikator_tujuan')
				->where('tit_id', $input['id'])
				->update(['tit_status' => 1]);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Indikator Tujuan Sukses Terdelete!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}
	public function deletesasaran(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_sasaran_tujuan')
				->where('tst_id', $input['id'])
				->update(['tst_status' => 1]);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Indikator Tujuan Sukses Terdelete!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}
	public function deleteindikatorsasaran(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_indikator_sasaran')
				->where('tis_id', $input['id'])
				->update(['tis_status' => 1]);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Indikator Sasaran Sukses Terdelete!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}
	
	public function savetujuan(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_tujuan')->insert(
			['idmisi' => $input['idmisi'], 'tt_tujuan' => $input['tujuan'], 'tt_keterangan' => $input['desc'], 'tahun' => $_COOKIE['tahun']]
		);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Tujuan Sukses Tersimpan!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}

	public function saveindikatortujuan(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_indikator_tujuan')->insert(
			[
				'idtujuan' => $input['idtujuan'], 
				'tit_indikator_tujuan' => $input['inditujuan'], 
				'tit_keterangan' => $input['descinditujuan'], 
				'tit_satuan' => $input['satuan'], 
				'tit_target' => $input['target'], 
				'tit_status' => 0, 
				'tahun' => $_COOKIE['tahun']
				]
		);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Indikator Tujuan Sukses Tersimpan!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}

	

	public function savesasaran(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_sasaran_tujuan')->insert(
			[
				'idtujuan' => $input['idtujuan'], 
				'tst_sasaran_tujuan' => $input['sasaran'], 
				'tst_keterangan' => $input['desc'], 
				'tst_iku' => 0, 
				'tst_status' => 0, 
				'tahun' => $_COOKIE['tahun']
				]
		);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Sasaran Sukses Tersimpan!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}

	public function savevisi(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_visi')->insert(
			['iddinas' => $input['iddinas'], 'tv_visi' => $input['visi'], 'tv_keterangan' => $input['desc'], 'tahun' => $input['tahun']]
		);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Visi Sukses Tersimpan!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}
	
	public function savemisi(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_misi')->insert(
			['idvisi' => $input['idvisi'], 'tm_misi' => $input['misi'], 'tm_keterangan' => $input['desc'], 'tahun' => $input['tahun']]
		);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Misi Sukses Tersimpan!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}
	public function saveprogram(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_program_sasaran')->insert(
			['idindisasaran' => $input['idindikatorsasaran'], 'tps_program_sasaran' => $input['program'], 'tps_keterangan' => $input['desc'], 'tahun' => $input['tahun']]
		);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Program Sukses Tersimpan!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}
	
	public function editvisi(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_visi')
			->where('tv_id', $input['idvisi'])
			->update(['tv_visi' => $input['visi'], 'tv_keterangan' => $input['desc']]
		);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Visi Sukses Terupdate!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}
	
	public function editmisi(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_misi')
			->where('tm_id', $input['idvisi'])
			->update(['tm_misi' => $input['visi'], 'tm_keterangan' => $input['desc']]
		);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Misi Sukses Terupdate!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
    }
	public function edittujuan(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_tujuan')
			->where('tt_id', $input['idmisi'])
			->update(['tt_tujuan' => $input['tujuan'], 'tt_keterangan' => $input['desc']]
		);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Tujuan Sukses Terupdate!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
    }
	public function editsasaran(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_sasaran_tujuan')
			->where('tst_id', $input['idsasaran'])
			->update(['tst_sasaran_tujuan' => $input['sasaran'], 'tst_keterangan' => $input['desc']]
		);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Sasaran Sukses Terupdate!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
    }
	public function editindikatortujuan(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_indikator_tujuan')
			->where('tit_id', $input['idtujuan'])
			->update([
						'tit_indikator_tujuan' => $input['inditujuan'], 
						'tit_keterangan' => $input['desc'],
						'tit_satuan' => $input['satuan'],
						'tit_target' => $input['target']
				]
		);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Indikator Tujuan Sukses Terupdate!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
    }
	
	public function saveindikatorsasaran(Request $request){ 
		$input = $request->all();
		DB::beginTransaction();
		try {
			
			DB::table('tbl_indikator_sasaran')->insert(
				[
					'idsasaran' => $input['idsasaran'], 
					'tis_indikator_sasaran' => $input['indisasaran'], 
					'tis_keterangan' => $input['descindisasaran'], 
					'tis_satuan' => $input['satuan'], 
					'tis_target' => $input['target'], 
					'tis_iku' => $input['iku'], 
					'formula' => $input['formula'], 
					'tis_tahun' => 1,
					'tahun' => $_COOKIE['tahun']
					]
			);
			$id 		= DB::getPdo()->lastInsertId();
			DB::table('tbl_iku')->insert(
				[
					'tis_id' => $id, 
					'formula' => $input['formula'], 
					'tahun' => $_COOKIE['tahun']
					]
			);
			DB::table('tbl_rencana_aksi')->insert(
				[
					'tis_id' => $id
				]
			);

			DB::table('tbl_indikator_sasaran_pk')->insert(
				[
					'idsasaran' => $input['idsasaran'], 
					'tis_indikator_sasaran' => $input['indisasaran'], 
					'tis_keterangan' => $input['descindisasaran'], 
					'tis_satuan' => $input['satuan'], 
					'tis_target' => $input['target'], 
					'tis_iku' => $input['iku'], 
					'formula' => $input['formula'], 
					'tis_tahun' => 1,
					'tahun' => $_COOKIE['tahun']
					]
			);
			DB::commit();
			$result = array( 'success' => true, 'message' => 'Indikator Sasaran Sukses Tersimpan!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);	
	}

	public function editindikatorsasaran(Request $request){ 
		$input = $request->all();
		DB::beginTransaction();
		try {
			DB::table('tbl_indikator_sasaran')
				->where('tis_id', $input['idsasaran'])
				->update([
							'tis_indikator_sasaran' => $input['indisasaran'], 
							'tis_keterangan' => $input['desc'],
							'tis_satuan' => $input['satuan'],
							'tis_iku' => $input['iku'],
							'formula' => $input['formula']
					]
			);
			DB::table('tbl_iku')
				->where('tis_id', $input['idsasaran'])
				->update([
							'formula' => $input['formula']
					]
			);

			DB::table('tbl_indikator_sasaran_pk')
				->where('tis_id', $input['idsasaran'])
				->update([
							'tis_indikator_sasaran' => $input['indisasaran'], 
							'tis_keterangan' => $input['desc'],
							'tis_satuan' => $input['satuan'],
							'tis_iku' => $input['iku'],
							'formula' => $input['formula']
					]
			);
			DB::commit();
			$result = array( 'success' => true, 'message' => 'Indikator Sasaran Sukses Terupdate!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
		return response()->json($result);	
	}
	
	public function rubahiku(Request $request){ 
		$input = $request->all();
		DB::beginTransaction();
		try {
			
			DB::table('tbl_indikator_sasaran')
			->where('tis_id', $input['id'])
			->update([
						'tis_iku' => $input['status']
			]);

			DB::table('tbl_indikator_sasaran_pk')
			->where('tis_id', $input['id'])
			->update([
						'tis_iku' => $input['status']
			]);

			DB::commit();
			if($input['status'] == "T"){
				$result = array( 'success' => true, 'message' => 'IKU Sukses Aktif!');
			}
			$result = array( 'success' => true, 'message' => 'IKU Sukses Non Aktif!');
		}catch(\Illuminate\Database\QueryException $ex) {
			DB::rollback();
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
		}
    }
	
}
