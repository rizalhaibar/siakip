<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserValidation;
use App\Models\Role;
use Carbon\Carbon;
use DB;
/**
 * Class UserController.
 */
class LaporankinerjaController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
	 
	function __construct()
	{
		$this->middleware(['auth']);
	}
	
	public function index()
    {
		$data['breadcrumb'] = [
            'parent' => 'Laporan Kinerja',
            'child' => 'List Laporan Kinerja'
		];
		$data['listdinas'] = $this->listdinas();
		return view('backend.laporankinerja.index', $data);
	}
	
	public function detail($id)
    {
		$data['breadcrumb'] = [
            'parent' => 'Detail Laporan Kinerja',
            'child' => ''
		];
		$data['id'] = $id;
		$data['sasaran'] = $this->listsasaran($id);
		$data['laporan'] = $this->listlaporan($id);
		return view('backend.laporankinerja.detail', $data);
	}
	
	
   
	public function listdinas(){ 
		$hasil = DB::select('SELECT td_id, 
									td_dinas, 
									td_keterangan 
								FROM tbl_dinas
								WHERE 
									status = 1 
								ORDER BY td_dinas');
		return $hasil;
	}

	public function listsasaran($id){ 
		$hasil = DB::select('SELECT * FROM tbl_sasaran_tujuan WHERE tst_id = '.$id);
		return $hasil;
	}

	public function listlaporan($id){ 
		$hasil = DB::select('SELECT * FROM tbl_pelaporan WHERE idsasaran = '.$id);
		return $hasil;
	}
	
}
