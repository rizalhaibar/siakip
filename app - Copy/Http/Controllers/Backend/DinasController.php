<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserValidation;
use App\Models\Role;
use Carbon\Carbon;
use DB;
/**
 * Class UserController.
 */
class DinasController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
	 
	function __construct()
	{
		$this->middleware(['auth']);
	}
	
	public function index()
    {
		$data['breadcrumb'] = [
            'parent' => 'Dinas',
            'child' => ''
		];
		$data['listdinas'] = $this->listdinas();
		return view('backend.dinas.index', $data);
	}
	
	public function eselon($id)
    {
		$data['breadcrumb'] = [
            'parent' => 'Dinas',
            'child' => 'Perangkat Daerah'
		];
		$data['id'] = $id;
		$data['dinas'] = $this->dinas($id);
		$data['eselon'] = $this->eselons($id);
		$data['eselonisi'] = $this->eselonisi($id);
		return view('backend.dinas.eselon', $data);
	}

	
	public function tambaheselon($id)
    {
		$data['id'] = $id;
		$data['dinas'] = $this->dinas($id);
		$data['eselon'] = $this->eselons($id);
		$data['eselonisi'] = $this->eselonisi($id);
		return view('backend.dinas.tambaheselon', $data);
	}
	
	public function addeselon($id, $ids)
    {
		$query = DB::select("SELECT * FROM tbl_eselon WHERE te_id = ".$id." AND status = 1");
		$data['detaileselon'] = $query;
        $data['id'] = $id;
		$data['iddinas'] = $ids;
		return view('backend.dinas.detaileselon', $data);
	}
	public function addeselon3($id, $ids)
    {
		$query = DB::select("SELECT * FROM tbl_eselon WHERE te_id = ".$id." AND status = 1");
		$data['detaileselon'] = $query;
        $data['id'] = $id;
		$data['iddinas'] = $ids;

		return view('backend.dinas.detaileselon3', $data);
	}
	public function addeselon4($id, $ids)
    {
		$query = DB::select("SELECT * FROM tbl_eselon WHERE te_id = ".$id." AND status = 1");
		$data['detaileselon'] = $query;
        $data['id'] = $id;
		$data['iddinas'] = $ids;

		return view('backend.dinas.detaileselon4', $data);
	}
	public function addeselon5($id, $ids)
    {
		$query = DB::select("SELECT * FROM tbl_eselon WHERE te_id = ".$id." AND status = 1");
		$data['detaileselon'] = $query;
        $data['id'] = $id;
		$data['iddinas'] = $ids;

		return view('backend.dinas.detaileselon5', $data);
	}

	
    public function tambahdinas()
    {
        return view('backend.dinas.create');
	}
	
	
	public function editdinas($id)
    {
		$query = DB::select("SELECT * FROM tbl_dinas WHERE td_id = ".$id);
		$data['detail'] = $query;
        $data['id'] = $id;

		return view('backend.dinas.edit', $data);
	}
	
	
	public function listdinas(){ 
		$hasil = DB::select('SELECT td_id, 
									td_dinas, 
									td_keterangan 
								FROM tbl_dinas
								WHERE 
									status = 1 
								ORDER BY `index` asc');
		return $hasil;
	}
	
	public function dinas($id){ 
		$hasil = DB::select('SELECT * FROM tbl_dinas WHERE td_id = '.$id);
		return $hasil;
	}
	
	public function eselons($id){ 
		$hasil = DB::select('select * from tbl_eselon where iddinas = '.$id.' and status = 1 and te_eselon in("2A","2B")');
		return $hasil;
	}
	
	public function eselonisi($id){ 
		$hasil = DB::select('select * from tbl_eselon where iddinas = '.$id.' and status = 1');
		return $hasil;
	}
	
	
	public function saveeselon(Request $request){ 
		$input = $request->all();
		$aa = explode("_",$input['eselon']);
        $eselon = $aa[0];
        $jabatan = $aa[1];
		$data = DB::table('tbl_eselon')->insert(
			['iddinas' => $input['iddinas'], 'te_eselon' => $eselon, 'te_jabatan' => $jabatan, 'te_isi' => $input['bagian'], 'te_parent' => 1, 'te_child' => 0]
		);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Eselon Sukses Tersimpan!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}
	
	public function saveeselon3(Request $request){ 
		$input = $request->all();
		$aa = explode("_",$input['eselon']);
        $eselon = $aa[0];
        $jabatan = $aa[1];
		$data = DB::table('tbl_eselon')->insert(
			['id_parent' => $input['ideselon3'],'iddinas' => $input['iddinas'], 'te_eselon' => $eselon, 'te_jabatan' => $jabatan, 'te_isi' => $input['bagian'], 'te_parent' => 0, 'te_child' => 1]
		);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Eselon Sukses Tersimpan!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}
	public function saveeselon4(Request $request){ 
		$input = $request->all();
		$aa = explode("_",$input['eselon']);
        $eselon = $aa[0];
        $jabatan = $aa[1];
		$data = DB::table('tbl_eselon')->insert(
			['id_parent' => $input['ideselon3'],'iddinas' => $input['iddinas'], 'te_eselon' => $eselon, 'te_jabatan' => $jabatan, 'te_isi' => $input['bagian'], 'te_parent' => 0, 'te_child' => 2]
		);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Eselon Sukses Tersimpan!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}
	public function saveeselon5(Request $request){ 
		$input = $request->all();
		$aa = explode("_",$input['eselon']);
        $eselon = $aa[0];
        $jabatan = $aa[1];
		$data = DB::table('tbl_eselon')->insert(
			['id_parent' => $input['ideselon3'],'iddinas' => $input['iddinas'], 'te_eselon' => $eselon, 'te_jabatan' => $jabatan, 'te_isi' => $input['bagian'], 'te_parent' => 0, 'te_child' => 3]
		);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Eselon Sukses Tersimpan!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}

	
	public function eselondelete(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_eselon')->where('te_id', '=', $input['id'])->delete();
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Eselon Sukses Terhapus!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}

	
	public function eselonedit(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_eselon')
				->where('te_id', $input['ideselon2'])
				->update(['te_isi' => $input['bagian']]);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Dinas Sukses Terupdate!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}
	
	public function dinastambah(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_dinas')->insert(
			['td_dinas' => $input['dinas'], 'td_keterangan' => $input['ket'], 'index' => $input['index'], 'status' => 1]
		);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Dinas Sukses Tersimpan!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}

	public function dinasdelete(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_dinas')
				->where('td_id', $input['id'])
				->update(['status' => 0]);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Dinas Sukses Terhapus!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}
	public function dinasedit(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_dinas')
				->where('td_id', $input['iddinas'])
				->update(['td_dinas' => $input['dinas'], 'td_keterangan' => $input['ket'], 'index' => $input['index']]);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Dinas Sukses Terupdate!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}
	
}
