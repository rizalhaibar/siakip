<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserValidation;
use App\Models\Role;
use Carbon\Carbon;
use DB;
/**
 * Class UserController.
 */
class KeselarasanController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
	 
	function __construct()
	{
		$this->middleware(['auth']);
		
        $this->middleware(function ($request, $next)
        {
            $this->user = session('data')->user;
            return $next($request);
        });
	}
	
	public function index()
    {
		if($this->user->role_id == 99 || $this->user->role_id == 97){
			$data['breadcrumb'] = [
				'parent' => 'Keselarasan',
				'child' => 'List Dinas'
			];
			$data['listdinas'] = $this->listdinas();

			return view('backend.keselarasan.index', $data);
		}else{
			$data['breadcrumb'] = [
				'parent' => 'Keselarasan',
				'child' => 'List Keselarasan'
			];
			$data['listsasaran'] = $this->listsasaran();

			return view('backend.keselarasan.home', $data);
		}
	}

	public function home()
    {
		$data['breadcrumb'] = [
			'parent' => 'Keselarasan',
			'child' => 'List Keselarasan'
		];
		$data['tujuan'] = $this->tujuan();
		$data['sasaran'] = $this->sasaran();
		$data['listkegiatan'] = $this->listkegiatan();
		$data['listsasaran'] = $this->listsasaran();

		return view('backend.keselarasan.home', $data);
	}

	
	public function listdinas(){ 
		$hasil = DB::select('SELECT td_id, 
									td_dinas, 
									td_keterangan 
								FROM tbl_dinas
								WHERE 
									status = 1 
								ORDER BY `index`');
		return $hasil;
	}
	
	public function tujuan(){ 
		$where = '';
		if($this->user->dinas_id != 1){
			$where = 'aa.`iddinas` = '.$this->user->dinas_id.' AND';
		}
		$hasil = DB::select('SELECT a.* FROM tbl_tujuan a
		LEFT JOIN tbl_misi bb ON bb.tm_id = a.idmisi
		LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi 
		WHERE '.$where.' aa.tahun = '.$_COOKIE['tahun']);
		return $hasil;
	}
	
	public function sasaran(){ 
		$hasil = DB::select('SELECT * FROM tbl_sasaran_tujuan');
		return $hasil;
	}
	
	public function listkegiatan(){ 
		$hasil = DB::select('SELECT * 
		FROM tbl_program_sasaran ');
		return $hasil;
	}
	public function listindikegiatan(){ 
		$hasil = DB::select('SELECT a.*, b.* 
		FROM tbl_indikator_kegiatan a
		LEFT JOIN tbl_anggaran b ON a.tik_id = b.idindkegiatan

		WHERE a.tik_status = 1');
		return $hasil;
	}
	
	
}
