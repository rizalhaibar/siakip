<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserValidation;
use App\Models\Role;
use Carbon\Carbon;
use DB;
/**
 * Class UserController.
 */
class MonitoringController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
	 
	function __construct()
	{
		$this->middleware(['auth']);
		
        $this->middleware(function ($request, $next)
        {
            $this->user = session('data')->user;
            return $next($request);
        });
	}
	
	public function index()
    {
		
		if($this->user->role_id == 99 || $this->user->role_id == 97){
			$data['breadcrumb'] = [
				'parent' => 'Pengkuran Kinerja',
				'child' => 'Monitoring'
			];
			$data['listdinas'] = $this->listdinas();
			return view('backend.monitoring.index', $data);
		}else{
			$data['breadcrumb'] = [
				'parent' => 'Monitoring',
				'child' => 'List Sasaran'
			];
			$dinas_id = $this->user->dinas_id;
			$data['id'] = $dinas_id;
			$data['listsasaran'] = $this->listsasaran($dinas_id);
			return view('backend.monitoring.home', $data);
		}
	}

	public function homesuper($id)
    {
		$data['breadcrumb'] = [
            'parent' => 'Monitoring',
            'child' => 'List Sasaran'
        ];
		$dinas_id = $id;
		$data['id'] = $dinas_id;
		$data['listsasaran'] = $this->listsasaran($dinas_id);
		return view('backend.monitoring.home', $data);
	}

	public function indikator($id)
    {
		$data['breadcrumb'] = [
            'parent' => 'Monitoring',
            'child' => 'Indikator Sasaran'
        ];
		$dinas_id = $id;
		$data['id'] = $dinas_id;
		$data['listindikator'] = $this->listindikator($dinas_id);
		return view('backend.monitoring.home', $data);
	}
	
	public function tambahtarget($id, $ids)
    {
		$data['id'] = $id;
		$data['ids'] = $ids;
		$data['target'] = $this->detailindikator($dinas_id);
		return view('backend.monitoring.detail', $data);
	}
	
	
   
	public function listsasaran($id){ 
		$hasil = DB::select('SELECT  a.tst_id, a.tst_sasaran_tujuan, a.tst_keterangan
		FROM tbl_sasaran_tujuan a
		LEFT JOIN tbl_tujuan cc ON cc.tt_id = a.idtujuan
		LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
		LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi WHERE aa.iddinas = '.$id.' AND a.tahun = '.$_COOKIE['tahun']);
		return $hasil;
	}
	
   
	public function listdinas(){ 
		$hasil = DB::select('SELECT td_id, 
									td_dinas, 
									td_keterangan 
								FROM tbl_dinas
								WHERE 
									status = 1 
								ORDER BY td_dinas');
		return $hasil;
	}

	public function detailindikator($id){ 
		$hasil = DB::select('SELECT * FROM tbl_rencana_aksi WHERE tis_id = '.$id.' ORDER BY date desc limit 1');
		return $hasil;
	}

	
	public function savevisi(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_visi')->insert(
			['iddinas' => $input['iddinas'], 'tv_visi' => $input['visi'], 'tv_keterangan' => $input['desc'], 'tahun' => $input['tahun']]
		);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Visi Sukses Tersimpan!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
	}
   
	public function listindikator($id){ 
		$hasil = DB::select('SELECT 
							aadc.tis_id, aadc.tis_indikator_sasaran, 
							aadc.tis_satuan,  aadc.tis_target,
							bbdc.tw1, bbdc.twtarget1, 
							bbdc.tw2, bbdc.twtarget2, 
							bbdc.tw3, bbdc.twtarget3, 
							bbdc.tw4, bbdc.twtarget4
						FROM
						(
						SELECT a.tis_id, a.tis_indikator_sasaran, a.tis_satuan, a.tis_target
						FROM tbl_indikator_sasaran_pk a
						LEFT JOIN tbl_sasaran_tujuan dd ON dd.`tst_id` = a.`idsasaran`
						LEFT JOIN tbl_tujuan cc ON cc.tt_id = dd.idtujuan
						LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
						LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
						)aadc
						LEFT JOIN
						(

						SELECT a.tis_id, a.tis_indikator_sasaran, a.tis_satuan, 
							sa.`tw1`, sa.`twtarget1`, 
							sa.`tw2`, sa.`twtarget2`, 
							sa.`tw3`, sa.`twtarget3`, 
							sa.`tw4`, sa.`twtarget4`
						FROM tbl_indikator_sasaran_pk a
						LEFT JOIN tbl_rencana_aksi sa ON sa.tis_id = a.tis_id
						LEFT JOIN tbl_sasaran_tujuan dd ON dd.`tst_id` = a.`idsasaran`
						LEFT JOIN tbl_tujuan cc ON cc.tt_id = dd.idtujuan
						LEFT JOIN tbl_misi bb ON bb.tm_id = cc.idmisi
						LEFT JOIN tbl_visi aa ON aa.tv_id = bb.idvisi
						AND a.idsasaran = '.$id.'
						)bbdc ON aadc.tis_id = bbdc.tis_id ');
		return $hasil;
	}

	

	public function targetedit(Request $request){ 
		$input = $request->all();
		$data = DB::table('tbl_rencana_aksi')
				->where('id', $input['idindikator'])
				->update(['tw1' => $input['tw1'], 'tw2' => $input['tw2'], 'tw3' => $input['tw3'], 'tw4' => $input['tw4']]);
		if ( $data ) {			
			$result = array( 'success' => true, 'message' => 'Target Sukses Terupdate!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			return response()->json($result);
		}	
    }
	
	
}
