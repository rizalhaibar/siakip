<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Role\RoleRepositoryInterface;
use App\Repositories\Company\CompanyRepositoryInterface;
use App\Traits\FlashMessageTraits;
use DB;
class UserController extends Controller
{
    use FlashMessageTraits;

    function __construct(
        UserRepositoryInterface $userRepo,
        RoleRepositoryInterface $roleRepo,
        CompanyRepositoryInterface $companyRepo
    ) {
        $this->userRepo     = $userRepo;
        $this->roleRepo     = $roleRepo;
        $this->companyRepo  = $companyRepo;
        $this->_pass_regex  = config('setting.pass.regex');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = $this->userRepo->pagination(10);
        return
            view('backend.user.index')
            ->with('users', $data->datas)
            ->with('status', $data->status)
        ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$data['breadcrumb'] = [
            'parent' => 'User Management',
            'child' => 'Create New User'
        ];
        $dinas = DB::select("SELECT * FROM tbl_dinas WHERE status = 1 and td_id not in(1)");
        $role = DB::select("SELECT * FROM roles WHERE status = 1 and id not in(99) order by role asc");
        return
            view('backend.user.create', $data)
            ->with('roles', $role)
            ->with('dinas', $dinas)
        ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'role_id' => 'required',
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users',
            'password' => 'required|string|min:8|confirmed|'.$this->_pass_regex,
        ]);
        $response = $this->userRepo->store($request);
        $this->message($response->level, $response->message);
        return redirect()->route('backend.user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $id = $this->decryptingId($id);
        $dinas = DB::select("SELECT * FROM tbl_dinas WHERE status = 1 and td_id not in(1)");
        $role = DB::select("SELECT * FROM roles WHERE status = 1 and id not in(99) order by role asc");
        return
            view('backend.user.edit')
            ->with('detail', $this->userRepo->find($id))
            ->with('roles', $role)
            ->with('dinas', $dinas)
        ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $id = $this->decryptingId($id);
        
        $request->validate([
            'role_id' => 'required',
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users,email,'.$id,
            'password' => 'string|min:8|confirmed|'.$this->_pass_regex,
        ]);
        
        $response = $this->userRepo->update($id, $request);
        $this->message($response->level, $response->message);
        return redirect()->route('backend.user.index');
    }

    public function toggle(Request $request)
    {
        //
        $response = $this->userRepo->toggle($request);
        $this->message($response->level, $response->message);
        return back();
    }

}
