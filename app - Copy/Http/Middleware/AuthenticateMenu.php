<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        $menus = $user->role->menuses;
        $current_route = $request->route()->getName();

        if ( array_search($current_route, array_column($menus->toArray(), "route")) !== FALSE )
            return $next($request);

        if($request->ajax()){
            return response()->json(['message' => 'Access denied'], 403);
        }

        abort(403, 'Access denied');
    }
}
