

function xls(table, title) {
	if(Array.isArray(table)){
		var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
        , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
		
			
			
			  // console.log(name);
			  // console.log(template);
		table.forEach(function(entry) {
			if (!table.nodeType) table = document.getElementById(table)
			var ctx = {worksheet: 'Worksheet', table: table.innerHTML}
		});
		// window.download = "filename.xls";
		// window.location.href = uri + base64(format(template, ctx));
		
		var link = document.createElement("a");
		link.download = title+'.xls';
		link.href = uri + base64(format(template, ctx));
		link.click();
		
		
	}else{
		var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
        , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
		
			
			
			  // console.log(name);
			  // console.log(template);
		if (!table.nodeType) table = document.getElementById(table)
		var ctx = {worksheet: 'Worksheet', table: table.innerHTML}
		// window.download = "filename.xls";
		// window.location.href = uri + base64(format(template, ctx));
		
		var link = document.createElement("a");
		link.download = title+'.xls';
		link.href = uri + base64(format(template, ctx));
		link.click();
	}
	
}

function xlsx(table, title){
	if(Array.isArray(table)){
		let a = [];
		table.forEach(function(entry) {
			var config = { raw:true, type: 'string' };
			var tbl = document.getElementById(entry);
			var wb = XLSX.utils.table_to_sheet(tbl, config);
			console.log(wb)
			a = a.concat(XLSX.utils.sheet_to_json(wb, { header: 1, raw: false })).concat([''])
			
			
		});
		
	
		
		let worksheet = XLSX.utils.json_to_sheet(a, { skipHeader: true, cellStyles : true })
	   
		const new_workbook = XLSX.utils.book_new()
		XLSX.utils.book_append_sheet(new_workbook, worksheet, "worksheet")
		XLSX.writeFile(new_workbook, title+'.xlsx')
	}else{
		// let a = [];
		var config = { raw:true, type: 'string', cellStyles : true };
		// var tbl = document.getElementById(table);
		// var wb = XLSX.utils.table_to_sheet(tbl, config);
		// console.log(wb)
		// a = a.concat(XLSX.utils.sheet_to_json(wb, { header: 1, raw: false })).concat(['']);
		
		
		// let worksheet = XLSX.utils.json_to_sheet(a, { skipHeader: true, cellStyles : true });
	   
		// const new_workbook = XLSX.utils.book_new();
		// XLSX.utils.book_append_sheet(new_workbook, worksheet, "worksheet");
		// XLSX.writeFile(new_workbook, title+'.xlsx');
		// var canvas = document.querySelector('#barChart2');
		//creates image
		// var canvasImg = canvas.toDataURL("image/png", 1.0);
		// console.log(canvasImg);
		var tbl = document.getElementById(table);
		var wb = XLSX.utils.table_to_book(tbl, config);
		// wb.Sheets['my-sheet']['!images'] = [
			// {
				// name: 'image1.jpg',
				// data: canvasImg,
				// opts: { base64: true },
				// position: {
					// type: 'twoCellAnchor',
					// attrs: { editAs: 'oneCell' },
					// from: { col: 2, row : 2 },
					// to: { col: 6, row: 5 }
				// }
			// }
		// ];
		
		
		var read_opts = {
			cellStyles : true
		}
		
		// XLSX.readFile(wb, read_opts);
		XLSX.writeFile(wb, title+'.xlsx', read_opts);
	}
	
}

function htmlToExcel(tableName, fName, title){
	$(':button[id="download_excel"]').prop('disabled', true);
	$(':button[id="download_excel"]').html("Sedang Proses...");
	
	
	setTimeout(function(){ 
		
		if(fName == "xls"){
			xls(tableName, title);
		} 
		if(fName == "xlsx"){
			xlsx(tableName, title);
			
			// var tbl = document.getElementById(tableName);
			// var wb = XLSX.utils.table_to_book(tbl);
			// console.log(XLSX);
		}
		
		
		$(':button[id="download_excel"]').prop('disabled', false);
		$(':button[id="download_excel"]').html("<i class='fa fa-file-excel-o '></i> Excel");
		$('#myModal').modal('hide');
		 toastr.info("Download sukses!");
	}, 1000);
	
}