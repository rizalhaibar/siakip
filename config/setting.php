<?php

return [

    'isAdmin' => [
        'key' => 1,
        'name' => "Administrator"
    ],

    'pagination' => [
        'limit' => 10,
    ],
    
    'value' => [
        'exist' => 1,
        'zero' => 0,
    ],

    'status' => [
        'active' => 1,
        'notactive' => 0,
        'all' => '-0'
    ],

    'url' => [
        'crash' => "#",
    ],

    'pass' => [
        'regex' => "regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/",
    ],

    'store-path' => [
        'profile' => "/upload/profile/",
        
        'visitor' => [
            'card' => "/upload/visitor/card/",
            'person' => "/upload/visitor/person/",
            'package' => "/upload/visitor/package/",
        ],

        'courrier' => [
            'card' => "/upload/courrier/card/",
            'person' => "/upload/courrier/person/",
            'package' => "/upload/courrier/package/",
        ]
    ],

    'view-path' => [
        'profile' => "/storage/upload/profile/",
        
        'visitor' => [
            'card' => "/storage/upload/visitor/card/",
            'person' => "/storage/upload/visitor/person/",
            'package' => "/storage/upload/visitor/package/",
        ],

        'courrier' => [
            'card' => "/storage/upload/courrier/card/",
            'person' => "/storage/upload/courrier/person/",
            'package' => "/storage/upload/courrier/package/",
        ]
    ],

    'visit' => [
        'purpose' => [
            'meeting' => [
                'id' => '1',
                'name' => 'Meeting'
            ]
        ]
    ]

];
