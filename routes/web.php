<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', function () {
	// return redirect()->route('login');
	return redirect()->route('pemerintah.index');
})->name('/');


Route::group(['as' => 'auth.'], function (){
	Route::post('auth/login', 'Auth\LoginController@login')->name('login');
});

Route::group(['as' => 'pemerintah.'], function (){

	Route::get('index', 'Backend\PemerintahController@index')->name('index');
	Route::get('about', 'Backend\PemerintahController@about')->name('about');
	Route::get('contact', 'Backend\PemerintahController@contact')->name('contact');
	Route::get('detaildinas', 'Backend\PemerintahController@detaildinas')->name('detaildinas');
	Route::get('iku', 'Backend\PemerintahController@iku')->name('iku');
	Route::get('rkt', 'Backend\PemerintahController@rkt')->name('rkt');
	Route::get('pk', 'Backend\PemerintahController@pk')->name('pk');
	Route::get('pkperubahan', 'Backend\PemerintahController@pkperubahan')->name('pkperubahan');
	Route::get('rencanaaksi', 'Backend\PemerintahController@rencanaaksi')->name('rencanaaksi');
	Route::get('program', 'Backend\PemerintahController@program')->name('program');
	Route::get('detailpelaporan', 'Backend\PemerintahController@detailpelaporan')->name('detailpelaporan');
	Route::get('detailpengukuran', 'Backend\PemerintahController@detailpengukuran')->name('detailpengukuran');


	// Route::get('changetahun/{tahun}', 'Backend\PemerintahController@changetahun')->name('changetahun');
	Route::any('gantitahun/{tahun}', 'Backend\PemerintahController@gantitahun')->name('gantitahun');

	Route::any('perencanaan/{id}/{idsasaran}/{idjabatan}', 'Backend\PemerintahController@perencanaan')->name('perencanaan');
	Route::any('pengukuran/{id}/{idsasaran}', 'Backend\PemerintahController@pengukuran')->name('pengukuran');
	Route::any('pelaporan/{id}/{idsasaran}', 'Backend\PemerintahController@pelaporan')->name('pelaporan');
	Route::any('evaluasi', 'Backend\PemerintahController@evaluasi')->name('evaluasi');
});


Auth::routes();

Route::group([
	'namespace' => 'Backend',
	'prefix' => 'backend',
	'as' => 'backend.',
	'middleware' => 'auth',
], function () {

	// Route::get('home', 'HomeController@index')->name('home')->middleware('auth-menu');
	Route::get('home', 'HomeController@index')->name('home');



    /*
	 * RPJMD/RENSTRA Modul Routes
	 * route('web.rpjmd')
	 * route('web.rpjmd.detail')
	 */
	Route::group(['as' => 'rpjmd.'], function (){
		Route::get('rpjmd', 'RpjmdController@index')->name('index');
		Route::get('rpjmd/masukoperator', 'RpjmdController@masukoperator')->name('masukoperator');
		Route::get('rpjmd/masuk/{id}/{tahun}', 'RpjmdController@masuk')->name('masuk');
		Route::get('rpjmd/visidetail/{id}/{ids}', 'RpjmdController@visidetail')->name('visidetail');
		Route::get('rpjmd/tambahmisi/{id}/{ids}', 'RpjmdController@tambahmisi')->name('tambahmisi');
		Route::get('rpjmd/tambahtujuan/{id}/{ids}', 'RpjmdController@tambahtujuan')->name('tambahtujuan');
		Route::get('rpjmd/tambahindikatortujuan/{id}/{ids}', 'RpjmdController@tambahindikatortujuan')->name('tambahindikatortujuan');
		Route::get('rpjmd/tambahindikatorsasaran/{id}/{ids}', 'RpjmdController@tambahindikatorsasaran')->name('tambahindikatorsasaran');
		Route::get('rpjmd/tambahsasaran/{id}/{ids}', 'RpjmdController@tambahsasaran')->name('tambahsasaran');
		Route::get('rpjmd/tambahprogram/{id}/{ids}', 'RpjmdController@tambahprogram')->name('tambahprogram');
		Route::get('rpjmd/misidetail/{id}/{ids}', 'RpjmdController@misidetail')->name('misidetail');
		Route::get('rpjmd/tujuandetail/{id}/{ids}', 'RpjmdController@tujuandetail')->name('tujuandetail');
		Route::get('rpjmd/sasarandetail/{id}/{ids}', 'RpjmdController@sasarandetail')->name('sasarandetail');
		Route::get('rpjmd/detailsasaranindikator/{id}/{ids}', 'RpjmdController@detailsasaranindikator')->name('detailsasaranindikator');
		Route::get('rpjmd/detailtujuanindikator/{id}/{ids}', 'RpjmdController@detailtujuanindikator')->name('detailtujuanindikator');
		Route::get('rpjmd/tujuan/{id}/{tahun}/{ids}', 'RpjmdController@tujuan')->name('tujuan');
		Route::get('rpjmd/program/{id}/{ids}', 'RpjmdController@program')->name('program');
		Route::post('rpjmd/deletevisi', 'RpjmdController@deletevisi')->name('deletevisi');
		Route::post('rpjmd/deletemisi', 'RpjmdController@deletemisi')->name('deletemisi');
		Route::post('rpjmd/deletetujuan', 'RpjmdController@deletetujuan')->name('deletetujuan');
		Route::post('rpjmd/deletesasaran', 'RpjmdController@deletesasaran')->name('deletesasaran');
		Route::post('rpjmd/deleteindikatorsasaran', 'RpjmdController@deleteindikatorsasaran')->name('deleteindikatorsasaran');
		Route::post('rpjmd/savevisi', 'RpjmdController@savevisi')->name('savevisi');
		Route::post('rpjmd/editvisi', 'RpjmdController@editvisi')->name('editvisi');
		Route::post('rpjmd/savemisi', 'RpjmdController@savemisi')->name('savemisi');
		Route::post('rpjmd/editmisi', 'RpjmdController@editmisi')->name('editmisi');
		Route::post('rpjmd/savetujuan', 'RpjmdController@savetujuan')->name('savetujuan');
		Route::post('rpjmd/edittujuan', 'RpjmdController@edittujuan')->name('edittujuan');
		Route::post('rpjmd/editindikatortujuan', 'RpjmdController@editindikatortujuan')->name('editindikatortujuan');
		Route::post('rpjmd/rubahiku', 'RpjmdController@rubahiku')->name('rubahiku');
		Route::post('rpjmd/deleteinditujuan', 'RpjmdController@deleteinditujuan')->name('deleteinditujuan');
		Route::post('rpjmd/saveindikatortujuan', 'RpjmdController@saveindikatortujuan')->name('saveindikatortujuan');
		Route::post('rpjmd/savesasaran', 'RpjmdController@savesasaran')->name('savesasaran');
		Route::post('rpjmd/editsasaran', 'RpjmdController@editsasaran')->name('editsasaran');
		Route::post('rpjmd/editindikatorsasaran', 'RpjmdController@editindikatorsasaran')->name('editindikatorsasaran');
		Route::post('rpjmd/saveindikatorsasaran', 'RpjmdController@saveindikatorsasaran')->name('saveindikatorsasaran');
		Route::post('rpjmd/saveprogram', 'RpjmdController@saveprogram')->name('saveprogram');
		// Route::get('role/edit/{id}', 'RoleController@edit')->name('edit');
		// Route::post('role/update/{id}', 'RoleController@update')->name('update');
		// Route::get('role/delete/{id}', 'RoleController@delete')->name('delete');
	});

	/**
	 * Change Password Modul Routes
	 * route('backend.change-password.*')
	 */
	Route::group(['as' => 'change-password.'], function (){
		Route::get('change-password', 'ChangePasswordController@index')->name('index');
		Route::post('change-password/change', 'ChangePasswordController@change')->name('change');
	});

	/**
	 * Profile Modul Routes
	 * route('backend.profile.*')
	 */
	Route::group(['as' => 'profile.'], function (){
		Route::get('profile', 'ProfileController@index')->name('index');
		Route::post('profile/update', 'ProfileController@update')->name('update');
	});

	/**
	 * User Modul Routes
	 * route('backend.user.*')
	 */
	Route::group(['as' => 'user.'], function (){
		Route::get('user', 'UserController@index')->name('index');
		Route::post('user/toggle', 'UserController@toggle')->name('toggle');
		Route::get('user/create', 'UserController@create')->name('create');
		Route::post('user/store', 'UserController@store')->name('store');
		Route::get('user/edit/{id}', 'UserController@edit')->name('edit');
		Route::get('editprofile/edit/{id}', 'UserController@editprofile')->name('editprofile');
		Route::post('user/update/{id}', 'UserController@update')->name('update');
	});

	/**
	 * Roles Modul Routes
	 * route('backend.role.*')
	 */
	Route::group(['as' => 'role.'], function (){
		Route::get('role', 'RoleController@index')->name('index')->middleware('auth-menu');
		Route::post('role/toggle', 'RoleController@toggle')->name('toggle');
		Route::get('role/create', 'RoleController@create')->name('create');
		Route::post('role/store', 'RoleController@store')->name('store');
		Route::get('role/edit/{id}', 'RoleController@edit')->name('edit');
		Route::post('role/update/{id}', 'RoleController@update')->name('update');
	});

	/**
	 * Menus Modul Routes
	 * route('backend.menu.*')
	 */
	Route::group(['as' => 'menu.'], function (){
		Route::get('menu', 'MenuController@index')->name('index');
		Route::post('menu/toggle', 'MenuController@toggle')->name('toggle');
		Route::get('menu/create', 'MenuController@create')->name('create');
		Route::post('menu/store', 'MenuController@store')->name('store');
		Route::get('menu/edit/{id}', 'MenuController@edit')->name('edit');
		Route::post('menu/update/{id}', 'MenuController@update')->name('update');
	});

	/**
	 * Access Control Modul Routes
	 * route('backend.accesscontrol.*')
	 */
	Route::group(['as' => 'accesscontrol.'], function (){
		Route::get('accesscontrol', 'AccessControlController@index')->name('index');
		Route::post('accesscontrol/update', 'AccessControlController@update')->name('update');
	});

	 /*
	 * Dinas Modul Routes
	 * route('web.dinas')
	 * route('web.dinas.detail')
	 */
		Route::group(['as' => 'dinas.'], function (){
			Route::get('dinas', 'DinasController@index')->name('index');
			Route::get('dinas/tambahdinas', 'DinasController@tambahdinas')->name('tambahdinas');
			Route::get('dinas/copydinas', 'DinasController@copydinas')->name('copydinas');
			Route::get('dinas/editdinas/{id}', 'DinasController@editdinas')->name('editdinas');
			Route::get('dinas/eselon/{id}', 'DinasController@eselon')->name('eselon');
			Route::get('dinas/tambaheselon/{id}', 'DinasController@tambaheselon')->name('tambaheselon');
			Route::get('dinas/addeselon/{id}/{ids}', 'DinasController@addeselon')->name('addeselon');
			Route::get('dinas/addeselon3/{id}/{ids}', 'DinasController@addeselon3')->name('addeselon3');
			Route::get('dinas/addeselon4/{id}/{ids}', 'DinasController@addeselon4')->name('addeselon4');
			Route::get('dinas/addeselon5/{id}/{ids}', 'DinasController@addeselon5')->name('addeselon5');
			Route::post('dinas/dinasdelete', 'DinasController@dinasdelete')->name('dinasdelete');
			Route::post('dinas/dinastambah', 'DinasController@dinastambah')->name('dinastambah');
			Route::post('dinas/dinasedit', 'DinasController@dinasedit')->name('dinasedit');
			Route::post('dinas/eselonedit', 'DinasController@eselonedit')->name('eselonedit');
			Route::post('dinas/eselondelete', 'DinasController@eselondelete')->name('eselondelete');
			Route::post('dinas/saveeselon', 'DinasController@saveeselon')->name('saveeselon');
			Route::post('dinas/saveeselon3', 'DinasController@saveeselon3')->name('saveeselon3');
			Route::post('dinas/saveeselon4', 'DinasController@saveeselon4')->name('saveeselon4');
			Route::post('dinas/saveeselon5', 'DinasController@saveeselon5')->name('saveeselon5');
			// Route::get('role/add', 'RoleController@add')->name('add');
			// Route::post('role/store', 'RoleController@store')->name('store');
			// Route::get('role/edit/{id}', 'RoleController@edit')->name('edit');
			// Route::post('role/update/{id}', 'RoleController@update')->name('update');
			// Route::get('role/delete/{id}', 'RoleController@delete')->name('delete');
		});

     /*
	 * evaluasikinerja Modul Routes
	 * route('web.evaluasikinerja')
	 * route('web.evaluasikinerja.detail')
	 */
		Route::group(['as' => 'evaluasikinerja.'], function (){
			Route::get('evaluasikinerja', 'EvaluasikinerjaController@index')->name('index');
			Route::get('evaluasikinerja/detail/{id}/{tahun}', 'EvaluasikinerjaController@detail')->name('detail');
			Route::post('evaluasikinerja/nilaievaluasiedit', 'EvaluasikinerjaController@nilaievaluasiedit')->name('nilaievaluasiedit');
			Route::post('evaluasikinerja/nilaievaluasi', 'EvaluasikinerjaController@nilaievaluasi')->name('nilaievaluasi');
			// Route::get('role/edit/{id}', 'RoleController@edit')->name('edit');
			// Route::post('role/update/{id}', 'RoleController@update')->name('update');
			// Route::get('role/delete/{id}', 'RoleController@delete')->name('delete');
		});

     /*
	 * laporankinerja Modul Routes
	 * route('web.laporankinerja')
	 * route('web.laporankinerja.detail')
	 */
		Route::group(['as' => 'laporankinerja.'], function (){
			Route::get('laporankinerja', 'LaporankinerjaController@index')->name('index');
			Route::get('laporankinerja/detail/{id}/{idsasaran}', 'LaporankinerjaController@detail')->name('detail');
			Route::post('laporankinerja/savelaporan', 'LaporankinerjaController@savelaporan')->name('savelaporan');
			// Route::get('role/edit/{id}', 'RoleController@edit')->name('edit');
			// Route::post('role/update/{id}', 'RoleController@update')->name('update');
			// Route::get('role/delete/{id}', 'RoleController@delete')->name('delete');
		});

     /*
	 * monitoring Modul Routes
	 * route('web.monitoring')
	 * route('web.monitoring.detail')
	 */
		Route::group(['as' => 'monitoring.'], function (){
			Route::get('monitoring', 'MonitoringController@index')->name('index');
			Route::get('monitoring/home/{id}/{idsasaran}', 'MonitoringController@home')->name('home');

			Route::get('monitoring/tambahtargetkegiatan/{id}/{ids}/{idsasaran}', 'MonitoringController@tambahtargetkegiatan')->name('tambahtargetkegiatan');
			Route::get('monitoring/edittargetkegiatan/{id}/{ids}/{idsasaran}', 'MonitoringController@edittargetkegiatan')->name('edittargetkegiatan');
			Route::post('monitoring/savetargetindikatorkegiatan', 'MonitoringController@savetargetindikatorkegiatan')->name('savetargetindikatorkegiatan');
			Route::post('monitoring/edittargetindikatorkegiatan', 'MonitoringController@edittargetindikatorkegiatan')->name('edittargetindikatorkegiatan');


			Route::get('monitoring/tambahtargetprogram/{id}/{ids}/{idsasaran}', 'MonitoringController@tambahtargetprogram')->name('tambahtargetprogram');
			Route::get('monitoring/edittargetprogram/{id}/{ids}/{idsasaran}', 'MonitoringController@edittargetprogram')->name('edittargetprogram');
			Route::post('monitoring/savetargetindikatorprogram', 'MonitoringController@savetargetindikatorprogram')->name('savetargetindikatorprogram');
			Route::post('monitoring/edittargetindikatorprogram', 'MonitoringController@edittargetindikatorprogram')->name('edittargetindikatorprogram');


			Route::get('monitoring/tambahtargetsasaran/{id}/{ids}/{idsasaran}', 'MonitoringController@tambahtargetsasaran')->name('tambahtargetsasaran');
			Route::get('monitoring/edittargetsasaran/{id}/{ids}/{idsasaran}', 'MonitoringController@edittargetsasaran')->name('edittargetsasaran');
			Route::post('monitoring/savetargetindikatorsasaran', 'MonitoringController@savetargetindikatorsasaran')->name('savetargetindikatorsasaran');
			Route::post('monitoring/edittargetindikatorsasaran', 'MonitoringController@edittargetindikatorsasaran')->name('edittargetindikatorsasaran');

		});





     /*
	 * pengukurankinerjaperubahan Modul Routes
	 * route('web.pengukurankinerjaperubahan')
	 * route('web.pengukurankinerjaperubahan.detail')
	 */
		Route::group(['as' => 'pengukurankinerjaperubahan.'], function (){
			Route::get('pengukurankinerjaperubahan', 'PengukurankinerjaperubahanController@index')->name('index');
			// Route::get('role/add', 'RoleController@add')->name('add');
			// Route::post('role/store', 'RoleController@store')->name('store');
			// Route::get('role/edit/{id}', 'RoleController@edit')->name('edit');
			// Route::post('role/update/{id}', 'RoleController@update')->name('update');
			// Route::get('role/delete/{id}', 'RoleController@delete')->name('delete');
		});
     /*
	 * pengukurankinerja Modul Routes
	 * route('web.pengukurankinerja')
	 * route('web.pengukurankinerja.detail')
	 */
		Route::group(['as' => 'pengukurankinerja.'], function (){
			Route::get('pengukurankinerja', 'PengukurankinerjaController@index')->name('index');
			Route::get('pengukurankinerja/home/{id}/{idsasaran}', 'PengukurankinerjaController@home')->name('home');
			Route::get('pengukurankinerja/tambahindikatorsasaranpic/{id}/{idsasaran}/{iddinas}', 'PengukurankinerjaController@tambahindikatorsasaranpic')->name('tambahindikatorsasaranpic');
			Route::get('pengukurankinerja/tambahindikatorprogrampic/{id}/{idsasaran}/{iddinas}', 'PengukurankinerjaController@tambahindikatorprogrampic')->name('tambahindikatorprogrampic');
			Route::get('pengukurankinerja/tambahindikatorkegiatanpic/{id}/{idsasaran}/{iddinas}', 'PengukurankinerjaController@tambahindikatorkegiatanpic')->name('tambahindikatorkegiatanpic');


			Route::get('pengukurankinerja/tambahprogram/{id}/{ids}/{idsasaran}', 'PengukurankinerjaController@tambahprogram')->name('tambahprogram');
			Route::get('pengukurankinerja/editprogram/{id}/{iddinas}/{idsasaran}', 'PengukurankinerjaController@editprogram')->name('editprogram');
			Route::post('pengukurankinerja/saveprogram', 'PengukurankinerjaController@saveprogram')->name('saveprogram');
			Route::post('pengukurankinerja/programedit', 'PengukurankinerjaController@programedit')->name('programedit');
			Route::post('pengukurankinerja/deleteprogram', 'PengukurankinerjaController@deleteprogram')->name('deleteprogram');


			Route::get('pengukurankinerja/tambahindikatorkegiatan/{id}/{ids}/{idsasaran}', 'PengukurankinerjaController@tambahindikatorkegiatan')->name('tambahindikatorkegiatan');
			Route::post('pengukurankinerja/saveindikatorkegiatan', 'PengukurankinerjaController@saveindikatorkegiatan')->name('saveindikatorkegiatan');
			Route::post('pengukurankinerja/deleteindikatorkegiatan', 'PengukurankinerjaController@deleteindikatorkegiatan')->name('deleteindikatorkegiatan');
			Route::post('pengukurankinerja/indikatorkegiatanedit', 'PengukurankinerjaController@indikatorkegiatanedit')->name('indikatorkegiatanedit');
			Route::get('pengukurankinerja/editindikatorkegiatan/{id}/{idprogram}/{ids}/{idsasaran}', 'PengukurankinerjaController@editindikatorkegiatan')->name('editindikatorkegiatan');



			Route::get('pengukurankinerja/tambahkegiatan/{id}/{ids}/{idsasaran}', 'PengukurankinerjaController@tambahkegiatan')->name('tambahkegiatan');
			Route::get('pengukurankinerja/editkegiatan/{id}/{ids}/{idsasaran}', 'PengukurankinerjaController@editkegiatan')->name('editkegiatan');
			Route::post('pengukurankinerja/savekegiatan', 'PengukurankinerjaController@savekegiatan')->name('savekegiatan');
			Route::post('pengukurankinerja/deletekegiatan', 'PengukurankinerjaController@deletekegiatan')->name('deletekegiatan');
			Route::post('pengukurankinerja/kegiatanedit', 'PengukurankinerjaController@kegiatanedit')->name('kegiatanedit');



			Route::get('pengukurankinerja/tambahindikatorprogram/{id}/{ids}/{idsasaran}', 'PengukurankinerjaController@tambahindikatorprogram')->name('tambahindikatorprogram');
			Route::get('pengukurankinerja/editindikatorprogram/{id}/{idprogram}/{ids}/{idsasaran}', 'PengukurankinerjaController@editindikatorprogram')->name('editindikatorprogram');
			Route::post('pengukurankinerja/saveindikatorprogram', 'PengukurankinerjaController@saveindikatorprogram')->name('saveindikatorprogram');
			Route::post('pengukurankinerja/indikatorprogramedit', 'PengukurankinerjaController@indikatorprogramedit')->name('indikatorprogramedit');
			Route::post('pengukurankinerja/deleteindikatorprogram', 'PengukurankinerjaController@deleteindikatorprogram')->name('deleteindikatorprogram');



			Route::post('pengukurankinerja/saveindikatorsasaranpic', 'PengukurankinerjaController@saveindikatorsasaranpic')->name('saveindikatorsasaranpic');
			Route::post('pengukurankinerja/saveindikatorprogrampic', 'PengukurankinerjaController@saveindikatorprogrampic')->name('saveindikatorprogrampic');
			Route::post('pengukurankinerja/saveindikatorkegiatanpic', 'PengukurankinerjaController@saveindikatorkegiatanpic')->name('saveindikatorkegiatanpic');
			Route::post('pengukurankinerja/printperjanjiankinerja', 'PengukurankinerjaController@printperjanjiankinerja')->name('printperjanjiankinerja');

            // Route::get('pengukurankinerja/printview/{id}/{idsasaran}', 'PengukurankinerjaController@printview')->name('printview');
            Route::get('pengukurankinerja/printpk/{id}', 'PengukurankinerjaController@printpk')->name('printpk');
			// Route::post('role/store', 'RoleController@store')->name('store');
			// Route::get('role/edit/{id}', 'RoleController@edit')->name('edit');
			// Route::post('role/update/{id}', 'RoleController@update')->name('update');
			// Route::get('role/delete/{id}', 'RoleController@delete')->name('delete');
		});
/*
	 * RKT Modul Routes
	 * route('web.rkt')
	 * route('web.rkt.detail')
	 */
		Route::group(['as' => 'rkt.'], function (){
			Route::get('rkt', 'RktController@index')->name('index');
			Route::get('rkt/home/{id}', 'RktController@home')->name('home');
			Route::get('rkt/indikator/{id}', 'RktController@indikator')->name('indikator');
            Route::get('rkt/printview/{id}/', 'RktController@printview')->name('printview');
			Route::get('rkt/tambahtarget/{id}/{iddinas}', 'RktController@tambahtarget')->name('tambahtarget');
            Route::get('rkt/printpk/{id}', 'RktController@printpk')->name('printpk');
            Route::post('rkt/print', 'RktController@print')->name('print');

			Route::post('rkt/targetedit', 'RktController@targetedit')->name('targetedit');
		});

     /*
	 * keselarasan Modul Routes
	 * route('web.keselarasan')
	 * route('web.keselarasan.detail')
	 */
    Route::group(['as' => 'keselarasan.'], function (){
        Route::get('keselarasan', 'KeselarasanController@index')->name('index');
        Route::get('keselarasan/home/{id}', 'KeselarasanController@home')->name('home');

        Route::get('keselarasan/printview/{id}/', 'KeselarasanController@printview')->name('printview');
        Route::get('keselarasan/printkesel/{id}', 'KeselarasanController@printkesel')->name('printkesel');
        Route::post('keselarasan/print', 'KeselarasanController@print')->name('print');
        // Route::get('role/add', 'RoleController@add')->name('add');
        // Route::post('role/store', 'RoleController@store')->name('store');
        // Route::get('role/edit/{id}', 'RoleController@edit')->name('edit');
        // Route::post('role/update/{id}', 'RoleController@update')->name('update');
        // Route::get('role/delete/{id}', 'RoleController@delete')->name('delete');
    });

    /*
	 * IKU Modul Routes
	 * route('web.iku')
	 * route('web.iku.detail')
	 */
		Route::group(['as' => 'iku.'], function (){
			Route::get('iku', 'IkuController@index')->name('index');
			Route::get('iku/home/{id}', 'IkuController@home')->name('home');
			Route::get('iku/tambahformula/{id}/', 'IkuController@tambahformula')->name('tambahformula');
            Route::get('iku/printiku/{id}', 'IkuController@printiku')->name('printiku');
            Route::post('iku/print', 'IkuController@print')->name('print');
			Route::post('iku/formulasave', 'IkuController@formulasave')->name('formulasave');
			Route::post('iku/formulaedit', 'IkuController@formulaedit')->name('formulaedit');
		});
 	/*
	 * rencanaaksi Modul Routes
	 * route('web.rencanaaksi')
	 * route('web.rencanaaksi.detail')
	 */
	Route::group(['as' => 'rencanaaksi.'], function (){
		Route::get('rencanaaksi', 'RencanaaksiController@index')->name('index');
		Route::get('rencanaaksi/home/{id}/{idsasaran}', 'RencanaaksiController@home')->name('home');
		Route::get('rencanaaksi/indikator/{id}', 'RencanaaksiController@indikator')->name('indikator');


		Route::get('rencanaaksi/tambahtargetkegiatan/{id}/{ids}/{idsasaran}', 'RencanaaksiController@tambahtargetkegiatan')->name('tambahtargetkegiatan');
		Route::get('rencanaaksi/edittargetkegiatan/{id}/{ids}/{idsasaran}', 'RencanaaksiController@edittargetkegiatan')->name('edittargetkegiatan');
		Route::post('rencanaaksi/savetargetindikatorkegiatan', 'RencanaaksiController@savetargetindikatorkegiatan')->name('savetargetindikatorkegiatan');
		Route::post('rencanaaksi/edittargetindikatorkegiatan', 'RencanaaksiController@edittargetindikatorkegiatan')->name('edittargetindikatorkegiatan');


		Route::get('rencanaaksi/tambahtargetprogram/{id}/{ids}/{idsasaran}', 'RencanaaksiController@tambahtargetprogram')->name('tambahtargetprogram');
		Route::get('rencanaaksi/edittargetprogram/{id}/{ids}/{idsasaran}', 'RencanaaksiController@edittargetprogram')->name('edittargetprogram');
		Route::post('rencanaaksi/savetargetindikatorprogram', 'RencanaaksiController@savetargetindikatorprogram')->name('savetargetindikatorprogram');
		Route::post('rencanaaksi/edittargetindikatorprogram', 'RencanaaksiController@edittargetindikatorprogram')->name('edittargetindikatorprogram');


		Route::get('rencanaaksi/tambahtargetsasaran/{id}/{ids}/{idsasaran}', 'RencanaaksiController@tambahtargetsasaran')->name('tambahtargetsasaran');
		Route::get('rencanaaksi/edittargetsasaran/{id}/{ids}/{idsasaran}', 'RencanaaksiController@edittargetsasaran')->name('edittargetsasaran');
		Route::post('rencanaaksi/savetargetindikatorsasaran', 'RencanaaksiController@savetargetindikatorsasaran')->name('savetargetindikatorsasaran');
		Route::post('rencanaaksi/edittargetindikatorsasaran', 'RencanaaksiController@edittargetindikatorsasaran')->name('edittargetindikatorsasaran');



        Route::get('rencanaaksi/printrencanaaksi/{id}', 'RencanaaksiController@printrencanaaksi')->name('printrencanaaksi');
        Route::get('rencanaaksi/printrencanaaksipelaporan/{id}', 'RencanaaksiController@printrencanaaksipelaporan')->name('printrencanaaksipelaporan');
        Route::post('rencanaaksi/print', 'RencanaaksiController@print')->name('print');
        Route::post('rencanaaksi/printlaporan', 'RencanaaksiController@printlaporan')->name('printlaporan');
    });


});
