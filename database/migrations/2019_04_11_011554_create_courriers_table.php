<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourriersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courriers', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->bigInteger('visit_id')->unsigned()->index()->nullable();
            $table->foreign('visit_id')->references('id')->on('visits')->onDelete('cascade');
            
            $table->string('package')->nullable();
            $table->text('image_package')->nullable();
            $table->text('image_person')->nullable();
            $table->text('image_card')->nullable();

            $table->integer('status')->default(1);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courriers');
    }
}
