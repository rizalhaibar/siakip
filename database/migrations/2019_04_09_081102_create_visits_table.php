<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->bigInteger('visit_code')->nullable();

            $table->bigInteger('company_id')->unsigned()->index()->nullable();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');

            $table->bigInteger('department_id')->unsigned()->index()->nullable();
            $table->foreign('department_id')->references('id')->on('departments')->onDelete('cascade');

            $table->bigInteger('person_id')->unsigned()->index()->nullable();
            $table->foreign('person_id')->references('id')->on('persons')->onDelete('cascade');

            $table->bigInteger('visit_purpose_id')->unsigned()->index()->nullable();
            $table->foreign('visit_purpose_id')->references('id')->on('visit_purposes')->onDelete('cascade');

            $table->string('visit_card_number')->nullable();
            
            $table->date('visit_date')->nullable();
            $table->time('visit_time_in')->nullable();
            $table->time('visit_time_out')->nullable();

            $table->integer('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visits');
    }
}
