<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meetings', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->bigInteger('visit_id')->unsigned()->index()->nullable();
            $table->foreign('visit_id')->references('id')->on('visits')->onDelete('cascade');

            $table->bigInteger('meeting_room_id')->unsigned()->index()->nullable();
            $table->foreign('meeting_room_id')->references('id')->on('meeting_rooms')->onDelete('cascade');

            $table->integer('status')->default(1);
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meetings');
    }
}
