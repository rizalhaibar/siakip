<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitLuggageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visit_luggage', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->bigInteger('visitor_id')->unsigned()->index()->nullable();
            $table->foreign('visitor_id')->references('id')->on('visitors')->onDelete('cascade');
            
            $table->string('name')->nullable();
            $table->string('total')->nullable();
            $table->string('permission')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visit_luggage');
    }
}
