<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('params', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->string('codename')->nullable();
            $table->string('xn1')->nullable();
            $table->string('xs1')->nullable();

            $table->string('xn2')->nullable();
            $table->string('xs2')->nullable();
            
            $table->text('description')->nullable();
            
            $table->integer('status')->default(1);

            $table->timestamps();
            $table->softDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('params');
    }
}
